package com.oduzhyi.google;

import java.util.concurrent.CountDownLatch;

public class DeadLock extends Thread {

    private final Object mon1;
    private final Object mon2;
    private final CountDownLatch latch;

    public DeadLock(Object mon1, Object mon2, CountDownLatch latch) {
        this.mon1 = mon1;
        this.mon2 = mon2;
        this.latch = latch;
    }

    public static void main(String[] args) {
        Object one = new Object();
        Object two = new Object();

        CountDownLatch latch = new CountDownLatch(2);

        new DeadLock(one, two, latch).start();
        new DeadLock(two, one, latch).start();
    }

    @Override
    public void run() {
        synchronized (mon1) {

            latch.countDown();
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (mon2) {
                System.out.println("made it");
            }
        }
    }
}
