package com.oduzhyi.facebook;

import java.util.ArrayDeque;
import java.util.Arrays;

public class ContigousSubarrays {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(new ContigousSubarrays().countSubarrays(new int[]{1, 2, 3, 4, 5, 6, 7})));
    }

    int[] countSubarrays(int[] arr) {
        // Write your code here
        int[] left = new int[arr.length];
        ArrayDeque<Integer> stack = new ArrayDeque<>();

        for (int i = 0; i < arr.length; i++) {
            while (!stack.isEmpty() && arr[stack.peek()] < arr[i]) {
                left[i] += left[stack.pop()] + 1;
            }
            stack.push(i);
        }

        stack.clear();
        int[] right = new int[arr.length];
        for (int i = arr.length - 1; i >= 0; i--) {
            while (!stack.isEmpty() && arr[stack.peek()] < arr[i]) {
                right[i] += right[stack.pop()] + 1;
            }
            stack.push(i);
        }

        for (int i = 0; i < left.length; i++) {
            left[i] += right[i] + 1;
        }
        return left;
    }
}
