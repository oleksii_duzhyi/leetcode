package com.oduzhyi.algos;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Solution {

    private static int tCnt;

    public static void main(String[] args) {
//        System.out.println(solution("1", "10000000000000000000000000000000000000000000000000000000"));
//        System.out.println(solution("2", "1"));
//        System.out.println(solution("4", "7"));
//        System.out.println(solution("2", "4"));
//        System.out.println(Math.pow(10, 50));
////[[-2,-3,3],[-5,-10,1],[10,30,-5]]
//        System.out.println(new Solution().calculateMinimumHP(new int[][]{
//                new int[]{-2, -3, 3},
//                new int[]{-5, -10, 1},
//                new int[]{10, 30, -5}
        //        [1, 2, 3, 0, 0, 0],
        //        [4, 5, 6, 0, 0, 0],
        //        [7, 8, 9, 1, 0, 0],
        //        [0, 0, 0, 0, 1, 2],
        //        [0, 0, 0, 0, 0, 0],
        //        [0, 0, 0, 0, 0, 0]
        //         0, 0, 12, 0, 15, 0, 0, 0, 1, 8],
        //        [0, 0, 60, 0, 0, 7, 13, 0, 0, 0],
        //        [0, 15, 0, 8, 7, 0, 0, 1, 9, 0],
        //        [23, 0, 0, 0, 0, 1, 0, 0, 0, 0],
        //        [37, 35, 0, 0, 0, 0, 3, 21, 0, 0],
        //        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        //        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        //        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        //        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        //        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
//        }));
//        System.out.println(Arrays.toString(solution(new int[][]{
//                new int[]{1, 2, 3, 0, 0, 0},
//                new int[]{4, 5, 6, 0, 0, 0},
//                new int[]{7, 8, 9, 1, 0, 0},
//                new int[]{0, 0, 0, 0, 1, 2},
//                new int[]{0, 0, 0, 0, 0, 0},
//                new int[]{0, 0, 0, 0, 0, 0}
//        })));
        System.out.println(Arrays.toString(solution(new int[][]{
                new int[]{1, 1, 1, 0, 1, 0, 1, 0, 1, 0},
                new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{1, 0, 1, 1, 1, 0, 1, 0, 1, 0},
                new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{1, 0, 1, 0, 1, 1, 1, 0, 1, 0},
                new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{1, 0, 1, 0, 1, 0, 1, 1, 1, 0},
                new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{1, 0, 1, 0, 1, 0, 1, 0, 1, 1},
                new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        })));
        System.out.println(Arrays.toString(solution(new int[][]{
                new int[]{0, 86, 61, 189, 0, 18, 12, 33, 66, 39},
                new int[]{0, 0, 2, 0, 0, 1, 0, 0, 0, 0},
                new int[]{15, 187, 0, 0, 18, 23, 0, 0, 0, 0},
                new int[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        })));
//        Fraction[][] stdForm = transform(
//                new int[][]{
//                        new int[]{0, 1, 0, 0, 0, 1},
//                        new int[]{4, 0, 0, 3, 2, 0},
//                        new int[]{0, 0, 0, 0, 0, 0},
//                        new int[]{0, 0, 0, 0, 0, 0},
//                        new int[]{0, 0, 0, 0, 0, 0},
//                        new int[]{0, 0, 0, 0, 0, 0}
//                }
//        );
//        Fraction[][] q = getQ(stdForm);
//        Fraction[][] r = getR(stdForm);
//
//        inverse(new Fraction[][]{
//                new Fraction[]{new Fraction(25, 1), new Fraction(5, 1), Fraction.ONE},
//                new Fraction[]{new Fraction(64, 1), new Fraction(8, 1), Fraction.ONE},
//                new Fraction[]{new Fraction(144, 1), new Fraction(12, 1), Fraction.ONE}
//        });
//        System.out.println(gcd(0, 24));
//        System.out.println(new Fraction(3, 9).add(new Fraction(3, 9)));
//        System.out.println(new Fraction(3, 9).add(new Fraction(3, 9)));
    }

    public static int[] solution(int[][] m) {
        Fraction[][] stdForm = transform(m);
        Fraction[][] q = getQ(stdForm);
        if (q.length == 0) {
            int[] res = new int[m.length + 1];
            res[0] = 1;
            res[m.length] = 1;
            return res;
        }
        Fraction[][] r = getR(stdForm);

        Fraction[][] n = calcN(q);

        Fraction[] prob = termStateProbFromZeroState(n, r);
        int[] res = new int[prob.length + 1];
        int maxDenom = Arrays.stream(prob).map(p -> p.demon).reduce(Solution::lcm)
                .orElseThrow(IllegalArgumentException::new).intValue();
        res[res.length - 1] = maxDenom;

        for (int i = 0; i < prob.length; i++) {
            res[i] = (int) (prob[i].nom * (maxDenom / prob[i].demon));
        }
        return res;
    }

    private static Fraction[] termStateProbFromZeroState(Fraction[][] n, Fraction[][] r) {
        Fraction[] result = new Fraction[r[0].length];

        for (int col = 0; col < r[0].length; col++) {
            result[col] = Fraction.ZERO;
            for (int i = 0; i < r.length; i++) {
                result[col] = result[col].add(n[0][i].mult(r[i][col]));
            }
        }

        return result;
    }

    private static Fraction multiplyMatricesCell(Fraction[][] n, Fraction[][] r, int col) {
        Fraction cell = Fraction.ZERO;
        for (int i = 0; i < r.length; i++) {
            cell = cell.add(n[0][i].mult(r[i][col]));
        }
        return cell;
    }

    private static Fraction[][] calcN(Fraction[][] q) {
        Fraction[][] diff = calcIdentityDiff(q);
        return inverse(diff);
    }

    private static Fraction[][] inverse(Fraction[][] m) {
        Fraction[][] u = new Fraction[m.length][m.length];
        Fraction[][] l = new Fraction[m.length][m.length];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m.length; j++) {
                u[i][j] = m[i][j];
                if (i == j) {
                    l[i][j] = Fraction.ONE;
                } else {
                    l[i][j] = Fraction.ZERO;
                }
            }
        }

        for (int j = 0; j < m.length - 1; j++) {
            Fraction red = u[j][j];
            l[j][j] = Fraction.ONE;
            for (int i = j + 1; i < m.length; i++) {
                Fraction multiplier = u[i][j].divide(red);
                l[i][j] = multiplier;
                for (int k = j; k < m.length; k++) {
                    u[i][k] = u[i][k].minus(u[j][k].mult(multiplier));
                }
            }
        }

        Fraction[][] inverse = new Fraction[m.length][m.length];
        for (int j = 0; j < m.length; j++) {
            Fraction[][] z = forwardSubstitution(l, j);
            backwardSubstitution(inverse, u, z, j);
        }

        return inverse;
    }

    private static void backwardSubstitution(Fraction[][] inverse, Fraction[][] u, Fraction[][] z, int j) {
        for (int i = u.length - 1; i >= 0; i--) {
            inverse[i][j] = z[i][0];
            int k = i;
            while (k < u.length - 1) {
                inverse[i][j] = inverse[i][j].minus(inverse[k + 1][j].mult(u[i][k + 1]));
                k++;
            }
            inverse[i][j] = inverse[i][j].divide(u[i][i]);
        }
    }

    private static Fraction[][] forwardSubstitution(Fraction[][] l, int j) {
        Fraction[][] z = new Fraction[l.length][1];
        Fraction[][] idenCol = new Fraction[l.length][1];
        for (int k = 0; k < l.length; k++) {
            if (j == k) {
                idenCol[k][0] = Fraction.ONE;
            } else {
                idenCol[k][0] = Fraction.ZERO;
            }
        }

        for (int i = 0; i < l.length; i++) {
            z[i][0] = idenCol[i][0].divide(l[i][i]);
            int k = i;
            while (k > 0) {
                z[i][0] = z[i][0].minus(z[k - 1][0].mult(l[i][k - 1]));
                k--;
            }
        }
        return z;
    }

    private static Fraction[][] calcIdentityDiff(Fraction[][] q) {
        Fraction[][] diff = new Fraction[q.length][q[0].length];
        for (int i = 0; i < q.length; i++) {
            for (int j = 0; j < q[0].length; j++) {
                if (i == j) {
                    diff[i][j] = Fraction.ONE.minus(q[i][j]);
                } else {
                    diff[i][j] = q[i][j].mult(-1);
                }
            }
        }
        return diff;
    }

    private static Fraction[][] getR(Fraction[][] stdForm) {
        Fraction[][] r = new Fraction[stdForm.length - tCnt][tCnt];
        for (int i = tCnt; i < stdForm.length; i++) {
            for (int j = 0; j < tCnt; j++) {
                r[i - tCnt][j] = stdForm[i][j];
            }
        }
        return r;
    }

    private static Fraction[][] getQ(Fraction[][] stdForm) {
        Fraction[][] q = new Fraction[stdForm.length - tCnt][stdForm[0].length - tCnt];
        for (int i = tCnt; i < stdForm.length; i++) {
            for (int j = tCnt; j < stdForm.length; j++) {
                q[i - tCnt][j - tCnt] = stdForm[i][j];
            }
        }
        return q;
    }

    private static Fraction[][] transform(int[][] m) {
        Fraction[][] stdForm = new Fraction[m.length][m[0].length];

        Map<Integer, Integer> termSt = new HashMap<>();
        Map<Integer, Integer> nonTermSt = new HashMap<>();
        Map<Integer, Integer> nonTermStDenoms = new HashMap<>();
        int tCnt = 0;
        int nTCnt = 0;
        for (int i = 0; i < m.length; i++) {
            int denom = 0;
            for (int j = 0; j < m[0].length; j++) {
                denom += m[i][j];
            }
            if (denom == 0) {
                termSt.put(tCnt++, i);
            } else {
                nonTermSt.put(nTCnt, i);
                nonTermStDenoms.put(nTCnt++, denom);
            }
        }
        Solution.tCnt = tCnt;
        for (int i = tCnt; i < m.length; i++) {
            int origSt = nonTermSt.get(i - tCnt);
            int denom = nonTermStDenoms.get(i - tCnt);
            for (int j = 0; j < m[0].length; j++) {
                stdForm[i][j] = new Fraction(m[origSt][termSt.getOrDefault(j, nonTermSt.get(j - tCnt))], denom);
            }
        }
        return stdForm;
    }

    private static long gcd(long a, long b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }

    private static long lcm(long a, long b) {
        return (a * b) / gcd(a, b);
    }

    private final static class Fraction {
        public static final Fraction ONE = new Fraction(1, 1);
        public static final Fraction ZERO = new Fraction(0, 1);

        private final long nom;
        private final long demon;

        private Fraction(long nom, long demon) {
            this.nom = nom;
            this.demon = demon;
        }

        public static Fraction of(long nom, long demon) {
            return simplify(nom, demon);
        }

        private static Fraction simplify(long nom, long demon) {
            long gcd = gcd(nom, demon);
            return new Fraction(nom / gcd, demon / gcd);
        }

        public Fraction add(Fraction f) {
            long lcm = lcm(demon, f.demon);
            long newNom = this.nom * (lcm / demon) + f.nom * lcm / f.demon;
            return simplify(newNom, lcm);
        }

        public Fraction minus(Fraction f) {
            long lcm = lcm(demon, f.demon);
            return simplify(nom * (lcm / demon) - f.nom * lcm / f.demon, lcm);
        }

        public Fraction mult(long m) {
            return simplify(nom * m, demon);
        }

        public Fraction mult(Fraction f) {
            return simplify(nom * f.nom, demon * f.demon);
        }

        public Fraction divide(Fraction f) {
            return simplify(nom * f.demon, demon * f.nom);
        }
    }
}