package com.oduzhyi.algos;

import java.util.ArrayDeque;
import java.util.Arrays;

public class BunniesSolution2 {
    //[
    //  [0, 2, 2, 2, -1],  # 0 = Start
    //  [9, 0, 2, 2, -1],  # 1 = Bunny 0
    //  [9, 3, 0, 2, -1],  # 2 = Bunny 1
    //  [9, 3, 2, 0, -1],  # 3 = Bunny 2
    //  [9, 3, 2, 2,  0],  # 4 = Bulkhead
    //]

    public static void main(String[] args) {
        test(new int[][]{
                new int[]{0, 1, 5, 5, 2,},
                new int[]{10, 0, 2, 6, 10,},
                new int[]{10, 10, 0, 1, 5,},
                new int[]{10, 10, 10, 0, 1,},
                new int[]{10, 10, 10, 10, 0},
        }, 5, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{0, 1, 3, 4, 2,},
                new int[]{10, 0, 2, 3, 4,},
                new int[]{10, 10, 0, 1, 2,},
                new int[]{10, 10, 10, 0, 1,},
                new int[]{10, 10, 10, 10, 0},
        }, 4, new int[]{});


        test(new int[][]{
                new int[]{0, 2, 2, 2, -1,},
                new int[]{9, 0, 2, 2, -1,},
                new int[]{9, 3, 0, 2, -1,},
                new int[]{9, 3, 2, 0, -1,},
                new int[]{9, 3, 2, 2, 0},
        }, 1, new int[]{1, 2});


        test(new int[][]{
                new int[]{0, 1, 10, 10, 10,},
                new int[]{10, 0, 1, 1, 2,},
                new int[]{10, 1, 0, 10, 10,},
                new int[]{10, 1, 10, 0, 10,},
                new int[]{10, 10, 10, 10, 0},
        }, 7, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{0, 1, 1, 1, 1,},
                new int[]{1, 0, 1, 1, 1,},
                new int[]{1, 1, 0, 1, 1,},
                new int[]{1, 1, 1, 0, 1,},
                new int[]{1, 1, 1, 1, 0},
        }, 3, new int[]{0, 1});


        test(new int[][]{
                new int[]{0, 5, 11, 11, 1,},
                new int[]{10, 0, 1, 5, 1,},
                new int[]{10, 1, 0, 4, 0,},
                new int[]{10, 1, 5, 0, 1,},
                new int[]{10, 10, 10, 10, 0},
        }, 10, new int[]{0, 1});


        test(new int[][]{
                new int[]{0, 20, 20, 20, -1,},
                new int[]{90, 0, 20, 20, 0,},
                new int[]{90, 30, 0, 20, 0,},
                new int[]{90, 30, 20, 0, 0,},
                new int[]{-1, 30, 20, 20, 0},
        }, 0, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{0, 10, 10, 10, 1,},
                new int[]{0, 0, 10, 10, 10,},
                new int[]{0, 10, 0, 10, 10,},
                new int[]{0, 10, 10, 0, 10,},
                new int[]{1, 1, 1, 1, 0},
        }, 5, new int[]{0, 1});


        test(new int[][]{
                new int[]{2, 2,},
                new int[]{2, 2},
        }, 5, new int[]{});


        test(new int[][]{
                new int[]{0, 10, 10, 1, 10,},
                new int[]{10, 0, 10, 10, 1,},
                new int[]{10, 1, 0, 10, 10,},
                new int[]{10, 10, 1, 0, 10,},
                new int[]{1, 10, 10, 10, 0},
        }, 6, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1},
        }, 1, new int[]{});

        test(new int[][]{
                new int[]{0, 0, 1, 1, 1,},
                new int[]{0, 0, 0, 1, 1,},
                new int[]{0, 0, 0, 0, 1,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0},
        }, 0, new int[]{0, 1, 2});

        test(new int[][]{
                new int[]{1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1},
        }, 1, new int[]{0, 1, 2});

        test(new int[][]{
                        new int[]{0, 1, 5, 5, 5, 5,},
                        new int[]{5, 0, 1, 5, 5, 5,},
                        new int[]{5, 5, 0, 5, 5, -1,},
                        new int[]{5, 5, 1, 0, 5, 5,},
                        new int[]{5, 5, 1, 5, 0, 5,},
                        new int[]{5, 5, 1, 1, 1, 0},
                }
                , 3, new int[]{0, 1, 2, 3});

        test(new int[][]{
                        new int[]{0, 1, 5, 5, 5, 5, 5,},
                        new int[]{5, 0, 1, 5, 5, 5, 5,},
                        new int[]{5, 5, 0, 5, 5, 0, -1,},
                        new int[]{5, 5, 1, 0, 5, 5, 5,},
                        new int[]{5, 5, 1, 5, 0, 5, 5,},
                        new int[]{5, 5, 0, 5, 5, 0, 0,},
                        new int[]{5, 5, 1, 1, 1, 0, 0},
                }
                , 3, new int[]{0, 1, 2, 3, 4});

        test(new int[][]{
                        new int[]{0, -1, 0, 9, 9, 9, 9, 9,},
                        new int[]{9, 0, 1, 9, 9, 9, 9, 9,},
                        new int[]{0, 9, 0, 0, 9, 9, 1, 1,},
                        new int[]{9, 9, 9, 0, 1, 9, 9, 9,},
                        new int[]{9, 9, 9, 9, 0, -1, 9, 9,},
                        new int[]{9, 9, 0, 9, 9, 0, 9, 9,},
                        new int[]{9, 9, -1, 9, 9, 9, 0, 9,},
                        new int[]{9, 9, 9, 9, 9, 9, 9, 0},
                },
                1, new int[]{0, 1, 2, 3, 4, 5});

        test(new int[][]{
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0},
        }, 0, new int[]{0, 1, 2});
        test(new int[][]{
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
        }, 0, new int[]{0, 1, 2, 3, 4});

        test(new int[][]{
                new int[]{0, 100, 100, 2, 100},
                new int[]{100, 0, 100, 100, 0,},
                new int[]{100, 100, 0, 100, 0,},
                new int[]{100, 2, 1, 0, 0,},
                new int[]{100, 100, 100, 100, 0},
        }, 4, new int[]{0, 2});
    }

    private static void test(int[][] ints, int i, int[] ints1) {
        System.out.println("Test case: " + Arrays.toString(ints1));
        int[] solution = solution(ints, i);
        System.out.println("Answer   : " + Arrays.toString(solution));
        if (!Arrays.equals(ints1, solution)) {
            throw new IllegalArgumentException();
        }
    }

    public static int[] solution(int[][] times, int timeLimit) {
        int l = times.length;
        int[] dist = calcBellmanFordFromVirtual(times, l);

        if (detectNegativeCycle(times, l, dist)) {
            int[] res = new int[l - 2];
            for (int i = 0; i < res.length; i++) {
                res[i] = i;
            }
            return res;
        }
        if (dist[l - 1] > timeLimit) {
            return new int[]{};
        }
        adjustWeights(times, l, dist);
        timeLimit = timeLimit + dist[0] - dist[l - 1];


        State best = bfsStateTree(times, timeLimit);
        int[] res = new int[best.savedCnt];
        int j = 0;
        for (int i = 0; i < best.bunniesSaved.length; i++) {
            if (best.bunniesSaved[i] == 1) {
                res[j++] = i;
            }
        }
        return res;
    }

    private static State bfsStateTree(int[][] times, int timeLimit) {
        int l = times.length;
        State best = new State(timeLimit, new int[times.length - 2], 0, 0);
        ArrayDeque<State> queue = new ArrayDeque<>();
        queue.add(best);

        while (!queue.isEmpty()) {
            State next = queue.poll();

            if (next.position == l - 1) {
                if (next.compareTo(best) > 0) {
                    best = State.merge(next, best);
                    //saved all bunnies
                    if (best.savedCnt == l - 2) {
                        break;
                    }
                }
            }
            for (int i = 0; i < l; i++) {
                if (i == next.position || (next.timeLeft - times[next.position][i] < 0)) {
                    continue;
                }
                if (i == 0 || i == l - 1) {
                    queue.add(new State(next.timeLeft - times[next.position][i], next.bunniesSaved, i, next.savedCnt));
                } else {
                    int nextBunnyId = i - 1;
                    int savedCnt = next.savedCnt;
                    int[] saved = next.bunniesSaved;
                    if (next.bunniesSaved[nextBunnyId] == 0) {
                        saved = new int[saved.length];
                        System.arraycopy(next.bunniesSaved, 0, saved, 0, saved.length);
                        saved[nextBunnyId] = 1;
                        savedCnt++;
                    }
                    queue.add(new State(next.timeLeft - times[next.position][i], saved, i, savedCnt));
                }
            }
        }
        return best;
    }

    private static int[] calcBellmanFordFromVirtual(int[][] times, int l) {
        int[] dist = new int[l + 1];

        Arrays.fill(dist, 10000);
        dist[l] = 0;

        for (int i = 0; i < l; i++) {
            for (int j = 0; j <= l; j++) {
                for (int k = 0; k < l; k++) {
                    if (j == k) {
                        continue;
                    }
                    int weight = (j == l) ? 0 : times[j][k];
                    if (dist[k] > dist[j] + weight) {
                        dist[k] = dist[j] + weight;
                    }
                }
            }
        }
        return dist;
    }

    private static void adjustWeights(int[][] times, int l, int[] dist) {
        for (int j = 0; j < l; j++) {
            for (int k = 0; k < l; k++) {
                if (j == k) {
                    continue;
                }
                times[j][k] = times[j][k] + dist[j] - dist[k];
            }
        }
    }

    private static boolean detectNegativeCycle(int[][] times, int l, int[] dist) {
        for (int j = 0; j <= l; j++) {
            for (int k = 0; k < l; k++) {
                if (j == k) {
                    continue;
                }
                int weight = (j == l) ? 0 : times[j][k];
                if (dist[k] > dist[j] + weight) {
                    return true;
                }
            }
        }
        return false;
    }

    private static class State implements Comparable<State> {
        final int timeLeft;
        final int[] bunniesSaved;
        final int position;
        final int savedCnt;

        public State(int timeLeft, int[] bunniesSaved, int position, int savedCnt) {
            this.timeLeft = timeLeft;
            this.bunniesSaved = bunniesSaved;
            this.position = position;
            this.savedCnt = savedCnt;
        }

        private static State merge(State newBest, State oldBest) {
            if (newBest.savedCnt > oldBest.savedCnt) {
                return newBest;
            }
            int[] bunniesSaved = new int[newBest.bunniesSaved.length];
            int cnt = newBest.savedCnt;

            for (int i = 0; i < bunniesSaved.length && cnt > 0; i++) {
                if (newBest.bunniesSaved[i] == 1 || oldBest.bunniesSaved[i] == 1) {
                    bunniesSaved[i] = 1;
                    cnt--;
                }
            }
            return new State(newBest.timeLeft, bunniesSaved, newBest.position, newBest.savedCnt);
        }

        @Override
        public int compareTo(State o) {
            int compare = Integer.compare(savedCnt, o.savedCnt);
            if (compare == 0 && savedCnt != 0) {
                for (int i = 0; i < bunniesSaved.length; i++) {
                    if (bunniesSaved[i] != o.bunniesSaved[i]) {
                        return Integer.compare(bunniesSaved[i], o.bunniesSaved[i]);
                    }
                }
            }
            return compare;
        }
    }
}
