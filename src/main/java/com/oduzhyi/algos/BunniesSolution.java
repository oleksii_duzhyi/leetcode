package com.oduzhyi.algos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BunniesSolution {
    //[
    //  [0, 2, 2, 2, -1],  # 0 = Start
    //  [9, 0, 2, 2, -1],  # 1 = Bunny 0
    //  [9, 3, 0, 2, -1],  # 2 = Bunny 1
    //  [9, 3, 2, 0, -1],  # 3 = Bunny 2
    //  [9, 3, 2, 2,  0],  # 4 = Bulkhead
    //]

    public static void main(String[] args) {
        System.out.println(
                Arrays.toString(
                        solution(
                                new int[][]{
                                        new int[]{0, 2, 2, 2, -1},
                                        new int[]{9, 0, 2, 2, -1},
                                        new int[]{9, 3, 0, 2, -1},
                                        new int[]{9, 3, 2, 0, -1},
                                        new int[]{9, 3, 2, 2, 0}
                                },
                                1
                        )
                )
        );
        System.out.println(
                Arrays.toString(
                        solution(
                                new int[][]{
                                        new int[]{0, 2, 2, 2, -1},
                                        new int[]{4, 0, 2, 2, -1},
                                        new int[]{9, -3, 0, 2, -1},
                                        new int[]{9, 3, -2, 0, -1},
                                        new int[]{9, 3, 2, -2, 0}
                                },
                                1
                        )
                )
        );
        System.out.println(
                Arrays.toString(
                        solution(
                                new int[][]{
                                        new int[]{0, 1, 1, 1, 1},
                                        new int[]{1, 0, 1, 1, 1},
                                        new int[]{1, 1, 0, 1, 1},
                                        new int[]{1, 1, 1, 0, 1},
                                        new int[]{1, 1, 1, 1, 0}
                                },
                                3
                        )
                )
        );
    }

    public static int[] solution(int[][] times, int timeLimit) {
        int l = times.length;
        int[] dist = new int[times.length];
        int[] parents = new int[times.length];

        Arrays.fill(dist, 10000);
        dist[0] = 0;

        for (int i = 0; i < l - 1; i++) {
            for (int j = 0; j < l; j++) {
                for (int k = 0; k < l; k++) {
                    if (j == k) {
                        continue;
                    }
                    if (dist[k] > dist[j] + times[j][k]) {
                        dist[k] = dist[j] + times[j][k];
                        parents[k] = j;
                    }
                }
            }
        }

        for (int j = 0; j < l; j++) {
            for (int k = 0; k < l; k++) {
                if (j == k) {
                    continue;
                }
                if (dist[k] > dist[j] + times[j][k]) {
                    int[] res = new int[l - 2];
                    for (int i = 0; i < res.length; i++) {
                        res[i] = i;
                    }
                    return res;
                }
            }
        }
        if (dist[l - 1] > timeLimit) {
            throw new IllegalArgumentException("Can't reach bulkhead");
        }

        Set<Integer> savedBunnies = new HashSet<>();
        int p = l - 1;
        while (parents[p] != p) {
            if (parents[p] != 0) {
                savedBunnies.add(parents[p] - 1);
            }
            p = parents[p];
        }

        if (savedBunnies.size() == l - 2) {
            int[] res = new int[l - 2];
            for (int i = 0; i < res.length; i++) {
                res[i] = i;
            }
            return res;
        } else {
            int timeLeft = timeLimit - dist[l - 1];
            int[][] allDists = new int[l][l];
            int[][] allParents = new int[l][l];
            allDists[0] = dist;
            allParents[0] = parents;

            for (int from = 1; from < l; from++) {
                int[] fromDist = new int[l];
                int[] fromParents = new int[times.length];

                Arrays.fill(fromDist, 10000);
                fromDist[from] = 0;
                fromParents[from] = from;
                for (int i = 0; i < l - 1; i++) {
                    for (int j = 0; j < l; j++) {
                        for (int k = 0; k < l; k++) {
                            if (j == k) {
                                continue;
                            }
                            if (fromDist[k] > fromDist[j] + times[j][k]) {
                                fromDist[k] = fromDist[j] + times[j][k];
                                fromParents[k] = j;
                            }
                        }
                    }
                }
                allDists[from] = fromDist;
                allParents[from] = fromParents;
            }

            List<DistToBunny> bunnySaveCost = new ArrayList<>();
            for (int i = 0; i < l - 2; i++) {
                bunnySaveCost.add(new DistToBunny(i, allDists[l - 1][i + 1] + allDists[i + 1][l - 1]));
            }
            Collections.sort(bunnySaveCost);
            for (DistToBunny dtb : bunnySaveCost) {
                int i = dtb.bunnyId;
                if (!savedBunnies.contains(i)) {
                    if (timeLeft - (allDists[l - 1][i + 1] + allDists[i + 1][l - 1]) >= 0) {
                        timeLeft -= allDists[l - 1][i + 1] + allDists[i + 1][l - 1];
                        savedBunnies.add(i);
                        p = i + 1;
                        while (allParents[l - 1][p] != p) {
                            if (allParents[l - 1][p] != 0 && allParents[l - 1][p] != l - 1) {
                                savedBunnies.add(allParents[l - 1][p] - 1);
                            }
                            p = allParents[l - 1][p];
                        }
                        p = l - 1;
                        while (allParents[i + 1][p] != p) {
                            if (allParents[i + 1][p] != 0 && allParents[i + 1][p] != l - 1) {
                                savedBunnies.add(allParents[i + 1][p] - 1);
                            }
                            p = allParents[i + 1][p];
                        }
                    }
                }
            }

            int[] res = new int[savedBunnies.size()];
            int i = 0;
            for (int sb : savedBunnies) {
                res[i++] = sb;
            }
            Arrays.sort(res);
            return res;
        }
    }

    private static class DistToBunny implements Comparable<DistToBunny> {
        final int bunnyId;
        final int dist;


        private DistToBunny(int bunnyId, int dist) {
            this.bunnyId = bunnyId;
            this.dist = dist;
        }


        @Override
        public int compareTo(DistToBunny o) {
            return Comparator.comparing((DistToBunny dtb) -> dtb.dist).thenComparing((DistToBunny dtb) -> dtb.bunnyId)
                    .compare(this, o);
        }
    }
}
