package com.oduzhyi.algos;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

class subsetComparator implements Comparator<LinkedList<Integer>> {
    @Override
    public int compare(LinkedList<Integer> a, LinkedList<Integer> b) {

        if (a.size() != b.size()) {
            return (b.size() - a.size());
        } else {
            for (int i = 0; i < a.size(); i++) {

                if (a.get(i) < b.get(i)) {
                    return a.get(i) - b.get(i);
                } else if (a.get(i) > b.get(i)) {
                    return a.get(i) - b.get(i);
                }

            }
            return 1;
        }
    }
}

public class Answer {

    static Set<LinkedList<Integer>> set = new HashSet<LinkedList<Integer>>();
    static LinkedList<LinkedList<Integer>> permutations = new LinkedList<LinkedList<Integer>>();

    public static void main(String[] args) {
        test(new int[][]{
                new int[]{0, 1, 5, 5, 2,},
                new int[]{10, 0, 2, 6, 10,},
                new int[]{10, 10, 0, 1, 5,},
                new int[]{10, 10, 10, 0, 1,},
                new int[]{10, 10, 10, 10, 0},
        }, 5, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{0, 1, 3, 4, 2,},
                new int[]{10, 0, 2, 3, 4,},
                new int[]{10, 10, 0, 1, 2,},
                new int[]{10, 10, 10, 0, 1,},
                new int[]{10, 10, 10, 10, 0},
        }, 4, new int[]{});


        test(new int[][]{
                new int[]{0, 2, 2, 2, -1,},
                new int[]{9, 0, 2, 2, -1,},
                new int[]{9, 3, 0, 2, -1,},
                new int[]{9, 3, 2, 0, -1,},
                new int[]{9, 3, 2, 2, 0},
        }, 1, new int[]{1, 2});


        test(new int[][]{
                new int[]{0, 1, 10, 10, 10,},
                new int[]{10, 0, 1, 1, 2,},
                new int[]{10, 1, 0, 10, 10,},
                new int[]{10, 1, 10, 0, 10,},
                new int[]{10, 10, 10, 10, 0},
        }, 7, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{0, 1, 1, 1, 1,},
                new int[]{1, 0, 1, 1, 1,},
                new int[]{1, 1, 0, 1, 1,},
                new int[]{1, 1, 1, 0, 1,},
                new int[]{1, 1, 1, 1, 0},
        }, 3, new int[]{0, 1});


        test(new int[][]{
                new int[]{0, 5, 11, 11, 1,},
                new int[]{10, 0, 1, 5, 1,},
                new int[]{10, 1, 0, 4, 0,},
                new int[]{10, 1, 5, 0, 1,},
                new int[]{10, 10, 10, 10, 0},
        }, 10, new int[]{0, 1});


        test(new int[][]{
                new int[]{0, 20, 20, 20, -1,},
                new int[]{90, 0, 20, 20, 0,},
                new int[]{90, 30, 0, 20, 0,},
                new int[]{90, 30, 20, 0, 0,},
                new int[]{-1, 30, 20, 20, 0},
        }, 0, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{0, 10, 10, 10, 1,},
                new int[]{0, 0, 10, 10, 10,},
                new int[]{0, 10, 0, 10, 10,},
                new int[]{0, 10, 10, 0, 10,},
                new int[]{1, 1, 1, 1, 0},
        }, 5, new int[]{0, 1});


        test(new int[][]{
                new int[]{2, 2,},
                new int[]{2, 2},
        }, 5, new int[]{});


        test(new int[][]{
                new int[]{0, 10, 10, 1, 10,},
                new int[]{10, 0, 10, 10, 1,},
                new int[]{10, 1, 0, 10, 10,},
                new int[]{10, 10, 1, 0, 10,},
                new int[]{1, 10, 10, 10, 0},
        }, 6, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1},
        }, 1, new int[]{});

        test(new int[][]{
                new int[]{0, 0, 1, 1, 1,},
                new int[]{0, 0, 0, 1, 1,},
                new int[]{0, 0, 0, 0, 1,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0},
        }, 0, new int[]{0, 1, 2});

        test(new int[][]{
                new int[]{1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1},
        }, 1, new int[]{0, 1, 2});

        test(new int[][]{
                        new int[]{0, 1, 5, 5, 5, 5,},
                        new int[]{5, 0, 1, 5, 5, 5,},
                        new int[]{5, 5, 0, 5, 5, -1,},
                        new int[]{5, 5, 1, 0, 5, 5,},
                        new int[]{5, 5, 1, 5, 0, 5,},
                        new int[]{5, 5, 1, 1, 1, 0},
                }
                , 3, new int[]{0, 1, 2, 3});

        test(new int[][]{
                        new int[]{0, 1, 5, 5, 5, 5, 5,},
                        new int[]{5, 0, 1, 5, 5, 5, 5,},
                        new int[]{5, 5, 0, 5, 5, 0, -1,},
                        new int[]{5, 5, 1, 0, 5, 5, 5,},
                        new int[]{5, 5, 1, 5, 0, 5, 5,},
                        new int[]{5, 5, 0, 5, 5, 0, 0,},
                        new int[]{5, 5, 1, 1, 1, 0, 0},
                }
                , 3, new int[]{0, 1, 2, 3, 4});

        test(new int[][]{
                        new int[]{0, -1, 0, 9, 9, 9, 9, 9,},
                        new int[]{9, 0, 1, 9, 9, 9, 9, 9,},
                        new int[]{0, 9, 0, 0, 9, 9, 1, 1,},
                        new int[]{9, 9, 9, 0, 1, 9, 9, 9,},
                        new int[]{9, 9, 9, 9, 0, -1, 9, 9,},
                        new int[]{9, 9, 0, 9, 9, 0, 9, 9,},
                        new int[]{9, 9, -1, 9, 9, 9, 0, 9,},
                        new int[]{9, 9, 9, 9, 9, 9, 9, 0},
                },
                1, new int[]{0, 1, 2, 3, 4, 5});

        test(new int[][]{
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0},
        }, 0, new int[]{0, 1, 2});
        test(new int[][]{
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
        }, 0, new int[]{0, 1, 2, 3, 4});

        test(new int[][]{
                new int[]{0, 100, 100, 2, 100},
                new int[]{100, 0, 100, 100, 0,},
                new int[]{100, 100, 0, 100, 0,},
                new int[]{100, 2, 1, 0, 0,},
                new int[]{100, 100, 100, 100, 0},
        }, 4, new int[]{0, 2});
    }

    private static void test(int[][] ints, int i, int[] ints1) {
        System.out.println("Test case: " + Arrays.toString(ints1));
        int[] solution = answer(ints, i);
        System.out.println("Answer   : " + Arrays.toString(solution));
        if (!Arrays.equals(ints1, solution)) {
            throw new IllegalArgumentException();
        }
    }

    public static void permute(LinkedList<LinkedList<Integer>> subsets, int at, int index) {

        LinkedList<Integer> subset = subsets.get(index);

        for (int i = at; i < subset.size(); i++) {

            Collections.swap(subset, i, at);

            if (!set.contains(subset)) {

                LinkedList<Integer> new_perm = new LinkedList<Integer>(subset);

                set.add(subset);
                permutations.add(new_perm);

            }

            permute(subsets, at + 1, index);

            Collections.swap(subset, at, i);
        }


    }


    public static LinkedList<LinkedList<Integer>> subsets(int bunnies) {

        LinkedList<LinkedList<Integer>> subsets = new LinkedList<LinkedList<Integer>>();

        int max = (int) Math.pow(2, bunnies) - 1;

        int maxLen = Integer.toBinaryString(max).length();

        String bin;

        for (; max >= 0; max--) {

            bin = Integer.toBinaryString(max);

            int diff = maxLen - bin.length();

            for (int l = 0; l < diff; l++) {
                bin = "0" + bin;
            }

            LinkedList<Integer> subset = new LinkedList<Integer>();

            for (int i = 0; i < bin.length(); i++) {
                if (bin.charAt(i) == '1') {
                    subset.add(i);
                }
            }

            subsets.add(subset);
        }

        Collections.sort(subsets, new subsetComparator());

        return subsets;

    }


    public static int[] answer(int[][] times, int time_limit) {

        // Your code goes here.
        int len = times.length;

        LinkedList<LinkedList<Integer>> subsets = subsets(len - 2);

        int[][] min = new int[len][len];

        int src = 0;

        for (int z = 0; z < len; z++) {
            for (src = 0; src < len; src++) {
                if (z == 0) {
                    for (int i = 0; i < len; ++i) {
                        min[src][i] = Integer.MAX_VALUE;
                    }
                    min[src][src] = 0;
                }

                for (int i = 0; i < len; ++i) {
                    for (int j = 0; j < len; ++j) {
                        if (min[src][i] != Integer.MAX_VALUE && min[src][i] + times[i][j] < min[src][j]) {
                            min[src][j] = min[src][i] + times[i][j];
                        }
                    }
                }
            }
        }


        for (src = 0; src < len; src++) {
            for (int i = 0; i < len; ++i) {
                for (int j = 0; j < len; ++j) {
                    if (min[src][i] + times[i][j] < min[src][j]) {
                        int[] cycle = new int[len - 2];

                        for (int c = 0; c < len - 2; c++) {
                            cycle[c] = c;
                        }

                        return cycle;
                    }
                }
            }
        }

        // -=-

        int from;
        int time;

        for (int index = 0; index < subsets.size(); index++) {
            permute(subsets, 0, index);

            //Collections.sort(permutations, new subsetComparator());

            for (int p = 0; p < permutations.size(); p++) {

                from = 0;
                time = time_limit;

                LinkedList<Integer> perm = permutations.get(p);

                for (int i = 0; i < perm.size(); i++) {

                    time -= min[from][perm.get(i) + 1];
                    from = perm.get(i) + 1;

                }

                time -= min[from][len - 1];

                if (time >= 0) {

                    int[] result = new int[subsets.get(index).size()];

                    for (int r = 0; r < subsets.get(index).size(); r++) {
                        result[r] = subsets.get(index).get(r);
                    }

                    set.clear();
                    permutations.clear();

                    return result;
                }

            }

            set.clear();
            permutations.clear();

        }

        int[] result = new int[0];

        return result;
    }
}