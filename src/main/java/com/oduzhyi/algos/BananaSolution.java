package com.oduzhyi.algos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BananaSolution {
    public static void main(String[] args) {
        System.out.println(solution(new int[]{1 << 30 - 1, 2, 1, 6}));
        System.out.println(solution(new int[]{1, 7, 3, 21, 13, 19}));
        System.out.println(solution(new int[]{3, 5}));
        System.out.println(solution(new int[]{9, 15}));
        System.out.println(solution(new int[]{1, 1, 6}));
    }

    public static int solution(int[] banana_list) {
        List<Integer>[] bananaGraph = new List[banana_list.length];
        for (int i = 0; i < banana_list.length; i++) {
            bananaGraph[i] = new ArrayList<>();
        }
        for (int i = 0; i < banana_list.length - 1; i++) {
            for (int j = i; j < banana_list.length; j++) {
                if (endlessPair(banana_list[i], banana_list[j])) {
                    bananaGraph[i].add(j);
                    bananaGraph[j].add(i);
                }
            }
        }

        return banana_list.length - maxMatching(bananaGraph);
    }

    static int lca(int[] match, int[] base, int[] p, int a, int b) {
        boolean[] used = new boolean[match.length];
        while (true) {
            a = base[a];
            used[a] = true;
            if (match[a] == -1) {
                break;
            }
            a = p[match[a]];
        }
        while (true) {
            b = base[b];
            if (used[b]) {
                return b;
            }
            b = p[match[b]];
        }
    }

    static void markPath(int[] match, int[] base, boolean[] blossom, int[] p,
                         int v, int b, int children) {
        for (; base[v] != b; v = p[match[v]]) {
            blossom[base[v]] = blossom[base[match[v]]] = true;
            p[v] = children;
            children = match[v];
        }
    }

    static int findPath(List<Integer>[] graph, int[] match, int[] p, int root) {
        int n = graph.length;
        boolean[] used = new boolean[n];
        Arrays.fill(p, -1);
        int[] base = new int[n];
        for (int i = 0; i < n; ++i) {
            base[i] = i;
        }
        used[root] = true;
        int qh = 0;
        int qt = 0;
        int[] q = new int[n];
        q[qt++] = root;
        while (qh < qt) {
            int v = q[qh++];
            for (int to : graph[v]) {
                if (base[v] == base[to] || match[v] == to) {
                    continue;
                }
                if (to == root || match[to] != -1 && p[match[to]] != -1) {
                    int curbase = lca(match, base, p, v, to);
                    boolean[] blossom = new boolean[n];
                    markPath(match, base, blossom, p, v, curbase, to);
                    markPath(match, base, blossom, p, to, curbase, v);
                    for (int i = 0; i < n; ++i) {
                        if (blossom[base[i]]) {
                            base[i] = curbase;
                            if (!used[i]) {
                                used[i] = true;
                                q[qt++] = i;
                            }
                        }
                    }
                } else if (p[to] == -1) {
                    p[to] = v;
                    if (match[to] == -1) {
                        return to;
                    }
                    to = match[to];
                    used[to] = true;
                    q[qt++] = to;
                }
            }
        }
        return -1;
    }

    public static int maxMatching(List<Integer>[] graph) {
        int n = graph.length;
        int[] match = new int[n];
        Arrays.fill(match, -1);
        int[] p = new int[n];
        for (int i = 0; i < n; ++i) {
            if (match[i] == -1) {
                int v = findPath(graph, match, p, i);
                while (v != -1) {
                    int pv = p[v];
                    int ppv = match[pv];
                    match[v] = pv;
                    match[pv] = v;
                    v = ppv;
                }
            }
        }
        int matches = 0;
        for (int i = 0; i < n; ++i) {
            if (match[i] != -1) {
                ++matches;
            }
        }
        return matches;
    }

    public static int solutionBAD(int[] banana_list) {
        Map<Integer, Set<Integer>> posLoops = new HashMap<>();
        for (int i = 0; i < banana_list.length - 1; i++) {
            for (int j = i; j < banana_list.length; j++) {
                if (endlessPair(banana_list[i], banana_list[j])) {
                    posLoops.computeIfAbsent(i, HashSet::new).add(j);
                    posLoops.computeIfAbsent(j, HashSet::new).add(i);
                }
            }
        }


        int pairCnt = 0;
        while (posLoops.size() > 1) {
            int minPairs = Integer.MAX_VALUE;
            int minIdx = -1;

            for (Map.Entry<Integer, Set<Integer>> entry : posLoops.entrySet()) {
                if (entry.getValue().size() < minPairs) {
                    minIdx = entry.getKey();
                    minPairs = entry.getValue().size();
                }
            }

            Set<Integer> candidates = posLoops.get(minIdx);
            minPairs = Integer.MAX_VALUE;
            int nextMinIdx = -1;
            for (int next : candidates) {
                if (posLoops.get(next).size() < minPairs) {
                    nextMinIdx = next;
                    minPairs = posLoops.get(next).size();
                }
            }

            pairCnt++;
            deleteFromMap(posLoops, minIdx, nextMinIdx);
            deleteFromMap(posLoops, nextMinIdx, minIdx);

        }

        return banana_list.length - pairCnt * 2;
    }

    private static void deleteFromMap(Map<Integer, Set<Integer>> posLoops, int key, int value) {
        posLoops.remove(key);
        for (Map.Entry<Integer, Set<Integer>> entry : posLoops.entrySet()) {
            Set<Integer> values = entry.getValue();
            values.remove(value);
        }
    }

    private static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }

    private static boolean endlessPair(int a, int b) {
        int c = (a + b) / gcd(a, b);
        return ((c - 1) & c) != 0;
    }
}
