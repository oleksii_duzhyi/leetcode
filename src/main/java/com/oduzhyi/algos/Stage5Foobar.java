package com.oduzhyi.algos;

import java.util.ArrayDeque;
import java.util.Arrays;

public class Stage5Foobar {

    private static boolean[][] prevOs = new boolean[][]{
            new boolean[]{true, false, false, false},
            new boolean[]{false, true, false, false},
            new boolean[]{false, false, true, false},
            new boolean[]{false, false, false, true},
    };
    private static boolean[][] prevDots = new boolean[][]{
            new boolean[]{true, true, false, false},
            new boolean[]{true, false, true, false},
            new boolean[]{true, false, false, true},
            new boolean[]{false, true, true, false},
            new boolean[]{false, true, false, true},
            new boolean[]{false, false, true, true},
            new boolean[]{true, true, true, false},
            new boolean[]{true, true, false, true},
            new boolean[]{true, false, true, true},
            new boolean[]{false, true, true, true},
            new boolean[]{true, true, true, true},
            new boolean[]{false, false, false, false},
    };

    public static void main(String[] args) {
        long l = System.currentTimeMillis();
        System.out.println(solution(new boolean[][]{
                new boolean[]{true},

        }));
        System.out.println(solution(new boolean[][]{
                new boolean[]{false},

        }));
        System.out.println(solution(new boolean[][]{
                new boolean[]{true, false},
                new boolean[]{false, true},

        }));
        System.out.println(solution(new boolean[][]{
                new boolean[]{true, false, true},
                new boolean[]{false, true, false},
                new boolean[]{true, false, true},

        }));
        System.out.println(solution(new boolean[][]{
                {true, true, false, true, false, true, false, true, true, false},
                {true, true, false, false, false, false, true, true, true, false},
                {true, true, false, false, false, false, false, false, false, true},
                {false, true, false, false, false, false, true, true, false, false}
        }));
        System.out.println(solution(new boolean[][]{
                {true, false, true, false, false, true, true, true},
                {true, false, true, false, false, false, true, false},
                {true, true, true, false, false, false, true, false},
                {true, false, true, false, false, false, true, false},
                {true, false, true, false, false, true, true, true}
        }));
        System.out.println(System.currentTimeMillis() - l);
    }

    public static int solution(boolean[][] g) {
        int cnt = 0;
        ArrayDeque<State> states = new ArrayDeque<>();
        int r = g.length + 1;
        int c = g[0].length + 1;

        boolean[][] first = g[0][0] ? prevOs : prevDots;
        for (boolean[] prevO : first) {
            boolean[][] prevState = new boolean[r][c];
            for (int i = 0; i < prevO.length; i++) {
                prevState[i / 2][i % 2] = prevO[i];
            }
            states.offer(new State(prevState, 0));
        }

        while (!states.isEmpty()) {
            State state = states.poll();
            if (state.iteration == g.length * g[0].length - 1) {
                cnt++;
                continue;
            }

            int nextI = state.iteration + 1;
            int nextR = nextI / g[0].length;
            int nextC = nextI % g[0].length;
            boolean next = g[nextR][nextC];
            boolean[][] possiblePrevs = next ? prevOs : prevDots;

            for (boolean[] possiblePrev : possiblePrevs) {
                boolean matches = possiblePrev[0] == state.state[nextR][nextC];
                if (nextC > 0 && matches) {
                    matches = possiblePrev[2] == state.state[nextR + 1][nextC];
                }
                if (nextR > 0 && matches) {
                    matches = possiblePrev[1] == state.state[nextR][nextC + 1];
                }
                if (matches) {
                    boolean[][] nextS = copyArray(state.state);
                    nextS[nextR][nextC + 1] = possiblePrev[1];
                    nextS[nextR + 1][nextC] = possiblePrev[2];
                    nextS[nextR + 1][nextC + 1] = possiblePrev[3];
                    states.offer(new State(nextS, nextI));
                }
            }
        }

        return cnt;
    }

    private static boolean[][] copyArray(boolean[][] arr) {
        boolean[][] newCopy = new boolean[arr.length][];
        for (int i = 0; i < arr.length; i++) {
            newCopy[i] = Arrays.copyOf(arr[i], arr[i].length);
        }
        return newCopy;
    }

    private static class State {
        final boolean[][] state;
        final int iteration;

        public State(boolean[][] state, int iteration) {
            this.state = state;
            this.iteration = iteration;
        }

        @Override
        public String toString() {
            return "State{" +
                    "state=" + getString() +
                    ", iteration=" + iteration +
                    '}';
        }

        private String getString() {
            StringBuilder sb = new StringBuilder();
            for (boolean[] subArr : state) {
                sb.append(Arrays.toString(subArr)).append("\n");
            }
            return sb.toString();
        }
    }
}
