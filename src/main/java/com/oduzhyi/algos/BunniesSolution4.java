package com.oduzhyi.algos;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class BunniesSolution4 {
    //[
    //  [0, 2, 2, 2, -1],  # 0 = Start
    //  [9, 0, 2, 2, -1],  # 1 = Bunny 0
    //  [9, 3, 0, 2, -1],  # 2 = Bunny 1
    //  [9, 3, 2, 0, -1],  # 3 = Bunny 2
    //  [9, 3, 2, 2,  0],  # 4 = Bulkhead
    //]

    public static void main(String[] args) {
        test(new int[][]{
                new int[]{0, 1, 5, 5, 2,},
                new int[]{10, 0, 2, 6, 10,},
                new int[]{10, 10, 0, 1, 5,},
                new int[]{10, 10, 10, 0, 1,},
                new int[]{10, 10, 10, 10, 0},
        }, 5, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{0, 1, 3, 4, 2,},
                new int[]{10, 0, 2, 3, 4,},
                new int[]{10, 10, 0, 1, 2,},
                new int[]{10, 10, 10, 0, 1,},
                new int[]{10, 10, 10, 10, 0},
        }, 4, new int[]{});


        test(new int[][]{
                new int[]{0, 2, 2, 2, -1,},
                new int[]{9, 0, 2, 2, -1,},
                new int[]{9, 3, 0, 2, -1,},
                new int[]{9, 3, 2, 0, -1,},
                new int[]{9, 3, 2, 2, 0},
        }, 1, new int[]{1, 2});


        test(new int[][]{
                new int[]{0, 1, 10, 10, 10,},
                new int[]{10, 0, 1, 1, 2,},
                new int[]{10, 1, 0, 10, 10,},
                new int[]{10, 1, 10, 0, 10,},
                new int[]{10, 10, 10, 10, 0},
        }, 7, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{0, 1, 1, 1, 1,},
                new int[]{1, 0, 1, 1, 1,},
                new int[]{1, 1, 0, 1, 1,},
                new int[]{1, 1, 1, 0, 1,},
                new int[]{1, 1, 1, 1, 0},
        }, 3, new int[]{0, 1});


        test(new int[][]{
                new int[]{0, 5, 11, 11, 1,},
                new int[]{10, 0, 1, 5, 1,},
                new int[]{10, 1, 0, 4, 0,},
                new int[]{10, 1, 5, 0, 1,},
                new int[]{10, 10, 10, 10, 0},
        }, 10, new int[]{0, 1});


        test(new int[][]{
                new int[]{0, 20, 20, 20, -1,},
                new int[]{90, 0, 20, 20, 0,},
                new int[]{90, 30, 0, 20, 0,},
                new int[]{90, 30, 20, 0, 0,},
                new int[]{-1, 30, 20, 20, 0},
        }, 0, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{0, 10, 10, 10, 1,},
                new int[]{0, 0, 10, 10, 10,},
                new int[]{0, 10, 0, 10, 10,},
                new int[]{0, 10, 10, 0, 10,},
                new int[]{1, 1, 1, 1, 0},
        }, 5, new int[]{0, 1});


        test(new int[][]{
                new int[]{2, 2,},
                new int[]{2, 2},
        }, 5, new int[]{});


        test(new int[][]{
                new int[]{0, 10, 10, 1, 10,},
                new int[]{10, 0, 10, 10, 1,},
                new int[]{10, 1, 0, 10, 10,},
                new int[]{10, 10, 1, 0, 10,},
                new int[]{1, 10, 10, 10, 0},
        }, 6, new int[]{0, 1, 2});


        test(new int[][]{
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1,},
                new int[]{1, 1, 1, 1, 1, 1, 1},
        }, 1, new int[]{});

        test(new int[][]{
                new int[]{0, 0, 1, 1, 1,},
                new int[]{0, 0, 0, 1, 1,},
                new int[]{0, 0, 0, 0, 1,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0},
        }, 0, new int[]{0, 1, 2});

        test(new int[][]{
                new int[]{1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1,},
                new int[]{-1, 1, 1, 1, 1},
        }, 1, new int[]{0, 1, 2});

        test(new int[][]{
                        new int[]{0, 1, 5, 5, 5, 5,},
                        new int[]{5, 0, 1, 5, 5, 5,},
                        new int[]{5, 5, 0, 5, 5, -1,},
                        new int[]{5, 5, 1, 0, 5, 5,},
                        new int[]{5, 5, 1, 5, 0, 5,},
                        new int[]{5, 5, 1, 1, 1, 0},
                }
                , 3, new int[]{0, 1, 2, 3});

        test(new int[][]{
                        new int[]{0, 1, 5, 5, 5, 5, 5,},
                        new int[]{5, 0, 1, 5, 5, 5, 5,},
                        new int[]{5, 5, 0, 5, 5, 0, -1,},
                        new int[]{5, 5, 1, 0, 5, 5, 5,},
                        new int[]{5, 5, 1, 5, 0, 5, 5,},
                        new int[]{5, 5, 0, 5, 5, 0, 0,},
                        new int[]{5, 5, 1, 1, 1, 0, 0},
                }
                , 3, new int[]{0, 1, 2, 3, 4});

        test(new int[][]{
                        new int[]{0, -1, 0, 9, 9, 9, 9, 9,},
                        new int[]{9, 0, 1, 9, 9, 9, 9, 9,},
                        new int[]{0, 9, 0, 0, 9, 9, 1, 1,},
                        new int[]{9, 9, 9, 0, 1, 9, 9, 9,},
                        new int[]{9, 9, 9, 9, 0, -1, 9, 9,},
                        new int[]{9, 9, 0, 9, 9, 0, 9, 9,},
                        new int[]{9, 9, -1, 9, 9, 9, 0, 9,},
                        new int[]{9, 9, 9, 9, 9, 9, 9, 0},
                },
                1, new int[]{0, 1, 2, 3, 4, 5});

        test(new int[][]{
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0},
        }, 0, new int[]{0, 1, 2});
        test(new int[][]{
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
                new int[]{0, 0, 0, 0, 0, 0, 0,},
        }, 0, new int[]{0, 1, 2, 3, 4});

        test(new int[][]{
                new int[]{0, 100, 100, 2, 100},
                new int[]{100, 0, 100, 100, 0,},
                new int[]{100, 100, 0, 100, 0,},
                new int[]{100, 2, 1, 0, 0,},
                new int[]{100, 100, 100, 100, 0},
        }, 4, new int[]{0, 2});
    }

    private static void test(int[][] ints, int i, int[] ints1) {
        System.out.println("Test case: " + Arrays.toString(ints1));
        int[] solution = solution(ints, i);
        System.out.println("Answer   : " + Arrays.toString(solution));
        if (!Arrays.equals(ints1, solution)) {
            throw new IllegalArgumentException();
        }
    }

    public static int[] solution(int[][] times, int timeLimit) {
        //alternatively Johnson's algorithm can be also used
        int[][] allDists = calcBellmanFord(times);

        int l = times.length;
        if (detectNegativeCycle(times, allDists)) {
            int[] res = new int[l - 2];
            for (int i = 0; i < res.length; i++) {
                res[i] = i;
            }
            return res;
        }
        if (allDists[0][l - 1] > timeLimit) {
            return new int[]{};
        }

        State best = bfsStateTree(allDists, timeLimit);
        int[] res = new int[best.visited.size() - 1];
        int j = 0;
        for (int i = 0; i < best.bunniesSaved.length; i++) {
            if (best.bunniesSaved[i] == 1) {
                res[j++] = i;
            }
        }
        return res;
    }

    private static State bfsStateTree(int[][] times, int timeLimit) {
        int l = times.length;
        Set<Integer> visited = new HashSet<>();
        visited.add(0);
        State best = new State(timeLimit, new int[times.length - 2], 0, visited);
        ArrayDeque<State> queue = new ArrayDeque<>();
        queue.add(best);

        while (!queue.isEmpty()) {
            State next = queue.poll();

            if (next.position == l - 1 && next.timeLeft >= 0) {
                if (next.compareTo(best) > 0) {
                    best = next;
                    //saved all bunnies
                    if (best.visited.size() - 1 == l - 2) {
                        break;
                    }
                }
            }
            for (int i = 0; i < l; i++) {
                if (i == next.position || (next.visited.contains(i))) {
                    continue;
                }
                if (i == 0 || i == l - 1) {
                    queue.add(new State(next.timeLeft - times[next.position][i], next.bunniesSaved, i, next.visited));
                } else {
                    Set<Integer> newVisited = new HashSet<>(next.visited);
                    newVisited.add(i);
                    int nextBunnyId = i - 1;
                    int[] saved = new int[next.bunniesSaved.length];
                    System.arraycopy(next.bunniesSaved, 0, saved, 0, saved.length);
                    saved[nextBunnyId] = 1;
                    queue.add(new State(next.timeLeft - times[next.position][i], saved, i, newVisited));
                }
            }
        }
        return best;
    }

    private static int[][] calcBellmanFord(int[][] times) {
        int l = times.length;
        int[][] allDists = new int[l][l];

        for (int from = 0; from < l; from++) {
            int[] fromDist = new int[l];

            Arrays.fill(fromDist, 10000);
            fromDist[from] = 0;
            for (int i = 0; i < l - 1; i++) {
                for (int j = 0; j < l; j++) {
                    for (int k = 0; k < l; k++) {
                        if (j == k) {
                            continue;
                        }
                        if (fromDist[k] > fromDist[j] + times[j][k]) {
                            fromDist[k] = fromDist[j] + times[j][k];
                        }
                    }
                }
            }
            allDists[from] = fromDist;
        }

        return allDists;
    }

    private static boolean detectNegativeCycle(int[][] times, int[][] allDist) {
        int l = times.length;
        int[] dist = allDist[0];
        for (int j = 0; j < l; j++) {
            for (int k = 0; k < l; k++) {
                if (j == k) {
                    continue;
                }
                int weight = (j == l) ? 0 : times[j][k];
                if (dist[k] > dist[j] + weight) {
                    return true;
                }
            }
        }
        return false;
    }

    private static class State implements Comparable<State> {
        final int timeLeft;
        final int[] bunniesSaved;
        final int position;
        final Set<Integer> visited;

        public State(int timeLeft, int[] bunniesSaved, int position, Set<Integer> visited) {
            this.timeLeft = timeLeft;
            this.bunniesSaved = bunniesSaved;
            this.position = position;
            this.visited = visited;
        }

        @Override
        public int compareTo(State o) {
            int compare = Integer.compare(visited.size(), o.visited.size());
            if (compare == 0 && visited.size() != 0) {
                for (int i = 0; i < bunniesSaved.length; i++) {
                    if (bunniesSaved[i] != o.bunniesSaved[i]) {
                        return Integer.compare(bunniesSaved[i], o.bunniesSaved[i]);
                    }
                }
            }
            return compare;
        }
    }
}
