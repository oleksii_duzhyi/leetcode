package com.oduzhyi.algos;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class CustomBlockingQueue<E> {
    private ReentrantLock lock = new ReentrantLock();
    private Condition notEmpty = lock.newCondition();
    private Condition notFull = lock.newCondition();
    private Node<E> head = null;
    private Node<E> tail = null;
    private int capacity;
    private AtomicInteger currentCapacity = new AtomicInteger(0);

    public CustomBlockingQueue(int capacity) {
        this.capacity = capacity;
    }

    public static void main(String[] args) throws Exception {
        final CustomBlockingQueue<Integer> queue = new CustomBlockingQueue<>(10);
        final AtomicInteger newElements = new AtomicInteger(0);
        Runnable consumer = () -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    System.out.println(Thread.currentThread().getName() + " Consuming : " + queue.take());
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        };
        Runnable producer = () -> {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    int next = newElements.getAndIncrement();
                    queue.put(next);
                    System.out.println(Thread.currentThread().getName() + " Produced: " + next);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        };

        new Thread(consumer, "Consumer").start();
        new Thread(producer, "Producer1 ").start();
        Thread p2 = new Thread(producer, "Producer2 ");
        p2.start();
        p2.join();

    }

    public void put(E val) throws InterruptedException {
        lock.lock();
        try {
            Thread.sleep(1000);
            while (currentCapacity.get() == capacity) {
                notEmpty.await();
            }
            currentCapacity.incrementAndGet();
            if (head == null) {
                head = new Node<>(val);
                tail = head;
            } else {
                Node<E> last = new Node<>(val);
                tail.next = last;
                tail = last;
            }
            notFull.signal();
        } finally {
            lock.unlock();
        }
    }

    public E take() throws InterruptedException {
        E val;
        lock.lock();
        try {
            while (currentCapacity.get() == 0) {
                notFull.await();
            }
            currentCapacity.decrementAndGet();
            val = head.val;
            if (head == tail) {
                tail = null;
                head = null;
            } else {
                head = head.next;
            }
            notEmpty.signal();
        } finally {
            lock.unlock();
        }
        return val;
    }

    static class Node<E> {
        Node<E> next;
        E val;

        Node(E val) {
            this.val = val;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "next=" + next +
                    ", val=" + val +
                    '}';
        }
    }

}

