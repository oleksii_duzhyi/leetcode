package com.oduzhyi.algos;

public class SolutionCodes {

    public static void main(String[] args) {
        System.out.println(solution(new int[]{2, 2, 4, 2, 6, 8}));
    }

    public static int solution(int[] l) {
        int res = 0;
        for (int i = 1; i < l.length - 1; i++) {
            int num = l[i];
            int dividerCount = 0;

            for (int j = i - 1; j >= 0; j--) {
                if (num % l[j] == 0) {
                    dividerCount++;
                }
            }

            int divisorCount = 0;
            for (int j = i + 1; j < l.length; j++) {
                if (l[j] % num == 0) {
                    divisorCount++;
                }
            }
            if (dividerCount > 0 && divisorCount > 0) {
                res += (dividerCount * divisorCount);
            }
        }

        return res;
    }
}
