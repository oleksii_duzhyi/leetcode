package com.oduzhyi.coursera;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
//[
//        ["O","O","O","O","X","X"],
//        ["O","O","O","O","O","O"],
//        ["O","X","O","X","O","O"],
//        ["O","X","O","O","X","O"],
//        ["O","X","O","X","O","O"],
//        ["O","X","O","O","O","O"]
//]

//[[
//        "O","O","O","O","X","X"],
//        ["O","O","O","O","O","O"],
//        ["O","X","O","X","O","O"],
//        ["O","X","O","O","X","O"],
//        ["O","X","O","X","O","O"],
//        ["O","X","O","O","O","O"]
//        ]
public class Solution {
    Set<Integer> visited = new HashSet<>();

    public static int solution(String n, int b) {
        Map<String, Integer> firstOcc = new HashMap<>();
        String z = n;
        int cnt = 0;
        do {
            firstOcc.put(z, cnt++);
            char[] y = z.toCharArray();
            Arrays.sort(y);
            char[] x = new char[y.length];
            for (int i = 0; i < x.length; i++) {
                x[i] = y[y.length - 1 - i];
            }
            z = diff(x, y, b);
        } while (!firstOcc.containsKey(z));
        return cnt - firstOcc.get(z);
    }

    private static String diff(char[] x, char[] y, int b) {
        char[] res = new char[x.length];
        int carry = 0;
        for (int i = x.length - 1; i >= 0; i--) {
            if (x[i] - carry >= y[i]) {
                res[i] = (char) (x[i] - carry - y[i] + '0');
                carry = 0;
            } else {
                res[i] = (char) (x[i] + b - carry - y[i] + '0');
                carry = 1;
            }
        }
        return new String(res);
    }

    public static void main(String[] args) {
        Arrays.sort(new char[]{});
        System.out.println("211112".indexOf('2', 0 + 1));
//        System.out.println(solution("210022".indexOf(2), 3));
//        new Solution().solve(
//                new char[][]{
//                        new char[]{'O', 'O', 'O', 'O', 'X', 'X'},
//                        new char[]{'O', 'O', 'O', 'O', 'O', 'O'},
//                        new char[]{'O', 'X', 'O', 'X', 'O', 'O'},
//                        new char[]{'O', 'X', 'O', 'O', 'X', 'O'},
//                        new char[]{'O', 'X', 'O', 'X', 'O', 'O'},
//                        new char[]{'O', 'X', 'O', 'O', 'O', 'O'}
//                }
//        );
    }

    public void solve(char[][] board) {
        if (board.length == 0 || board[0].length == 0) {
            return;
        }
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] == 'O') {
                    dfs(board, i, j);
                }
            }
        }
    }

    private boolean dfs(char[][] board, int i, int j) {
        if (i < 0 || i == board.length || j < 0 || j == board[0].length
                || board[i][j] == 'X') {
            return false;
        }
//        visited.add(i * board[0].length + j);
        if (isOnBorder(i, j, board.length, board[0].length)) {
            return true;
        }

        board[i][j] = 'X';
        if (dfs(board, i + 1, j)
                | dfs(board, i - 1, j)
                | dfs(board, i, j + 1)
                | dfs(board, i, j - 1)) {
            board[i][j] = 'O';
            return true;
        }
        return false;
    }

    public void solveWQU(char[][] board) {
        if (board.length == 0 || board[0].length == 0) {
            return;
        }
        int r = board.length;
        int c = board[0].length;
        QuickUnion qu = new QuickUnion(r * c + 1);

        int[] delta = new int[]{-1, 1};
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                if (board[i][j] == 'O') {
                    if (isOnBorder(i, j, r, c)) {
                        qu.union(i * c + j, r * c);
                    }
                    for (int d = 0; d < 2; d++) {
                        if (i + delta[d] < r && i + delta[d] >= 0 && board[i + delta[d]][j] == 'O') {
                            qu.union(i * c + j, (i + delta[d]) * c + j);
                        }
                        if (j + delta[d] < c && j + delta[d] >= 0 && board[i][j + delta[d]] == 'O') {
                            qu.union(i * c + j, i * c + j + delta[d]);
                        }
                    }
                }
            }
        }
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                if (board[i][j] == 'O' && !qu.connected(i * c + j, r * c)) {
                    board[i][j] = 'X';
                }
            }
        }
        System.out.println("done");
    }

    boolean isOnBorder(int i, int j, int r, int c) {
        if (i == 0 || i == r - 1 || j == 0 || j == c - 1) {
            return true;
        }
        return false;
    }

    class QuickUnion {
        int[] array;
        int[] size;

        QuickUnion(int size) {
            this.array = new int[size];
            this.size = new int[size];
            for (int i = 0; i < size; i++) {
                array[i] = i;
                this.size[i] = 1;
            }
        }

        int find(int i) {
            while (array[i] != i) {
                i = array[i];
            }
            return i;
        }

        void union(int i, int j) {
            int rootI = find(i);
            int rootJ = find(j);
            // make smaller root point to larger one
            if (size[rootI] < size[rootJ]) {
                array[rootI] = rootJ;
                size[rootJ] += size[rootI];
            } else {
                array[rootJ] = rootI;
                size[rootI] += size[rootJ];
            }
        }

        boolean connected(int i, int j) {
            return find(i) == find(j);
        }
    }
}
