package com.oduzhyi.leetcode;

public class E0243ShortestWordDistance {
    public int shortestDistance(String[] words, String word1, String word2) {
        int p1 = -1;
        int p2 = -1;
        int minDist = Integer.MAX_VALUE;
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(word1)) {
                p1 = i;
                if (p2 != -1) {
                    minDist = Math.min(minDist, Math.abs(p1 - p2));
                }
            } else if (words[i].equals(word2)) {
                p2 = i;
                if (p1 != -1) {
                    minDist = Math.min(minDist, Math.abs(p1 - p2));
                }
            }
        }
        return minDist;
    }
}

// questions:
// can w1 == w2?
// is list always contain words?
