package com.oduzhyi.leetcode;

public class E0237DeleteNodeInLL {
    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }
}