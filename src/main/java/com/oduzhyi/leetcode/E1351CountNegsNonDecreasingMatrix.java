package com.oduzhyi.leetcode;

public class E1351CountNegsNonDecreasingMatrix {
    public int countNegatives(int[][] grid) {
        if (grid.length == 0 || grid[0].length == 0
                || grid[grid.length - 1][grid[0].length - 1] >= 0) {
            return 0;
        }
        int totalR = grid.length;
        int maxC = grid[0].length;

        int cnt = 0;
        for (int r = 0; r < totalR; r++) {
            for (int c = 0; c < maxC; c++) {
                if (grid[r][c] < 0) {
                    cnt += (totalR - r) * (maxC - c);
                    maxC = c;
                }
            }
        }
        return cnt;
    }
}

// first pretty straigt forward - iterate over grid and find negative numbers
// [
// [ 4, 3, 2,-1],
// [ 3, 2, 1,-1],
// [ 1, 1,-1,-2],
// [-1,-1,-2,-3]
// ]