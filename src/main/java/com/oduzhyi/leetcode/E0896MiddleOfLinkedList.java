package com.oduzhyi.leetcode;

public class E0896MiddleOfLinkedList {

    public ListNode middleNode(ListNode head) { // 1 - 2 - 3
        int size = 0;
        ListNode mid = head;
        while (head != null) {
            head = head.next;
            size++;
            if (size % 2 == 0) {
                mid = mid.next;
            }
        }
        return mid;
    }

    public ListNode middleNode2Iter(ListNode head) { // 1 - 2 - 3 - 4
        if (head == null || head.next == null) {
            return head;
        }
        int size = 0;
        ListNode tmp = head;
        while (tmp != null) {
            tmp = tmp.next;
            size++;
        }

        int mid = size / 2;
        while (mid > 0) {
            head = head.next;
            mid--;
        }
        return head;
    }

}