package com.oduzhyi.leetcode;

/**
 * Created by Нелла on 17.11.2019.
 */
public class E0226InvertBinaryTree {

    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }

        TreeNode l = root.left;
        root.left = root.right;
        root.right = l;

        invertTree(root.left);
        invertTree(root.right);

        return root;
    }

    public TreeNode invertTreeOld(TreeNode root) {
        if (root == null) {
            return root;
        }
        TreeNode tmp = root.left;
        root.left = root.right;
        root.right = tmp;
        invertTreeOld(root.left);
        invertTreeOld(root.right);
        return root;
    }
}
