package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class H0282ExpressionAddOperator {
    List<String> result = new ArrayList<>();
    int target;

    public List<String> addOperators(String num, int target) {
        this.target = target;
        addOperator(num, 0, new StringBuilder(), target, false);
        return result;
    }

    private void addOperator(String num, int idx, StringBuilder acc, int target, boolean startsWithZero) {
        if (idx == num.length()) {
            String potential = acc.toString();
            int res = expressionEval(potential);
            if (res == target) {
                result.add(potential);
            }
        } else {
            acc.append(num.charAt(idx));
            if ((num.charAt(idx) == '0' && acc.length() > 2 && (acc.charAt(acc.length() - 2) == '-' || acc.charAt(acc.length() - 2) == '+' || acc.charAt(acc.length() - 2) == '*')) || (idx == 0 && num.charAt(idx) == '0')) {
                addOperator(num, idx + 1, acc, target, true);
            } else {
                if (!startsWithZero) {
                    addOperator(num, idx + 1, acc, target, false);
                }
            }
            acc.deleteCharAt(acc.length() - 1);

            if (idx == 0 || acc.charAt(acc.length() - 1) == '-' || acc.charAt(acc.length() - 1) == '+'
                    || acc.charAt(acc.length() - 1) == '*') {
                return;
            }
            acc.append('-');
            addOperator(num, idx, acc, target, false);
            acc.deleteCharAt(acc.length() - 1);

            acc.append('+');
            addOperator(num, idx, acc, target, false);
            acc.deleteCharAt(acc.length() - 1);

            acc.append('*');
            addOperator(num, idx, acc, target, false);
            acc.deleteCharAt(acc.length() - 1);
        }
    }

    private int expressionEval(String exp) {
        if (exp.isEmpty()) {
            return 0;
        }
        String[] numbers = exp.split("[+\\-*]");
        if (numbers.length == 1) {
            if (Long.parseLong(numbers[0]) > Integer.MAX_VALUE) {
                return target - 1;
            }
            return Integer.parseInt(numbers[0]);
        }
        List<Integer> sum = new ArrayList<>();
        int res = 0;
        int i = 0;
        int pos = 0;
        while (i < numbers.length) {
            int sign = 1;
            if (pos > 0 && exp.charAt(pos - 1) == '-') {
                sign = -1;
            }
            if (Long.parseLong(numbers[0]) > Integer.MAX_VALUE) {
                return target - 1;
            }
            int mult = Integer.parseInt(numbers[i]);
            pos += numbers[i].length() + 1;
            i++;
            while (i < numbers.length && pos - 1 < exp.length() && exp.charAt(pos - 1) == '*') {
                pos += numbers[i].length() + 1;
                if (Long.parseLong(numbers[0]) > Integer.MAX_VALUE) {
                    return target - 1;
                }
                mult *= Integer.parseInt(numbers[i]);
                i++;
            }
            sum.add(mult * sign);
        }
        return sum.stream().reduce(Integer::sum).get();
    }
}

// backtracking approach - from every position we have 4 options - add number to previous,add, minus, mult
// instead of calculating at the end - calculate on the fly and add to result list with strings
// if reached an end and == target add to result set
