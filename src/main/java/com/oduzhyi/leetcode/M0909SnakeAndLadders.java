package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

public class M0909SnakeAndLadders {

    public static void main(String[] args) {
        System.out.println(new M0909SnakeAndLadders().snakesAndLadders(
                new int[][]{
                        new int[]{-1, 10, -1, 15, -1},
                        new int[]{-1, -1, 18, 2, 20},
                        new int[]{-1, -1, 12, -1, -1},
                        new int[]{2, 4, 11, 18, 8},
                        new int[]{-1, -1, -1, -1, -1},
                }
        ));
    }

    public int snakesAndLadders(int[][] board) {
        int n = board.length;

        Deque<Integer> queue = new ArrayDeque<>();
        queue.add(0);

        Set<Integer> visited = new HashSet<>();
        visited.add(0);

        int moves = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();

            for (int i = 0; i < size; i++) {
                int next = queue.poll();
                if (next == n * n - 1) {
                    return moves;
                }
                for (int k = next + 1; k <= Math.min(next + 6, n * n - 1); k++) {
                    int row = toRow(k, n);
                    int col = toCol(k, n, row);

                    int possible = board[row][col] == -1 ? k : (board[row][col] - 1);
                    if (!visited.contains(possible)) {
                        visited.add(possible);
                        queue.add(possible);
                    }
                }
            }
            moves++;
        }
        return -1;
    }

    private int toRow(int k, int n) {
        return n - 1 - k / n;
    }

    private int toCol(int k, int n, int row) {
        if (row % 2 != n % 2) { //1st, 3rd.. n-1
            return k % n;
        } else {
            return n - 1 - k % n;
        }
    }
}
