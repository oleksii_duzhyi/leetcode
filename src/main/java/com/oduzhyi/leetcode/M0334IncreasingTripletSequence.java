package com.oduzhyi.leetcode;

import java.util.LinkedList;

public class M0334IncreasingTripletSequence {
    public boolean increasingTriplet(int[] nums) {
        int num1 = Integer.MAX_VALUE;
        int num2 = Integer.MAX_VALUE;

        for (int i = 0; i < nums.length; i++) {
            if (num1 >= nums[i]) {
                num1 = nums[i];
            } else if (num2 >= nums[i]) {
                num2 = nums[i];
            } else {
                return true;
            }
        }

        return false;
    }

    public boolean increasingTripletMy(int[] nums) { // 2 -1 6 -4 8
        if (nums.length < 3) {
            return false;
        }
        LinkedList<Integer> stack = new LinkedList<>();
        LinkedList<Integer> newStack = new LinkedList<>();
        for (int i = 0; i < nums.length; i++) {
            if (stack.isEmpty()) {
                stack.push(nums[i]);
            } else {
                if (stack.size() == 1 && nums[i] < stack.peek()) {
                    stack.pop();
                    stack.push(nums[i]);
                } else if (nums[i] > stack.peek()) {
                    stack.push(nums[i]);
                    if (stack.size() == 3) {
                        return true;
                    }
                } else if (nums[i] < stack.peek() && nums[i] > stack.getLast()) {
                    stack.pop();
                    stack.push(nums[i]);
                } else if (nums[i] < stack.getLast()) {
                    if (newStack.isEmpty()) {
                        newStack.push(nums[i]);
                    } else if (nums[i] > newStack.peek()) {
                        newStack.push(nums[i]);
                        LinkedList<Integer> tmp = stack;
                        stack = newStack;
                        newStack = tmp;
                        newStack.clear();
                    }
                }
            }
        }

        return false;
    }
}

// basic idea
// pretend current numbers are maximum integers
// whenever algo meet new number smaller than num1 - overwrite it
// whenever algo meet new number num1 < n < num2 - overwrite 2
// if there is number bigger than num1 and num2 (impossible in the beginning) - return true
// otherwise - false
