package com.oduzhyi.leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class E0572SubTreeOfTreeAmazon {
    public boolean isSubtree(TreeNode s, TreeNode t) {
        if (s == null || t == null) {
            return false;
        }

        LinkedList<TreeNode> sQ = new LinkedList<>();
        sQ.add(s);

        while (!sQ.isEmpty()) {
            TreeNode sN = sQ.poll();

            if (sN.val == t.val) {
                boolean equal = compareSubTrees(sN, t);
                if (equal) {
                    return true;
                }
            }
            enqueue(sN, sQ);
        }

        return false;
    }

    private boolean compareSubTrees(TreeNode s, TreeNode t) {
        LinkedList<TreeNode> tQueue = new LinkedList<>();
        LinkedList<TreeNode> sQueue = new LinkedList<>();
        enqueue(s, sQueue);
        enqueue(t, tQueue);
        while (!sQueue.isEmpty() && !tQueue.isEmpty()) {
            TreeNode sNext = sQueue.poll();
            TreeNode tNext = tQueue.poll();

            if (sNext.val != tNext.val) {
                return false;
            }
            enqueue(sNext, sQueue);
            enqueue(tNext, tQueue);
        }
        return sQueue.isEmpty() && tQueue.isEmpty();
    }

    private void enqueue(TreeNode curr, Queue<TreeNode> queue) {
        if (curr.left != null) {
            queue.add(curr.left);
        }
        if (curr.right != null) {
            queue.add(curr.right);
        }
    }

    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
}

// are there duplicate values possible in tree?
// what is the maximum size?
// can there be more than one subtree?
// can the tree be empty?

//
// start investigating tree using BFS and look for t root
// if t root is found simultaneously iterate using BFS root t and s      <---
// if both are exausted - and every pair of nodes are equals - return true  |
// otherwise continue iterate over s tree (given duplicates are possible) ---