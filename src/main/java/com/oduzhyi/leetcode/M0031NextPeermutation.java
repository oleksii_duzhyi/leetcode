package com.oduzhyi.leetcode;

public class M0031NextPeermutation {
    public void nextPermutation(int[] nums) {
        int reverse = 0;
        for (int i = nums.length - 1; i >= 1; i--) {
            if (nums[i - 1] < nums[i]) {
                reverse = i;
                for (int j = nums.length - 1; j >= i; j--) {
                    if (nums[j] > nums[i - 1]) {
                        swap(nums, i - 1, j);
                        break;
                    }
                }
                break;
            }
        }
        reverse(nums, reverse);
    }


    private void reverse(int[] nums, int start) {
        int i = start, j = nums.length - 1;
        while (i < j) {
            swap(nums, i, j);
            i++;
            j--;
        }
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}

// iterate from right to left
// when found [i - 1] < [i] (first drop) - [i-1] element that need to be swapped
// iterate from right to i (inclusive) - find [j] >= [i-1]
// swap [j] and [i-1]
// reverse array from i to the right
