package com.oduzhyi.leetcode;

import java.util.ArrayDeque;

public class M0994RottenOranges {

    public static void main(String[] args) {
        new M0994RottenOranges().orangesRotting(new int[][]{
                new int[]{2, 1, 1},
                new int[]{1, 0, 0},
                new int[]{0, 1, 1}
        });
    }

    public int orangesRotting(int[][] grid) {
        if (grid.length == 0) {
            return 0;
        }
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        int row = grid.length;
        int col = grid[0].length;

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 2) {
                    queue.add(i * col + j);
                }
            }
        }

        int minutes = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                int idx = queue.poll();
                int r = idx / col;
                int c = idx % col;
                if (r + 1 < row && grid[r + 1][c] == 1) {
                    grid[r + 1][c] = 2;
                    queue.add((r + 1) * col + c);
                }
                if (r - 1 >= 0 && grid[r - 1][c] == 1) {
                    grid[r - 1][c] = 2;
                    queue.add((r - 1) * col + c);
                }
                if (c + 1 < col && grid[r][c + 1] == 1) {
                    grid[r][c + 1] = 2;
                    queue.add(r * col + c + 1);
                }
                if (c - 1 >= 0 && grid[r][c - 1] == 1) {
                    grid[r][c - 1] = 2;
                    queue.add(r * col + c - 1);
                }
            }
            minutes++;
        }
        return isAllRotten(grid) ? Math.max(0, minutes - 1) : -1;
    }

    public int orangesRottingInPlace(int[][] grid) {
        int minutes = 0;

        boolean addedNewRotten = false;
        while (!isAllRotten(grid)) {
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[0].length; j++) {
                    if (grid[i][j] >= 2 && grid[i][j] < 2 + minutes + 1) {
                        addedNewRotten = addedNewRotten
                                | rot(grid, i, j - 1, minutes)
                                | rot(grid, i, j + 1, minutes)
                                | rot(grid, i - 1, j, minutes)
                                | rot(grid, i + 1, j, minutes);
                    }
                }
            }
            if (!addedNewRotten) {
                return -1;
            }
            minutes++;
            addedNewRotten = false;
        }
        return minutes;
    }

    private boolean rot(int[][] grid, int i, int j, int minutes) {
        if (i < 0 || i > grid.length - 1 || j < 0 || j > grid[0].length - 1 || grid[i][j] == 0 || grid[i][j] != 1) {
            return false;
        }
        grid[i][j] = 2 + minutes + 1;
        return true;
    }

    private boolean isAllRotten(int[][] grid) {
        for (int[] row : grid) {
            for (int or : row) {
                if (or == 1) {
                    return false;
                }
            }
        }
        return true;
    }
}
