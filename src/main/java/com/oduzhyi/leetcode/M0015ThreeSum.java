package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

public class M0015ThreeSum {
    public static void main(String[] args) {
//        new M0015ThreeSum().threeSumNSquared(new int[]{-1,0,1,2,-1,-4, 8, -8, 10, -5, -5, -8, 8});
    }

    List<List<Integer>> result = new ArrayList<>();

    public List<List<Integer>> threeSum(int[] n) {
        Arrays.sort(n);

        for (int i = 0; i < n.length - 2; ) {
            twoSum(n[i], n, i + 1);
            int prevN = n[i];
            while (i < n.length - 2 && n[i] == prevN) {
                i++;
            }
        }
        return result;
    }

    private void twoSum(int num, int[] n, int from) {
        int i = from;
        int j = n.length - 1;

        while (i < j) {
            int sum = n[i] + n[j];
            if (sum + num == 0) {
                result.add(Arrays.asList(num, n[i], n[j]));
                int prevI = n[i];
                int prevJ = n[j];
                while (i < j && n[i] == prevI) {
                    i++;
                }
                while (j > i && n[j] == prevJ) {
                    j--;
                }
            } else if (sum + num > 0) {
                j--;
            } else {
                i++;
            }
        }
    }

    public List<List<Integer>> threeSumOld(int[] n) {
        Arrays.sort(n);
        List<List<Integer>> res = new ArrayList<>();
        for (int i = 0; i < n.length - 2; i++) {
            if (i == 0 || n[i - 1] != n[i]) {
                int lo = i + 1;
                int hi = n.length - 1;

                while (lo < hi) {
                    int sum = n[i] + n[lo] + n[hi];
                    if (sum > 0 || (hi < n.length - 1 && n[hi] == n[hi + 1])) {
                        hi--;
                    } else if (sum < 0 || (lo > i + 1 && n[lo] == n[lo - 1])) {
                        lo++;
                    } else {
                        res.add(Arrays.asList(n[i], n[lo], n[hi]));
                        hi--;
                        lo++;
                    }
                }
            }
        }
        return new ArrayList<>(res);
    }

    public List<List<Integer>> threeSumNSquared(int[] n) {
        int[][] twoSum = new int[n.length][n.length];
        Set<List<Integer>> res = new HashSet<>();
        Map<Integer, List<Integer>> nToK = new HashMap<>();
        for (int i = 0; i < n.length; i++) {
            for (int j = i + 1; j < n.length - 1; j++) {
                twoSum[i][j] = n[i] + n[j];
            }
            nToK.computeIfAbsent(n[i], k -> new ArrayList<>()).add(i);
        }

        for (int i = 0; i < n.length; i++) {
            for (int j = i + 1; j < n.length - 1; j++) {
                List<Integer> k = nToK.get(-twoSum[i][j]);
                if (k != null) {
                    if ((n[i] == n[j] && n[i] == 0 && k.size() < 3)
                            || ((n[i] == n[k.get(0)] || n[j] == n[k.get(0)]) && k.size() < 2)) {
                        continue;
                    }
                    res.add(toListPQ(n[i], n[j], n[k.get(0)]));
                }
            }
        }
        return new ArrayList<>(res);
    }


    public List<List<Integer>> threeSumNSquaredLogN(int[] n) {
        Set<List<Integer>> res = new HashSet<>();
        Arrays.sort(n);

        for (int i = 0; i < n.length - 2; i++) {
            for (int j = i + 1; j < n.length - 1; j++) {
                if (binarySearch(-(n[i] + n[j]), n, j + 1, n.length - 1)) {
                    res.add(toList(n[i], n[j], -(n[i] + n[j])));
                }
            }
        }
        return new ArrayList<>(res);
    }

    private boolean binarySearch(int target, int[] n, int f, int t) {
        if (f > t) {
            return false;
        }
        int mid = t + (f - t) / 2;
        if (target > n[mid]) {
            return binarySearch(target, n, mid + 1, t);
        } else if (target < n[mid]) {
            return binarySearch(target, n, f, mid - 1);
        } else {
            return true;
        }
    }

    private List<Integer> toListPQ(int i, int j, int k) {
        List<Integer> oneSol = new ArrayList<>();
        oneSol.add(i);
        oneSol.add(j);
        oneSol.add(k);
        Collections.sort(oneSol);
        return oneSol;
    }

    public List<List<Integer>> threeSumNCubeBetter(int[] n) {
        Arrays.sort(n);
        Set<List<Integer>> res = new HashSet<>();
        for (int k = n.length - 1; k >= 2 && n[k] >= 0; k--) {
            for (int i = 0; i <= k - 2 && n[i] <= 0; i++) {
                for (int j = i + 1; j <= k - 1; j++) {
                    int sum = n[i] + n[j] + n[k];
                    if (sum > 0) {
                        break;
                    }
                    if (sum == 0) {
                        res.add(toList(n[i], n[j], n[k]));
                        // while (n[i] == n[j]) {
                        // i++;
                        // }
                        break;
                    }
                }
            }
        }
        return new ArrayList<>(res);
    }

    public List<List<Integer>> threeSumNCube(int[] n) {
        Arrays.sort(n);
        Set<List<Integer>> res = new HashSet<>();
        for (int i = 0; i < n.length; i++) {
            for (int j = i + 1; j < n.length; j++) {
                for (int k = j + 1; k < n.length; k++) {
                    if (n[i] + n[k] + n[j] == 0) {
                        res.add(toList(n[i], n[j], n[k]));
                    }
                }
            }
        }

        return new ArrayList<>(res);
    }

    private List<Integer> toList(int i, int j, int k) {
        List<Integer> oneSol = new ArrayList<>(new PriorityQueue<>());
        oneSol.add(i);
        oneSol.add(j);
        oneSol.add(k);
        return oneSol;
    }
}

// first sort array,
// start i and solve Two Sum from i + 1 and length with 2 pointers
// if solution is found - increment low and decrement hi
// if n[low] == n[low + 1] => lo++
// if n[hi - 1] == n[hi] => hi--
// if n[i - 1] == n[i] => i++