package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class H0689MaxSumof3NonOverlappingArrays {
    int bestSum = Integer.MIN_VALUE;
    List<Integer> best = new ArrayList<>();
    Map<Integer, Integer> sums = new HashMap<>();

    public int[] maxSumOfThreeSubarrays(int[] nums, int k) {
        int[] possibleSums = new int[nums.length - k + 1];
        int currSum = 0;
        for (int i = 0; i < nums.length; i++) {
            currSum += nums[i];
            if (i >= k) {
                currSum -= nums[i - k];
            }
            if (i >= k - 1) {
                possibleSums[i - k + 1] = currSum;
            }
        }
        int[] left = new int[possibleSums.length];
        int best = 0;
        for (int i = 0; i < possibleSums.length; i++) {
            if (possibleSums[i] > possibleSums[best]) {
                best = i;
            }
            left[i] = best;
        }

        int[] right = new int[possibleSums.length];
        best = possibleSums.length - 1;
        for (int i = possibleSums.length - 1; i >= 0; i--) {
            if (possibleSums[i] >= possibleSums[best]) {
                best = i;
            }
            right[i] = best;
        }

        int[] ans = new int[]{-1, -1, -1};
        for (int j = k; j < possibleSums.length - k; j++) {
            int i = left[j - k], l = right[j + k];
            if (ans[0] == -1 || possibleSums[i] + possibleSums[j] + possibleSums[l] > possibleSums[ans[0]] + possibleSums[ans[1]] + possibleSums[ans[2]]) {
                ans[0] = i;
                ans[1] = j;
                ans[2] = l;
            }
        }
        return ans;
    }

    //[3,3,3,8,13,12]
    //[1,2,1,2, 6, 7,5]
    public int[] maxSumOfThreeSubarraysRec(int[] nums, int k) {
        for (int i = 0; i < nums.length - k + 1; i++) {
            int sum = 0;
            for (int j = i; j < i + k; j++) {
                sum += nums[j];
            }
            sums.put(i, sum);
        }


        maxSumOfThreeSubarrays(nums, 0, 0, new ArrayList<>(), k);
        int[] res = new int[]{best.get(0), best.get(1), best.get(2)};
        return res;
    }

    private void maxSumOfThreeSubarrays(int[] nums, int start, int cnt, List<Integer> indexes, int k) {
        if (cnt == 3) {
            int sum = findSum(nums, indexes, k);
            if (sum > bestSum) {
                best.clear();
                best.addAll(indexes);
                bestSum = sum;
            }
            return;
        }

        for (int i = start; i < nums.length - (3 - cnt) * k + 1; i++) {
            indexes.add(i);
            maxSumOfThreeSubarrays(nums, i + k, cnt + 1, indexes, k);
            indexes.remove(indexes.size() - 1);
        }
    }

    private int findSum(int[] nums, List<Integer> indexes, int k) {
        int sum = 0;
        for (int idx : indexes) {
            sum += sums.get(idx);
        }
        return sum;
    }
}

// from every position in array we have 2 choises
// I can skip this number and not include it in the sum
// or I can start calculating current sum => with caching N^3
// Better: calculate prefix sum for nums.length - k + 1
// calculte max from left and max from right
// at j = k, move it k times right and find the maximum
