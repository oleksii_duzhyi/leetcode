package com.oduzhyi.leetcode;

public class E1446ConsecutiveCharacters {
    public int maxPower(String s) {
        int maxP = 1;
        char[] chars = s.toCharArray();
        char prev = chars[0];
        int currP = 1;
        for (int i = 1; i < chars.length; i++) {
            if (prev == chars[i]) {
                currP++;
            } else {
                prev = chars[i];
                maxP = Math.max(currP, maxP);
                currP = 1;
            }
        }
        return Math.max(currP, maxP);
    }
}