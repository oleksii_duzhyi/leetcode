package com.oduzhyi.leetcode;

/**
 * Created by Нелла on 14.11.2018.
 */
public class Problem461HammingDistance {
    public int hammingDistance(int x, int y) {
        String binaryRep = Integer.toBinaryString(x ^ y);
        int result = 0;
        for (char c : binaryRep.toCharArray()) {
            if (c == '1') {
                result++;
            }
        }
        return result;
    }
}
