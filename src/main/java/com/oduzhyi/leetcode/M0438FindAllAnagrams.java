package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class M0438FindAllAnagrams {

    public List<Integer> findAnagrams(String s, String p) {
        if (p.length() > s.length()) {
            return Collections.EMPTY_LIST;
        }
        int[] anCnt = new int[26];
        int[] sCnt = new int[26];
        for (char c : p.toCharArray()) {
            anCnt[c - 'a']++;
        }

        List<Integer> res = new ArrayList<>();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            sCnt[chars[i] - 'a']++;
            if (i - p.length() >= 0) {
                sCnt[chars[i - p.length()] - 'a']--;
            }
            if (i + 1 >= p.length()) {
                if (isAnagram(sCnt, anCnt)) {
                    res.add(i + 1 - p.length());
                }
            }
        }
        return res;
    }

    private boolean isAnagram(int[] sCnt, int[] anCnt) {
        int i = 0;
        while (i < sCnt.length) {
            if (sCnt[i] != anCnt[i]) {
                return false;
            }
            i++;
        }
        return true;
    }

    public List<Integer> findAnagramsMy(String s, String p) {
        int[] anCnt = new int[26];
        for (char c : p.toCharArray()) {
            anCnt[c - 'a']++;
        }

        List<Integer> res = new ArrayList<>();
        char[] chars = s.toCharArray();
        for (int i = 0; i <= chars.length - p.length(); i++) {
            if (anCnt[chars[i] - 'a'] != 0) {
                int[] anCopy = Arrays.copyOf(anCnt, anCnt.length);
                boolean anagram = true;
                for (int j = i; j < i + p.length() && j < chars.length; j++) {
                    anCopy[chars[j] - 'a']--;
                    if (anCopy[chars[j] - 'a'] < 0) {
                        anagram = false;
                        break;
                    }
                }
                if (anagram) {
                    res.add(i);
                }
            }
        }
        return res;
    }

}
