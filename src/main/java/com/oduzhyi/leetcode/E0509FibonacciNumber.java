package com.oduzhyi.leetcode;

public class E0509FibonacciNumber {

    public int fibLoop(int n) {
        if (n == 0) {
            return 0;
        }
        int[] cache = new int[n + 1];
        cache[1] = 1;
        for (int i = 2; i < n + 1; i++) {
            cache[i] = cache[i - 1] + cache[i - 2];
        }
        return cache[n];
    }

    public int fibRec1st(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return 1;
        }
        return fibRec1st(n - 1) + fibRec1st(n - 2);
    }

    public int fib(int n) {
        if (n == 0) {
            return 0;
        }
        int[] cache = new int[n + 1];
        cache[1] = 1;
        return fibMem(n, cache);
    }

    private int fibMem(int n, int[] cache) {
        if (n == 0) {
            return 0;
        }
        if (cache[n] != 0) {
            return cache[n];
        }
        cache[n] = fibMem(n - 1, cache) + fibMem(n - 2, cache);
        return cache[n];
    }
}
