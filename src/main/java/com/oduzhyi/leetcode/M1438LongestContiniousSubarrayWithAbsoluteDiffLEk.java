package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

public class M1438LongestContiniousSubarrayWithAbsoluteDiffLEk {
    public int longestSubarray(int[] nums, int limit) {
        Deque<Integer> maxDeque = new ArrayDeque<>();
        Deque<Integer> minDeque = new ArrayDeque<>();

        int ans = 0;
        int l = 0;

        for (int r = 0; r < nums.length; r++) {
            while (!maxDeque.isEmpty() && nums[maxDeque.peekLast()] < nums[r]) {
                maxDeque.removeLast();
            }
            maxDeque.addLast(r);

            while (!minDeque.isEmpty() && nums[minDeque.peekLast()] > nums[r]) {
                minDeque.removeLast();
            }
            minDeque.addLast(r);

            if (nums[maxDeque.peekFirst()] - nums[minDeque.peekFirst()] <= limit) {
                ans = Math.max(ans, r - l + 1);
            } else {
                if (!minDeque.isEmpty() && minDeque.peekFirst() <= l) {
                    minDeque.removeFirst();
                }
                if (!maxDeque.isEmpty() && maxDeque.peekFirst() <= l) {
                    maxDeque.removeFirst();
                }
                l++;
            }
        }
        return ans;
    }

    public int longestSubarrayBF(int[] nums, int limit) {
        int maxLength = 0;

        for (int i = 0; i < nums.length - maxLength; i++) {
            int min = nums[i];
            int max = nums[i];

            int l = 1;
            for (int j = i + 1; j < nums.length; j++) {
                min = Math.min(nums[j], min);
                max = Math.max(nums[j], max);
                if (Math.abs(max - min) <= limit) {
                    l++;
                } else {
                    break;
                }
            }
            maxLength = Math.max(l, maxLength);
        }
        return maxLength;
    }
}

// BF: keep  max and min for particular subarray and find the diff between them  for every subarray
// O(N) ->  create 2 deques for max and min in sliding window
// maxdeque -> is monotonically decreasing of idx of nums in input array
// mindeque -> is monotonically increasing of idx of nums in input array
// max and min in the head, if max - min <= limit => update answer
// other wise move left end to the right, if idx in the head <= left border - remove it
