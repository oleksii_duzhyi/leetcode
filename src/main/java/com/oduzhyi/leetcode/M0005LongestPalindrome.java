package com.oduzhyi.leetcode;

public class M0005LongestPalindrome {
    public String longestPalindrome(String s) {  // abaaba
        int f = 0, t = 0;
        char[] chars = s.toCharArray();
        for (int i = 0; i < s.length() - 1; i++) {
            int j = -1, k = -1;
            if (chars[i] == chars[i + 1]) {
                j = i;
                k = i + 1;
            }
            while (j > 0 && k < s.length() - 1 && chars[j - 1] == chars[k + 1]) {
                j--;
                k++;
            }
            if (k - j > t - f) {
                f = j;
                t = k;
            }
            if (i > 0 && chars[i - 1] == chars[i + 1]) {
                j = i - 1;
                k = i + 1;
            }
            while (j > 0 && k < s.length() - 1 && chars[j - 1] == chars[k + 1]) {
                j--;
                k++;
            }
            if (k - j > t - f) {
                f = j;
                t = k;
            }
        }
        return s.isEmpty() ? "" : s.substring(f, t + 1);
    }

    public String longestPalindromePretty(String s) {  // abaaba
        int f = 0, t = 0;
        char[] chars = s.toCharArray();
        for (int i = 0; i < s.length() - 1; i++) {
            int[] s1 = expandAroundCenter(chars, i, i + 1);
            if (s1[1] - s1[0] > t - f) {
                f = s1[0];
                t = s1[1];
            }
            int[] s2 = expandAroundCenter(chars, i - 1, i + 1);
            if (s2[1] - s2[0] > t - f) {
                f = s2[0];
                t = s2[1];
            }
        }
        return s.isEmpty() ? "" : s.substring(f, t + 1);
    }

    private int[] expandAroundCenter(char[] chars, int j, int k) {
        if (j < 0 || chars[j] != chars[k]) {
            return new int[2];
        }
        while (j > 0 && k < chars.length - 1 && chars[j - 1] == chars[k + 1]) {
            j--;
            k++;
        }
        return new int[]{j, k};
    }
}

// basic idea - try to expand around center
// starting at i try to expand around center even and odd length