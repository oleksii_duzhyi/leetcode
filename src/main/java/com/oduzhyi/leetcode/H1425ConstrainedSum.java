package com.oduzhyi.leetcode;

import java.util.Arrays;

public class H1425ConstrainedSum {
    private int k;
    private int[] cache;

    public static void main(String[] args) {
        System.out.println(new H1425ConstrainedSum().constrainedSubsetSum(new int[]{-100, 1, -100, 1, 1, -100, 1, -100, 1, -100}, 9));
    }

    public int constrainedSubsetSum(int[] nums, int k) {
        this.k = k;
        cache = new int[nums.length];
        Arrays.fill(cache, Integer.MIN_VALUE);
        int maxSum = Integer.MIN_VALUE;
        for (int i = nums.length - 1; i >= 0; i--) {
            int ithSum = subsequenceSum(nums, i, 0);
            maxSum = Math.max(maxSum, ithSum);
            cache[i] = ithSum;
        }
        return maxSum;
    }

    private int subsequenceSum(int[] nums, int idx, int sum) {
        if (idx >= nums.length) {
            return sum;
        }
        if (cache[idx] != Integer.MIN_VALUE) {
            return sum + cache[idx];
        }
        int maxSum = nums[idx];

        for (int i = idx; i < idx + k && i < nums.length; i++) {
            maxSum = Math.max(maxSum, subsequenceSum(nums, i + 1, nums[i] + sum));
        }
        return maxSum;
    }
}
