package com.oduzhyi.leetcode;

public class E0459RepeatedSubstringPatternGoogle {

    public static void main(String[] args) {
        System.out.println(new E0459RepeatedSubstringPatternGoogle().repeatedSubstringPatternFromHighToLow("aba"));
    }

    public boolean repeatedSubstringPatternFromHighToLow(String s) {
        for (int i = s.length() / 2; i > 0; i--) {
            if (s.length() % i == 0) {
                boolean result = true;
                for (int j = i; j < s.length() && result; ) {
                    for (int k = 0; k < i && result; ) {
                        result = result && s.charAt(k++) == s.charAt(j++);
                    }
                }
                if (result) {
                    return true;
                }
            }
        }
        return false;
    } // 11ms

    private boolean repeatedSubstringPattern(String s) {
        int idx = (s + s).indexOf(s, 1);
        return idx < s.length();
    }

    public boolean repeatedSubstringPatternBF(String s) {
        for (int i = 0; i < s.length() / 2; i++) {
            int len = i + 1; // 1
            if (s.length() % len == 0) {
                boolean result = true;
                for (int j = len; j < s.length() && result; ) {
                    for (int k = 0; k < len && result; ) {
                        result = s.charAt(k++) == s.charAt(j++);
                    }
                }
                if (result) {
                    return true;
                }
            }
        }
        return false;
    }
}

// brute force solution
// Start with one character and compare it to the rest of the string by every character comparison
// If failed -> try 2 characters and so on until s.length() / 2
// otherwise -> return true
// complexity O(n2)

// Can we do it better?
//
// We can check length of a string. if s.length() % 2 == 1 - there is no need to check even combinations
// But worst case scenario - is still O(n2)

// Can we do it even better?
//
// int idx = (s + s).indexOf(s, 1);
// return  idx < s.length();
// connect 2 strings and try to find half of the string starting from the second position (1)
// Still can be O(n^2) - for example, aaaaaaaac => aaaaaaaacaaaaaaaac
//                                                  aaaaaaaac
// abcabcabc => abcabcabc + abcabcabc = abcabcabcabcabcabc
//                                         abcabcabc      //49ms

// Can we do better?
// what if we move instead from the middle of the string?
// motivation = abababab = ab * 4 = abab * 2
// so we if the string contans from substring we potentially can find an answer faster