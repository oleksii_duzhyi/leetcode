package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class E0169MajorityElement {
    public int majorityElement(int[] nums) {
        int res = nums[0];
        int count = 1;
        for (int i = 1; i < nums.length; i++) {
            if (count == 0) {
                res = nums[i];
                count = 1;
            } else {
                count = (res == nums[i]) ? count + 1 : count - 1;
            }
        }
        return res;
    }

    public int majorityElementHS(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }
        Map<Integer, Integer> counts = new HashMap<>();

        for (int n : nums) {
            Integer prev = counts.putIfAbsent(n, 1);
            if (prev != null) {
                if (prev + 1 > nums.length / 2) {
                    return n;
                } else {
                    counts.put(n, prev + 1);
                }
            }
        }
        return -1;
    }
}
//
//    в решении с счетчиками - основная идея точно такая же - если каждый раз когда мы видим какое-то число i - мы увеличиваем его счетчик, когда видим число к (к != n) - уменьшаем счетчик (и если счетчик равен 0 - начинаем считать сколько раз вчтречается к) - из-за того что есть число L которое встречается n/2 + 1 раз в массиве - мы гарантировано с конце останемся с числом L и счетчиком минимум в 1 единицу
//
//        я бы представил себе еще так
//
//        если есть массив
//        5, 6, 7, 8, 5, 5, 5
//
//        и мы заменим самое частое чило (5) на +1, а все остальные числа на -1,
//        1, -1, -1, -1, 1, 1, 1
//
//        при условии что число 5 гарантиравано встрчается >= n/2 + 1 = cумма всех чисел такого массива гарантировано >=1