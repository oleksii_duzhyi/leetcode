package com.oduzhyi.leetcode;

public class M0033SearchInRotatedSortedArray {
    public int search(int[] nums, int target) {
        return binarySearch(nums, target, 0, nums.length - 1);
    }

    private int binarySearch(int[] nums, int target, int from, int to) {
        if (from > to) {
            return -1;
        }
        int mid = to + (from - to) / 2;

        if (nums[mid] == target) {
            return mid;
        }
        if (nums[mid] > nums[from]) {
            if (target >= nums[from] && target < nums[mid]) {
                return binarySearch(nums, target, from, mid - 1);
            } else {
                return binarySearch(nums, target, mid + 1, to);
            }
        } else {
            if (target <= nums[to] && target > nums[mid]) {
                return binarySearch(nums, target, mid + 1, to);
            } else {
                return binarySearch(nums, target, from, mid - 1);
            }
        }
    }
}