package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class M0139WordBreak {
    Map<Integer, Boolean> cache = new HashMap<>();

    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> words = new HashSet<>(wordDict);
        int maxL = -1;
        for (String word : words) {
            if (word.length() > maxL) {
                maxL = word.length();
            }
        }
        boolean[] dp = new boolean[s.length() + 1];
        dp[0] = true;
        for (int i = 1; i <= s.length(); i++) {
            for (int j = Math.max(0, i - maxL); j < i; j++) {
                if (dp[j] && words.contains(s.substring(j, i))) {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[s.length()];
    }

    public boolean wordBreakRec(String s, List<String> wordDict) {
        return wordBreakInner(s, new HashSet<>(wordDict), 0);
    }

    private boolean wordBreakInner(String s, Set<String> wordDict, int start) {
        if (start == s.length()) {
            return true;
        }
        if (cache.containsKey(start)) {
            return cache.get(start);
        }

        for (int i = start + 1; i <= s.length(); i++) {
            if (wordDict.contains(s.substring(start, i))
                    && wordBreakInner(s, wordDict, i)) {
                return true;
            }
        }
        cache.put(start, false);
        return false;
    }

    public boolean wordBreakBfs(String s, List<String> wordDict) {
        Set<String> wordDictSet = new HashSet(wordDict);
        Deque<Integer> queue = new ArrayDeque<>();
        int[] visited = new int[s.length()];
        queue.add(0);
        while (!queue.isEmpty()) {
            int start = queue.remove();
            if (visited[start] == 0) {
                for (int end = start + 1; end <= s.length(); end++) {
                    if (wordDictSet.contains(s.substring(start, end))) {
                        queue.add(end);
                        if (end == s.length()) {
                            return true;
                        }
                    }
                }
                visited[start] = 1;
            }
        }
        return false;
    }
}

// start with transforming list to set
// initialize the memo array so we  keep track of insuccessful attempts
// do not check length more than maxL in dict