package com.oduzhyi.leetcode;

public class M0713SubarrayProductLessThanK {
    public int numSubarrayProductLessThanK(int[] nums, int k) {
        int res = 0;
        int prod = 1;
        int j = 0;
        for (int i = 0; i < nums.length; i++) {
            prod *= nums[i];
            while (j <= i && prod >= k) {
                prod /= nums[j++];
            }
            res += i - j + 1;
        }
        return res;
    }
}

// if product a * b * c < k - this mean a < k, b < k, c < k, a * b < k, b*c<k
// two pointer approach, counting product
// if product < k - add to result amount of items traversed
// if product >= k, increment start and divide product by the number at start
// once it is smaller - add dif from end - start + 1

