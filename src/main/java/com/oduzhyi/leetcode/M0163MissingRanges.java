package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class M0163MissingRanges {

    public List<String> findMissingRanges(int[] nums, int lower, int upper) {
        if (nums.length == 0) {
            return Arrays.asList(toRange(lower, upper));
        }
        List<String> res = new ArrayList<>();
        for (int n : nums) {
            if (lower < n) {
                res.add(toRange(lower, n - 1));
            }

            if (n == upper || n == Integer.MAX_VALUE) {
                return res;
            }
            lower = n + 1;
        }
        res.add(toRange(lower, upper));
        return res;
    }

    public List<String> findMissingRangesMy(int[] nums, int lower, int upper) { // 0,2,3,4 0-10
        if (nums.length == 0) {
            return Arrays.asList(toRange(lower, upper));
        }
        List<String> res = new ArrayList<>();
        int i = 0;
        long l = lower;
        while (i < nums.length) {

            while (i < nums.length && nums[i] == l) {
                while (i < nums.length - 1 && nums[i] == nums[i + 1]) {
                    i++;
                }
                i++;
                l++;
            }
            if (l <= upper) {
                if (i < nums.length) {
                    res.add(toRange((int) l, nums[i] - 1));
                    l = nums[i];
                } else {
                    res.add(toRange((int) l, upper));
                }
            }
        }
        return res;
    }

    private String toRange(int from, int to) {
        if (from == to) {
            return String.valueOf(from);
        } else {
            return from + "->" + to;
        }
    }
}