package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class H9999MockMultipleKnapsacks {
//[76,23,64,72,88,81,62,14,14]capacity:100,3boats,total =300

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(Arrays.asList(76, 23, 64, 72, 98, 88, 81, 62, 98, 14, 14));
        System.out.println(new H9999MockMultipleKnapsacks().maxGold(list, 100, 3));
    }

    public int maxGold(List<Integer> bags, int capacity, int numBoats) {
        int maxGold = 0;
        for (int i = 0; i < numBoats; i++) {
            int[][] dp = new int[bags.size() + 1][capacity + 1];
            int[][] items = new int[bags.size() + 1][capacity + 1];

            for (int item = 1; item <= bags.size(); item++) {
                for (int c = 1; c <= capacity; c++) {
                    int weight = bags.get(item - 1);
                    int value = weight;
                    if (c >= weight) {
                        if (value + dp[item - 1][c - weight] >= dp[item - 1][c]) {
                            dp[item][c] = value + dp[item - 1][c - weight];
                            items[item][c] = 1;
                        } else {
                            dp[item][c] = dp[item - 1][c];
                            items[item][c] = 0;
                        }
                    }
                }
            }

            maxGold += dp[bags.size()][capacity];
            int cap = capacity;
            for (int item = bags.size(); item > 0; item--) {
                if (items[item][cap] == 1) {
                    int weight = bags.remove(item - 1);
                    cap -= weight;
                }
            }
        }
        return maxGold;
    }

}
