package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class E0997FindTheTownJudge {

    public int findJudge(int n, int[][] trust) {
        if (trust.length < n - 1) {
            return -1;
        }
        int[] inOutDegree = new int[n + 1];
        for (int[] t : trust) {
            inOutDegree[t[0]]--;
            inOutDegree[t[1]]++;
        }

        for (int i = 1; i <= n; i++) {
            if (inOutDegree[i] == n - 1) {
                return i;
            }
        }
        return -1;
    }

    public int findJudgeHM(int n, int[][] trust) {
        if (n == 1 && trust.length == 0) {
            return 1;
        }
        Map<Integer, Integer> potentialJudges = new HashMap<>();
        Set<Integer> citizens = new HashSet<>();
        for (int i = 0; i < trust.length; i++) {
            citizens.add(trust[i][0]);
            potentialJudges.put(trust[i][1], potentialJudges.getOrDefault(trust[i][1], 0) + 1);
        }

        for (Integer citizen : citizens) {
            potentialJudges.remove(citizen);
        }
        for (Map.Entry<Integer, Integer> entry : potentialJudges.entrySet()) {
            if (entry.getValue() == n - 1) {
                return entry.getKey();
            }
        }
        return -1;
    }
}