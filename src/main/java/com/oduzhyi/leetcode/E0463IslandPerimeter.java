package com.oduzhyi.leetcode;

public class E0463IslandPerimeter {
    public int islandPerimeter(int[][] grid) {
        if (grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        int rows = grid.length;
        int cols = grid[0].length;
        int p = 0;
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                if (grid[r][c] == 1) {
                    p += r == 0 ? 1 : grid[r - 1][c] == 0 ? 1 : 0;
                    p += r == rows - 1 ? 1 : grid[r + 1][c] == 0 ? 1 : 0;
                    p += c == 0 ? 1 : grid[r][c - 1] == 0 ? 1 : 0;
                    p += c == cols - 1 ? 1 : grid[r][c + 1] == 0 ? 1 : 0;
                }
            }
        }
        return p;
    }
}