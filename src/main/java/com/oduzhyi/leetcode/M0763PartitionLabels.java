package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class M0763PartitionLabels {
    public List<Integer> partitionLabels(String s) {
        int l = s.length();
        int[] lastIdx = new int[26];

        for (int i = 0; i < l; i++) {
            lastIdx[s.charAt(i) - 'a'] = i;
        }

        int start = 0;
        int end = 0;
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < l; i++) {
            char c = s.charAt(i);

            end = Math.max(end, lastIdx[c - 'a']);
            if (end == i) {
                res.add(end - start + 1);
                start = i + 1;
            }
        }
        return res;
    }
}
// find the last index of every character in the contant space 26 lowercase chars
// iterate through string, keep in mind start and end
// update end to be the max(end, map[char-'a'])
// if end == i -> add new result to output