package com.oduzhyi.leetcode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * public interface NestedInteger {
 * // Constructor initializes an empty nested list.
 * public NestedInteger();
 * <p>
 * // Constructor initializes a single integer.
 * public NestedInteger(int value);
 * <p>
 * // @return true if this NestedInteger holds a single integer, rather than a nested list.
 * public boolean isInteger();
 * <p>
 * // @return the single integer that this NestedInteger holds, if it holds a single integer
 * // Return null if this NestedInteger holds a nested list
 * public Integer getInteger();
 * <p>
 * // Set this NestedInteger to hold a single integer.
 * public void setInteger(int value);
 * <p>
 * // Set this NestedInteger to hold a nested list and adds a nested integer to it.
 * public void add(NestedInteger ni);
 * <p>
 * // @return the nested list that this NestedInteger holds, if it holds a nested list
 * // Return null if this NestedInteger holds a single integer
 * public List<NestedInteger> getList();
 * }
 */
public class E339NestedListWeightedSum {

    public int depthSum(List<NestedInteger> nestedList) {
        if (nestedList.isEmpty()) {
            return 0;
        }
        Queue<NIWithD> queue = new LinkedList<>();
        for (NestedInteger ni : nestedList) {
            queue.add(new NIWithD(ni, 1));
        }

        int sum = 0;

        do {
            NIWithD next = queue.poll();
            if (next.ni.isInteger()) {
                sum += next.ni.getInteger() * next.depth;
            } else {
                for (NestedInteger ni : next.ni.getList()) {
                    queue.add(new NIWithD(ni, next.depth + 1));
                }
            }
        } while (!queue.isEmpty());
        return sum;
    }

    public int depthSumRec(List<NestedInteger> nestedList) {
        return innerDepthSum(nestedList, 1);
    }

    private int innerDepthSum(List<NestedInteger> list, int depth) {
        int sum = 0;
        for (NestedInteger ni : list) {
            if (ni.isInteger()) {
                sum += depth * ni.getInteger();
            } else {
                sum += innerDepthSum(ni.getList(), depth + 1);
            }
        }
        return sum;
    }

    public interface NestedInteger {
        // Constructor initializes an empty nested list.
//        public NestedInteger();

        // Constructor initializes a single integer.
//        public NestedInteger(int value);

        // @return true if this NestedInteger holds a single integer, rather than a nested list.
        public boolean isInteger();

        // @return the single integer that this NestedInteger holds, if it holds a single integer
        // Return null if this NestedInteger holds a nested list
        public Integer getInteger();

        // Set this NestedInteger to hold a single integer.
        public void setInteger(int value);

        // Set this NestedInteger to hold a nested list and adds a nested integer to it.
        public void add(NestedInteger ni);

        // @return the nested list that this NestedInteger holds, if it holds a nested list
        // Return null if this NestedInteger holds a single integer
        public List<NestedInteger> getList();
    }

    class NIWithD {
        NestedInteger ni;
        int depth;

        NIWithD(NestedInteger ni, int depth) {
            this.ni = ni;
            this.depth = depth;
        }
    }
}

// Questions:
// what are the input sizes I can expect?
// can the result of finding sum be more than integer?
// what is the deepest nested list can I expect?

// from the first glance - problem should be really easy solvable by recursion
// we are starting with initial list and depth of 1 trying to find sum of all elements
// is element is nested list itself - we increase depth by 1 and doing recursive call
//
// but if the depth is really big - we should use for-cycle
// iterating over our list in DFS or BFS manner
