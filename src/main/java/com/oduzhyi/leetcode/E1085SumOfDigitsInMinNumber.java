package com.oduzhyi.leetcode;

public class E1085SumOfDigitsInMinNumber {
    public int sumOfDigits(int[] A) {
        int min = min(A);

        int sum = 0;
        while (min > 0) {
            sum += min % 10;
            min /= 10;
        }
        return sum % 2 == 0 ? 1 : 0;
    }

    public int min(int[] a) {
        int min = Integer.MAX_VALUE;
        for (int n : a) {
            min = Math.min(n, min);
        }
        return min;
    }
}
