package com.oduzhyi.leetcode;

import java.util.Collections;
import java.util.PriorityQueue;

public class H0295FindMedianInStream {
    /**
     * initialize your data structure here.
     */
    private PriorityQueue<Integer> minHeap = new PriorityQueue<>();
    private PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Collections.reverseOrder());
    private int size = 0;

    public H0295FindMedianInStream() {
    }

    public void addNum(int num) {
        size++;
        if (maxHeap.isEmpty()) {
            maxHeap.add(num);
        } else {
            if (num < maxHeap.peek()) {
                maxHeap.add(num);
            } else {
                minHeap.add(num);
            }
            if (maxHeap.size() - minHeap.size() == 2) {
                minHeap.add(maxHeap.poll());
            } else if (maxHeap.size() < minHeap.size()) {
                maxHeap.add(minHeap.poll());
            }
        }
    }

    public double findMedian() {
        if (maxHeap.size() == 0) {
            return 0;
        }
        if (size % 2 == 1) {
            return maxHeap.peek();
        } else {
            return (maxHeap.peek() + minHeap.peek()) / 2d;
        }
    }
}

// create 2 heaps - min and  max
// max heap contains first part of input
// min heap holds left part
// keep also track of the number of elements
// when adding - if maxHeap is empty - add to it
// else
//    if the number is smaller than in max - add to max
//    else to min
//    do balancing - if max is bigger min by 2 elements - transfer one from max to min
//    else max is smaller than min - transfer one from min to max
