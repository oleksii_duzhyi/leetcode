package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

public class M0199BinaryTreeRightSideView {
    public List<Integer> rightSideView(TreeNode root) {
        if (root == null) {
            return Collections.emptyList();
        }
        List<Integer> res = new ArrayList<>();
        Deque<TreeNode> queue = new ArrayDeque<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            TreeNode last = null;
            for (int i = 0; i < size; i++) {
                last = queue.poll();
                if (last.left != null) {
                    queue.add(last.left);
                }
                if (last.right != null) {
                    queue.add(last.right);
                }
            }
            res.add(last.val);
        }
        return res;
    }
}

// easy bfs - take last element from every level
// can be done with DFS - keep level, traverse from the left element, add to res if level == res.size()
