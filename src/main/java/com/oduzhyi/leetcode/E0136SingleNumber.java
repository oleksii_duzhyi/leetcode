package com.oduzhyi.leetcode;

import java.util.HashSet;
import java.util.Set;

public class E0136SingleNumber {

    public int singleNumber(int[] nums) {
        int res = 0;
        for (int n : nums) {
            res ^= n;
        }
        return res;
    }

    public int singleNumberFirst(int[] nums) {
        Set<Integer> seen = new HashSet<>();
        for (int n : nums) {
            if (seen.contains(n)) {
                seen.remove(n);
            } else {
                seen.add(n);
            }
        }
        return seen.iterator().next();
    }
}

// questions:
// is input valid? non null? odd length? have only one solution?
// what are the input value ranges?
//
// first thing that comes to mind use an additional memory - set
// add element when we first see it
// remove element if we see it second time
// in the end there will be one element in the set and this going to be the answer
// but is it possible to do it without set?
//
// another idea - trying to keep a sum - sebtraction of all elements
// doesn't work - [100, 2, 1, 2, 1]
// another idea - take out the first element away, for the rest of array add even number and remove odd munbers from sum
// doesn't work - [100, 2, 1, 2, 1]  => 100     2 - 1 + 2 - 1
//
// we need some how selfdestruct pairs of numbers - we can do this with division mutiplication, or adding substraction
// for the second method it shows that we need to know that has been already adedd
// lets try use division and mutiplication
// we need to account for 3 special cases - 1, 0, -1
// still won't work [200, 2, 2, 1, 1]
//
// great idea to use XOR  a^a = 0,   (a^a)^c = c