package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class E0819MostFrequentWord {
    public String mostCommonWord(String paragraph, String[] banned) {
        String[] normalized = paragraph.toLowerCase().split("[\\s.,:;'!?\\-]+");

        Set<String> bannedSet = new HashSet<>();
        for (String ban : banned) {
            bannedSet.add(ban);
        }

        int cnt = 0;
        String maxW = "";
        Map<String, Integer> freq = new HashMap<>();
        for (String word : normalized) {
            if (!bannedSet.contains(word)) {
                int nextFreq = freq.getOrDefault(word, 0) + 1;
                freq.put(word, nextFreq);
                if (nextFreq > cnt) {
                    cnt = nextFreq;
                    maxW = word;
                }
            }
        }

        return maxW;
    }
}

//pretty straight forward - important part is to actually take care of max word in the first loop
