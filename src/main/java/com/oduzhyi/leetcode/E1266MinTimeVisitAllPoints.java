package com.oduzhyi.leetcode;

public class E1266MinTimeVisitAllPoints {

    public int minTimeToVisitAllPoints(int[][] points) {
        if (points.length < 2) {
            return 0;
        }

        int sum = 0;

        for (int i = 1; i < points.length; i++) {
            sum += Math.max(Math.abs(points[i - 1][0] - points[i][0]),
                    Math.abs(points[i - 1][1] - points[i][1]));
        }
        return sum;
    }


    public int minTimeToVisitAllPointsMy(int[][] points) {
        if (points.length < 2) {
            return 0;
        }

        int sum = 0;

        for (int i = 1; i < points.length; i++) {
            sum += dist(points[i - 1], points[i]);
        }
        return sum;
    }

    private int dist(int[] f, int[] t) {
        int diag = Math.min(Math.abs(f[0] - t[0]), Math.abs(f[1] - t[1]));
        if (f[0] + diag == t[0] || f[0] - diag == t[0]) {
            return diag + Math.min(Math.abs(f[1] + diag - t[1]), Math.abs(f[1] - diag - t[1]));
        }
        return diag + Math.min(Math.abs(f[0] + diag - t[0]), Math.abs(f[0] - diag - t[0]));
    }
}

// question:
// can points be empty?
// are point valid?
// what is the starting point?
//
// basically we start @ points[0] and then we have to calculte manhattan distansnce to points[i]
// it is not the manhattan distanse apparently from the task condition
// we can also move diagonally
// suppose we have 2 points 0,0  and 3,4
// initial idea split into diagonal moves and non-diagonal - calculate the sym of two

// better idea = diff between to points is basically equal to maximum absolute difference between corresponding coordinates