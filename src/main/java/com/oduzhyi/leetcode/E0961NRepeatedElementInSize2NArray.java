package com.oduzhyi.leetcode;

import java.util.HashSet;
import java.util.Set;

public class E0961NRepeatedElementInSize2NArray {
    public int repeatedNTimes(int[] A) {
        Set<Integer> seen = new HashSet<>();

        for (int a : A) {
            if (seen.contains(a)) {
                return a;
            }
            seen.add(a);
        }
        return -1;
    }

}

// questions:
// what is the array size?
// what are the values ranges in array?
//
// easy hashset solution
