package com.oduzhyi.leetcode;

public class H0717MinWindowSubsequence {
    public static void main(String[] args) {
        System.out.println(new H0717MinWindowSubsequence().minWindow("cnhczm", "cm"));
    }

    public String minWindow(String s, String t) {
        return min(s, 0, t, 0, new StringBuilder());
    }

    private String min(String s, int si, String t, int ti, StringBuilder sb) {
        if (ti == t.length()) {
            return sb.toString();
        }
        if (si == s.length()) {
            return "";
        }
        String min = null;
        for (int i = si; i < s.length() && (min == null || min.length() > sb.length()); i++) {
            if (s.charAt(i) == t.charAt(ti)) {
                sb.append(s.charAt(i));
                String cand = min(s, i + 1, t, ti + 1, new StringBuilder(sb));
                if (min == null || (!cand.isEmpty() && cand.length() < min.length())) {
                    min = cand;
                }
                if (ti == 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }
            } else if (ti > 0) {
                sb.append(s.charAt(i));
            }
        }
        return min == null ? "" : min;
    }
}
