package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.PriorityQueue;
import java.util.TreeMap;

public class H0239MaxInSlidingWindow {
    public int[] maxSlidingWindow(int[] nums, int k) {
        Deque<Integer> deque = new ArrayDeque<>();
        int[] res = new int[nums.length - k + 1];

        int resP = 0;
        int i = 0;
        while (i < nums.length) {
            if (!deque.isEmpty() && deque.peekFirst() <= i - k) {
                deque.removeFirst();
            }

            int next = nums[i];
            while (!deque.isEmpty() && nums[deque.peekLast()] < next) {
                deque.removeLast();
            }
            deque.addLast(i);
            if (i >= k - 1) {
                res[resP++] = nums[deque.peekFirst()];
            }
            i++;
        }
        return res;
    }

    public int[] maxSlidingWindowPQ(int[] nums, int k) {
        int[] res = new int[nums.length - k + 1];

        PriorityQueue<Integer> pq = new PriorityQueue<>(k, Collections.reverseOrder());
        int resP = 0;
        int i = 0;
        int j = 0;
        while (i < nums.length) {
            if (i - j < k) {
                pq.add(nums[i++]);
            } else {
                res[resP++] = pq.peek();
                pq.remove(nums[j++]);
                pq.add(nums[i++]);
            }
        }
        res[resP] = pq.peek();
        return res;
    }

    public int[] maxSlidingWindowMap(int[] nums, int k) {
        int[] res = new int[nums.length - k + 1];

        TreeMap<Integer, Integer> map = new TreeMap<>();
        int resP = 0;
        int i = 0;
        int j = 0;
        while (i < nums.length) {
            if (i - j < k) {
                int n = nums[i++];
                map.put(n, map.getOrDefault(n, 0) + 1);
            } else {
                res[resP++] = map.lastKey();
                int nJ = nums[j++];
                int nI = nums[i++];
                if (map.get(nJ) == 1) {
                    map.remove(nJ);
                } else {
                    map.put(nJ, map.get(nJ) - 1);
                }
                map.put(nI, map.getOrDefault(nI, 0) + 1);
            }
        }
        res[resP] = map.lastKey();
        return res;
    }
}
// can be done in O(N log K) with heap and tree map
// alternatively can be done in O(N) with the deque
// I need to  keep deque always decreasing with indexes of element
// if idx ofthe first element is bigger than or equal to i - k -> it needs to be removed
// before adding to the deque tail - need to  check if num[deq.last()] < num[i]
// while it is - poll last
// then add i
// the maximum is going to be the first element
