package com.oduzhyi.leetcode;

public class M0002Add2Numbers {

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode l2 = new ListNode(9);
        l2.next = new ListNode(9);
        new M0002Add2Numbers().addTwoNumbers(l1, l2);
    }

    public ListNode addTwoNumbersNew(ListNode l1, ListNode l2) {
        ListNode res = new ListNode();
        ListNode head = res;
        boolean carry = false;
        while (l1 != null || l2 != null || carry) {
            int x = 0;
            if (l1 != null) {
                x = l1.val;
                l1 = l1.next;
            }
            int y = 0;
            if (l2 != null) {
                y = l2.val;
                l2 = l2.next;
            }
            int sum = carry ? x + y + 1 : x + y;
            head.next = new ListNode(sum % 10);
            head = head.next;
            carry = sum >= 10;
        }
        return res.next;
    }

    public ListNode addTwoNumbersOld(ListNode l1, ListNode l2) {
        ListNode res = new ListNode(-1);
        ListNode cur = res;
        boolean carryOn = false;
        do {
            int x = (l1 != null) ? l1.val : 0;
            int y = (l2 != null) ? l2.val : 0;
            int sum = x + y;
            sum = carryOn ? sum + 1 : sum;
            cur.val = sum % 10;
            carryOn = sum >= 10;
            if (l1 != null || l2 != null) {
                cur.next = new ListNode(-1);
                cur = cur.next;
            }
            if (l1 != null) {
                l1 = l1.next;
            }
            if (l2 != null) {
                l2 = l2.next;
            }
        } while (l1 != null || l2 != null);

        if (carryOn) {
            cur.next = new ListNode(1);
        }

        return res;
    }


    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        return add2Lists(l1, l2, false);
    }

    public ListNode add2Lists(ListNode l1, ListNode l2, boolean carry) {
        if (l1 == null && l2 == null) {
            if (carry) {
                return new ListNode(1);
            } else {
                return null;
            }
        } else if (l1 == null || l2 == null) {
            ListNode nonNull = l1 == null ? l2 : l1;
            if (!carry) {
                return nonNull;
            }
            int newVal = nonNull.val + 1;
            if (newVal == 10) {
                nonNull.val = 0;
                nonNull.next = add2Lists(l1 == null ? null : l1.next, l2 == null ? null : l2.next, true);
            } else {
                nonNull.val = newVal;
            }
            return nonNull;
        }
        int sum = l1.val + l2.val;
        if (carry) {
            sum += 1;
        }
        l1.val = sum % 10;
        carry = sum >= 10;
        l1.next = add2Lists(l1.next, l2.next, carry);
        return l1;
    }

    public ListNode addTwoNumbersNewLN(ListNode l1, ListNode l2) {
        ListNode res = new ListNode();
        ListNode head = res;
        boolean carry = false;
        while (l1 != null && l2 != null) {
            int sum = l1.val + l2.val;
            sum = carry ? sum + 1 : sum;
            head.next = new ListNode(sum % 10);
            carry = sum >= 10;
            l1 = l1.next;
            l2 = l2.next;
            head = head.next;
        }
        if (l1 != null) {
            head.next = l1;
        } else if (l2 != null) {
            head.next = l2;
        }
        while (carry) {
            if (head.next == null) {
                head.next = new ListNode(1);
                break;
            }
            int newVal = head.next.val + 1;
            head.next.val = newVal % 10;
            carry = newVal >= 10;
            head = head.next;
        }
        return res.next;
    }
}