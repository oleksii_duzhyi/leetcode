package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class H0115DistinctSubsequence {
    Map<Integer, Map<Integer, Integer>> cache = new HashMap<>();
    private String t;

    public static void main(String[] args) {
        new LinkedList<>().add(null);
    }

    public int numDistinct(String s, String t) {
        int[][] dp = new int[s.length() + 1][t.length() + 1];
        for (int i = 0; i <= s.length(); i++) {
            dp[i][t.length()] = 1;
        }
        for (int i = 0; i < t.length(); i++) {
            dp[s.length()][i] = 0;
        }
        for (int i = s.length() - 1; i >= 0; i--) {
            for (int j = t.length() - 1; j >= 0; j--) {
                if (s.charAt(i) == t.charAt(j)) {
                    dp[i][j] = dp[i + 1][j + 1] + dp[i + 1][j];
                } else {
                    dp[i][j] = dp[i + 1][j];
                }
            }
        }
        return dp[0][0];
    }

    public int numDistinctRec(String s, String t) {
        this.t = t;
        return helper(s, 0, 0);
    }

    private int helper(String s, int collected, int position) {
        if (cache.get(collected) != null && cache.get(collected).get(position) != null) {
            return cache.get(collected).get(position);
        }
        if (s.length() - position + collected < t.length()) {
            return 0;
        }
        if (collected == t.length()) {
            return 1;
        }
        char c = s.charAt(position);
        int res = 0;
        if (c == t.charAt(collected)) {
            res += helper(s, collected + 1, position + 1);
        }
        res += helper(s, collected, position + 1);
        cache.computeIfAbsent(collected, k -> new HashMap()).put(position, res);
        return res;
    }


}

// obvious recursive approach - O(2^n)
// start with position 0 and collected 0
// at any char with the positon in S - I have 2 choices
// try to collect it - if they are equal and advance position by one
// skip it
// Optimization 1: add multimap for collected, position -> reduces complexity to O(collected * position)
// Optimization 2: dp approach calculating from the tail
