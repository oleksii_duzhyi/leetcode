package com.oduzhyi.leetcode;

public class M1249MinimumRemoveToMakeValidParanthesis {
    public String minRemoveToMakeValid(String s) {  // ))(aaa((()))
        StringBuilder res = new StringBuilder(); // (aaa((()))
        int open = 0; // 1                               |

        for (char c : s.toCharArray()) {
            if (c == '(') {
                open++;
            } else if (c == ')') {
                if (open == 0) {
                    continue;
                }
                open--;
            }
            res.append(c);
        }

        int last = res.length() - 1;
        while (open > 0) {
            if (res.charAt(last) == '(') {
                open--;
                res.deleteCharAt(last);
            }
            last--;
        }

        return res.toString();
    }
}

// simplified idea of a stack with using only sum of open - close brackets
// in the first iteration
// if we encounter open bracket - increment open and add char to result
// if we encounter close braket AND open is not 0 - decrement open and add to res - OTHERWISE SKIP
// else add character
// while open is > 0
// strat from either way and keep deleting characters until open == 0
// return result

