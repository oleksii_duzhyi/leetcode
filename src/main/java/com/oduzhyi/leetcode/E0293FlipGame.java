package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class E0293FlipGame {
    public List<String> generatePossibleNextMoves(String s) {
        List<String> possibleMoves = new ArrayList<>();
        char[] chars = s.toCharArray();

        for (int i = 0; i < s.length() - 1; i++) {
            if (chars[i] == '+' && chars[i + 1] == '+') {
                StringBuilder nextState = new StringBuilder();
                nextState.append(s.substring(0, i)).append("--");
                if (i + 2 < s.length()) {
                    nextState.append(s.substring(i + 2));
                }
                possibleMoves.add(nextState.toString());
            }
        }
        return possibleMoves;
    }
}
