package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class M0249GroupStrings {
    public List<List<String>> groupStrings(String[] strings) {
        Map<String, List<String>> res = new HashMap<>();

        for (String s : strings) {
            StringBuilder code = new StringBuilder();
            char c = s.charAt(0);
            for (char alpha : s.toCharArray()) {
                code.append(alpha - c < 0 ? alpha - c + 26 : alpha - c).append('#');
            }
            res.computeIfAbsent(code.toString(), k -> new ArrayList<>()).add(s);
        }
        return new ArrayList<>(res.values());
    }
}
// transform each string to the code
// integer#intger...
// take the first charcter and substract it from all the next characters
// if substraction result is smaller than 0 - add 26
// group by  codes, and convert to result
