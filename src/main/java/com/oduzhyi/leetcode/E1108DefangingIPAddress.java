package com.oduzhyi.leetcode;

public class E1108DefangingIPAddress {
    public String defangIPaddr(String address) {
        StringBuilder res = new StringBuilder();

        for (char c : address.toCharArray()) {
            if (c == '.') {
                res.append("[.]");
            } else {
                res.append(c);
            }
        }
        return res.toString();
    }
}
//simple iteration over IP and construction of a new string
