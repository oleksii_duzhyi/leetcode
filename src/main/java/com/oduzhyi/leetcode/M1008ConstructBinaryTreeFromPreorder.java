package com.oduzhyi.leetcode;

public class M1008ConstructBinaryTreeFromPreorder {
    public TreeNode bstFromPreorder(int[] preorder) {
        if (preorder.length == 0) {
            return null;
        }
        return build(preorder, 0, preorder.length - 1);
    }

    private TreeNode build(int[] preorder, int index, int to) {
        if (index == -1) {
            return null;
        }
        TreeNode node = new TreeNode(preorder[index]);
        int left = -1;
        int right = -1;
        for (int i = index + 1; i <= to && (left == -1 || right == -1); i++) {
            if (left == -1 && preorder[i] < preorder[index]) {
                left = i;
            }
            if (right == -1 && preorder[i] > preorder[index]) {
                right = i;
            }
        }
        node.left = build(preorder, left, right == -1 ? to : right - 1);
        node.right = build(preorder, right, to);

        return node;
    }
}