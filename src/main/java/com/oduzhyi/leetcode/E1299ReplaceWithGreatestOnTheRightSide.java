package com.oduzhyi.leetcode;

public class E1299ReplaceWithGreatestOnTheRightSide {

    public int[] replaceElementsInPlace(int[] arr) {  // 6, 1, 2, 3, 4     // 0 0 0 0 0
        int max = arr[arr.length - 1];         // 4
        arr[arr.length - 1] = -1;              // 0 0 0 0 -1
        for (int i = arr.length - 2; i >= 0; i--) {
            int cur = arr[i];
            arr[i] = max;
            max = Math.max(max, cur);
        }
        return arr;
    }

    public int[] replaceElementsExtraArray(int[] arr) {  // 6, 1, 2, 3, 4
        int[] res = new int[arr.length];       // 0 0 0 0 0
        int max = arr[arr.length - 1];         // 4
        res[arr.length - 1] = -1;              // 0 0 0 0 -1
        for (int i = arr.length - 2; i >= 0; i--) {
            res[i] = max;
            max = Math.max(max, arr[i]);
        }
        return res;
    }
}

// question:
// should the move be done in place?
// what is the size of intial array
// what are the values?