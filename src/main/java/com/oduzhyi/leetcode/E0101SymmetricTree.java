package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class E0101SymmetricTree {
    public boolean isSymmetric(TreeNode root) {
        return dfs(root, root);
    }

    private boolean dfs(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) {
            return true;
        }
        if (t1 == null || t2 == null) {
            return false;
        }
        return (t1.val == t2.val)
                && dfs(t1.left, t2.right)
                && dfs(t1.right, t2.left);
    }

    public boolean isSymmetricBSF(TreeNode tn) {
        if (tn == null) {
            return true;
        }
        List<TreeNode> level = new ArrayList<>(); // 2 null
        level.add(tn.left);
        level.add(tn.right);

        boolean hasNextLevel = true;
        while (!level.isEmpty()) {
            List<TreeNode> curr = level; // 2, null
            int i = 0; // 0
            int j = curr.size() - 1; // 1
            while (i < j) { // 	0 < 1
                Integer left = curr.get(i) == null ? null : curr.get(i).val;
                Integer right = curr.get(j) == null ? null : curr.get(j).val;
                if (!Objects.equals(left, right)) {
                    return false;
                }
                i++;
                j--;
            }
            List<TreeNode> next = new ArrayList<>();
            for (TreeNode node : curr) {
                if (node != null) {
                    next.add(node.left);
                    next.add(node.right);
                }
            }
            level = next;
        }
        return true;
    }
}
