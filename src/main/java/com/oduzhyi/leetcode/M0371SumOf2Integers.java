package com.oduzhyi.leetcode;

public class M0371SumOf2Integers {
    public int getSum(int a, int b) {
        while (b != 0) {
            int answer = a ^ b;
            int carry = (a & b) << 1;
            a = answer;
            b = carry;
        }

        return a;
    }
}

// XOR -> gives result of sum w/o carry
// carry -> equal to a&b << 1 (in places where there both 1 - carry it one position left)
// perform operations until carry  is 0
