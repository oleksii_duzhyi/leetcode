package com.oduzhyi.leetcode;

import java.util.Arrays;

/**
 * Created by Нелла on 20.11.2018.
 */
public class E0942DIString {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(new E0942DIString().diStringMatch("IDID")));
        System.out.println(Arrays.toString(new E0942DIString().diStringMatch("III")));
        System.out.println(Arrays.toString(new E0942DIString().diStringMatch("DDI")));
    }

    public int[] diStringMatch(String s) {
        if (s.length() == 0) {
            return new int[0];
        }
        int[] res = new int[s.length() + 1];
        char[] chars = s.toCharArray();
        int i = 0;
        int d = s.length();
        for (int j = 0; j < chars.length; j++) {
            if (chars[j] == 'I') {
                res[j] = i++;
            } else {
                res[j] = d--;
            }
        }
        res[s.length()] = chars[s.length() - 1] == 'I' ? i : d;
        return res;
    }

    public int[] diStringMatchOld(String s) {
        int min = 0;
        int max = s.length();
        int[] res = new int[max + 1];
        for (int i = 0; i < res.length - 1; i++) {
            if (s.charAt(i) == 'I') {
                res[i] = min++;
            } else {
                res[i] = max--;
            }
        }

        res[s.length()] = min;

        return res;
    }
}
