package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class M1462CourseScheduleIV {
    Map<Integer, List<Integer>> vert = new HashMap<>();
    Set<Integer> visited = new HashSet<>();
    Map<Integer, List<Integer>> parents = new HashMap<>();

    public List<Boolean> checkIfPrerequisite(int n, int[][] pr, int[][] queries) {
        boolean[][] reach = new boolean[n][n];
        for (int[] p : pr) {
            vert.computeIfAbsent(p[0], ArrayList::new).add(p[1]);
        }
        for (int i = 0; i < n; i++) {
            dfs(i, i, reach);
        }
        List<Boolean> res = new ArrayList<>();
        for (int[] q : queries) {
            res.add(reach[q[0]][q[1]]);
        }
        return res;
    }

    private void dfs(int from, int node, boolean[][] reach) {
        if (reach[from][node]) {
            return;
        }
        reach[from][node] = true;
        for (int child : vert.computeIfAbsent(node, ArrayList::new)) {
            dfs(from, child, reach);
        }
    }

//     public List<Boolean> checkIfPrerequisiteEvery(int n, int[][] pr, int[][] queries) {
//         Boolean[] res = new Boolean[queries.length];
//         Arrays.fill(res, false);
//         if (pr.length == 0) {
//             return Arrays.asList(res);
//         }

//         for (int[] p : pr) {
//             vert.computeIfAbsent(p[0], HashSet::new).add(p[1]);
//         }
//         for (int i = 0; i < n; i++) {
//             vert.computeIfAbsent(i, HashSet::new);
//         }

//         int i = 0;
//         for (int[] q : queries) {
//             res[i++] = findChild(q[0], q[1]);
//             visited.clear();
//         }
//         return Arrays.asList(res);
//     }

//     private boolean findChild(int node, int child) {
//         if (visited.contains(node)){
//             return false;
//         }
//         visited.add(node);
//         if (vert.get(node).contains(child)) {
//             return true;
//         }
//         for (int c : vert.get(node)) {
//             boolean found = findChild(c, child);
//             if (found) {
//                 return true;
//             }
//         }
//         return false;
//     }
}