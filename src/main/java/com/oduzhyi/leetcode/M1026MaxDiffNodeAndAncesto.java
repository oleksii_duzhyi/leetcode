package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

public class M1026MaxDiffNodeAndAncesto {
    public static void main(String[] args) {
        Deque<Integer> stack = new ArrayDeque<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        System.out.println(stack.peekLast());
        System.out.println(stack.pollLast());
    }

    public int maxAncestorDiff(TreeNode root) {
        return dfs(root, root.val, root.val);
    }

    private int dfs(TreeNode tn, int min, int max) {
        if (tn == null) {
            return max - min;
        }
        max = Math.max(max, tn.val);
        min = Math.min(min, tn.val);
        return Math.max(dfs(tn.left, min, max), dfs(tn.right, min, max));
    }

}
// dfs with finding min and max and returning the max diff between them
