package com.oduzhyi.leetcode;

import java.util.Arrays;

/**
 * Created by Нелла on 20.11.2018.
 */
public class E0561ArrayPartitionI {
    private static boolean sorted(int[] nums) {
        boolean sorted = true;
        for (int i = 0; i < nums.length - 1 && sorted; i++) {
            sorted = nums[i] < nums[i + 1];
        }
        return sorted;
    }

    public static void main(String[] args) {
        System.out.println(new E0561ArrayPartitionI().arrayPairSum(new int[]{1, 4, 3, 2}));
        System.out.println(new E0561ArrayPartitionI().arrayPairSum(new int[]{1, 2, 3, 4}));
    }

    public int arrayPairSumOld(int[] nums) {
        if (nums.length % 2 == 1) {
            throw new IllegalArgumentException("Can't create pair. Number of elements is odd");
        }
        if (!sorted(nums)) {
            Arrays.sort(nums);
        }
        int sum = 0;
        for (int i = 0; i < nums.length; i += 2) {
            sum = sum + nums[i];
        }
        return sum;
    }


    public int arrayPairSumNonOptimized(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        Arrays.sort(nums);
        int res = 0;
        for (int i = 0; i < nums.length / 2; i++) {
            res += Math.min(nums[2 * i], nums[2 * i + 1]);
        }
        return res;
    }

    public int arrayPairSum(int[] nums) {
        int[] hash = new int[20001]; // -10.000 .. 10.000
        //count the frequencies
        for (int i : nums) {
            hash[i + 10_000]++;
        }

        int sum = 0;
        boolean carry = false;
        for (int i = 0; i < hash.length; i++) {
            // if the number of occurances is not zero
            if (hash[i] != 0) {
                int freq = hash[i] / 2;
                boolean even = hash[i] % 2 == 0;
                // adding at least freq/2
                sum += (i - 10_000) * freq;
                // if there was nothing to carry
                if (!carry) {
                    // and if number id not even
                    if (!even) {
                        // we need to carry
                        carry = true;
                        // and add current number one mor
                        sum += (i - 10_000);
                    }
                } else {
                    // if we carried, we need to carry only if current number is even
                    carry = even;
                }
            }
        }
        return sum;
    }
}

// Questions:
// what is the max size of input array
// can there be negative in the array?
// what is the range of numbers inside that array?
// is the array size is proportional to 2*n?
//
// the task is to make the BIGGEST possible sum out of the SMALLEST integers in pairs.
// to find the biggest sum we need to combine 2 adjasent integers by value together
// to minimally decrease the possible min value
// for example
// [2,10,7,5] - the maximum sum from min pairs is 9 (2 + 7). Lets check all pairs to be sure:
// (2,10)//-8 + (7,5)//-2 = 7
// the easiest way to do this is to sort array
// find min between i, i+1
// add min to result
//
// very interesting idea:
// if the rnage of possible values is known before hand and it is not much bigger that total number of events
// - sort can be avoided due to the following fact
// if we count number of occurences of every element and then go from smaller element to bigger -
// we can add to the sum freq/2 number of occurances
// if freq%2 == 1 - than we have to consider current event one more time
// and decrease next element freq by 1 to account for this pair

// [1,1,1,2,2,4]
