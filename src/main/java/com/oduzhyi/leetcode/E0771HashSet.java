package com.oduzhyi.leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * Jewels and Stones
 */
public class E0771HashSet {

    public int numJewelsInStones(String J, String S) {
        boolean[] jewels = new boolean[58];
        for (char c : J.toCharArray()) {
            jewels[c - 'A'] = true;
        }

        int result = 0;
        for (char s : S.toCharArray()) {
            if (jewels[s - 'A']) {
                result++;
            }
        }
        return result;
    }

    public int numJewelsInStonesHM(String J, String S) {
        Set<Character> jewels = new HashSet<>();
        for (char c : J.toCharArray()) {
            jewels.add(c);
        }

        int result = 0;
        for (char s : S.toCharArray()) {
            if (jewels.contains(s)) {
                result++;
            }
        }
        return result;
    }
}

// this looks like a find problem, potentially S can be very long as
// well as J. Iterating over this 2 string will result in O(J.l * S.l) complexity
// on the other hand we can use a hash map to collect chars from J into a Set
// and than iterate over
//
// assumption - J and S are not null and of length > 0
//
// also to make it even faster but less readable
// if character in J and S are in range of a-zA-Z - we can allocate an array instead of hash map