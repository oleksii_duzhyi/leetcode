package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

public class M0173BSTIterator {
}

class BSTIterator {

    private Deque<TreeNode> stack = new ArrayDeque<>();

    public BSTIterator(TreeNode root) {
        if (root != null) {
            do {
                stack.push(root);
                root = root.left;
            } while (root != null);
        }
    }

    /**
     * @return the next smallest number
     */
    public int next() {
        TreeNode nextTn = stack.pop();
        int next = nextTn.val;
        nextTn = nextTn.right;
        if (nextTn != null) {
            do {
                stack.push(nextTn);
                nextTn = nextTn.left;
            } while (nextTn != null);
        }
        return next;
    }

    /**
     * @return whether we have a next smallest number
     */
    public boolean hasNext() {
        return !stack.isEmpty();
    }
}
// First approach - construct a list from BST and then go through it, tc: O(1), sc: O(n)
// second iterate through in inorder manner pushing nodes to the stack
