package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class Solution2233 {
    public static void main(String[] args) {
        System.out.println(new Solution2233().bstDistance(2, Arrays.asList(1, 2), 1, 2));
        System.out.println(new Solution2233().retrieveMostFrequentlyUsedWords("a99)&(*%%#$&*&^%%#*(*a sdg )(&(*R%$&*)(**^%&&(*^%^$^&a b c d d d.r09ugjn.replaceAll(\"[^a-z]\", \" \")ы/\"", Arrays.asList("b", "c")));
    }

    List<String> retrieveMostFrequentlyUsedWords(String helpText,
                                                 List<String> wordsToExclude) {
        // first I need to cleanup helpText and split it into the words
        String[] cleanWords = helpText.toLowerCase()
                .replaceAll("[^a-z]", " ")
                .split(" ");
        // I've decided to use set at the cost of the additional space
        // I've considered length of the wordsToExclude may be big
        // and serch in set is O(1) compared to list O(n)
        Set<String> excludeSet = new HashSet<>(wordsToExclude);
        // Building frequency map: word -> frequency
        Map<String, Integer> freq = new HashMap<>();
        for (String word : cleanWords) {
            if (!word.isEmpty() && !excludeSet.contains(word)) {
                freq.put(word, freq.getOrDefault(word, 0) + 1);
            }
        }

        List<String> mostFrequent = new ArrayList<>();

        // I iterate through the frequency map
        for (String word : freq.keySet()) {
            // if list with most mostFrequent words is empty
            // or if the frequency is the same - I add to list
            if (mostFrequent.isEmpty() || freq.get(word) == freq.get(mostFrequent.get(0))) {
                mostFrequent.add(word);
            } else if (freq.get(word) > freq.get(mostFrequent.get(0))) {
                // if the frequency is higher than what I've seen already
                // I clear a list and add new word
                mostFrequent.clear();
                mostFrequent.add(word);
            }
        }
        return mostFrequent;
    }

    // METHOD SIGNATURE BEGINS, THIS METHOD IS REQUIRED
    public int bstDistance(int num, List<Integer> values,
                           int node1, int node2) {
//        if (num < 2) {
//            return -1;
//        }
        TreeNode tree = buildBst(values, node1, node2);
        // node1 and/or node2 are not found
        if (tree == null) {
            return -1;
        }
        // now we need to find least common ancestor for n1 and n2
        TreeNode lca = lca(tree, node1, node2);
        return getDistance(lca, node1) + getDistance(lca, node2);
    }
    // METHOD SIGNATURE ENDS

    private int getDistance(TreeNode tree, int val) {
        if (tree.val == val) {
            return 0;
        }
        if (val > tree.val) {
            return 1 + getDistance(tree.right, val);
        }
        return 1 + getDistance(tree.left, val);
    }

    private TreeNode lca(TreeNode tree, int n1, int n2) {
        if (n1 > tree.val && n2 > tree.val) {
            // if both nodes are bigger than current node value - they in the right subree
            return lca(tree.right, n1, n2);
        } else if (n1 < tree.val && n2 < tree.val) {
            // if both nodes are smaller than current node value - they in the left subree
            return lca(tree.left, n1, n2);
        }
        // otherwise nodes are on the different sides from current node
        // thus making this node - lowest common ancestor
        return tree;
    }

    private TreeNode buildBst(List<Integer> values, int n1, int n2) {
        // check is  omitted based on the Constraint 0 < num < 2 ^ 31
        TreeNode root = new TreeNode(values.get(0));

        boolean n1Found = false;
        boolean n2Found = false;
        for (int i = 1; i < values.size(); i++) {
            int val = values.get(i);
            if (val == n1) {
                n1Found = true;
            }
            if (val == n2) {
                n2Found = true;
            }

            addToBst(root, val);
        }

        return n1Found && n2Found ? root : null;
    }

    private TreeNode addToBst(TreeNode root, int val) {
        // if root is null - it is a place where value needs to be added
        if (root == null) {
            return new TreeNode(val);
        }
        // if value bigger than the root value - we look for place in right part
        // otherwise in the left
        if (val > root.val) {
            root.right = addToBst(root.right, val);
        } else {
            root.left = addToBst(root.left, val);
        }
        return root;
    }

    private class TreeNode {
        private int val;
        private TreeNode left;
        private TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }
}