package com.oduzhyi.leetcode;

import java.util.LinkedList;

public class M0654MaximumBinaryTree {

    public TreeNode constructMaximumBinaryTree(int[] nums) { // 1 3 0 2
        if (nums == null || nums.length == 0) {
            return null;
        }
        LinkedList<TreeNode> stack = new LinkedList<>();
        for (int n : nums) {
            TreeNode tn = new TreeNode(n);
            while (!stack.isEmpty() && stack.peek().val < n) {
                tn.left = stack.pop();
            }
            if (!stack.isEmpty()) {
                stack.peek().right = tn;
            }
            stack.push(tn);
        }
        return stack.peekLast();
    }

    public TreeNode constructMaximumBinaryTreeRec(int[] nums) { // 3 2 1
        return maxBT(nums, 0, nums.length - 1);
    }

    public TreeNode maxBT(int[] nums, int f, int t) {
        int maxI = -1;
        int max = -1;
        for (int i = f; i <= t; i++) {
            if (nums[i] > max) {
                maxI = i;
                max = nums[i];
            }
        }
        if (maxI == -1) {
            return null;
        }
        TreeNode curr = new TreeNode(nums[maxI]);
        curr.left = maxBT(nums, f, maxI - 1);
        curr.right = maxBT(nums, maxI + 1, t);
        return curr;
    }
}