package com.oduzhyi.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeMap;

public class E1068HighFive {

    public int[][] highFive(int[][] items) {
        Arrays.sort(items, (a, b) -> a[0] == b[0] ? b[1] - a[1] : a[0] - b[0]);
        Set<Integer> count = new HashSet<>();
        for (int i = 0; i < items.length; i++) {
            count.add(items[i][0]);
        }
        int[][] res = new int[count.size()][2];
        int pointer = 0;

        for (int i = 0; i < items.length; i++) {
            int studentId = items[i][0];
            int sum = items[i][1];
            for (int j = i + 1; j < i + 5; j++) {
                sum += items[j][1];
            }
            i += 4;
            res[pointer][0] = studentId;
            res[pointer][1] = sum / 5;
            pointer++;
            while (i + 1 < items.length && studentId == items[i + 1][0]) {
                i++;
            }
        }
        return res;
    }

    public int[][] highFiveHeap(int[][] items) {
        Map<Integer, PriorityQueue<Integer>> stTop5s = new HashMap<>();

        for (int i = 0; i < items.length; i++) {
            PriorityQueue<Integer> top5s = stTop5s.computeIfAbsent(items[i][0], k -> new PriorityQueue<>(5));
            if (top5s.size() == 5) {
                if (top5s.peek() <= items[i][1]) {
                    top5s.poll();
                    top5s.add(items[i][1]);
                }
            } else {
                top5s.add(items[i][1]);
            }

        }

        TreeMap<Integer, Integer> stAvg = new TreeMap<>();
        for (Map.Entry<Integer, PriorityQueue<Integer>> stTop5 : stTop5s.entrySet()) {
            int avg = stTop5.getValue().stream().reduce(Integer::sum).get() / 5;
            stAvg.put(stTop5.getKey(), avg);
        }

        int[][] res = new int[stAvg.size()][2];
        int i = 0;
        for (Map.Entry<Integer, Integer> entry : stAvg.entrySet()) {
            res[i][0] = entry.getKey();
            res[i][1] = entry.getValue();
            i++;
        }
        return res;
    }
}

// questions:
// is there at least 5 student's ids?
// how many scores are possible for each student? is it possble student has 3 score for example?
//
// we need to keep track of 2 orders - student orders and student top 5 scores
// for keeping top 5 scores we can use a binary heap - java implematation Priority queue
// and collect top 5 scores for each student first - O(n log 5)
// after that we will need to sort students and calculate avg score for every student
//
// probably better idea - first sort arrays,
// then count unique student ids
// then iterate over sorted array and take first 5 entries