package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class E0104MaxDepthOfBinaryTree {
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(1 + maxDepth(root.left), 1 + maxDepth(root.right));
    }

    public int maxDepthIterative(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Deque<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        int depth = 0;

        while (!queue.isEmpty()) {
            depth++;

            int lvlLeftOver = queue.size();
            for (int i = 0; i < lvlLeftOver; i++) {
                TreeNode currLevel = queue.poll();
                if (currLevel.left != null) {
                    queue.add(currLevel.left);
                }

                if (currLevel.right != null) {
                    queue.add(currLevel.right);
                }
            }
        }
        return depth;
    }

    public int maxDepthOld(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Queue<NodeWithDepth> el = new LinkedList<>();
        int maxDepth = 1;
        el.offer(new NodeWithDepth(root, maxDepth));
        while (!el.isEmpty()) {
            NodeWithDepth nwd = el.poll();
            maxDepth = nwd.depth;
            if (nwd.treeNode.left != null) {
                el.offer(new NodeWithDepth(nwd.treeNode.left, maxDepth + 1));
            }
            if (nwd.treeNode.right != null) {
                el.offer(new NodeWithDepth(nwd.treeNode.right, maxDepth + 1));
            }
        }
        return maxDepth;
    }
}


class NodeWithDepth {
    int depth;
    TreeNode treeNode;

    NodeWithDepth(TreeNode tn, int depth) {
        this.depth = depth;
        this.treeNode = tn;
    }
}

// questions:
// can the tree be null?
// what is the maximum depth of the tree?
//
// if the can assume tree depth is not too big (not causing stack overflow) - it is easy to solve
// this task by simple recursive call
// otherwise - we will need to traverse it using BFS or DFS - it doesn't really matter
// we need to visit each node at least once