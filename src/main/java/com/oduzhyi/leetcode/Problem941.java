package com.oduzhyi.leetcode;

/**
 * Created by Нелла on 20.11.2018.
 */
public class Problem941 {
    public static void main(String[] args) {
        System.out.println(new Problem941().validMountainArray(new int[]{0, 1, 0}));
        System.out.println(new Problem941().validMountainArray(new int[]{0, 2, 1, 0}));
        System.out.println(new Problem941().validMountainArray(new int[]{0, 1, 2, 4, 2, 1}));
        System.out.println(new Problem941().validMountainArray(new int[]{0, 1, 2, 1, 2}));
        System.out.println(new Problem941().validMountainArray(new int[]{2, 1, 2, 3, 5, 7, 9, 10, 12, 14, 15, 16, 18, 14, 13}));
    }

    public boolean validMountainArray(int[] a) {
        if (a.length <= 2) {
            return false;
        }
        boolean ascend = true;
        for (int i = 1; i < a.length; i++) {
            if (ascend) {
                if (a[i - 1] < a[i]) {
                    if (i + 1 < a.length && a[i] > a[i + 1]) {
                        ascend = false;
                    }
                } else {
                    return false;
                }
            } else if (!ascend && a[i - 1] <= a[i]) {
                return false;
            }
        }
        if (ascend) {
            return false;
        }
        return true;
    }
}
