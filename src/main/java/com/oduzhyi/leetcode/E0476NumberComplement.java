package com.oduzhyi.leetcode;

import java.util.LinkedList;

public class E0476NumberComplement {
    public int findComplement(int num) {
        LinkedList<Integer> stack = new LinkedList<>();

        while (num != 0) {
            stack.push(num % 2 == 0 ? 1 : 0);
            num /= 2;
        }
        int res = 0;
        while (!stack.isEmpty()) {
            res = res << 1 | stack.pop();
        }

        return res;
    }
}