package com.oduzhyi.leetcode;

public class M0240SearchA2DMatrixII {

    public boolean searchMatrix(int[][] matrix, int target) {
        // start our "pointer" in the bottom-left
        int row = matrix.length - 1;
        int col = 0;

        while (row >= 0 && col < matrix[0].length) {
            if (matrix[row][col] > target) {
                row--;
            } else if (matrix[row][col] < target) {
                col++;
            } else { // found it
                return true;
            }
        }

        return false;
    }

    public boolean searchMatrixIncorrect(int[][] matrix, int target) {

        if (matrix.length == 0 || matrix[0].length == 0
                || target < matrix[0][0] || target > matrix[matrix.length - 1][matrix[0].length - 1]) {
            return false;
        }
        int r = matrix.length;
        int c = matrix[0].length;
        int i = 0;
        int j = 0;

        while (i + 1 < r && target >= matrix[i + 1][j]) {
            i++;
        }
        if (target == matrix[i][j]) {
            return true;
        }
        while (j < c && target > matrix[i][j]) {
            j++;
        }
        if (target == matrix[i][j]) {
            return true;
        }
        while (i >= 0 && target <= matrix[i][j]) {
            if (target == matrix[i][j]) {
                return true;
            }
            i--;
        }

        return false;
    }
}