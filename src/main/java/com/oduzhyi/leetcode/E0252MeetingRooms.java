package com.oduzhyi.leetcode;

import java.util.Arrays;
import java.util.Comparator;

public class E0252MeetingRooms {
    public boolean canAttendMeetingsOld(int[][] intervals) {
        if (intervals.length == 0 || intervals.length == 1) {
            return true;
        }
        Arrays.sort(intervals, Comparator.comparingInt(i -> i[0]));
        for (int i = 0; i < intervals.length - 1; i++) {
            if (intervals[i + 1][0] < intervals[i][1]) {
                return false;
            }
        }
        return true;
    }

    public boolean canAttendMeetings(int[][] intervals) {
        if (intervals.length <= 1) {
            return true;
        }
        Arrays.sort(intervals, Comparator.comparingInt(i -> i[0]));

        int maxEnd = intervals[0][1];
        for (int i = 1; i < intervals.length; i++) {
            if (intervals[i][0] < maxEnd) {
                return false;
            }
            maxEnd = Math.max(maxEnd, intervals[i][1]);
        }
        return true;
    }
}

// sort intervals
// remember the first interval end
// if the  next interval start < end  - return  false
// update end