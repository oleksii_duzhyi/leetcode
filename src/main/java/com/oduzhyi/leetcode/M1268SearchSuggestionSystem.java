package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class M1268SearchSuggestionSystem {
    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        Arrays.sort(products);

        List<List<String>> result = new ArrayList<>();

        for (int i = 0; i < searchWord.length(); i++) {
            String prefix = searchWord.substring(0, i + 1);
            int startIndex = binarySearch(products, prefix);
            List<String> subRes = new ArrayList<>();
            if (startIndex >= 0) {
                int k = 3;
                while (k > 0 && startIndex < products.length && products[startIndex].startsWith(prefix)) {
                    subRes.add(products[startIndex++]);
                    k--;
                }
            }
            result.add(subRes);
        }
        return result;
    }

    private int binarySearch(String[] products, String prefix) {
        int start = 0;
        int end = products.length - 1;

        int firstPrefix = -1;

        while (start <= end) {
            int mid = (start - end) / 2 + end;
            if (products[mid].startsWith(prefix)) {
                firstPrefix = mid;
            }
            if (prefix.compareTo(products[mid]) < 0) {
                end = mid - 1;
            } else if (prefix.compareTo(products[mid]) > 0) {
                start = mid + 1;
            } else {
                return mid;
            }
        }
        return firstPrefix;
    }
}

// Brief description
// Sort the input
// For every prefix
//      Use binary search to find start point
//      get up to 3 repetition of that item to result
//      continue