package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class E1134ArmstrongNumber {
    public boolean isArmstrong(int N) {
        if (N == 0) {
            return false;
        }
        List<Integer> nums = new ArrayList<>();
        int tmp = N;
        while (tmp > 0) {
            nums.add(tmp % 10);
            tmp /= 10;
        }
        int sum = 0;
        for (Integer n : nums) {
            sum += (int) Math.pow(n, nums.size());
        }
        return sum == N;
    }

    public boolean isArmstrongStrems(int N) {
        if (N == 0) {
            return false;
        }
        List<Integer> nums = new ArrayList<>();
        int tmp = N;
        while (tmp > 0) {
            nums.add(tmp % 10);
            tmp /= 10;
        }

        return nums.stream().map(i -> (int) Math.pow(i, nums.size())).reduce(Integer::sum).get() == N;
    }
}

// Questions:
// can there be negative armstrong numbers? probably yes -153 = -1 ^ 3 ...
// 0 is not an armstrong number
//
// I think it is pretty easy task - get every digit of N to list
// find out a sum of list(i) ^ list.size()
// return result