package com.oduzhyi.leetcode;

import java.util.HashSet;
import java.util.Set;

public class E0804UniqueMorseCode {
    public int uniqueMorseRepresentations(String[] words) {
        Set<String> mWords = new HashSet<>();
        String[] letters = new String[]{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};

        for (String word : words) {
            mWords.add(toMCode(word, letters));
        }
        return mWords.size();
    }

    private String toMCode(String word, String[] letters) {
        StringBuilder sb = new StringBuilder();
        for (char c : word.toCharArray()) {
            sb.append(letters[c - 'a']);
        }
        return sb.toString();
    }
}

// Questions:
// how many words can be provided as input?
// are empty words possible?
// is every word lower cased?
//
// this task can be done in O(n) time. First we need to convert our words to Morse code
// after conversion put to set and return set size