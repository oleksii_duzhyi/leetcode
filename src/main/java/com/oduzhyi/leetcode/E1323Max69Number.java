package com.oduzhyi.leetcode;

public class E1323Max69Number {
    public int maximum69Number(int num) {
        int pos = -1;
        int tmp = num;
        int order = 0;
        while (tmp > 0) {
            if (tmp % 10 == 6) {
                pos = order;
            }
            order++;
            tmp /= 10;
        }

        return pos == -1 ? num : num + 3 * (int) Math.pow(10, pos);
    }
}


