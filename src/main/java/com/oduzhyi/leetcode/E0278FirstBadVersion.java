package com.oduzhyi.leetcode;

public class E0278FirstBadVersion {
    public int firstBadVersion(int n) { // n = 2126753390, bad = 1702766716
        int s = 1;
        int e = n;
        int mid = e + (s - e) / 2;  //  1,860,909,216
        int minBad = Integer.MAX_VALUE;
        while (s <= e) {
            if (isBadVersion(mid)) { // F
                e = mid - 1;  // 1,703,064,236
                minBad = Math.min(minBad, mid);
            } else {
                s = mid + 1; // 1,698,910,423
            }
            mid = e + (s - e) / 2;  // (1,698,910,423 + 1,703,064,236) / 2 = 1,698,910,422
        }
        return minBad;
    }

    private boolean isBadVersion(int mid) {
        return false;
    }
}

// questions:
// is n valid? bigger than 0?
// is there always a solution? should we return -1 if there is no solutin?
//
// first idea - check every version from 1 to n - it is invalid return it- this will result in
// O(n) complexity in worst case - when there is no solution
// Alternatively - I can use binary search
// starting with the middle element - if it is a valid version - binary search right part
//                                  - if is invalid version - binary search left part 3390 766719
