package com.oduzhyi.leetcode;

public class M0162FindPeakElement {
    public int findPeakElement(int[] nums) {
        return binarySearch(nums, 0, nums.length - 1);
    }

    private int binarySearch(int[] nums, int from, int to) {
        if (from == to) {
            return from;
        }
        int mid = to + (from - to) / 2;
        int left = Math.max(0, mid - 1);
        int right = Math.min(nums.length - 1, mid + 1);

        if (nums[left] <= nums[mid] && nums[mid] >= nums[right]) {
            return mid;
        }

        if (nums[mid] < nums[right] || nums[left] < nums[mid]) {
            return binarySearch(nums, mid, to);
        } else {
            return binarySearch(nums, 0, mid - 1);
        }
    }
}