package com.oduzhyi.leetcode;

public class M0328OddEvenLinkedList {
    public ListNode oddEvenList(ListNode head) { // 1 -> 2 -> 3 -> 4 -> null
        if (head == null || head.next == null || head.next.next == null) {
            return head;
        }
        ListNode odd = head;
        ListNode lastOdd = odd;
        ListNode even = head.next;
        ListNode firstEven = even;
        while (odd != null) {
            if (odd.next != null) {
                odd.next = odd.next.next;
            }
            if (even != null && even.next != null) {
                even.next = even.next.next;
                even = even.next;
            }
            lastOdd = odd;
            odd = odd.next;
        }
        lastOdd.next = firstEven;

        return head;
    }
}