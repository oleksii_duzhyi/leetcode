package com.oduzhyi.leetcode;

public class M0074SearchA2DMatrix {
    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix.length == 0 || matrix[0].length == 0) {
            return false;
        }
        int r = matrix.length;
        int c = matrix[0].length;
        int f = 0;
        int t = r * c - 1;
        while (f <= t) {
            int mid = t + (f - t) / 2;
            int midV = matrix[mid / c][mid % c];
            if (midV == target) {
                return true;
            } else if (midV < target) {
                f = mid + 1;
            } else {
                t = mid - 1;
            }
        }
        return false;
    }
}