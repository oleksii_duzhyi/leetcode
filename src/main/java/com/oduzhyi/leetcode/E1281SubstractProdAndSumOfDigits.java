package com.oduzhyi.leetcode;

public class E1281SubstractProdAndSumOfDigits {
    public int subtractProductAndSum(int n) {
        if (n == 0) {
            return 0;
        }
        int prod = 1;
        int sum = 0;
        int div = 10;
        //123
        while (n > 0) {
            int rest = n % div;
            prod *= rest;
            sum += rest;
            n /= div;
        }
        return prod - sum;
    }
}

// Questions:
// can N be negative?
// how do we first/all digit in this case?
// result looks like can be negative  - 111 = 1 -3 = -2
//
// 2 possible solutions I can see right away:
// 1. We transform n to String
//      getting char at i and substructing from it '0' char to convert ASCII code to real number
//      simultaneously compute product and sum
//      return result
// 2. We are finding rest of division on 10
//      update product and sum
//      update n = n / 10
//      if left over == 0 - we break and return result