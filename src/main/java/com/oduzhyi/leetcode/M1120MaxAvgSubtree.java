package com.oduzhyi.leetcode;

public class M1120MaxAvgSubtree {
    private double res = Double.MIN_VALUE;

    public static void main(String[] args) {
        TreeNode tn = new TreeNode(-1);
        tn.left = new TreeNode(-2);
        tn.right = new TreeNode(-3);
        System.out.println(new M1120MaxAvgSubtree().maximumAverageSubtree(tn));
    }

    public double maximumAverageSubtree(TreeNode root) {
        dfs(root);

        return res;
    }

    private int[] dfs(TreeNode node) {
        if (node == null) {
            return new int[2];
        }

        int[] leftSumAndSize = dfs(node.left);
        int[] rightSumAndSize = dfs(node.right);
        int[] currSumAndSize = new int[]{leftSumAndSize[0] + rightSumAndSize[0] + node.val,
                leftSumAndSize[0] + rightSumAndSize[0] + 1};
        res = Math.max(res, currSumAndSize[0] / (double) currSumAndSize[1]);
        System.out.println(res);
        return currSumAndSize;
    }
}
