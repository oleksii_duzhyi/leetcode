package com.oduzhyi.leetcode;

public class E0706DesignHashMapAmazon {
    private Node[] holder = new Node[10000];

    /**
     * Initialize your data structure here.
     */
    public E0706DesignHashMapAmazon() {

    }

    /**
     * value will always be non-negative.
     */
    public void put(int key, int value) {
        int index = key % holder.length;
        Node newNode = new Node(key, value);
        Node current = holder[index];
        if (current == null) {
            holder[index] = new Node(key, value);
        } else {
            Node prev = current;
            while (current != null && current.key != key) {
                prev = current;
                current = current.next;
            }
            if (current != null) {
                swapNode(current, newNode);
            } else {
                prev.next = newNode;
                newNode.prev = prev;
            }
        }
    }

    private void swapNode(Node current, Node newNode) {
        if (current.prev == null) {
            if (current.next == null) {
                holder[current.key % holder.length] = newNode;
            } else {
                current.next.prev = newNode;
                if (newNode == null) {
                    holder[current.key % holder.length] = current.next;
                } else {
                    newNode.next = current.next;
                }
            }
        } else {
            Node prev = current.prev;
            Node next = current.next;

            if (newNode != null) {
                newNode.next = next;
                newNode.prev = prev;
                prev.next = newNode;
                if (next != null) {
                    next.prev = newNode;
                }
            } else {
                prev.next = next;
                if (next != null) {
                    next.prev = prev;
                }
            }
        }
        current = null;
    }

    /**
     * Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key
     */
    public int get(int key) {
        Node node = getNode(key);
        return node != null ? node.value : -1;
    }

    private Node getNode(int key) {
        Node val = holder[key % holder.length];
        while (val != null && val.key != key) {
            val = val.next;
        }
        return val;
    }

    /**
     * Removes the mapping of the specified value key if this map contains a mapping for the key
     */
    public void remove(int key) {
        Node current = getNode(key);
        if (current != null) {
            swapNode(current, null);
        }
    }

    class Node {
        Node prev;
        Node next;
        int key;
        int value;

        Node(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }
}

// really straight forward algorithm, but my implementation is so ugly)
// need to be improved
