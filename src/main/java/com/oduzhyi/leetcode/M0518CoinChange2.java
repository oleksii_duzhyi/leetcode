package com.oduzhyi.leetcode;

public class M0518CoinChange2 {
    public int change(int amount, int[] coins) {
        int[] dp = new int[amount + 1];
        dp[0] = 1;

        for (int coin : coins) {
            for (int i = coin; i <= amount; i++) {
                dp[i] += dp[i - coin];
            }
        }
        return dp[amount];
    }
}
// dp problem
// if amount == 0 => answer is 1
// for each coin calculate the number to  split  as
// dp[x] += dp[x - coin]
// for example coins 2,5; amount is 7
// start: 1 0 0 0 0 0 0 0
// coin2: 1 0 1 0 1 0 1 0
// coin5: 1 0 1 0 1 1 1 1