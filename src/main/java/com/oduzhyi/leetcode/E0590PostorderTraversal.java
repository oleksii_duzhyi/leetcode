package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class E0590PostorderTraversal {

    public List<Integer> postorder(Node root) {
        LinkedList<Integer> res = new LinkedList<>();
        if (root == null) {
            return res;
        }

        LinkedList<Node> stack = new LinkedList<>();
        stack.push(root);

        while (!stack.isEmpty()) {
            Node next = stack.pop();
            res.addFirst(next.val);

            if (next.children != null) {
                for (int i = 0; i < next.children.size(); i++) {
                    stack.push(next.children.get(i));
                }
            }
        }

        return res;
    }

    public List<Integer> postorderRec(Node root) {
        List<Integer> res = new ArrayList<>();
        postorderRec(root, res);
        return res;
    }

    public void postorderRec(Node root, List<Integer> res) {
        if (root != null) {
            for (Node n : root.children) {
                postorderRec(n, res);
            }
            res.add(root.val);
        }
    }
}