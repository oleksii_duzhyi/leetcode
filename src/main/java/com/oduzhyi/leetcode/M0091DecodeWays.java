package com.oduzhyi.leetcode;

public class M0091DecodeWays {

    public int numDecodings(String s) {
        if (s.isEmpty()) {
            return 1;
        }
        return helper(s, 0, 1) + helper(s, 0, 2);
    }

    private int helper(String s, int prev, int start) { // 0, 2
        if (start > s.length() || s.charAt(prev) == '0') {
            return 0;
        }

        int number = 0;
// char - '0'
        for (int i = prev; i < start; i++) {
            number = number * 10 + (s.charAt(i) - '0');
        }
        if (number > 26) {
            return 0;
        }
        if (start == s.length()) {
            return 1;
        }
        return helper(s, start, start + 1) + helper(s, start, start + 2);

    }

}
