package com.oduzhyi.leetcode;

public class E1309DecryptString {
    public String freqAlphabets(String s) {
        if (s == null || s.isEmpty()) {
            return s;
        }
        StringBuilder sb = new StringBuilder();
        char[] chars = s.toCharArray();
        for (int i = 0; i < s.length(); ) {
            if (i + 2 < s.length() && chars[i + 2] == '#') {
                if (chars[i] == '1') {
                    sb.append((char) (chars[i] + chars[i + 1] + 9));
                } else {
                    sb.append((char) (chars[i] + chars[i + 1] + 18));
                }
                i += 3;
            } else {
                sb.append((char) (chars[i++] + 48));
            }
        }
        return sb.toString();
    }
}