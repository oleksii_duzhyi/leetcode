package com.oduzhyi.leetcode;

public class M0701InsertIntoBST {
    public TreeNode insertIntoBSTrec(TreeNode root, int val) {
        if (root == null) {
            return new TreeNode(val);
        }
        if (val > root.val) {
            root.right = insertIntoBSTrec(root.right, val);
        } else {
            root.left = insertIntoBSTrec(root.left, val);
        }
        return root;
    }

    public TreeNode insertIntoBST(TreeNode root, int val) {
        if (root == null) {
            return new TreeNode(val);
        }
        TreeNode prev = null;
        TreeNode cur = root;
        while (cur != null) {
            prev = cur;
            if (val > cur.val) {
                cur = cur.right;
            } else {
                cur = cur.left;
            }
        }
        if (val > prev.val) {
            prev.right = new TreeNode(val);
        } else {
            prev.left = new TreeNode(val);
        }
        return root;
    }
}

// it is always possible to insert to BST somewhere in the end
// explore BST find position and add new node