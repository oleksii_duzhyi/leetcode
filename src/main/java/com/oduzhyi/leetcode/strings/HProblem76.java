package com.oduzhyi.leetcode.strings;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HProblem76 {

    public static void main(String[] args) {
        System.out.println(minWindow("aa", "aa"));
    }

    public static String minWindow(String s, String t) {
        if (s.length() == 0 || t.length() == 0 || t.length() > s.length()) {
            return "";
        }

        String minSeen = null;
        Map<Character, Integer> seen = new HashMap<>();
        int left = 0;
        for (int right = 0; right < s.length(); right++) {
            seen.put(s.charAt(right), right);
            if (right - left + 1 >= t.length()) {
                if (subFound(seen.keySet(), t)) {
                    while (subFound(seen.keySet(), t)) {
                        if (left == seen.get(s.charAt(left))) {
                            seen.remove(s.charAt(left));
                        }
                        left++;
                    }
                    if (minSeen == null || minSeen.length() > right - left + 2) {
                        minSeen = s.substring(left - 1, right + 1);
                    }
                }
            }
        }

        return minSeen == null ? "" : minSeen;
    }

    public static boolean subFound(Set<Character> seen, String t) {
        for (int i = 0; i < t.length(); i++) {
            if (!seen.contains(t.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
