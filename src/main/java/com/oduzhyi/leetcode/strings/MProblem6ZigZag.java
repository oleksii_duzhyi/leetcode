package com.oduzhyi.leetcode.strings;

/**
 * Created by Нелла on 16.11.2019.
 */
public class MProblem6ZigZag {

    public String convert(String input, int numRows) {
        if (numRows == 1) {
            return input;
        }
        StringBuilder[] rBld = new StringBuilder[numRows];
        for (int i = 0; i < rBld.length; i++) {
            rBld[i] = new StringBuilder();
        }

        int curRN = 1;
        boolean increment = true;
        for (int i = 0; i < input.length(); i++) {
            rBld[(curRN - 1)].append(input.charAt(i));
            if (curRN == numRows) {
                increment = false;
            }
            if (curRN == 1) {
                increment = true;
            }
            if (increment) {
                curRN++;
            } else {
                curRN--;
            }
        }
        StringBuilder res = new StringBuilder();
        for (StringBuilder aRBld : rBld) {
            res.append(aRBld);
        }
        return res.toString();
    }

    public String convertOneSB(String input, int numRows) {
        if (numRows == 1) {
            return input;
        }
        int maxDist = numRows * 2 - 2;
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < numRows; i++) {
            int nextDist = (numRows - i - 1) * 2;
            int[] steps = new int[]{nextDist, maxDist - nextDist};
            int j = i;
            res.append(input.charAt(i));
            for (int k = 0; j + steps[k % 2] < input.length(); k++) {
                if (steps[k % 2] != 0) {
                    j += steps[k % 2];
                    res.append(input.charAt(j));
                }
            }
        }
        return res.toString();
    }

}
