package com.oduzhyi.leetcode.array;

/**
 * Created by Нелла on 24.12.2019.
 */
public class MProblem289GameOfLife {
    public void gameOfLife(int[][] board) {
        int rows = board.length;
        int cols = board[0].length;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                int neighboors = countNeighboors(board, i, j);

                if (board[i][j] == 1) {
                    if (neighboors < 2 || neighboors > 3) {
                        board[i][j] = -1;
                    }
                } else {
                    if (neighboors == 3) {
                        board[i][j] = 2;
                    }
                }
            }

        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (board[i][j] > 0) {
                    board[i][j] = 1;
                } else {
                    board[i][j] = 0;
                }
            }
        }
    }

    private int countNeighboors(int[][] board, int i, int j) {
        int n = 0;
        for (int k = Math.max(0, i - 1); k <= Math.min(i, board.length - 1); k++) {
            for (int l = Math.max(0, j - 1); l <= Math.min(j, board[0].length - 1); l++) {
                if (k != i && l != j && Math.abs(board[k][l]) == 1) {
                    n += 1;
                }
            }
        }
        return n;
    }
}
