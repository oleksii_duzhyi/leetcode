package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class H0340LongestSubstringWithAtMostKChars {
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        if (k >= s.length()) {
            return s.length();
        }

        LinkedHashMap<Character, Integer> charPos = new LinkedHashMap<>();
        int max = k;
        int left = 0;
        for (int right = 0; right < s.length(); right++) {
            char next = s.charAt(right);
            charPos.remove(next);
            charPos.put(next, right);
            if (charPos.size() > k) {
                left = charPos.remove(charPos.keySet().iterator().next()) + 1;
            }
            max = Math.max(max, right - left + 1);
        }
        return max;
    }


    public int lengthOfLongestSubstringKDistinctHashMap(String s, int k) {
        if (k >= s.length()) {
            return s.length();
        }

        Map<Character, Integer> freqMap = new HashMap<>();
        int max = k;
        int left = 0;
        for (int right = 0; right < s.length(); right++) {
            freqMap.put(s.charAt(right), freqMap.getOrDefault(s.charAt(right), 0) + 1);
            while (freqMap.size() > k) {
                decrementOrRemove(freqMap, s.charAt(left++));
            }
            max = Math.max(max, right - left + 1);
        }
        return max;
    }

    private void decrementOrRemove(Map<Character, Integer> freqMap, char c) {
        Integer val = freqMap.get(c);
        if (val == 1) {
            freqMap.remove(c);
        } else {
            freqMap.put(c, val - 1);
        }
    }
}
// 2 pointers + HashMap
// start moving right and keep frequencies in the map
// in map size > k - start decrement frequencies and move left to right
// update max
