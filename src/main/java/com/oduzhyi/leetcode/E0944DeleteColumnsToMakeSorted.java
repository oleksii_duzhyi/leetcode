package com.oduzhyi.leetcode;

/**
 * Created by Нелла on 21.07.2019.
 */
public class E0944DeleteColumnsToMakeSorted {
    public static void main(String[] args) {
        Solution s = new Solution();
        s.minDeletionSize(new String[]{"rrjk", "furt", "guzm"});
    }
}

class Solution {
    public int minDeletionSize(String[] w) {
        if (w.length == 1) {
            return 0;
        }

        int wordLength = w[0].length();
        int res = 0;

        for (int i = 0; i < wordLength; i++) {
            for (int j = 0; j < w.length - 1; j++) {
                if (w[j].charAt(i) > w[j + 1].charAt(i)) {
                    res++;
                    break;
                }
            }
        }
        return res;
    }

    public int minDeletionSizeOld(String[] A) {
        int res = 0;
        for (int i = 0; i < A[0].length(); i++) {
            boolean sorted = true;
            for (int j = 0; j < A.length - 1 && sorted; j++) {
                sorted &= A[j].charAt(i) <= A[j + 1].charAt(i);
            }
            if (sorted) {
                res++;
            }
        }
        return A.length - res;
    }
}