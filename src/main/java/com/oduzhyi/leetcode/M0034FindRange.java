package com.oduzhyi.leetcode;

public class M0034FindRange {
    public int[] searchRange(int[] nums, int target) {
        int left = binarySearch(nums, target, 0, nums.length - 1, true);
        if (left == -1) {
            return new int[]{-1, -1};
        } else {
            return new int[]{left, binarySearch(nums, target, left, nums.length - 1, false)};
        }
    }

    private int binarySearch(int[] nums, int target, int from, int to, boolean first) {
        if (from > to) {
            return -1;
        }

        int mid = to + (from - to) / 2;
        if (first) {
            if (nums[mid] == target && (mid == 0 || nums[mid - 1] < nums[mid])) {
                return mid;
            }
        } else {
            if (nums[mid] == target && (mid == nums.length - 1 || nums[mid] < nums[mid + 1])) {
                return mid;
            }
        }
        int left = Math.max(0, mid - 1);
        int right = Math.min(nums.length - 1, mid + 1);

        if (first) {
            if (nums[mid] >= target) {
                return binarySearch(nums, target, from, mid - 1, first);
            } else {
                return binarySearch(nums, target, mid + 1, to, first);
            }
        } else {
            if (nums[mid] > target) {
                return binarySearch(nums, target, from, mid - 1, first);
            } else {
                return binarySearch(nums, target, mid + 1, to, first);
            }
        }
    }
}