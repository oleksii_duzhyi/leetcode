package com.oduzhyi.leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Нелла on 14.11.2018.
 */
public class Problem804 {
    private final String[] morseCodeLetters = new String[]{
            ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..",
            "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};

    public static void main(String[] args) {
        System.out.println(new Problem804().uniqueMorseRepresentations(new String[]{"gin", "zen", "gig", "msg"}));
    }

    public int uniqueMorseRepresentations(String[] words) {
        Set<String> set = new HashSet<>();

        for (String word : words) {
            StringBuilder sb = new StringBuilder();
            for (char c : word.toCharArray()) {
                sb.append(morseCodeLetters[c - 'a']);
            }
            set.add(sb.toString());
        }
        return set.size();
    }
}
