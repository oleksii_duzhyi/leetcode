package com.oduzhyi.leetcode;

public class M0215KLargestElementInArray {

    public int findKthLargest(int[] nums, int k) {
        int from = 0;
        int to = nums.length - 1;

        while (from <= to) {
            int idx = partition(nums, from, to);
            if (idx == nums.length - k) {
                return nums[idx];
            } else if (idx < nums.length - k) {
                from = idx + 1;
            } else {
                to = idx - 1;
            }
        }
        return -1;
    }

    private int partition(int[] nums, int from, int to) {
        int partL = from;         // f = 0, t = 5
        // [3,2,1,6,4,5]

        from++;
        while (from <= to) {
            if (nums[from] <= nums[partL]) {
                from++;
            } else {
                swap(nums, from, to);
                to--;
            }
        }
        swap(nums, partL, to);
        return to;
    }

    private void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
}

// quick select algorithm


