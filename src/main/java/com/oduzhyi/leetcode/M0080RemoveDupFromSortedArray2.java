package com.oduzhyi.leetcode;

import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

public class M0080RemoveDupFromSortedArray2 {

    public static void main(String[] args) {
        System.out.println("aabb".compareTo("aaba"));
        new HashMap<Character, Set<Character>>().getOrDefault('c', Collections.emptySet());
    }

    public int removeDuplicates(int[] nums) {
        int i = 0;
        int j = 0;

        while (i < nums.length) {
            int next = nums[i];
            int cnt = 1;
            while (i < nums.length - 1 && nums[i + 1] == next) {
                i++;
                cnt++;
            }
            int numsToInsert = Math.min(2, cnt);
            while (numsToInsert > 0) {
                nums[j++] = next;
                numsToInsert--;
            }
            i++;
        }
        return j;
    }
}

// initiate 2 pointers  - one to keep the index  where it is needed to insert the value
//  second for counting dups
// while (nums[i] == nums[i+1]) ->increment cnt and increment i
// for (Math.min(2, cnt)) -> insert nums[i] @ j++
// return j
