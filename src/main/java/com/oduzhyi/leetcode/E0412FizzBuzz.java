package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class E0412FizzBuzz {
    public List<String> fizzBuzz(int n) {
        String[] cache = new String[]{"FizzBuzz", "Fizz", "Buzz"};
        List<String> res = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                res.add(cache[0]);
            } else if (i % 3 == 0) {
                res.add(cache[1]);
            } else if (i % 5 == 0) {
                res.add(cache[2]);
            } else {
                res.add(String.valueOf(i));
            }
        }
        return res;
    }
}