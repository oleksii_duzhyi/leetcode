package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class H0126WordLadder2 {
    String endWord;

    public static void main(String[] args) {
        System.out.println(new H0126WordLadder2().findLadders("hit"
                , "cog"
                , new String[]{"hot", "dot", "dog", "lot", "log", "cog"}));
        System.out.println(new H0126WordLadder2().findLaddersWorking("hit"
                , "cog"
                , new String[]{"hot", "dot", "dog", "lot", "log", "cog"}));
    }

    public List<List<String>> findLadders(String beginWord, String endWord, String[] wordList) {
        List<List<String>> result = new ArrayList<>();
        Map<String, List<String>> graph = new HashMap<>();
        Map<String, Set<String>> nextStates = new HashMap<>();
        boolean hasEndWord = false;
        for (String word : wordList) {
            if (word.equals(endWord)) {
                hasEndWord = true;
            }
            for (int i = 0; i < word.length(); i++) {
                String pattern = word.substring(0, i) + "*" + word.substring(i + 1);
                nextStates.computeIfAbsent(pattern, k -> new HashSet<>()).add(word);
            }
        }

        if (!hasEndWord) {
            return result;
        }

        buildGraph(graph, beginWord, endWord, nextStates);
        dfs(beginWord, endWord, graph, new ArrayList<>(), result);
        return result;
    }

    private void dfs(String beginWord, String endWord, Map<String, List<String>> graph, List<String> path, List<List<String>> result) {
        path.add(beginWord);
        if (beginWord.equals(endWord)) {
            result.add(new ArrayList<>(path));
        }

        for (String next : graph.getOrDefault(beginWord, Collections.emptyList())) {
            dfs(next, endWord, graph, path, result);
        }
        path.remove(path.size() - 1);
    }

    private void buildGraph(Map<String, List<String>> graph, String startWord, String endWord, Map<String, Set<String>> nextStates) {
        Set<String> currLevel = new HashSet<>();
        currLevel.add(startWord);
        Set<String> visited = new HashSet<>();

        boolean found = false;
        while (!found && !currLevel.isEmpty()) {
            visited.addAll(currLevel);
            Set<String> nextLevel = new HashSet<>();

            for (String word : currLevel) {
                Set<String> nextMoves = getNextLevel(word, nextStates);
                if (nextMoves.contains(endWord)) {
                    found = true;
                }
                for (String next : nextMoves) {
                    if (!visited.contains(next)) {
                        graph.computeIfAbsent(word, k -> new ArrayList<>()).add(next);
                        nextLevel.add(next);
                    }
                }
            }
            currLevel = nextLevel;
        }
    }

    private Set<String> getNextLevel(String word, Map<String, Set<String>> nextStates) {
        Set<String> result = new HashSet<>();


        for (int i = 0; i < word.length(); i++) {
            result.addAll(nextStates.getOrDefault(word.substring(0, i) + "*" + word.substring(i + 1), Collections.emptySet()));
        }
        result.remove(word);
        return result;
    }

    public List<List<String>> findLaddersWorking(String beginWord, String endWord, String[] wordList) {
        List<String> path = new ArrayList<>();
        List<List<String>> result = new ArrayList<>();
        HashMap<String, List<String>> graph = new HashMap<>();
        Map<String, Set<String>> nextStates = new HashMap<>();
        for (String word : wordList) {
            for (int i = 0; i < word.length(); i++) {
                String pattern = word.substring(0, i) + "*" + word.substring(i + 1);
                nextStates.computeIfAbsent(pattern, k -> new HashSet<>()).add(word);
            }
        }
        buildGraph(beginWord, endWord, graph, nextStates);
        dfs(beginWord, endWord, graph, path, result);
        return result;
    }

    private void buildGraph(String beginWord, String endWord, HashMap<String, List<String>> graph, Map<String, Set<String>> nextStates) {
        HashSet<String> visited = new HashSet<>();
        Deque<String> queue = new ArrayDeque<>();
        queue.offer(beginWord);
        visited.add(beginWord);
        boolean foundEnd = false;

        while (!queue.isEmpty()) {
            int count = queue.size();

            for (int i = 0; i < count; i++) {
                String word = queue.poll();
                List<String> children = getNextLevelWorking(word, nextStates);
                for (String child : children) {
                    if (child.equals(endWord)) {
                        foundEnd = true;
                    }
                    if (!visited.contains(child)) {
                        visited.add(child);
                        if (!graph.containsKey(word)) {
                            graph.put(word, new ArrayList<String>());
                        }
                        graph.get(word).add(child);
                        queue.offer(child);
                    }
                }
            }

            if (foundEnd) {
                break;
            }
        }
    }

    private List<String> getNextLevelWorking(String word, Map<String, Set<String>> nextStates) {
        List<String> result = new ArrayList<>();


        for (int i = 0; i < word.length(); i++) {
            result.addAll(nextStates.getOrDefault(word.substring(0, i) + "*" + word.substring(i + 1), Collections.emptySet()));
        }

        return result;
    }

    private void dfs(String curWord, String endWord, HashMap<String, List<String>> graph, List<String> path, List<List<String>> result) {
        path.add(curWord);

        if (curWord.equals(endWord)) {
            result.add(new ArrayList<String>(path));
        } else if (graph.containsKey(curWord)) {
            for (String nextWord : graph.get(curWord)) {
                dfs(nextWord, endWord, graph, path, result);
            }
        }

        path.remove(path.size() - 1);
    }
}
