package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class H0188BestTimeToBuyStocksIV {
    public int maxProfitFailingBigTests(int k, int[] prices) {
        if (k == 0 || prices.length < 2) {
            return 0;
        }
        Map<String, Integer> cache = new HashMap<>();
        return maxProfit(k, 0, prices.length, prices, cache);
    }

    public int maxProfit(int k, int from, int to, int[] prices, Map<String, Integer> cache) {
        if (to - from < 2) {
            return 0;
        }
        if (k == 1) {
            return maxOneProfit(from, to, prices, cache);
        }
        int maxProfit = 0;
        for (int i = from + 2; i <= to; i++) { //
            maxProfit = Math.max(maxProfit(1, from, i, prices, cache) // 0-1
                            + maxProfit(k - 1, i, to, prices, cache),
                    maxProfit);
        }
        return maxProfit;
    }

    public int maxOneProfit(int from, int to, int[] prices, Map<String, Integer> cache) {
        String key = from + "" + to;
        if (cache.containsKey(key)) {
            return cache.get(key);
        }
        int profit = 0;
        int buy = prices[from];

        for (int i = from + 1; i < to; i++) {
            if (buy > prices[i]) {
                buy = prices[i];
            } else {
                profit = Math.max(profit, prices[i] - buy);
            }
        }

        return profit;
    }
}