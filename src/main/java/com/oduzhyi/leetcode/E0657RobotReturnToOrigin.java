package com.oduzhyi.leetcode;

/**
 * Created by Нелла on 14.11.2018.
 */
public class E0657RobotReturnToOrigin {
    public boolean judgeCircleOld(String moves) {
        int sameRow = 0;
        int sameCol = 0;
        for (char c : moves.toCharArray()) {
            if (c == 'U') {
                sameRow += 1;

            } else if (c == 'D') {
                sameRow -= 1;

            } else if (c == 'L') {
                sameCol -= 1;

            } else if (c == 'R') {
                sameCol += 1;

            }
        }
        return sameRow == 0 && sameCol == 0;
    }

    public boolean judgeCircle(String moves) {
        if (moves.length() % 2 == 1) {
            return false;
        }
        int vert = 0;
        int hor = 0;
        for (char c : moves.toCharArray()) {
            if (c == 'U') {
                vert++;
            } else if (c == 'D') {
                vert--;
            } else if (c == 'R') {
                hor++;
            } else {
                hor--;
            }
        }
        return vert == 0 && hor == 0;
    }
}

// questions:
// is the string is valid - consist of UDLR chars only?
// can the input be empty? if robot doesn't move does it mean it finished in the same ocation?

// First of all - to return back to initial position - every robot action should have counter action
// and the actions are possible only in 2 pairs of direction - UD and LR.
// so we can count the amount of U and subsctract with amount of D
// smae for L and R
// also it requires event number of moves to finish in the same stop