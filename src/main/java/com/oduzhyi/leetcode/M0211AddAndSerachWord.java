package com.oduzhyi.leetcode;

public class M0211AddAndSerachWord {
}

class WordDictionary {
    private Trie root = new Trie();

    /**
     * Initialize your data structure here.
     */
    public WordDictionary() {

    }

    /**
     * Adds a word into the data structure.
     */
    public void addWord(String word) {
        Trie last = root;
        for (char c : word.toCharArray()) {
            if (last.inner[c - 'a'] == null) {
                last.inner[c - 'a'] = new Trie();
            }
            last = last.inner[c - 'a'];
        }
        last.end = true;
    }

    /**
     * Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter.
     */
    public boolean search(String word) {
        return search(word, 0, root);
    }

    private boolean search(String word, int pos, Trie trie) {
        if (trie == null) {
            return false;
        }
        if (pos + 1 > word.length()) {
            return trie.end;
        }
        if (word.charAt(pos) == '.') {
            for (Trie inner : trie.inner) {
                if (search(word, pos + 1, inner)) {
                    return true;
                }
            }
            return false;
        } else {
            return search(word, pos + 1, trie.inner[word.charAt(pos) - 'a']);
        }
    }

    private static class Trie {
        Trie[] inner = new Trie[26];
        boolean end = false;
    }
}

//using trie