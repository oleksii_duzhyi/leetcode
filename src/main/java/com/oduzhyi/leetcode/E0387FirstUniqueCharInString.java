package com.oduzhyi.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class E0387FirstUniqueCharInString {

    public int firstUniqCharAny(String s) {
        Map<Character, Integer> counts = new HashMap(64);
        for (char c : s.toCharArray()) {
            Integer prev = counts.putIfAbsent(c, 1);
            if (prev != null) {
                counts.put(c, prev + 1);
            }
        }

        for (int i = 0; i < s.length(); i++) {
            if (counts.get(s.charAt(i)) == 1) {
                return i;
            }
        }
        return -1;
    }

    public int firstUniqChar(String s) {
        int[] counts = new int[26];
        Arrays.fill(counts, -2);
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (counts[chars[i] - 'a'] == -2) {
                counts[chars[i] - 'a'] = i;
            } else {
                counts[chars[i] - 'a'] = -1;
            }
        }
        int res = -1;
        for (int i : counts) {
            if (i >= 0) {
                if (res == -1) {
                    res = i;
                } else {
                    res = Math.min(res, i);
                }
            }
        }
        return res;
    }
}

//questions:
// can the string be empty?
// what are the chars in the string?
//
