package com.oduzhyi.leetcode;

public class M0567PermutationInString {
    public boolean checkInclusion(String p, String s) {
        if (p.length() > s.length()) {
            return false;
        }
        int[] anCnt = new int[26];
        int[] sCnt = new int[26];
        for (char c : p.toCharArray()) {
            anCnt[c - 'a']++;
        }

        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            sCnt[chars[i] - 'a']++;
            if (i - p.length() >= 0) {
                sCnt[chars[i - p.length()] - 'a']--;
            }
            if (i + 1 >= p.length()) {
                if (isAnagram(sCnt, anCnt)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isAnagram(int[] sCnt, int[] anCnt) {
        int i = 0;
        while (i < sCnt.length) {
            if (sCnt[i] != anCnt[i]) {
                return false;
            }
            i++;
        }
        return true;
    }
}
