package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class H0759EmployeesFreeTime {
    public List<Interval> employeeFreeTime(List<List<Interval>> schedule) {
        List<Interval> res = new ArrayList<>();

        PriorityQueue<IntervalWithList> pq = new PriorityQueue<>();
        for (List<Interval> list : schedule) {
            pq.add(new IntervalWithList(list.get(0), list));
        }

        Interval last = null;
        while (!pq.isEmpty()) {
            IntervalWithList curr = pq.poll();
            if (last == null) {
                last = curr.interval;
            } else {
                if (last.end >= curr.interval.start) {
                    last.end = Math.max(last.end, curr.interval.end);
                } else {
                    res.add(new Interval(last.end, curr.interval.start));
                    last = curr.interval;
                }
            }
            curr.list.remove(0);
            if (!curr.list.isEmpty()) {
                pq.add(new IntervalWithList(curr.list.get(0), curr.list));
            }
        }


        return res;

    }

}

class IntervalWithList implements Comparable<IntervalWithList> {
    Interval interval;
    List<Interval> list;

    public IntervalWithList(Interval interval, List<Interval> list) {
        this.interval = interval;
        this.list = list;
    }

    public int compareTo(IntervalWithList o) {
        return Integer.compare(interval.start, o.interval.start);
    }

    public String toString() {
        return interval.start + "-" + interval.end;
    }
}
//Merge sort with a  tweak for intervals14
