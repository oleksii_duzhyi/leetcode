package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class M0220CountDuplicates3 {
    public static void main(String[] args) {
        System.out.println(getID(-3, 5));
        System.out.println(new M0220CountDuplicates3().containsNearbyAlmostDuplicate(new int[]{4, 6, 1, 2, 3, 1}, 3, 0));
    }

    private static long getID(long x, long w) {
        return x < 0 ? (x + 1) / w - 1 : x / w;
    }

    public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        if (nums.length <= 1) {
            return false;
        }

        boolean idxBased = true;
        if (k > t) {
            idxBased = false;
        }
        Map<Integer, Integer> map = new HashMap<>();
        if (!idxBased) {
            map.put(nums[0], 0);
        }
        for (int i = 1; i < nums.length; i++) {

            int f = idxBased ? Math.max(0, i - k) : nums[i] - t;
            int to = idxBased ? Math.min(nums.length - 1, i + k) : nums[i] + t;
            for (int j = f; j <= to; j++) {
                if ((j != i && idxBased && Math.abs((long) nums[i] - nums[j]) <= t)
                        || (!idxBased && map.containsKey(j) && i - map.get(j) <= k)) {
                    return true;
                }
            }
            if (!idxBased) {
                map.put(nums[0], 0);
            }
        }
        return false;
    }
}
