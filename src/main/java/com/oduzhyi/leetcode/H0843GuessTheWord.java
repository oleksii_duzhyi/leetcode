package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class H0843GuessTheWord {
    public void findSecretWord(String[] wordList, Master master) {
        Map<String, Integer> guessResults = new HashMap<>();
        int guess = master.guess(wordList[0]);
        guessResults.put(wordList[0], guess);
        while (guess != 6) {
            String nextGuess = selectNextGuess(wordList, guessResults);

            guess = master.guess(nextGuess);
            guessResults.put(nextGuess, guess);
        }
    }

    private String selectNextGuess(String[] words, Map<String, Integer> guessResults) {
        String nextGuess = null;
        for (String word : words) {
            if (!guessResults.containsKey(word) && matches(word, guessResults)) {
                return word;
            }
        }
        return nextGuess;
    }

    private boolean matches(String possible, Map<String, Integer> guessResults) {
        for (Map.Entry<String, Integer> entry : guessResults.entrySet()) {
            int m = 0;
            for (int i = 0; i < possible.length(); i++) {
                if (possible.charAt(i) == entry.getKey().charAt(i)) {
                    m++;
                }
            }
            if (m != entry.getValue()) {
                return false;
            }
        }
        return true;
    }

    private static class Master {
        public int guess(String word) {
            return 0;
        }
    }
}

//reduce search space. Start with quessing the first word
//remember the number of matched characters
// if it is not the answer - filter potential guesses by finding EXACT match
// try new match as new guess
