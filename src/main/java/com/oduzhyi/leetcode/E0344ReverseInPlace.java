package com.oduzhyi.leetcode;

/**
 * Created by Нелла on 16.11.2019.
 */
public class E0344ReverseInPlace {

    public void reverseString(char[] s) {  //abc
        for (int i = 0; i < s.length / 2; i++) {
            char tmp = s[i];
            s[i] = s[s.length - 1 - i];
            s[s.length - 1 - i] = tmp;
        }
    }

    public void reverseStringOld(char[] c) {
        int i = 0, j = c.length - 1;
        while (i < j) {
            char tmp = c[i];
            c[i++] = c[j];
            c[j--] = tmp;
        }
    }
}
