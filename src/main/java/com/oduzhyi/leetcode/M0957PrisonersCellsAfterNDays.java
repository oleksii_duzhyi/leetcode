package com.oduzhyi.leetcode;

import java.util.Arrays;
import java.util.HashMap;

public class M0957PrisonersCellsAfterNDays {
    public static void main(String[] args) {
        System.out.println(Arrays.toString("/a/b/c".split("/")));
    }

    protected int cellsToBitmap(int[] cells) {
        int stateBitmap = 0x0;
        for (int cell : cells) {
            stateBitmap <<= 1;
            stateBitmap = (stateBitmap | cell);
        }
        return stateBitmap;
    }

    protected int[] nextDay(int[] cells) {
        int[] newCells = new int[cells.length];
        newCells[0] = 0;
        for (int i = 1; i < cells.length - 1; i++) {
            newCells[i] = (cells[i - 1] == cells[i + 1]) ? 1 : 0;
        }
        newCells[cells.length - 1] = 0;
        return newCells;
    }

    public int[] prisonAfterNDays(int[] cells, int N) {

        HashMap<Integer, Integer> seen = new HashMap<>();
        boolean isFastForwarded = false;

        // step 1). run the simulation with hashmap
        while (N > 0) {
            if (!isFastForwarded) {
                int stateBitmap = this.cellsToBitmap(cells);
                if (seen.containsKey(stateBitmap)) {
                    // the length of the cycle is seen[state_key] - N
                    N %= seen.get(stateBitmap) - N;
                    isFastForwarded = true;
                } else {
                    seen.put(stateBitmap, N);
                }
            }
            // check if there is still some steps remained,
            // with or without the fast-forwarding.
            if (N > 0) {
                N -= 1;
                cells = this.nextDay(cells);
            }
        }
        return cells;
    }
}

// idea of fast forwarding. because there just 6 cells that are mutating -
// there are only 2^6 number of possible states, thus we can find the difference between 2 states
// and fasf forward by finding the reminder of division
// also it is easily  transformable into integer by shifting bits
