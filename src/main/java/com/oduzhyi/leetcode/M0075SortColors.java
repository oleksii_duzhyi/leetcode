package com.oduzhyi.leetcode;

public class M0075SortColors {
    public void sortColors(int[] nums) {
        int r = 0;
        int b = nums.length - 1;
        int i = 0;
        while (i <= b) {
            if (nums[i] == 0) {
                nums[i] = nums[r];
                nums[r++] = 0;
                i++;
            } else if (nums[i] == 2) {
                nums[i] = nums[b];
                nums[b--] = 2;
            } else {
                i++;
            }
        }
    }

    public void sortColors2N(int[] nums) {
        int r = 0;
        int w = 0;
        int b = 0;
        for (int n : nums) {
            switch (n) {
                case 0:
                    r++;
                    break;
                case 1:
                    w++;
                    break;
                case 2:
                    b++;
                    break;
            }
        }
        int i = 0;
        while (r-- > 0) {
            nums[i++] = 0;
        }

        while (w-- > 0) {
            nums[i++] = 1;
        }

        while (b-- > 0) {
            nums[i++] = 2;
        }
    }
}