package com.oduzhyi.leetcode;

public class M1123LCADeepestLeaves {

    public static void main(String[] args) {
        TreeNode tn = new TreeNode(1, new TreeNode(2), new TreeNode(3));
        tn.left.left = new TreeNode(4);
        tn.right.right = new TreeNode(5);
        new M1123LCADeepestLeaves().lcaDeepestLeaves(tn);
    }

    public TreeNode lcaDeepestLeaves(TreeNode root) {
        Pair p = getLca(root, 0);
        return p.node;
    }

    private Pair getLca(TreeNode root, int d) {
        if (root == null) {
            return new Pair(null, d);
        }
        Pair l = getLca(root.left, d + 1);
        Pair r = getLca(root.right, d + 1);
        if (l.d == r.d) {
            return new Pair(root, l.d);
        } else {
            return l.d > r.d ? l : r;
        }
    }

    class Pair {
        TreeNode node;
        int d;

        Pair(TreeNode node, int d) {
            this.node = node;
            this.d = d;
        }

        @Override
        public String toString() {
            return node + "";
        }
    }
}
