package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class M0022GenerateParentheses {
    private List<String> res = new ArrayList<>();

    public List<String> generateParenthesis(int n) {
        gp(n, n, "");
        return res;
    }

    private void gp(int open, int closed, String acc) {
        if (open == closed && open == 0) {
            res.add(acc);
        } else {
            if (open > 0) {
                gp(open - 1, closed, acc + "(");
            }
            if (closed > open) {
                gp(open, closed - 1, acc + ")");
            }
        }
    }
}