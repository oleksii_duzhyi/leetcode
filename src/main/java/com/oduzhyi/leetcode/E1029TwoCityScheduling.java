package com.oduzhyi.leetcode;

import java.util.Arrays;
import java.util.Comparator;

public class E1029TwoCityScheduling {
    public int twoCitySchedCost(int[][] costs) {
        Arrays.sort(costs, Comparator.comparingInt(i -> Math.abs(i[1] - i[0])));
        int a = costs.length / 2;
        int b = costs.length / 2;
        int min = 0;
        for (int i = costs.length - 1; i >= 0; i--) {
            if (costs[i][0] < costs[i][1]) {
                if (a > 0) {
                    a--;
                    min += costs[i][0];
                } else {
                    b--;
                    min += costs[i][1];
                }
            } else {
                if (b > 0) {
                    b--;
                    min += costs[i][1];
                } else {
                    a--;
                    min += costs[i][0];
                }

            }
        }
        return min;
    }
}

