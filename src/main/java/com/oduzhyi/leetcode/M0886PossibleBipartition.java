package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class M0886PossibleBipartition {
    Map<Integer, Set<Integer>> vertex;

    public boolean possibleBipartition(int N, int[][] dislikes) {
        vertex = new HashMap<>();
        for (int[] dis : dislikes) {
            vertex.computeIfAbsent(dis[0], k -> new HashSet<>()).add(dis[1]);
            vertex.computeIfAbsent(dis[1], k -> new HashSet<>()).add(dis[0]);
        }

        Map<Integer, Integer> colored = new HashMap<>();
        for (Integer start : vertex.keySet()) {
            if (!colored.containsKey(start) && !dfs(start, 0, colored)) {
                return false;
            }
        }
        return true;
    }

    private boolean dfs(int vert, int color, Map<Integer, Integer> colored) {
        if (colored.containsKey(vert)) {
            return color == colored.get(vert);
        }
        colored.put(vert, color);

        for (Integer child : vertex.get(vert)) {
            if (!dfs(child, color == 0 ? 1 : 0, colored)) {
                return false;
            }
        }
        return true;
    }
}

// bipartite coloring
// create vert map
// for every vert, start with color 0 and do dfs, alter colors on every level
// if during dfs encountered the same node with diferent color - return false
// otherwise return true