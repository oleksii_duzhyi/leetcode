package com.oduzhyi.leetcode;

import java.util.Arrays;

public class E0977SortedSquares {

    public int[] sortedSquares(int[] A) {
        int[] res = new int[A.length];
        int pointer = A.length - 1;
        int i = 0;
        int j = A.length - 1;

        while (pointer >= 0) {
            if (Math.abs(A[i]) > Math.abs(A[j])) {
                res[pointer--] = A[i] * A[i];
                i++;
            } else {
                res[pointer--] = A[j] * A[j];
                j--;
            }
        }
        return res;
    }

    public int[] sortedSquaresSort(int[] A) {
        for (int i = 0; i < A.length; i++) {
            A[i] *= A[i];
        }
        Arrays.sort(A);
        return A;
    }
}