package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class E0589PreorderTraversal {

    public List<Integer> preorder(Node root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }

        LinkedList<Node> stack = new LinkedList<>();
        stack.push(root);

        while (!stack.isEmpty()) {
            Node next = stack.pop();
            res.add(next.val);

            if (next.children != null) {
                for (int i = next.children.size() - 1; i >= 0; i--) {
                    stack.push(next.children.get(i));
                }
            }
        }
        return res;
    }

    public List<Integer> preorderRec(Node root) {
        List<Integer> res = new ArrayList<>();
        preorderRec(root, res);
        return res;
    }

    private void preorderRec(Node root, List<Integer> res) {
        if (root != null) {
            res.add(root.val);
            for (Node child : root.children) {
                preorderRec(child, res);
            }
        }
    }
}

// lets do it in recursive and iterative manner
