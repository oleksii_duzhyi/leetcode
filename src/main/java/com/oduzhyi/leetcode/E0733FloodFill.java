package com.oduzhyi.leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class E0733FloodFill {
    private int[] idxChg = new int[]{1, -1};

    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        if (image.length == 0 || image[0].length == 0 || image[sr][sc] == newColor) {
            return image;
        }
        int color = image[sr][sc];
        dfs(image, sr, sc, color, newColor);
        return image;
    }

    private void dfs(int[][] image, int r, int c, int color, int newColor) {
        if (image[r][c] == color) {
            image[r][c] = newColor;
            for (int i : idxChg) {
                if (r + i >= 0 && r + i < image.length) {
                    dfs(image, r + i, c, color, newColor);
                }
                if (c + i >= 0 && c + i < image[0].length) {
                    dfs(image, r, c + i, color, newColor);
                }
            }
        }
    }

    public int[][] floodFillBfs(int[][] image, int sr, int sc, int newColor) {
        if (image.length == 0 || image[0].length == 0 || image[sr][sc] == newColor) {
            return image;
        }
        int rows = image.length;
        int cols = image[0].length;
        int currentCol = image[sr][sc];
        Queue<Integer> queue = new LinkedList<>();
        queue.add(sr * cols + sc);
        while (!queue.isEmpty()) {
            int next = queue.poll();
            int r = next / cols;
            int c = next % cols;
            image[r][c] = newColor;
            for (int i : idxChg) {
                if (r + i >= 0 && r + i < rows && image[r + i][c] == currentCol) {
                    queue.add((r + i) * cols + c);
                }
                if (c + i >= 0 && c + i < cols && image[r][c + i] == currentCol) {
                    queue.add(r * cols + c + i);
                }
            }

        }
        return image;
    }

}