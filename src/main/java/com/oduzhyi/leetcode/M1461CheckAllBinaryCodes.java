package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class M1461CheckAllBinaryCodes {
    public boolean hasAllCodes(String s, int k) {
        Set<String> seen = new HashSet<>();
        int target = (int) Math.pow(2, k);
        for (int i = 0; i + k <= s.length(); i++) {
            seen.add(s.substring(i, i + k));
            if (seen.size() == target) {
                return true;
            }
        }
        return seen.size() == target;
    }

    public boolean hasAllCodesTL(String s, int k) {

        int max = (1 << k) - 1;
        int min = (1 << (k - 1)) - 1;
        List<String> p = new ArrayList<>();
        for (int i = max; i >= min; i--) {
            StringBuilder sb = new StringBuilder(Integer.toBinaryString(i));
            while (sb.length() < k) {
                sb.insert(0, "0");
            }
            if (!s.contains(sb.toString())) {
                return false;
            }
        }
        for (int i = min; i >= 0; i = i >> 1) {
            StringBuilder sb = new StringBuilder(Integer.toBinaryString(i));
            while (sb.length() < k) {
                sb.insert(0, "0");
            }
            if (!s.contains(sb.toString())) {
                return false;
            }
            if (i == 0) {
                break;
            }
        }

        return true;
    }
}
