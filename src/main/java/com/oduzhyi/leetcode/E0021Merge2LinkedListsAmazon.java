package com.oduzhyi.leetcode;

public class E0021Merge2LinkedListsAmazon {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode root = new ListNode(-1);

        ListNode forCycle = root;

        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                forCycle.next = new ListNode(l1.val);
                l1 = l1.next;
            } else {
                forCycle.next = new ListNode(l2.val);
                l2 = l2.next;
            }
            forCycle = forCycle.next;
        }

        forCycle.next = l1 == null ? l2 : l1;

        return root.next;
    }
}

// okay, this task looks pretty straight forward
// what we have to do is to iterate over every list, compare values, and add them to result list based on that comparison
// we should also be aware of cases with empty lists
// cool trick - once we exausted the first list - we can reference as next element non-exausted list
// but this may create mutability problems