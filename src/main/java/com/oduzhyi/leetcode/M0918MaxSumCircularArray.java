package com.oduzhyi.leetcode;

public class M0918MaxSumCircularArray {

    public int maxSubarraySumCircular(int[] A) {
        int nonCircularSum = kadane(A);

        int totalSum = 0;
        for (int i = 0; i < A.length; i++) {
            totalSum += A[i];
            A[i] = -A[i];
        }

        int minSum = kadane(A);
        int circularSum = totalSum + minSum;
        if (circularSum == 0) { // all negative elements
            return nonCircularSum;
        }

        return Math.max(circularSum, nonCircularSum);
    }

    private int kadane(int[] A) {
        int best = A[0];
        int sum = A[0];
        for (int i = 1; i < A.length; i++) {
            if (sum < 0) {
                sum = 0;
            }
            sum += A[i];
            best = Math.max(best, sum);
        }
        return best;
    }
}

// use kadanes to find maximum sum non circular
// find total sum and inverse elements as I go
// find minimum sum on onversed array
// if TotalSum + minsum ==0 (all numbers negativE) - return nonCircSum
// othewise return max(nonCircSum, totalsum + minSum);