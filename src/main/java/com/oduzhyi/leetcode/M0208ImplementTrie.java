package com.oduzhyi.leetcode;

public class M0208ImplementTrie {

    class Triev2 {

        private TrieNode root = new TrieNode();

        /**
         * Initialize your data structure here.
         */
        public Triev2() {
        }

        /**
         * Inserts a word into the trie.
         */
        public void insert(String word) {
            TrieNode node = root;
            for (int i = 0; i < word.length(); i++) {
                char c = word.charAt(i);
                if (!node.contains(c)) {
                    node.put(c, new TrieNode());
                }
                node = node.get(c);
            }
            node.setEnd();
        }

        /**
         * Returns if the word is in the trie.
         */
        public boolean search(String word) {
            TrieNode target = getPrefixNode(word);
            return target != null && target.isEnd();
        }

        /**
         * Returns if there is any word in the trie that starts with the given prefix.
         */
        public boolean startsWith(String prefix) {
            return getPrefixNode(prefix) != null;
        }

        private TrieNode getPrefixNode(String prefix) {
            TrieNode node = root;
            int i = 0;
            while (node != null && i < prefix.length()) {
                node = node.get(prefix.charAt(i++));
            }
            return node;
        }

        class TrieNode {
            private boolean end;

            private TrieNode[] next = new TrieNode[26];

            public TrieNode() {
            }

            public boolean contains(char c) {
                return next[c - 'a'] != null;
            }

            public TrieNode get(char c) {
                return next[c - 'a'];
            }

            public void put(char c, TrieNode tn) {
                next[c - 'a'] = tn;
            }

            public void setEnd() {
                this.end = true;
            }

            public boolean isEnd() {
                return end;
            }
        }
    }

    class Trie {

        private Node[] root;

        /**
         * Initialize your data structure here.
         */
        public Trie() {
            root = new Node[26];
        }

        /**
         * Inserts a word into the trie.
         */
        public void insert(String word) {
            Node[] rootRef = root;
            Node last = null;
            for (int i = 0; i < word.length(); i++) {
                Node n = rootRef[word.charAt(i) - 'a'];
                if (n == null) {
                    n = new Node(word.charAt(i));
                    rootRef[word.charAt(i) - 'a'] = n;
                }
                rootRef = n.next;
                last = n;
            }
            last.end = true;
        }

        /**
         * Returns if the word is in the trie.
         */
        public boolean search(String word) {
            Node last = root[word.charAt(0) - 'a'];
            for (int i = 1; i < word.length() && last != null; i++) {
                last = last.next[word.charAt(i) - 'a'];
            }
            return last != null && last.end;
        }

        /**
         * Returns if there is any word in the trie that starts with the given prefix.
         */
        public boolean startsWith(String prefix) {
            Node last = root[prefix.charAt(0) - 'a'];
            for (int i = 1; i < prefix.length() && last != null; i++) {
                last = last.next[prefix.charAt(i) - 'a'];
            }
            return last != null;
        }

        class Node {
            char val;
            boolean end;

            Node[] next = new Node[26];

            Node(char c) {
                this(c, false);
            }

            Node(char c, boolean end) {
                this.val = c;
                this.end = end;
            }
        }
    }

/**
 * Your Trie object will be instantiated and called as such:
 * Trie obj = new Trie();
 * obj.insert(word);
 * boolean param_2 = obj.search(word);
 * boolean param_3 = obj.startsWith(prefix);
 */

}
