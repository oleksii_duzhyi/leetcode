package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class M904FruitsIntoBuckets {
    public int totalFruit(int[] tree) {
        if (tree.length < 2) {
            return tree.length;
        }
        int f = -1;
        int s = -1;
        int sCount = 0;
        int max = 0;
        int curr = 0;

        for (int fruit : tree) {
            if (fruit == f || fruit == s) {
                curr++;
            } else {
                curr = sCount + 1;
            }

            if (fruit == s) {
                sCount++;
            } else {
                sCount = 1;
            }

            if (fruit != s) {
                f = s;
                s = fruit;
            }
            max = Math.max(max, curr);
        }

        return max;

    }

    public int totalFruitHM(int[] tree) {
        if (tree.length < 2) {
            return tree.length;
        }
        Map<Integer, Integer> fruits = new HashMap<>();
        int st = 0;
        int max = 2;
        for (int i = 0; i < tree.length; i++) {
            fruits.put(tree[i], i);
            if (fruits.size() > 2) {
                int fruitToKeep = tree[i - 1];
                max = Math.max(i - st, max);
                int fruitToRemove = -1;
                for (int f : fruits.keySet()) {
                    if (f != fruitToKeep && f != tree[i]) {
                        fruitToRemove = f;
                        break;
                    }
                }
                st = fruits.remove(fruitToRemove) + 1;
            }
        }
        max = Math.max(tree.length - st, max);
        return max;

    }
}
