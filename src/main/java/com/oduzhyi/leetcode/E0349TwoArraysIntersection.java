package com.oduzhyi.leetcode;

import java.util.HashSet;
import java.util.Set;

public class E0349TwoArraysIntersection {
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> n1 = new HashSet<>();
        for (int i : nums1) {
            n1.add(i);
        }
        Set<Integer> result = new HashSet<>();
        for (int i : nums2) {
            if (n1.contains(i)) {
                result.add(i);
            }
        }
        int[] r = new int[result.size()];
        int i = 0;
        for (int n : result) {
            r[i++] = n;
        }
        return r;
    }
}

// easiest solution N 2
// are numbers unique in nums?
// what should be in the result - are numbers unique?