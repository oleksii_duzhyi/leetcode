package com.oduzhyi.leetcode;

public class E0383RansomNote {
    public boolean canConstruct(String ransomNote, String magazine) {
        if (magazine.length() < ransomNote.length()) {
            return false;
        }
        int[] magHash = new int[26];
        for (char c : magazine.toCharArray()) {
            magHash[c - 'a']++;
        }
        for (char c : ransomNote.toCharArray()) {
            magHash[c - 'a']--;
            if (magHash[c - 'a'] < 0) {
                return false;
            }
        }

        return true;
    }
}

// questions:
// can input be null?
// can magazine be emtpy?
// can note size be bigger magazine size?
// what are the possibe characters?
// what to return is ransomNote is empty?
//
//  because it may contain only lower case chars - we remeber count of each letter
// in magazine and note - ot a fixed size array
// and after that compare arrays at the correctponding indeces
// return false if the occurence of char in note is strictly less
// then occurence of char in the magazine