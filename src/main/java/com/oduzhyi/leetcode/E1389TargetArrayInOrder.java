package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class E1389TargetArrayInOrder {

    public int[] createTargetArray(int[] nums, int[] index) {
        int[] res = new int[nums.length];
        int j = -1;
        for (int i = 0; i < nums.length; i++) {
            if (index[i] <= j) {
                System.arraycopy(res, index[i], res, index[i] + 1, j + 1 - index[i]);
            }
            res[index[i]] = nums[i];
            j++;
        }
        return res;
    }

    public int[] createTargetArrayList(int[] nums, int[] index) {
        List<Integer> res = new ArrayList<>(100);

        for (int i = 0; i < nums.length; i++) {
            res.add(index[i], nums[i]);
        }
        return res.stream().mapToInt(i -> i).toArray();
    }
}

// Questions:
// can index[i] > i?
// are the array of equal sizes?
// what does insert mean - overwrite or addition?
// write some simple cases