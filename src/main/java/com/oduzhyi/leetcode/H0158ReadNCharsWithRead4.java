package com.oduzhyi.leetcode;

public class H0158ReadNCharsWithRead4 extends Reader4 {
    int pointer = 0;
    char[] lastFour = new char[4];
    int bytes = 0;

    /**
     * @param buf Destination buffer
     * @param n   Number of characters to read
     * @return The number of actual characters read
     */
    public int read(char[] buf, int n) {
        int resPointer = 0;
        int cnt = 0;
        while (pointer < bytes && (n - cnt) > 0) {
            buf[resPointer++] = lastFour[pointer++];
            cnt++;
        }
        int i = 0;
        int read = 0;
        while (i < (n - cnt) && (read = read4(lastFour)) > 0) {
            pointer = 0;
            bytes = read;
            for (int j = 0; j < read && resPointer < n; j++) {
                buf[resPointer++] = lastFour[j];
                pointer++;
            }
            i += Math.min(read, pointer);
        }

        return resPointer;

    }
}

class Reader4 {
    int read4(char[] chars) {
        return 0;
    }
}