package com.oduzhyi.leetcode;

public class E1374GenerateStringHaveOddNumOfChars {
    public String generateTheString(int n) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < n - 1; i++) {
            res.append('a');
        }
        if (n % 2 == 0) {
            res.append('b');
        } else {
            res.append('a');
        }
        return res.toString();
    }
}
