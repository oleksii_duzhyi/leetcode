package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class E0359LogRageLimiterGoogle {

    class Logger {

        private final Map<String, Integer> seenMsg;

        /**
         * Initialize your data structure here.
         */
        public Logger() {
            seenMsg = new HashMap<>();
        }

        /**
         * Returns true if the message should be printed in the given timestamp, otherwise returns false.
         * If this method returns false, the message will not be printed.
         * The timestamp is in seconds granularity.
         */
        public boolean shouldPrintMessage(int timestamp, String message) {
            Integer ts = seenMsg.get(message);
            if (ts == null || timestamp - ts >= 10) {
                seenMsg.put(message, timestamp);
                System.out.println(seenMsg);
                return true;
            } else {
                return false;
            }
        }
    }

/**
 * Your Logger object will be instantiated and called as such:
 * Logger obj = new Logger();
 * boolean param_1 = obj.shouldPrintMessage(timestamp,message);
 */

// lets start with a pseudo code
// Couple of things I can think about:
// 1. I need a way for message memorization as well as timestamp. I think a hashmap might be a good choise here
// 2. I need to think about a free space, if we assume all messages are unique, and message rate is high - we might encounter out of memory problem
//
// In initialization:
// init hashmap

// in the method
// check is message is in the map
// if not -> remember message and ts and return true
// if yes -> check if the ts diff is greater than 10s
//      if yes -> override ts and return true
//      otherwise -> return false

// for the space cleanup - i think it make sense to create a background thread
}

class Logger {

    private Map<String, Long> holder = new HashMap<>();

    public Logger() {
    }

    public boolean shouldPrintMessage(long timestamp, String message) {
        Long prev = holder.putIfAbsent(message, timestamp);
        if (prev == null) {
            return true;
        }
        if (timestamp - prev >= 10) {
            holder.put(message, timestamp);
            return true;
        }
        return false;
    }
}

/**
 * Your Logger object will be instantiated and called as such:
 * Logger obj = new Logger();
 * boolean param_1 = obj.shouldPrintMessage(timestamp,message);
 */

// Questions:
// how many messages can there be in 10 seconds?
// how many unique messages can there be during program runtime?
//
// the first most obvious solution that comes to mind
// because we need to know an interval for each specific message
// we can accumulate those messages in hashmap and record latest ts when returned true
// downside - this internal map will only grow
//
// to make it space efficient I recommend introducing