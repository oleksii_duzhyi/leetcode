package com.oduzhyi.leetcode;

public class E0709ToLowerCase {
    public String toLowerCase(String str) {
        char min = 'A';
        char max = 'Z';
        int diffToLower = 32;

        StringBuilder res = new StringBuilder();
        for (char c : str.toCharArray()) {
            if (c >= min && c <= max) {
                res.append((char) (c + diffToLower));
            } else {
                res.append(c);
            }
        }
        return res.toString();
    }
}

