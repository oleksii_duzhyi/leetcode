package com.oduzhyi.leetcode;

public class E0088MergeSortedArrays {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1;
        int j = n - 1;
        int pos = nums1.length - 1;

        while (i >= 0 || j >= 0) {
            if (i >= 0 && j >= 0) {
                if (nums2[j] > nums1[i]) {
                    nums1[pos--] = nums2[j--];
                } else {
                    nums1[pos--] = nums1[i--];
                }
            } else if (j >= 0) {
                nums1[pos--] = nums2[j--];
            } else {
                break;
            }
        }
    }
}

// fill the empty space from the right
