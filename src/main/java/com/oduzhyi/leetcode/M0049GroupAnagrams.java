package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class M0049GroupAnagrams {
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, Integer> anToI = new HashMap<>();
        List<List<String>> result = new ArrayList<>();
        AtomicInteger index = new AtomicInteger(0);
        for (String word : strs) {
            char[] sC = word.toCharArray();
            Arrays.sort(sC);
            String sorted = new String(sC);
            Integer i = anToI.computeIfAbsent(sorted, newI -> index.getAndIncrement());
            if (i + 1 > result.size()) {
                result.add(new ArrayList<>());
            }
            result.get(i).add(word);
        }
        return result;
    }
}
// idea:
// we need to group anagram by some characteristic: I've chosen to group by sorted characters
// alternatively it is possible to group by every character count
// based on the group - we compute an index in the result list
// if this index is new - we add new array list
// then get it, and append new string to it