package com.oduzhyi.leetcode;

/**
 * Created by Нелла on 20.11.2018.
 */
public class E0852PeakMountain {

    public int peakIndexInMountainArray(int[] A) {
        int s = 0;
        int e = A.length;
        int mid = A.length / 2;

        while (s < e) {
            if (A[mid] < A[mid + 1]) {
                s = mid + 1;
            } else {
                e = mid;
            }
            mid = s + (e - s) / 2;
        }
        return s;
    }


    public int peakIndexInMountainArrayCompares(int[] A) {
        for (int i = 0; i < A.length - 1; i++) {
            if (A[i] > A[i + 1]) {
                return i;
            }
        }
        return -1;
    }

    public int peakIndexInMountainArrayOld(int[] A) {
        for (int i = 1; i < A.length - 1; i++) {
            if (A[i - 1] < A[i] && A[i] > A[i + 1]) {
                return i;
            }
        }
        throw new IllegalArgumentException("A is not a mountain");
    }

    private int secondSolutionLessArrayAccess(int[] A) {
        for (int i = 1; i < A.length; i++) {
            if (A[i - 1] > A[i]) {
                if (A[i - 2] < A[i - 1]) {
                    return i - 1;
                }
            }
        }
        throw new IllegalArgumentException("A is not a mountain");
    }

    public int peakIndexInMountainArrayBinarySearch(int[] A) {
        int start = 0;
        int end = A.length;
        int mid;

        while (true) {
            //Get Midpoint to Check
            mid = (end + start) / 2;

            //Move Right
            if (A[mid] < A[mid + 1])
                start = mid;
            else if (A[mid - 1] > A[mid])
                end = mid;
            else
                return mid;
        }
    }
}
