package com.oduzhyi.leetcode;

import java.util.List;

public class M364NestedListWeightedSumII {
    public int depthSumInverse(List<NestedInteger> nestedList) {
        int maxDepth = maxDepth(nestedList, 1);
        return innerDepthSum(nestedList, maxDepth);
    }

    private int maxDepth(List<NestedInteger> list, int depth) {
        int maxD = depth;
        for (NestedInteger ni : list) {
            if (!ni.isInteger()) {
                maxD = Math.max(maxDepth(ni.getList(), depth + 1), maxD);
            }
        }
        return maxD;
    }

    private int innerDepthSum(List<NestedInteger> list, int depth) {
        int sum = 0;
        for (NestedInteger ni : list) {
            if (ni.isInteger()) {
                sum += depth * ni.getInteger();
            } else {
                sum += innerDepthSum(ni.getList(), depth - 1);
            }
        }
        return sum;
    }

    public interface NestedInteger {
        // Constructor initializes an empty nested list.
//        public NestedInteger();

        // Constructor initializes a single integer.
//        public NestedInteger(int value);

        // @return true if this NestedInteger holds a single integer, rather than a nested list.
        public boolean isInteger();

        // @return the single integer that this NestedInteger holds, if it holds a single integer
        // Return null if this NestedInteger holds a nested list
        public Integer getInteger();

        // Set this NestedInteger to hold a single integer.
        public void setInteger(int value);

        // Set this NestedInteger to hold a nested list and adds a nested integer to it.
        public void add(NestedInteger ni);

        // @return the nested list that this NestedInteger holds, if it holds a nested list
        // Return null if this NestedInteger holds a single integer
        public List<NestedInteger> getList();
    }
}

// to find inverse sum from the problem 339 - we need to know the depth first.
// i don't see a way we start iterating over leafs first
// so initial idea is to find maxDepth first and then caclulate sum

// but we can use a hash map for example to collect level + sum of values on this level and
// later on multiplicate them

// another interesting idea is to look differently at sum
// 1*3 + 4 * 2 + 6 * 1 =
// 1 * 2 + 4 * 1 + (1 + 4 + 6) =
// 1 + (1 + 4) + (1 + 4 + 6)
// so we can actually pass sum from the upper level to lower level to include it one more time in calculation