package com.oduzhyi.leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class H0065ValidNumber {
    private Set<Character> numbers = new HashSet<>(Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'));

    public boolean isNumber(String s) {
        s = s.trim();
        return checkNumber(s, true);
    }

    private boolean checkNumber(String s, boolean scientific) {
        if (s.isEmpty()) {
            return false;
        }
        int cntNums = 0;
        boolean sign = false;
        boolean dot = false;
        for (int i = 0; i < s.length(); ) {
            char c = s.charAt(i);
            if (!sign) {
                sign = true;
                if (c == '-' || c == '+') {
                    i++;
                } else if (c == '.') {
                    if (!scientific) {
                        return false;
                    }
                    dot = true;
                    i++;
                } else if (!numbers.contains(c)) {
                    return false;
                }
            } else {
                if (numbers.contains(c)) {
                    i++;
                    cntNums++;
                } else if (c == '.') {
                    if (dot || !scientific) {
                        return false;
                    }
                    dot = true;
                    i++;
                } else if (c == 'e' && scientific) {
                    return cntNums > 0 && checkNumber(s.substring(i + 1), false);
                } else {
                    return false;
                }
            }
        }
        return cntNums > 0;
    }
}
