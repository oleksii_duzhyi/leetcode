package com.oduzhyi.leetcode;

import java.util.Arrays;

/**
 * Created by Нелла on 14.11.2018.
 */
public class E0922SortByParity2 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(new E0922SortByParity2().sortArrayByParityIIInPlace(new int[]{4, 2, 5, 7})));
    }

    public int[] sortArrayByParityII(int[] A) {
        int[] res = new int[A.length];
        int evenI = 0;
        int oddI = 1;
        for (int i = 0; i < A.length; i++) {
            if (A[i] % 2 == 0) {
                res[evenI] = A[i];
                evenI += 2;
            } else {
                res[oddI] = A[i];
                oddI += 2;
            }
        }
        return res;
    }

    public int[] sortArrayByParityIISwap(int[] A) {
        for (int i = 0; i < A.length; i++) {
            if (i % 2 != A[i] % 2) {
                swap(A, i);
            }
        }
        return A;
    }

    private void swap(int[] A, int i) {
        for (int j = i + 1; j < A.length; j += 2) {
            if (A[j] % 2 == (j - 1) % 2) {
                int tmp = A[j];
                A[j] = A[i];
                A[i] = tmp;
                break;
            }

        }
    }

// questions:
// is there is always a solution?
//

    public int[] sortArrayByParityOld(int[] a) {
        int[] sorted = new int[a.length];
        int even = 0;
        int odd = 1;
        for (int i = 0; i < a.length; i++) {
            int number = a[i];
            if (number % 2 == 0) {
                sorted[even] = number;
                even += 2;
            } else {
                sorted[odd] = number;
                odd += 2;
            }
        }
        return sorted;
    }

    public int[] sortArrayByParityIIInPlace(int[] A) {
        int j = 1;
        int N = A.length;
        for (int i = 0; i < N; i += 2) {
            if (A[i] % 2 == 1) {
                while (A[j] % 2 == 1) {
                    j += 2;
                }
                swap(A, i, j);
            }
        }
        return A;
    }

    private void swap(int[] A, int i, int j) {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }
}
