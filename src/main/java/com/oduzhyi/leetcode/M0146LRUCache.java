package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class M0146LRUCache {
}

class LRUCache {
    private Node head = new Node(-1, -1);
    private Node tail = new Node(-1, -1);
    private Map<Integer, Node> map = new HashMap<>();
    private int maxSize;

    public LRUCache(int capacity) {
        this.maxSize = capacity;
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        Node n = getNode(key);
        return n != null ? n.value : -1;
    }

    private Node getNode(int key) {
        if (map.containsKey(key)) {
            Node cur = map.get(key);

            // taking the node from it's place
            Node prev = cur.prev;
            Node next = cur.next;
            prev.next = next;
            next.prev = prev;

            addToHead(cur);
            return cur;
        }
        return null;
    }

    private void addToHead(Node n) {
        Node lru = head.next;
        head.next = n;
        n.next = lru;
        lru.prev = n;
        n.prev = head;
    }

    public void put(int key, int value) {
        if (map.containsKey(key)) {
            Node node = getNode(key);
            node.value = value;
        } else {
            if (map.size() + 1 > maxSize) {
                Node last = tail.prev;
                Node prev = last.prev;

                tail.prev = prev;
                prev.next = tail;
                map.remove(last.key);
            }
            Node newNode = new Node(key, value);
            addToHead(newNode);
            map.put(key, newNode);
        }
    }

    private static class Node {
        private Node prev;
        private Node next;
        private int key;
        private int value;

        private Node(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }
}