package com.oduzhyi.leetcode;

public class M0238ProductOfArrayExceptSelf {
    public int[] productExceptSelf(int[] nums) {
        if (nums.length <= 1) {
            return nums;
        }
        int l = nums.length;
        int[] forwardMult = new int[l];
        int[] backwardMult = new int[l];
        forwardMult[0] = nums[0];
        backwardMult[l - 1] = nums[l - 1];

        for (int i = 1; i < nums.length; i++) {
            forwardMult[i] = forwardMult[i - 1] * nums[i];
            backwardMult[l - 1 - i] = backwardMult[l - i] * nums[l - 1 - i];
        }

        nums[0] = backwardMult[1];
        nums[l - 1] = forwardMult[l - 2];

        for (int i = 1; i < nums.length - 1; i++) {
            nums[i] = backwardMult[i + 1] * forwardMult[i - 1];
        }
        return nums;
    }
}

// construct 2 arrays - forwardProd and backwardProd
// elements in these arrays are representing the corresponding product starting from 0 and l - 1
// update res[i] = backward[i+1] * forward[i - 1]
// return res
