package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class M1110DeleteNodesAndReturnForest {
    List<TreeNode> res = new ArrayList<>();

    public List<TreeNode> delNodes(TreeNode root, int[] to_delete) {
        Set<Integer> toDelete = new HashSet<>();
        for (int n : to_delete) {
            toDelete.add(n);
        }
        dfs(root, true, toDelete);
        return res;
    }

    private TreeNode dfs(TreeNode node, boolean isRoot, Set<Integer> toDelete) {
        if (node == null) {
            return null;
        }
        boolean deleted = toDelete.contains(node.val);
        if (isRoot && !deleted) {
            res.add(node);
        }

        node.left = dfs(node.left, deleted, toDelete);
        node.right = dfs(node.right, deleted, toDelete);
        return deleted ? null : node;
    }

    public List<TreeNode> delNodesIter(TreeNode root, int[] to_delete) {
        Set<Integer> toDelete = new HashSet<>();
        for (int n : to_delete) {
            toDelete.add(n);
        }
        List<TreeNode> res = new ArrayList<>();
        Deque<Node> queue = new ArrayDeque<>();


        queue.add(new Node(root, false));

        while (!queue.isEmpty()) {
            Node next = queue.poll();

            boolean flag = next.parentAdded;
            if (!toDelete.contains(next.tn.val)) {
                if (!flag) {
                    flag = true;
                    res.add(next.tn);
                }
            } else {
                flag = false;
            }

            if (next.tn.left != null) {
                queue.add(new Node(next.tn.left, flag));
                if (toDelete.contains(next.tn.left.val)) {
                    next.tn.left = null;
                }
            }
            if (next.tn.right != null) {
                queue.add(new Node(next.tn.right, flag));
                if (toDelete.contains(next.tn.right.val)) {
                    next.tn.right = null;
                }
            }
        }
        return res;
    }

    class Node {
        TreeNode tn;
        boolean parentAdded;

        public Node(TreeNode tn, boolean parentAdded) {
            this.tn = tn;
            this.parentAdded = parentAdded;
        }
    }
}


// 2 approaches:
// iterative BFS, need a wrapper class to keep if the currnet node root has been deleted
// recursive DFS - if node is root and not delete - add to result
// update left and right references.
// is node deleted - it's child become new roots to add, if child is deleted - need to update it's ref
