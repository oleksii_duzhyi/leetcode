package com.oduzhyi.leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * Unique email address with replacements
 */
public class Problem929HashSet_UseSBOverString {
    public static void main(String[] args) {
        Problem929HashSet_UseSBOverString p = new Problem929HashSet_UseSBOverString();

        System.out.println(p.numUniqueEmails(new String[]{"test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com", "testemail2+david@leetcode.com"}));
    }

    public int numUniqueEmails(String[] emails) {
        Set<String> uniqueEmails = new HashSet<>();
        for (int i = 0; i < emails.length; i++) {
            String email = emails[i];
            StringBuilder sb = new StringBuilder();
            boolean beforeAt = true;
            boolean skipToAt = false;
            for (char c : email.toCharArray()) {
                if (beforeAt) {
                    if (c == '@') {
                        beforeAt = false;
                        sb.append(c);
                    } else if (c == '+') {
                        skipToAt = true;
                    } else if (c != '.' && !skipToAt) {
                        sb.append(c);
                    }
                } else {
                    sb.append(c);
                }
            }
            uniqueEmails.add(sb.toString());
        }
        return uniqueEmails.size();
    }
}
