package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Set;

public class M0279PerfectSquares {
    public int numSquares(int n) {
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        queue.offer(n);  // 7

        int level = 0; // 3, 5, 2
        while (!queue.isEmpty()) {
            int size = queue.size();  // 1, 2
            for (int i = 0; i < size; i++) {
                int next = queue.poll();  // 7, 6
                if (next == 0) {
                    return level;
                }
                for (int j = 1; j * j <= next; j++) { // 2^15.5
                    int reminder = next - j * j;  // 5, 2
                    if (reminder == 0) {
                        return level + 1;
                    }
                    if (next - j * j > 0) {
                        queue.offer(next - j * j);
                    }
                }
            }
            level++; // 1
        }
        return level;
    }


    public int numSquaresOld(int n) {
        int maxSquare = (int) Math.sqrt(n);

        Set<Integer> queue = new HashSet<>();
        int[] posSq = new int[maxSquare + 1];
        for (int i = 1; i < posSq.length; i++) {
            posSq[i] = i * i;
            queue.add(n - posSq[i]);
            if (n - posSq[i] == 0) {
                return 1;
            }
        }

        int res = 0;
        while (!queue.isEmpty()) {
            res++;
            Set<Integer> nextQueue = new HashSet<>();
            for (Integer next : queue) {
                for (int j = 1; j < posSq.length; j++) {
                    if (next - posSq[j] == 0) {
                        return res + 1;
                    }
                    if (next - posSq[j] > 0) {
                        nextQueue.add(next - posSq[j]);
                    }
                }
            }
            queue = nextQueue;
        }
        return res;
    }
}

// starting with the current number
// for every i, where i*i <= number
//   substract the square root and put to queue
// increment the depth
// return if equal to zero