package com.oduzhyi.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class E0496NextGreaterElement {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int[] res = new int[nums1.length];
        Map<Integer, Integer> nextGreater = new HashMap<>();
        LinkedList<Integer> stack = new LinkedList<>();
        for (int i = 0; i < nums2.length; i++) {
            if (stack.isEmpty() || nums2[i] < stack.peek()) {
                stack.push(nums2[i]);
            } else {
                while (!stack.isEmpty() && nums2[i] > stack.peek()) {
                    nextGreater.put(stack.pop(), nums2[i]);
                }
                stack.push(nums2[i]);
            }
        }
        int i = 0;
        for (int n : nums1) {
            res[i++] = nextGreater.getOrDefault(n, -1);
        }
        return res;
    }

    public int[] nextGreaterElementBF(int[] nums1, int[] nums2) {
        int[] res = new int[nums1.length];
        Arrays.fill(res, -1);
        for (int i = 0; i < nums1.length; i++) {
            boolean found = false;
            for (int j = 0; j < nums2.length; j++) {
                if (found && nums1[i] < nums2[j]) {
                    res[i] = nums2[j];
                    break;
                }
                if (nums1[i] == nums2[j]) {
                    found = true;
                }
            }
        }
        return res;
    }
}
