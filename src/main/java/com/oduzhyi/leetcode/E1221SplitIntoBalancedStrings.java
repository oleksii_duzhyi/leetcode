package com.oduzhyi.leetcode;

public class E1221SplitIntoBalancedStrings {
    public int balancedStringSplit(String s) {
        int res = 0;
        int cnt = 0;
        for (char c : s.toCharArray()) {
            if (c == 'R') {
                cnt++;
            } else {
                cnt--;
            }
            if (cnt == 0) {
                res++;
            }
        }
        return res;
    }
}

//RLRL => RL, RL => 1
//RRRLLL => 1

// Questions:
// I did not understand a question at first
// is string valid? can it contain not only R L
//
// we need to count number of R and L and once we reach 0 - this means it is a valid substring