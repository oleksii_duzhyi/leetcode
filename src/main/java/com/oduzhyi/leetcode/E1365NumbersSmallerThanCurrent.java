package com.oduzhyi.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class E1365NumbersSmallerThanCurrent {

    public int[] smallerNumbersThanCurrent(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        int[] copy = nums.clone();

        Arrays.sort(copy);

        for (int i = 0; i < nums.length; i++) {
            map.putIfAbsent(copy[i], i);
        }

        for (int i = 0; i < nums.length; i++) {
            copy[i] = map.get(nums[i]);
        }
        return copy;
    }

    public int[] smallerNumbersThanCurrentOptimized(int[] nums) {
        Map<Integer, LinkedList<Integer>> mappings = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            LinkedList<Integer> locs = mappings.computeIfAbsent(nums[i], num -> new LinkedList<>());
            locs.add(i);
        }
        int[] res = new int[nums.length];
        Arrays.sort(nums);

        int prev = nums[0];
        int prevCnt = 0;
        for (int i = 1; i < nums.length; i++) {
            int cur = nums[i];
            if (cur == prev) {
                res[mappings.get(cur).removeLast()] = prevCnt;
            } else {
                res[mappings.get(cur).removeLast()] = i;
                prevCnt = i;
            }
            prev = cur;
        }
        return res;
    }

    public int[] smallerNumbersThanCurrentHMAndSort(int[] nums) {
        Map<Integer, LinkedList<Integer>> mappings = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            LinkedList<Integer> locs = mappings.computeIfAbsent(nums[i], num -> new LinkedList<>());
            locs.add(i);
        }

        int[] res = new int[nums.length];
        Arrays.sort(nums);

        int prev = nums[0];
        for (int i = 1; i < nums.length; i++) {
            int cur = nums[i];
            if (cur == prev) {
                res[i] = res[i - 1];
            } else {
                res[i] = i;
            }
            prev = cur;
        }

        int[] finRes = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            finRes[mappings.get(nums[i]).removeLast()] = res[i];
        }
        return finRes;
    }

    public int[] smallerNumbersThanCurrentBF(int[] nums) {
        int[] res = new int[nums.length];

        for (int i = 0; i < nums.length; i++) {
            int n = nums[i];
            int cnt = 0;
            for (int j = 0; j < nums.length; j++) {
                if (nums[j] < nums[i]) {
                    cnt++;
                }
            }
            res[i] = cnt;
        }
        return res;
    }
}

// Questions:
// what is the size of the input array?
// are numbers integer?
// can I assume array is greater than size of one?
// are there duplicates in the input array?
//
// the most obvious solution that comes to mind first - is of course take each number and compare it with the rest
// but this solution is going to run in O(n^2) complexity
// can I utilize some data structure/algorithm to my advantage?
//
// First of all we can try use sorting, it provides O(n log n) complexity.
// If the array is sorted in the ascending order - than for ith element there are going to be
// n - i elements bigger than it.
// For the sorting we need somehow account for the duplicates in the list...
//
// [1, 2, 7, 7, 7, 8, 9] => [0, 1, 2, 2, 2, 5, 6]
// it should be pretty doable
//
// but we have anothe problem with sorting - we need to output result in the same order as the input array

// really great idea with putIfAbsent method