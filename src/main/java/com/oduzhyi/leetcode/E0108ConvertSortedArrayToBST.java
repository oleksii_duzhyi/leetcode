package com.oduzhyi.leetcode;

public class E0108ConvertSortedArrayToBST {
    public TreeNode sortedArrayToBST(int[] nums) {
        if (nums.length == 0) {
            return null;
        }
        if (nums.length == 1) {
            return new TreeNode(nums[0]);
        }
        return convert(nums, 0, nums.length - 1);
    }

    private TreeNode convert(int[] nums, int f, int t) {
        if (f > t) {
            return null;
        }
        int mid = (t + f) / 2;
        TreeNode tn = new TreeNode(nums[mid]);
        tn.left = convert(nums, f, mid - 1);
        tn.right = convert(nums, mid + 1, t);
        return tn;
    }
}