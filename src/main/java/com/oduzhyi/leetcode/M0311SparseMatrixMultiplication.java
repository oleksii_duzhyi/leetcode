package com.oduzhyi.leetcode;

public class M0311SparseMatrixMultiplication {
    public int[][] multiply(int[][] A, int[][] B) {
        int[][] res = new int[A.length][B[0].length];
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                if (A[i][j] != 0) {
                    for (int k = 0; k < B[0].length; k++) {
                        res[i][k] += A[i][j] * B[j][k];
                    }
                }
            }
        }
        return res;
    }

    public int[][] multiplyHS(int[][] A, int[][] B) {
        int[][] res = new int[A.length][B[0].length];
        boolean[] zeroRows = new boolean[A.length];
        boolean[] zeroCols = new boolean[B[0].length];

        for (int i = 0; i < A.length; i++) {
            boolean allZeros = true;
            for (int j = 0; j < A[0].length; j++) {
                if (A[i][j] != 0) {
                    allZeros = false;
                    break;
                }
            }
            zeroRows[i] = allZeros;
        }

        for (int i = 0; i < B[0].length; i++) {
            boolean allZeros = true;
            for (int j = 0; j < B.length; j++) {
                if (B[j][i] != 0) {
                    allZeros = false;
                    break;
                }
            }
            zeroCols[i] = allZeros;
        }


        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < B[0].length; j++) {
                if (!zeroRows[i] && !zeroCols[j]) {
                    int sum = 0;
                    for (int k = 0; k < A[0].length; k++) {
                        sum += (A[i][k] * B[k][j]);
                    }
                    res[i][j] = sum;
                }
            }
        }
        return res;
    }
}
// 2 ideas: 1st - remember zero rows and cols - do multiplication if both have at least 1 non zero
// 2nd: if A[i][j] == 0 - it is not needed to increase res[i][*] with mult B[j][*]
