package com.oduzhyi.leetcode;

public class M0540SingleElementInSortedArray {

    public static void main(String[] args) {
        System.out.println(new M0540SingleElementInSortedArray().singleNonDuplicateDaniil(new int[]{0, 1, 1, 2, 2}));
    }

    public int singleNonDuplicateDaniil(int[] nums) {  // 1 1 2 2 3 4 4
        return search(nums, 0, nums.length);
    }

    public int search(int[] nums, int from, int to) {
        if (from == to) {
            return nums[from];
        }

        int mid = (to - from) / 2 / 2 * 2 + from;

        if (mid + 1 == nums.length || nums[mid + 1] != nums[mid]) {
            return search(nums, from, mid);
        } else {
            return search(nums, mid + 2, to);
        }
    }

    public int singleNonDuplicate(int[] nums) {
        return binarySearch(nums, 0, nums.length - 1);
    }

    private int binarySearch(int[] nums, int from, int to) {
        if (from > to) {
            return -1;
        }
        int mid = to + (from - to) / 2;
        if (mid + 1 < nums.length && nums[mid] == nums[mid + 1]) {
            if ((to - mid - 1) % 2 == 1) {
                return binarySearch(nums, mid + 2, to);
            } else {
                return binarySearch(nums, from, mid - 1);
            }
        } else if (mid - 1 >= 0 && nums[mid - 1] == nums[mid]) {
            if ((to - mid) % 2 == 1) {
                return binarySearch(nums, mid + 1, to);
            } else {
                return binarySearch(nums, from, mid - 2);
            }
        } else {
            return nums[mid];
        }
    }

    public int singleNonDuplicateOn(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }
        for (int i = 0; i < nums.length - 1; i += 2) {
            if (nums[i] != nums[i + 1]) {
                return nums[i];
            }
        }
        return nums[nums.length - 1];
    }
}
