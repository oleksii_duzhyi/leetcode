package com.oduzhyi.leetcode;

public class E1342ReduceToZero {
    public int numberOfSteps(int num) {
        if (num == 0) {
            return 0;
        }

        return 1 + (num % 2 == 0 ? numberOfSteps(num / 2) : numberOfSteps(num - 1));
    }


}

// Questions:
// can the input number be negative?
//
// looks like really simple problem at the first glance. we can solve it in O(n/2) = O(n)
// it should be easily solvable by recurssion