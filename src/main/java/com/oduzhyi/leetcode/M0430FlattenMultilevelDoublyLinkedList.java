package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

public class M0430FlattenMultilevelDoublyLinkedList {
    public DuNode flatten(DuNode node) {
        DuNode pseudoHead = new DuNode(0, null, node, null);
        flatten(pseudoHead, node);
        pseudoHead.next.prev = null;
        return pseudoHead.next;
    }

    private DuNode flatten(DuNode prev, DuNode curr) {
        if (curr == null) {
            return prev;
        }
        curr.prev = prev;
        prev.next = curr;
        DuNode currNext = curr.next;
        DuNode tail = flatten(curr, curr.child);
        curr.child = null;

        return flatten(tail, currNext);
    }

    public DuNode flattenIter(DuNode node) {
        if (node == null) {
            return node;
        }
        Deque<DuNode> stack = new ArrayDeque<>();

        DuNode res = new DuNode();
        DuNode head = res;
        stack.push(node);

        while (!stack.isEmpty()) {
            DuNode n = stack.pop();
            head.next = n;
            n.prev = head;
            if (n.next != null) {
                stack.push(n.next);
            }
            if (n.child != null) {
                stack.push(n.child);
                n.child = null;
            }
            head = head.next;
        }
        res.next.prev = null;
        return res.next;
    }
}

class DuNode {
    public int val;
    public DuNode prev;
    public DuNode next;
    public DuNode child;

    public DuNode() {
    }

    public DuNode(int i, DuNode o, DuNode node, DuNode o1) {
    }
}

// iterative is really easy - create stack, push root
// while !stack is empty
// pop from set, update links
// is next is not null - push next
// if child not null - push child
// dont forget to update references before  returning