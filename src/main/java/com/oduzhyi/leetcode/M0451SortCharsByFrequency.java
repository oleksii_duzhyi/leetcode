package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class M0451SortCharsByFrequency {

    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString((int) Math.pow(2, 4)));
        System.out.println(Integer.toBinaryString((int) Math.pow(2, 3) - 1));
        System.out.println(7 & 1);

    }

    public String frequencySort(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        Map<Character, Integer> counts = new HashMap<>();
        for (char c : s.toCharArray()) {
            counts.put(c, counts.getOrDefault(c, 0) + 1);
        }
        int max = Collections.max(counts.values());
        List<List<Character>> buckets = new ArrayList<>(max + 1);
        for (int i = 0; i <= max; i++) {
            buckets.add(new ArrayList<>());
        }

        for (Map.Entry<Character, Integer> entry : counts.entrySet()) {
            buckets.get(entry.getValue()).add(entry.getKey());
        }
        StringBuilder res = new StringBuilder();
        for (int i = max; i > 0; i--) {
            List<Character> chars = buckets.get(i);
            for (Character c : chars) {
                for (int j = 0; j < i; j++) {
                    res.append(c);
                }
            }
        }
        return res.toString();
    }

    public String frequencySortSorting(String s) {
        Map<Character, CharWithFreq> map = new HashMap<>();
        for (char c : s.toCharArray()) {
            CharWithFreq curr = map.computeIfAbsent(c, k -> new CharWithFreq(c, 0));
            curr.freq++;
        }

        List<CharWithFreq> vals = new ArrayList(map.values());
        Collections.sort(vals);

        StringBuilder res = new StringBuilder();
        for (CharWithFreq cwf : vals) {
            for (int i = 0; i < cwf.freq; i++) {
                res.append(cwf.c);
            }
        }
        return res.toString();
    }

    class CharWithFreq implements Comparable<CharWithFreq> {
        char c;
        int freq;

        CharWithFreq(char c, int freq) {
            this.c = c;
            this.freq = freq;
        }

        public int compareTo(CharWithFreq o) {
            return Integer.compare(o.freq, freq);
        }
    }
}