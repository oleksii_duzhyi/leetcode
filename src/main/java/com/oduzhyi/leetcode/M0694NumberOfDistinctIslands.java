package com.oduzhyi.leetcode;

import java.util.Set;
import java.util.TreeSet;

public class M0694NumberOfDistinctIslands {
    int[][] grid;

    public static void main(String[] args) {
        new M0694NumberOfDistinctIslands()
                .numDistinctIslands(
                        new int[][]{
                                new int[]{1, 0, 0, 0, 0, 0},
                                new int[]{1, 1, 0, 1, 1, 0},
                                new int[]{0, 0, 0, 1, 0, 0},
                                new int[]{0, 0, 0, 0, 0, 0},
                        }
                );
    }

    public void explore(int r, int c, char code, StringBuilder sb) {
        if (0 <= r && r < grid.length && 0 <= c && c < grid[0].length && grid[r][c] == 1) {
            grid[r][c] = 0;
            sb.append(code);
            explore(r + 1, c, 'a', sb);
            explore(r - 1, c, 'b', sb);
            explore(r, c + 1, 'c', sb);
            explore(r, c - 1, 'd', sb);
        }
    }

    public int numDistinctIslands(int[][] grid) {
        this.grid = grid;
        Set<String> shapes = new TreeSet<>();

        for (int r = 0; r < grid.length; r++) {
            for (int c = 0; c < grid[0].length; c++) {
                if (grid[r][c] == 1) {
                    StringBuilder sb = new StringBuilder();
                    explore(r, c, 's', sb);
                    shapes.add(sb.toString());
                }
            }
        }
        System.out.println(shapes);
        return shapes.size();
    }
}
