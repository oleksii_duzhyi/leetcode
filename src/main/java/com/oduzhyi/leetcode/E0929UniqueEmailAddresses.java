package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class E0929UniqueEmailAddresses {
    public int numUniqueEmails(String[] emails) {

        Set<String> uniqueEmails = new HashSet<>();
        for (int i = 0; i < emails.length; i++) {
            StringBuilder sb = new StringBuilder();
            String email = emails[i];
            int indexOfPlus = email.indexOf("+");
            int indexOfAt = email.indexOf("@");
            String firstPart;
            if (indexOfPlus == -1) {
                firstPart = email.substring(0, indexOfAt);
            } else {
                firstPart = email.substring(0, indexOfPlus);
            }
            sb.append(firstPart.replace(".", ""));
            sb.append(email.substring(indexOfAt));

            uniqueEmails.add(sb.toString());
        }
        return uniqueEmails.size();
    }

    public int numUniqueEmailsOld(String[] emails) {

        Set<String> uniqueEmails = new HashSet<>();
        for (int i = 0; i < emails.length; i++) {
            String email = emails[i];
            StringBuilder sb = new StringBuilder();
            boolean beforeAt = true;
            boolean skipToAt = false;
            for (char c : email.toCharArray()) {
                if (beforeAt) {
                    if (c == '@') {
                        beforeAt = false;
                        sb.append(c);
                    } else if (c == '+') {
                        skipToAt = true;
                    } else if (c != '.' && !skipToAt) {
                        sb.append(c);
                    }
                } else {
                    sb.append(c);
                }
            }
            uniqueEmails.add(sb.toString());
        }
        return uniqueEmails.size();
    }

    public int numUniqueEmailsNew(String[] emails) {
        Map<String, Set<String>> sentEmails = new HashMap<>();
        for (String email : emails) {
            String[] parts = email.split("@");
            Set<String> domains;
            if (parts[0].indexOf("+") != -1) {
                domains = sentEmails.computeIfAbsent(
                        parts[0].substring(0, parts[0].indexOf("+")).replaceAll("\\.", ""), key -> new HashSet<>());
            } else {
                domains = sentEmails.computeIfAbsent(parts[0].replaceAll("\\.", ""), key -> new HashSet<>());
            }
            domains.add(parts[1]);
        }
        int res = 0;
        for (Set<String> dom : sentEmails.values()) {
            res += dom.size();
        }
        return res;
    }
}
