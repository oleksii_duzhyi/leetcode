package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class M0078Subsets {


    List<List<Integer>> output = new ArrayList();
    int n;

    public List<List<Integer>> subsets(int[] nums) {
        int max = (int) Math.pow(2, nums.length) - 1;
        List<List<Integer>> res = new ArrayList<>();
        for (int i = 0; i <= max; i++) {
            List<Integer> subset = new ArrayList<>();
            int tmp = i;
            for (int j = 0; j < nums.length; j++) {
                if ((tmp & 1) == 1) {
                    subset.add(nums[j]);
                }
                tmp = tmp >> 1;
            }
            res.add(subset);
        }
        return res;
    }

    public void backtrack(int first, ArrayList<Integer> curr, int[] nums, int k) {
        // if the combination is done
        if (curr.size() == k) {
            output.add(new ArrayList(curr));
        }

        for (int i = first; i < n; ++i) {
            // add i into the current combination
            curr.add(nums[i]);
            // use next integers to complete the combination
            backtrack(i + 1, curr, nums, k);
            // backtrack
            curr.remove(curr.size() - 1);
        }
    }

    public List<List<Integer>> subsetsBT(int[] nums) {
        n = nums.length;
        for (int k = 0; k < n + 1; ++k) {
            backtrack(0, new ArrayList<Integer>(), nums, k);
        }
        return output;
    }


    public List<List<Integer>> subsetsIter(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        res.add(Collections.EMPTY_LIST);

        for (int n : nums) {
            List<List<Integer>> newSubsets = new ArrayList<>();
            for (List<Integer> existing : res) {
                List<Integer> subRes = new ArrayList<>(existing);
                subRes.add(n);
                newSubsets.add(subRes);
            }
            res.addAll(newSubsets);
        }
        return res;
    }

    List<List<Integer>> res = new ArrayList<>();

    public List<List<Integer>> subsetsnew(int[] nums) {
        for (int size = 0; size <= nums.length; size++) {
            subsets(nums, 0, size, new ArrayList<>());
        }
        return res;
    }

    private void subsets(int[] nums, int idx, int size, List<Integer> oneRes) {
        if (oneRes.size() == size) {
            res.add(new ArrayList<>(oneRes));
        } else {
            for (int i = idx; i < nums.length; i++) {
                oneRes.add(nums[i]);
                subsets(nums, i + 1, size, oneRes);
                oneRes.remove(oneRes.size() - 1);
            }
        }
    }

    public List<List<Integer>> subsetsBitMasking(int[] nums) {
        int total = (int) Math.pow(2, nums.length);

        List<List<Integer>> res = new ArrayList<>();

        for (int i = 0; i < total; i++) {
            List<Integer> newRes = new ArrayList<>();
            int num = i;
            int j = 0;
            while (num > 0) {
                if ((num & 1) == 1) {
                    newRes.add(nums[j]);
                }
                j++;
                num = num >> 1;
            }
            res.add(newRes);
        }
        return res;
    }
}

// one of the idea is bit masking. Max number of combimations = 2 ^ N
// if we iterate from 0 to 2^N - 1  and include all the possible results
// we will have all possible subsets
// backtracking - construct subsets of lenght 0 to n via recursive call
// iterative - start with empty list, continuously add every element to existing lists copies