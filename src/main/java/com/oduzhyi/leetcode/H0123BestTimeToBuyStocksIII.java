package com.oduzhyi.leetcode;

public class H0123BestTimeToBuyStocksIII {
    public int maxProfit(int[] prices) {
        int t1Cost = Integer.MAX_VALUE,
                t2Cost = Integer.MAX_VALUE;
        int t1Profit = 0,
                t2Profit = 0;

        for (int price : prices) {
            // the maximum profit if only one transaction is allowed
            t1Cost = Math.min(t1Cost, price);
            t1Profit = Math.max(t1Profit, price - t1Cost);
            // reinvest the gained profit in the second transaction
            t2Cost = Math.min(t2Cost, price - t1Profit);
            t2Profit = Math.max(t2Profit, price - t2Cost);
        }

        return t2Profit;
    }

    public int maxProfitBiDirectional(int[] prices) {
        if (prices.length == 0) {
            return 0;
        }

        int[] leftProfits = new int[prices.length];
        int[] rightProfits = new int[prices.length + 1];

        int lMin = prices[0];
        int rMax = prices[prices.length - 1];

        for (int l = 1; l < prices.length; l++) {
            leftProfits[l] = Math.max(leftProfits[l - 1], prices[l] - lMin);
            lMin = Math.min(lMin, prices[l]);

            int r = prices.length - 1 - l;
            rightProfits[r] = Math.max(rightProfits[r + 1], rMax - prices[r]);
            rMax = Math.max(rMax, prices[r]);
        }

        int maxProfit = 0;

        for (int i = 0; i < leftProfits.length; i++) {
            maxProfit = Math.max(maxProfit, leftProfits[i] + rightProfits[i + 1]);
        }
        return maxProfit;
    }

    public int maxProfitN2(int[] prices) {
        if (prices.length == 0) {
            return 0;
        }
        int topProfit = maxOneProfit(prices, 0, prices.length);


        for (int i = 2; i <= prices.length - 2; i++) {
            topProfit = Math.max(maxOneProfit(prices, 0, i) + maxOneProfit(prices, i, prices.length), topProfit);
        }

        return topProfit;
    }

    public int maxOneProfit(int[] prices, int f, int t) {
        int profit = 0;
        int buy = prices[f]; // 2

        for (int i = f + 1; i < t; i++) {
            if (buy > prices[i]) {
                buy = prices[i]; // 1
            } else {
                profit = Math.max(profit, prices[i] - buy); // 7
            }
        }

        return profit;
    }
}

// naive approach with priority queue proved to be incorrect
// specific test case
//
// [1,2,4,2,5,7,2,4,9,0]
//  |   | |   | |   |
//    3     5     7          = 12
//  |         | |   |
//       6        7          = 13
// I think we need to use somehow dynamic programming in our advantage
// can we decompose the problem to a smaller one?
// if we know the answer for n items, can we find out an answer for n + 1?

