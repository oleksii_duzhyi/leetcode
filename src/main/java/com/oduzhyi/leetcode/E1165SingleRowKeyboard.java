package com.oduzhyi.leetcode;

public class E1165SingleRowKeyboard {
    public int calculateTime(String keyboard, String word) {
        int[] cache = new int[26];
        for (int i = 0; i < keyboard.length(); i++) {
            cache[keyboard.charAt(i) - 'a'] = i;
        }
        int start = 0;
        int total = 0;
        for (char c : word.toCharArray()) {
            total += Math.abs(start - cache[c - 'a']);
            start = cache[c - 'a'];
        }
        return total;
    }
}

// QUESTIONS:
// what type of characters are in keyboard?
// is it possible that we won't be able to print a word?
// can characters be repeted in different positions?
//
// brute force solution -
// start position is 0
// for every character in the string
//      get character index in keyboard
//      calculate distnace
//      update total distance
//      set start to new position
// complexity O(keyboard.length * word.length)


// but give keyboard is small (26) we can cache letters locations with array or hash map

