package com.oduzhyi.leetcode;

import java.util.LinkedList;

public class E0557StringReverseWords {

    public void swap(char[] c, int i, int j) {
        while (i < j) {
            char temp = c[i];
            c[i++] = c[j];
            c[j--] = temp;
        }
    }

    public String reverseWords(String s) {
        if (s.length() == 0) {
            return s;
        }
        char[] c = s.toCharArray();
        int start = 0;
        int end = 0;
        for (end = 0; end < c.length; end++) {
            if (c[end] == ' ') {
                swap(c, start, end - 1);
                start = end + 1;
            }
        }
        swap(c, start, end - 1);
        return new String(c);
    }

    public String reverseWordsSplit(String s) {
        StringBuilder res = new StringBuilder();
        String[] words = s.split(" ");

        for (String word : words) {
            char[] chars = word.toCharArray();
            for (int i = chars.length - 1; i >= 0; i--) {
                res.append(chars[i]);
            }
            res.append(' ');
        }
        res.deleteCharAt(res.length() - 1);

        return res.toString();

    }


    public String reverseWordsStack(String s) {
        LinkedList<Character> stack = new LinkedList<>();
        StringBuilder res = new StringBuilder();

        for (char c : s.toCharArray()) {
            if (c == ' ') {
                wordFromStack(stack, res);
                res.append(' ');
            } else {
                stack.push(c);
            }
        }
        wordFromStack(stack, res);
        return res.toString();

    }

    private void wordFromStack(LinkedList<Character> stack, StringBuilder res) {
        while (!stack.isEmpty()) {
            res.append(stack.pop());
        }
    }
}

// questions:
// can there be empty input?
// input only from whitespaces?
// can there be more than 1 whitespace between characters?
// can string start with whitespace?
// are there any puctuation characters?
//
// I think I will iterate a string - converted to a char array - I will collect every letter I encounter
// to a stack (LL) and then if space is encountered or string has ended, I'm going to empty a stack
// into a result
//
// if the words are separated by exactly one space - we can split string to words,
// and then for each word iterate backwards it
//
// another idea - is to remember offsets of the word and then do in place swapping of characters
