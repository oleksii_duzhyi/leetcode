package com.oduzhyi.leetcode;

public class E0367ValidPerfectSquare {
    public boolean isPerfectSquare(int num) {
        if (num == 0 || num == 1) {
            return true;
        }
        int start = 2;
        int end = 46340;
        while (start <= end) {
            int mid = (start + end) / 2;
            if (mid * mid == num) {
                return true;
            }
            if (mid * mid > num) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        return false;
    }
}