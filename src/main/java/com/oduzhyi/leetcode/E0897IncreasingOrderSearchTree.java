package com.oduzhyi.leetcode;

import java.util.LinkedList;

public class E0897IncreasingOrderSearchTree {
    TreeNode cur;

    public TreeNode increasingBST(TreeNode root) {
        if (root == null) {
            return root;
        }
        TreeNode ans = new TreeNode(0);
        cur = ans;
        expand(root);
        return ans.right;
    }

    private void expand(TreeNode tn) {
        if (tn.left != null) {
            expand(tn.left);
        }
        tn.left = null;
        cur.right = tn;
        cur = tn;
        if (tn.right != null) {
            expand(tn.right);
        }
    }

    public TreeNode increasingBSTInOrder(TreeNode root) {
        if (root == null) {
            return root;
        }
        LinkedList<TreeNode> queue = new LinkedList<>();
        expand(root, queue);
        TreeNode res = queue.poll();
        TreeNode prev = res;
        while (!queue.isEmpty()) {
            TreeNode next = queue.poll();
            next.left = null;
            next.right = null;
            prev.right = next;
            prev = next;
        }
        return res;
    }

    private void expand(TreeNode tn, LinkedList<TreeNode> queue) {
        if (tn.left != null) {
            expand(tn.left, queue);
        }
        queue.add(tn);
        if (tn.right != null) {
            expand(tn.right, queue);
        }
    }
}

// we need to traverse tree in the traverse tree in the inorder (left - root - right)
// leftmost node is going to be our new root
// every other element is going to be on the right side of the tree