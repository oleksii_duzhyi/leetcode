package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class H0999CriticalRouter {
    private int time = 0;
    private Map<Integer, Integer> visitedTime = new HashMap<>();
    private Map<Integer, Integer> lowTime = new HashMap<>();
    private Set<Integer> visited = new HashSet<>();
    private List<Integer> criticalRouter = new ArrayList<>();

    public static void main(String[] args) {
        List<List<Integer>> connections = new ArrayList<>();
        connections.add(Arrays.asList(1, 2));
        connections.add(Arrays.asList(2, 3));
        connections.add(Arrays.asList(3, 1));
        connections.add(Arrays.asList(4, 3));
        connections.add(Arrays.asList(4, 5));
        connections.add(Arrays.asList(5, 6));
        connections.add(Arrays.asList(5, 7));
        connections.add(Arrays.asList(6, 7));
        connections.add(Arrays.asList(6, 8));

        System.out.println(new H0999CriticalRouter().criticalRouters(connections));

        connections = new ArrayList<>();
        connections.add(Arrays.asList(1, 2));
        connections.add(Arrays.asList(0, 1));
        connections.add(Arrays.asList(3, 1));
        connections.add(Arrays.asList(2, 0));

        System.out.println(new H0999CriticalRouter().criticalRouters(connections));

        connections = new ArrayList<>();
        connections.add(Arrays.asList(1, 2));
        connections.add(Arrays.asList(2, 3));
        connections.add(Arrays.asList(3, 1));
        connections.add(Arrays.asList(4, 3));
        connections.add(Arrays.asList(4, 5));
        connections.add(Arrays.asList(5, 6));
        connections.add(Arrays.asList(6, 7));
        connections.add(Arrays.asList(7, 5));
        connections.add(Arrays.asList(6, 8));
        connections.add(Arrays.asList(7, 3));

        System.out.println(new H0999CriticalRouter().criticalRouters(connections));
    }

    public List<Integer> criticalRouters(List<List<Integer>> connections) {

        Map<Integer, List<Integer>> vertex = new HashMap<>();
        int last = -1;
        for (List<Integer> vert : connections) {
            vertex.computeIfAbsent(vert.get(0), key -> new ArrayList<>()).add(vert.get(1));
            vertex.computeIfAbsent(vert.get(1), key -> new ArrayList<>()).add(vert.get(0));
            last = vert.get(0);
        }

        dfs(last, null, vertex);
        return criticalRouter;
    }

    private void dfs(Integer node, Integer parent, Map<Integer, List<Integer>> vertex) {
        visited.add(node);
        lowTime.put(node, time);
        visitedTime.put(node, time);

        time++;

        int children = 0;
        boolean critical = false;
        for (Integer child : vertex.get(node)) {
            if (Objects.equals(child, parent)) {
                continue;
            }
            if (!visited.contains(child)) {
                children++;
                dfs(child, node, vertex);
                if (visitedTime.get(node) <= lowTime.get(child)) {
                    critical = true;
                } else {
                    lowTime.put(node, Math.min(lowTime.get(node), lowTime.get(child)));
                }
            } else {
                lowTime.put(node, Math.min(lowTime.get(node), lowTime.get(child)));
            }
        }

        if ((parent == null && children >= 2) || (parent != null && critical)) {
            criticalRouter.add(node);
        }
    }
}
