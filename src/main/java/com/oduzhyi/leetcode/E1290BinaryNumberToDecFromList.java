package com.oduzhyi.leetcode;

public class E1290BinaryNumberToDecFromList {
    public int getDecimalValue(ListNode head) {
        int res = 0;
        while (head != null) {
            res = res << 1;
            if (head.val == 1) {
                res = res | 1;
            }
            head = head.next;
        }
        return res;
    }
}

// questions:
// is list size <= 32?
// what if it is biiger?