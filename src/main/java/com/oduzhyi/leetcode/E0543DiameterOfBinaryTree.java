package com.oduzhyi.leetcode;

public class E0543DiameterOfBinaryTree {
    int d = 0;

    public int diameterOfBinaryTree(TreeNode root) {
        heightAndDiameter(root);
        return d;
    }

    private int heightAndDiameter(TreeNode tn) {
        if (tn == null) {
            return 0;
        }

        int l = heightAndDiameter(tn.left);
        int r = heightAndDiameter(tn.right);
        this.d = Math.max(l + r, d);
        return Math.max(l, r) + 1;
    }
}
// from every node:
//      find the depth of left subtree
//      find the depyh of right subtree
//      try update the diameter if l + r > d