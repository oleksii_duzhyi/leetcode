package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class E0937ReorderDataInLogFilesAmazon {
    public String[] reorderLogFilesInPlace(String[] logs) {
        Arrays.sort(logs, (l1, l2) -> {
            String[] parts1 = l1.split(" ", 2);
            String[] parts2 = l2.split(" ", 2);

            boolean isDigit1 = Character.isDigit(parts1[1].charAt(0));
            boolean isDigit2 = Character.isDigit(parts2[1].charAt(0));

            if (!isDigit1 && !isDigit2) {
                int compare = parts1[1].compareTo(parts2[1]);
                return compare == 0 ? parts1[0].compareTo(parts2[0]) : compare;
            }
            return isDigit1 ? (isDigit2 ? 0 : 1) : -1;
        });
        return logs;
    }

    public String[] reorderLogFilesNonStreams(String[] logs) {
        List<ComparableLog> list = new ArrayList<>();
        for (int i = 0; i < logs.length; i++) {
            list.add(new ComparableLog(i, logs[i]));
        }

        Collections.sort(list);
        String[] res = new String[logs.length];
        for (int i = 0; i < logs.length; i++) {
            res[i] = list.get(i).originalLine;
        }

        return res;
    }

    public String[] reorderLogFiles(String[] logs) {
        return IntStream.range(0, logs.length)
                .mapToObj(i -> new ComparableLog(i, logs[i]))
                .sorted()
                .map(cl -> cl.originalLine)
                .toArray(String[]::new);
    }


    private static class ComparableLog implements Comparable<ComparableLog> {
        private int type;
        private String id;
        private String content;
        private int pos;
        private String originalLine;

        public ComparableLog(int position, String logLine) {
            this.pos = position;
            this.originalLine = logLine;
            String[] parts = logLine.split(" ", 2);
            this.type = parts[1].charAt(0) > '9' ? -1 : 1;
            this.id = parts[0];
            this.content = parts[1];
        }

        public int compareTo(ComparableLog o) {
            if (this.type != o.type) {
                return this.type;
            }
            if (this.type == 1) {
                return this.pos - o.pos;
            } else {
                if (this.content.compareTo(o.content) == 0) {
                    return this.id.compareTo(o.id);
                }
                return this.content.compareTo(o.content);
            }
        }

    }
}


// okay, so I think I'm going to use java Collections.sort method
// but I need to create a proper abstraction above the log line
// to write correct comparator
// few things comparator should be aware of:
// 1. Is it a digit-log or letter-log - to cover the case that all letter logs are above digit
// 2. What is the log itself - without identifier - to compare log contents for letter logs
// 3. What is the log original postion - to cover original order of digit logs
//
// Complexity is going to be O(n log n) + additional space of O(2*n) will be needed
// Can this be done with comparator? I think no, because string is not enough to give all the answers,
// additionally index is needed
// but the correct answer is yes - it looks like in java arguments are passed in order to comparator
// that is why if the Compare o1 and o2 - o1 occured earlier in the list
