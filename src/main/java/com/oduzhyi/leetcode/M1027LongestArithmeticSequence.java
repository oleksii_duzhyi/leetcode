package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class M1027LongestArithmeticSequence {

    public static void main(String[] args) {
        System.out.println(new M1027LongestArithmeticSequence().getLongestArithmeticSubsequence(new int[]{22, 8, 57, 41, 36, 46, 42, 28, 42, 14, 9, 43, 27, 51, 0, 0, 38, 50, 31, 60, 29, 31, 20, 23, 37, 53, 27, 1, 47, 42, 28, 31, 10, 35, 39, 12, 15, 6, 35, 31, 45, 21, 30, 19, 5, 5, 4, 18, 38, 51, 10, 7, 20, 38, 28, 53, 15, 55, 60, 56, 43, 48, 34, 53, 54, 55, 14, 9, 56, 52}));
    }

    public int getLongestArithmeticSubsequence(int[] nums) {
        if (nums.length <= 2) {
            return nums.length;
        }
        Map<Integer, Integer>[] cache = new HashMap[nums.length];
        for (int i = 0; i < cache.length; i++) {
            cache[i] = new HashMap<>();
        }

        int max = 2;
        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < i; j++) {
                int diff = nums[i] - nums[j];
                int length = cache[j].getOrDefault(diff, 1) + 1;
                max = Math.max(length, max);
                cache[i].put(diff, length);
            }
        }
        return max;
    }
}

// create an array of map.
// start with element i
// for every element from 0 to j (i - 1) update array[i][diff] = (array[j][diff] or 1) + 1
// keep the max
// return max