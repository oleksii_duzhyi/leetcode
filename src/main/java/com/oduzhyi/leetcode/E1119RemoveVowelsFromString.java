package com.oduzhyi.leetcode;

public class E1119RemoveVowelsFromString {
    public String removeVowels(String S) {
        if (S == null) {
            return "";
        }
        StringBuilder res = new StringBuilder();

        for (char c : S.toCharArray()) {
            if (c != 'a' && c != 'e' && c != 'i' && c != 'o' && c != 'u') {
                res.append(c);
            }
        }
        return res.toString();
    }
}

// Questions:
// are letters all lowercase?
// are there any other characters than letters?

// first idea - create an empty StringBuilder,
// iterate over S
// if letter is not vowel append in to string builder
// return string builder result
// done in O(n) with O(n) additional space
//
// Test cases: abc => bc
//             "" => ""
//             null => ""
