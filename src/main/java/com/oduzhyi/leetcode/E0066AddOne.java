package com.oduzhyi.leetcode;

public class E0066AddOne {
    public int[] plusOne(int[] digits) {
        int i = digits.length - 1;
        int carry = 0;
        do {
            int cur = digits[i] + 1;
            digits[i] = cur % 10;
            carry = cur / 10;
            i--;
        } while (i >= 0 && carry > 0);

        if (carry == 1) {
            int[] digitsPlusOne = new int[digits.length + 1];
            digitsPlusOne[0] = 1;
            return digitsPlusOne;
        }
        return digits;
    }
}

// start with the last index of array
// try add 1
// assign % 10 to ith and update carry = ith/10
// continue until carry > 0
// if carry is 1 in the end of the loop - return arra 1 element bigger with 1 in 0th place
// otherwise return input array
