package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class M0535EncodeAndDecodeTinyUrl {
    private Map<String, String> encoded = new HashMap<>();

    // Encodes a URL to a shortened URL.
    public String encode(String longUrl) {
        String hash = hashWithSalt(longUrl);
        String tinyUrl = "http://tinyurl.com/" + hash;
        encoded.put("http://tinyurl.com/" + hash, longUrl);
        return tinyUrl;
    }

    // Decodes a shortened URL to its original URL.
    public String decode(String shortUrl) {
        return encoded.get(shortUrl);
    }

    private String hashWithSalt(String url) {
        String longHash = new java.util.UUID(url.hashCode(), System.currentTimeMillis()).toString();
        String hashStr = longHash.substring(0, 4) + longHash.substring(32);
        return hashStr;
    }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.decode(codec.encode(url));