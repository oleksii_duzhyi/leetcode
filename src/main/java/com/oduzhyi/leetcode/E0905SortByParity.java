package com.oduzhyi.leetcode;

import java.util.Arrays;

/**
 * Created by Нелла on 14.11.2018.
 */
public class E0905SortByParity {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(new E0905SortByParity().sortArrayByParityInPlace(new int[]{3, 1, 2, 4})));
    }

    public int[] sortArrayByParity(int[] a) {
        int[] sorted = new int[a.length];
        int start = 0;
        int end = sorted.length - 1;
        for (int i = 0; i < a.length; i++) {
            int number = a[i];
            if (number % 2 == 0) {
                sorted[start++] = number;
            } else {
                sorted[end--] = number;
            }
        }
        return sorted;
    }

    public int[] sortArrayByParityInPlace(int[] a) {
        int end = a.length - 1;
        for (int i = 0; i < a.length && end != i; ) {
            int number = a[i];
            if (number % 2 == 0) {
                i++;
            } else {
                a[i] = a[end];
                a[end--] = number;
            }
        }
        return a;
    }
}

// Questions:
// what is the size of A?
// can there be negative numbers?
