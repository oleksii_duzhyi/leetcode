package com.oduzhyi.leetcode;

import java.util.List;

interface BinaryMatrix {
    public int get(int row, int col);

    public List<Integer> dimensions();
}

public class M1428LeftMostColumnWithAtLeastOne {
    public int leftMostColumnWithOne(BinaryMatrix binaryMatrix) {
        int rows = binaryMatrix.dimensions().get(0);
        int cols = binaryMatrix.dimensions().get(1);

        int l = 0;
        int r = cols - 1;
        boolean hasOne = false;
        while (l <= r) {
            int mid = l + (r - l) / 2;
            boolean seenOne = false;
            int i = 0;
            while (!seenOne && i < rows) {
                if (binaryMatrix.get(i, mid) == 1) {
                    seenOne = true;
                    hasOne = true;
                }
                i++;
            }
            if (seenOne) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return hasOne ? l : -1;
    }
}