package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class M0398RandomPickIndex {
    private Map<Integer, List<Integer>> indexMap = new HashMap<>();
    private Random rnd;

    public M0398RandomPickIndex(int[] nums) {
        this.rnd = new Random();
        int i = 0;
        for (int n : nums) {
            indexMap.computeIfAbsent(n, k -> new ArrayList<>()).add(i++);
        }
    }

    public int pick(int target) {
        List<Integer> indexes = indexMap.get(target);
        if (indexes == null) {
            return -1;
        }
        return indexes.get(rnd.nextInt(indexes.size()));
    }
}

// can be also done with reservoir sampling, where the reservoir - is set of indexes of target
