package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class M0809ExpressiveWords {
    public int expressiveWords(String s, String[] words) {
        RLE target = new RLE(s);

        int res = 0;
        for (String w : words) {
            RLE other = new RLE(w);
            if (!other.key.equals(target.key)) {
                continue;
            }
            res++;
            for (int i = 0; i < target.key.length(); i++) {
                int tFreq = target.freq.get(i);
                int oFreq = other.freq.get(i);
                if (tFreq == oFreq || (oFreq < tFreq && tFreq >= 3)) {
                    continue;
                } else {
                    res--;
                    break;
                }
            }
        }
        return res;
    }
}

class RLE {
    String key;
    List<Integer> freq = new ArrayList<>();

    RLE(String word) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < word.length(); i++) {
            int cnt = 1;
            sb.append(word.charAt(i));
            while (i + 1 < word.length() && word.charAt(i) == word.charAt(i + 1)) {
                i++;
                cnt++;
            }
            freq.add(cnt);
        }
        this.key = sb.toString();
    }
}

// 2 approaches:
// 1st one RLE encoding of every string, and then first compare the keys, afterwards compare the values;
// 2nd - do this on the fly and compare every pair of string eith if-else