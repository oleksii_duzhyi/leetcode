package com.oduzhyi.leetcode;

import java.util.Arrays;

/**
 * Created by Нелла on 14.11.2018.
 */
public class E0832FlippingImage {
    public static void main(String[] args) {
        int[][] res = new E0832FlippingImage().flipAndInvertImage(new int[][]{{1, 1, 0}, {1, 0, 1}, {0, 0, 0}});
        for (int[] row : res) {
            System.out.println(Arrays.toString(row));
        }
        System.out.println();
        res = new E0832FlippingImage().flipAndInvertImage(new int[][]{{1, 1, 0, 0}, {1, 0, 0, 1}, {0, 1, 1, 1}, {1, 0, 1, 0}});
        for (int[] row : res) {
            System.out.println(Arrays.toString(row));
        }
    }

    public int[][] flipAndInvertImageOld(int[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            int rowLength = matrix[row].length;
            for (int col = 0; col < rowLength / 2; col++) {
                int fromBeg = matrix[row][col];
                int fromEnd = matrix[row][rowLength - 1 - col];
                if (fromEnd == fromBeg) {
                    if (fromEnd == 1) {
                        matrix[row][col] = 0;
                        matrix[row][rowLength - 1 - col] = 0;
                    } else {
                        matrix[row][col] = 1;
                        matrix[row][rowLength - 1 - col] = 1;
                    }
                }
            }
            if (rowLength % 2 == 1) {
                int col = rowLength / 2;
                int num = matrix[row][col];
                if (num == 1) {
                    matrix[row][col] = 0;
                } else {
                    matrix[row][col] = 1;
                }
            }
        }
        return matrix;
    }

    public int[][] flipAndInvertImageSuggestedButHardToUnderstand(int[][] A) {
        int C = A[0].length;
        for (int[] row : A) {
            for (int i = 0; i < (C + 1) / 2; ++i) {
                int tmp = row[i] ^ 1;
                row[i] = row[C - 1 - i] ^ 1;
                row[C - 1 - i] = tmp;
            }
        }

        return A;
    }

    public int[][] flipAndInvertImage(int[][] A) {
        if (A == null) {
            return null;
        }
        int l = A.length;
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < (l + 1) / 2; j++) {
                if (A[i][j] == A[i][l - 1 - j]) {
                    int inv = invert(A[i][j]);
                    // accounting for the case when j == l - 1 - j (l = 3, j = 1 )
                    A[i][j] = inv;
                    A[i][l - 1 - j] = inv;
                }
            }
        }
        return A;
    }

    private int invert(int i) {
        return i == 0 ? 1 : 0;
    }
}

// lets try first example:
// [
//     [0, 1],
//     [1, 0]
// ]
//
// horizontal flip:
// [
//     [1, 0],
//     [0, 1]
// ]
// inversion:
// [
//     [0, 1],
//     [1, 0]
// ]
//
// Questions:
// what is the size of an image ?
// what does it mean inversion
// if the image always square?
//
//
// The most obvious solution is to basically over the image twice:
// 1. flip it horizontally
// 2. then invert
//
// But from the example I made - I can already see that this operation is sometimes is unnecessary
// from what i see - if a[i] != a[j] - than nothing has to be done
// if a[i] == a[j] - then we need to invert both
// where i and j - corresponding locations in the row from 2 ends
//