package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class M0047Permutation2 {
    List<List<Integer>> res = new ArrayList<>();
    Map<Integer, Integer> freq = new HashMap<>();

    public List<List<Integer>> permuteUnique(int[] nums) {
        for (int n : nums) {
            freq.put(n, freq.getOrDefault(n, 0) + 1);
        }
        Set<Integer> numsSet = freq.keySet();
        permute(numsSet, 0, new ArrayList<>(), nums.length);
        return res;
    }

    private void permute(Set<Integer> numsSet, int from, List<Integer> list, int target) {
        if (from == target) {
            res.add(new ArrayList<>(list));
        } else {
            for (Integer num : numsSet) {
                int nFreq = freq.get(num);
                if (nFreq > 0) {
                    freq.put(num, nFreq - 1);
                    list.add(num);
                    permute(numsSet, from + 1, list, target);
                    list.remove(list.size() - 1);
                    freq.put(num, nFreq);
                }
            }
        }
    }
}

// still backtracking idea like in the unique numbers
// but instead of swapping elements of array back and forth
// we count occurence of every element
// then we add element to the list, decrement frequency and go 1 level deeper
// if reached the end of the input = add to result
// on the exit from the method - remove from result list, place back original freq
