package com.oduzhyi.leetcode;

import java.util.*;

public class E1180CountDistinctSubstringsWithOneLetter {
    public int countLetters(String s) { // "aabaa"
        if (s == null || s.length() == 0) {
            return 0;
        }
        int sum = 0;
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            int cnt = 1;
            while (i < chars.length - 1 && chars[i] == chars[i+1]) {
                cnt++;
                i++;
            }
            sum += arProg(cnt);
        }
        return sum;
    }

    public int arProg(int n) {
        return (n * (1 + n)) / 2;
    }
}

// questions:
// are there only letters in the string?
// are there different case letters?
// can there be integer overf
//
// a = 1
// aa = 1 + 2
// aaa = 1 + 2 + 3
// aaaa = 1 + 2 + 3 + 4
// tocount possible different combinations of specific letter we need to count sum of arithmetic progression
// so first we count number of same letter occurances - then adding sum of arithmetic progression to the sum