package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class E1213IntersectionOf3SortedArrays {
    public List<Integer> arraysIntersection(int[] arr1, int[] arr2, int[] arr3) { //
        List<Integer> res = new ArrayList<>();

        for (int i = 0, j = 0, k = 0;
             i < arr1.length && j < arr2.length && k < arr3.length; ) {
            if (arr1[i] == arr2[j] && arr2[j] == arr3[k]) {
                res.add(arr1[i]);
                i++;
                j++;
                k++;
            } else {
                int max = Math.max(Math.max(arr1[i], arr2[j]), arr3[k]);
                while (i < arr1.length && arr1[i] < max) {
                    i++;
                }

                while (j < arr2.length && arr2[j] < max) {
                    j++;
                }
                while (k < arr3.length && arr3[k] < max) {
                    k++;
                }
            }
        }


        return res;
    }
}

// question:
// are array of the same size?
// what are the array's sizes?
// are there duplicates?
//
// I think it makes sense to find same elements between 2 arrays to make code a bit easier
// on the other hand it is possible to do it all together at once