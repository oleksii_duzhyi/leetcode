package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class M0659ContiniousSequenceSpli {
    public boolean isPossible(int[] nums) {
        Map<Integer, Integer> freqMap = new HashMap<>();
        for (int n : nums) {
            freqMap.put(n, freqMap.getOrDefault(n, 0) + 1);
        }
        Map<Integer, Integer> ends = new HashMap<>();
        for (int n : nums) {
            int freq = freqMap.get(n);
            if (freq > 0) {
                freqMap.put(n, freq - 1);
                if (ends.getOrDefault(n - 1, 0) > 0) {
                    ends.put(n - 1, ends.get(n - 1) - 1);
                    ends.put(n, ends.getOrDefault(n, 0) + 1);
                } else if (freqMap.getOrDefault(n + 1, 0) > 0 && freqMap.getOrDefault(n + 2, 0) > 0) {
                    freqMap.put(n + 2, freqMap.get(n + 2) - 1);
                    freqMap.put(n + 1, freqMap.get(n + 1) - 1);
                    ends.put(n + 2, ends.getOrDefault(n + 2, 0) + 1);
                } else {
                    return false;
                }
            }
        }
        return true;
    }


    public boolean isPossibleNotOptimal(int[] nums) {

        List<List<Integer>> subseq = new ArrayList<>();

        for (int num : nums) {
            boolean added = false;
            for (int i = 0; i < subseq.size() && !added; i++) {
                List<Integer> s = subseq.get(i);
                if (s.get(s.size() - 1) - num == -1) {
                    s.add(num);
                    added = true;
                }
            }
            if (!added) {
                List<Integer> newSeq = new ArrayList<>();
                newSeq.add(num);
                subseq.add(0, newSeq);
            }
        }

        for (List<Integer> s : subseq) {
            if (s.size() < 3) {
                return false;
            }
        }
        return true;
    }
}

// first calculate the frequency of every element
// than for every element
// try to find if there is an n + 1 and n + 2 in the freq map
// if they are - decrement all counters by 1 and store the end (n+2) in the end of sequences
// else try to find if there is a sequnce ending n - 1 - if it is present - decrement previous seq
// and create occurence on new one ending n
// otherwise return false
