package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class M0105BinaryTreeFromPreorderAndInorder {
    private int rootIdx = 0;
    private Map<Integer, Integer> idxMap = new HashMap<Integer, Integer>();

    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder.length == 0) {
            return null;
        }
        int i = 0;
        for (int num : inorder) {
            idxMap.put(num, i++);
        }
        return buildTree(preorder, inorder, 0, inorder.length - 1);
    }

    private TreeNode buildTree(int[] preorder, int[] inorder, int iF, int iT) {
        TreeNode root = new TreeNode(preorder[rootIdx]);
        int i = idxMap.get(preorder[rootIdx]);
        rootIdx++;
        if (iF != i) {
            root.left = buildTree(preorder, inorder, iF, i - 1);
        }
        if (i != iT) {
            root.right = buildTree(preorder, inorder, i + 1, iT);
        }
        return root;
    }
}