package com.oduzhyi.leetcode;

public class E0461HammingDistance {

    public int hammingDistance(int x, int y) {
        int count = 0;
        for (int i = 0; i < 32; i++) {
            count += (x >> i & 1) ^ (y >> i & 1);
        }
        return count;
    }

    public int hammingDistanceLibs(int x, int y) {
        int xor = x ^ y;

        int count = 0;
        for (char c : Integer.toString(xor, 2).toCharArray()) {
            if (c == '1') {
                count++;
            }
        }
        return count;
    }
}

// No questions on this one