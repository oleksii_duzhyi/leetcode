package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class M1447SimplifiedFractions {
    public List<String> simplifiedFractions(int n) {
        List<String> res = new ArrayList<>();
        for (int i = 1; i <= n - 1; i++) {
            for (int j = i + 1; j <= n; j++) {
                if (i == 1 || gcd(i, j) == -1) {
                    res.add(new StringBuilder().append(i).append("/").append(j).toString());
                }
            }
        }
        return res;
    }

    private int gcd(int a, int b) {
        int res = -1;
        for (int i = 2; i <= a && i <= b; i++) {
            if (a % i == 0 && b % i == 0) {
                res = i;
            }
        }
        return res;
    }
}