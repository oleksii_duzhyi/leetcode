package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Нелла on 16.11.2019.
 */
public class E0013RomanToInt {
    private static final Map<Character, Integer> roman = new HashMap<>();

    static {
        roman.put('I', 1);
        roman.put('V', 5);
        roman.put('X', 10);
        roman.put('L', 50);
        roman.put('C', 100);
        roman.put('D', 500);
        roman.put('M', 1000);
    }

    public int romanToInt(String s) {
        int res = 0;
        char[] chars = s.toCharArray();
        int prev = Integer.MAX_VALUE;
        for (int i = 0; i < s.length(); i++) {
            int cur = roman.get(chars[i]);
            res += roman.get(chars[i]);
            if (prev < cur) {
                res -= prev * 2;
            }
            prev = cur;
        }
        return res;
    }

// queation:
// is the number valid?

    public int romanToIntOld(String s) {
        Map<Character, Integer> mappings = new HashMap<>();
        mappings.put('I', 1);
        mappings.put('V', 5);
        mappings.put('X', 10);
        mappings.put('L', 50);
        mappings.put('C', 100);
        mappings.put('D', 500);
        mappings.put('M', 1000);

        int prev = mappings.get(s.charAt(0));
        int sum = prev;
        for (int i = 1; i < s.length(); i++) {
            int cur = mappings.get(s.charAt(i));
            if (cur / prev >= 5) {
                sum -= prev * 2;
            }
            sum += cur;
            prev = cur;
        }
        return sum;
    }
}


// create a map with mapping of roman to int
// iterate over string
//      for every character - add correcponding value
//      if next > prev - substract 2 previous (account for 1 added and 1 neede to be substracted)
// return res