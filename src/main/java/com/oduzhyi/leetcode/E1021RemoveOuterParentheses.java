package com.oduzhyi.leetcode;

public class E1021RemoveOuterParentheses {
    public String removeOuterParentheses(String s) {
        if (s == null || s.length() <= 2) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        int cnt = 1;
        for (int i = 1; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == ')') {
                cnt--;
            } else {
                cnt++;
            }
            if (cnt != 0) {
                sb.append(c);
            } else {
                cnt = 1;
                i++;
            }
        }

        return sb.toString();
    }
}

// questions:
// is S always a valid string?
// do we have to validate it?
//
// lets assume S is valid and every substring is valid also, this means - valid subsctring ends
// once number of open ( equals number of close )
// thus we can skip first ( count till we reach  last ) and not include it proceeding to next char