package com.oduzhyi.leetcode;

import java.util.LinkedList;

public class E0617MergeTwoBinaryTrees {
    public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
        if (t1 == null) {
            return t2;
        }
        if (t2 == null) {
            return t1;
        }
        t1.val += t2.val;
        t1.left = mergeTrees(t1.left, t2.left);
        t1.right = mergeTrees(t1.right, t2.right);
        return t1;
    }

    public TreeNode mergeTreesLoop(TreeNode t1, TreeNode t2) {
        if (t1 == null) {
            return t2;
        }
        if (t2 == null) {
            return t1;
        }
        LinkedList<Pair> stack = new LinkedList<>();
        stack.push(new Pair(t1, t2));
        while (!stack.isEmpty()) {
            Pair p = stack.pop();

            p.t1.val += p.t2.val;
            if (p.t1.right != null && p.t2.right != null) {
                stack.push(new Pair(p.t1.right, p.t2.right));
            }
            if (p.t1.left != null && p.t2.left != null) {
                stack.push(new Pair(p.t1.left, p.t2.left));
            }
            if (p.t1.left == null) {
                p.t1.left = p.t2.left;
            }
            if (p.t1.right == null) {
                p.t1.right = p.t2.right;
            }
        }
        return t1;
    }

    public class Pair {
        TreeNode t1;
        TreeNode t2;

        public Pair(TreeNode t1, TreeNode t2) {
            this.t1 = t1;
            this.t2 = t2;
        }
    }
}

// questions
// can this be made in place? or new tree has to be created?
// can one tree be empty?
// how high is the tree? should I worry for stack overflow in case of recurssion solution?

//