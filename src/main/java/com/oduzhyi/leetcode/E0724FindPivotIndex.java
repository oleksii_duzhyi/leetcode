package com.oduzhyi.leetcode;

public class E0724FindPivotIndex {
    public int pivotIndex(int[] nums) {
        int sum = 0;
        for (int n : nums) {
            sum += n;
        }

        int left = 0;
        for (int i = 0; i < nums.length; i++) {
            if (left == sum - left - nums[i]) {
                return i;
            }
            left += nums[i];
        }
        return -1;
    }

    public int pivotIndexAdditionalSpace(int[] nums) {
        if (nums.length <= 1) {
            return nums.length - 1;
        }
        int l = nums.length;
        int[] prefix = new int[l + 1];
        int[] suffix = new int[l + 1];

        // p [0, 10, 10]
        // s [10, 0, 0]

        for (int i = 0; i < nums.length; i++) {  // 0
            prefix[i + 1] = prefix[i] + nums[i];
            suffix[l - i - 1] = suffix[l - i] + nums[l - i - 1];
        }
        for (int i = 0; i < nums.length; i++) {
            if (prefix[i] == suffix[i + 1]) {
                return i;
            }
        }
        return -1;
    }
}

// idea - I need to knwo  the sum of all elements to the left and the sum of all elements to the right
// minus current element - if they are equal - return i
// to do so I need to calculate sum of all  elements
// then iterate over the list
// if leftsum == sum - left - current number -> return index
// if such index is not found -> return -1 after loop ends