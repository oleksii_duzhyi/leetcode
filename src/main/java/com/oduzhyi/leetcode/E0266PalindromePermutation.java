package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class E0266PalindromePermutation {

    public boolean canPermutePalindrome(String s) {
        int[] hash = new int[128];
        for (char c : s.toCharArray()) {
            hash[c]++;
        }

        int cnt = 0;
        for (int occ : hash) {
            cnt += occ % 2;
        }
        return cnt <= 1;
    }

    public boolean canPermutePalindromeHMBetter(String s) {
        Map<Character, Integer> charCnt = new HashMap<>();
        for (char c : s.toCharArray()) {
            charCnt.put(c, charCnt.getOrDefault(c, 0) + 1);
        }

        int cnt = 0;
        for (Map.Entry<Character, Integer> entry : charCnt.entrySet()) {
            cnt += entry.getValue() % 2;
        }
        return cnt <= 1;
    }

    public boolean canPermutePalindromeHM(String s) {
        Map<Character, Integer> charCnt = new HashMap<>();
        for (char c : s.toCharArray()) {
            charCnt.put(c, charCnt.getOrDefault(c, 0) + 1);
        }

        boolean allowOdd = s.length() % 2 == 1;
        for (Map.Entry<Character, Integer> entry : charCnt.entrySet()) {
            if (entry.getValue() % 2 == 1) {
                if (allowOdd) {
                    allowOdd = false;
                } else {
                    return false;
                }
            }
        }
        return true;
    }
}

//If a string with an even length is a palindrome, every character in the string must always occur an even number of times.
//If the string with an odd length is a palindrome, every character except one of the characters must always occur an even number of times.