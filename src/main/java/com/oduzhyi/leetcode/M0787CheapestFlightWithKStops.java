package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

public class M0787CheapestFlightWithKStops {
    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int k) {
        Map<Integer, List<DestWithPrice>> verts = new HashMap<>();
        for (int[] flight : flights) {
            verts.computeIfAbsent(flight[0], ArrayList::new).add(new DestWithPrice(flight[1], flight[2], k));
        }

        boolean found = false;
        DestWithPrice last = null;
        PriorityQueue<DestWithPrice> queue = new PriorityQueue<>(verts.getOrDefault(src, Collections.emptyList()));
        Set<Integer> visited = new HashSet<>();
        visited.add(src);

        while (!queue.isEmpty() && !found) {
            DestWithPrice next = queue.poll();
            if (!visited.contains(next.dest)) {
                visited.add(src);
                if (next.dest == dst) {
                    found = true;
                } else if (next.stopsLeft > 0) {
                    for (DestWithPrice dwp : verts.getOrDefault(next.dest, Collections.emptyList())) {
                        queue.offer(new DestWithPrice(dwp.dest, dwp.price + next.price,
                                next.stopsLeft - 1));
                    }
                }
            }
            last = next;
        }
        return found ? last.price : -1;
    }

    class DestWithPrice implements Comparable<DestWithPrice> {
        int dest;
        int price;
        int stopsLeft;

        DestWithPrice(int dest, int price, int stopsLeft) {
            this.dest = dest;
            this.price = price;
            this.stopsLeft = stopsLeft;
        }

        public int compareTo(DestWithPrice o) {
            return Integer.compare(price, o.price);
        }
    }
}