package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class H0140WordBreak2 {
    HashMap<Integer, List<String>> map = new HashMap<>();
    List<List<String>> resOff = new ArrayList<>();
    int maxL = 0;

    public static void main(String[] args) {
        new H0140WordBreak2().wordBreak("andcats", Arrays.asList("cats", "ca", "ts", "and"));
    }

    public List<String> wordBreak(String s, List<String> wordDict) {
        int maxL = -1;
        for (String word : wordDict) {
            if (word.length() > maxL) {
                maxL = word.length();
            }
        }
        List<String> strings = word_Break(s, new HashSet<>(wordDict), 0, maxL);
        return strings;
    }

    public List<String> word_Break(String s, Set<String> wordDict, int start, int maxL) {
        if (map.containsKey(start)) {
            return map.get(start);
        }
        List<String> res = new ArrayList<>();
        if (start == s.length()) {
            res.add("");
            return res;
        }
        for (int end = start + 1; end <= s.length() && (end - start) <= maxL; end++) {
            if (wordDict.contains(s.substring(start, end))) {
                List<String> list = word_Break(s, wordDict, end, maxL);
                for (String l : list) {
                    res.add(s.substring(start, end) + (l.equals("") ? "" : " ") + l);
                }
            }
        }
        map.put(start, res);
        return res;
    }

    public List<String> wordBreakOld(String s, List<String> wordDict) {
        for (String word : wordDict) {
            if (word.length() > maxL) {
                maxL = word.length();
            }
        }
        wordBreak(s, new HashSet<>(wordDict), 0, new ArrayList<>());
        List<String> res = new ArrayList<>();
        for (List<String> parts : resOff) {
            StringBuilder sb = new StringBuilder();
            for (String part : parts) {
                sb.append(part).append(" ");
            }
            sb.deleteCharAt(sb.length() - 1);
            res.add(sb.toString());
        }
        return res;
    }

    private void wordBreak(String s, Set<String> words, int start, List<String> oneRes) {
        if (start == s.length()) {
            resOff.add(oneRes);
            return;
        }

        for (int end = start + 1; end <= s.length() && (end - start) <= maxL; end++) {
            if (words.contains(s.substring(start, end))) {
                List<String> newRes = new ArrayList<>(oneRes);
                newRes.add(s.substring(start, end));
                wordBreak(s, words, end, newRes);
            }
        }
    }
}
