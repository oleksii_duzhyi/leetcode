package com.oduzhyi.leetcode;

public class H0045JumpGameII {
    public int jump(int[] nums) {
        int start = 0;
        int jumps = 0;
        while (start < nums.length - 1) {
            int nextS = start + nums[start];
            if (nextS >= nums.length - 1) {
                return jumps + 1;
            }

            int max = nextS + nums[start + nums[start]];
            for (int i = start + 1; i < nums.length && i < start + 1 + nums[start]; i++) {
                if (start + nums[i] + (i - start) > max) {
                    nextS = i;
                    max = start + nums[i] + (i - start);
                }
            }
            start = nextS;
            jumps++;
        }
        return jumps;
    }

}
