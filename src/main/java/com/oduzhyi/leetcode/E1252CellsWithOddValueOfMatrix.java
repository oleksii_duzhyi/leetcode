package com.oduzhyi.leetcode;

public class E1252CellsWithOddValueOfMatrix {
    public int oddCells(int n, int m, int[][] indices) {
        if ((n == 0 && m == 0) || indices.length == 0) {
            return 0;
        }

        int[] rowP = new int[n];
        int[] colP = new int[m];
        for (int[] index : indices) {
            rowP[index[0]]++;
            colP[index[1]]++;
        }

        int res = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if ((rowP[i] + colP[j]) % 2 == 1) {
                    res++;
                }
            }
        }
        return res;
    }

    public int oddCellsOld(int n, int m, int[][] indices) {
        if (indices.length == 1) {
            return n + m - 2;
        }
        boolean[] oddRow = new boolean[n];
        boolean[] oddCel = new boolean[m];
        for (int[] idx : indices) {
            oddRow[idx[0]] ^= true;
            oddCel[idx[1]] ^= true;
        }

        int cnt = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                cnt += oddRow[i] ^ oddCel[j] ? 1 : 0;
            }
        }
        return cnt;
    }
}

// questions:
// is indecies always present?
// are n and m are valid
// does N*M fit into the memory?

// n = 2, m =3, i = [[0,1], [1,1]]
// oR = [f,f] , oC = [f,f,f]
// oR = [t,f] , oC = [f,t,f]
// oR = [t,t] , oC = [f,f,f]
//
