package com.oduzhyi.leetcode;

import java.util.ArrayDeque;

public class M0286WallsAndGates {
    public void wallsAndGates(int[][] rooms) {
        if (rooms.length == 0 || rooms[0].length == 0) {
            return;
        }
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        int rows = rooms.length;
        int cols = rooms[0].length;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (rooms[i][j] == 0) {
                    queue.add(i * cols + j);
                }
            }
        }
        int[] idx = new int[]{-1, 1};

        int dist = 1;
        while (!queue.isEmpty()) {
            int size = queue.size();

            for (int i = 0; i < size; i++) {
                Integer cur = queue.poll();
                int r = cur / cols;
                int c = cur % cols;

                for (int index : idx) {
                    if (r + index >= 0 && r + index < rows && rooms[r + index][c] == Integer.MAX_VALUE) {
                        rooms[r + index][c] = dist;
                        queue.add((r + index) * cols + c);
                    }
                    if (c + index >= 0 && c + index < cols && rooms[r][c + index] == Integer.MAX_VALUE) {
                        rooms[r][c + index] = dist;
                        queue.add(r * cols + c + index);
                    }
                }
            }
            dist++;
        }
    }
}
