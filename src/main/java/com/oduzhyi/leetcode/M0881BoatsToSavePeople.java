package com.oduzhyi.leetcode;

import java.util.Arrays;

public class M0881BoatsToSavePeople {
    public int numRescueBoats(int[] people, int capacity) {
        Arrays.sort(people);
        int left = 0;  // 1
        int right = people.length - 1;  // 0

        int trips = 0;
        while (left <= right) {
            if (people[left] + people[right] <= capacity) {
                left++;
            }
            right--;
            trips++;
        }

        return trips;

    }
}
// sort and use 2 pointers