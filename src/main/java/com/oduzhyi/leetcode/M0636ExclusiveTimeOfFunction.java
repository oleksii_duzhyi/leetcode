package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

public class M0636ExclusiveTimeOfFunction {
    public int[] exclusiveTime(int n, List<String> logs) {
        int[] res = new int[n];
        Deque<Log> stack = new ArrayDeque<>();

        for (String log : logs) {
            String[] parts = log.split("[:]");
            Log l = new Log(Integer.parseInt(parts[0]), parts[1].equals("start"), Integer.parseInt(parts[2]));
            if (l.start) {
                stack.push(l);
            } else {
                Log s = stack.pop();
                int dur = l.duration - s.duration + 1 - s.offset;
                if (!stack.isEmpty()) {
                    stack.peek().offset += dur + s.offset;
                }
                res[s.id] += dur;
            }
        }


        return res;
    }

}

class Log {
    int id;
    boolean start = false;
    int duration;
    int offset = 0;

    public Log(int id, boolean start, int duration) {
        this.id = id;
        this.start = start;
        this.duration = duration;
    }
}

// use stacks and transform log line to something meaningfull
// if it is  the start - push the  command to stack
// if end - calcualte duration, end - start + 1 - offset(other functions calls)
//          if  stack is not empty? - update the offset of the top element by current duration and current offset
// add to res[id] calculated duration
