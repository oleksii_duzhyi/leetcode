package com.oduzhyi.leetcode;

public class E0896IsMonotonic {
    public boolean isMonotonic(int[] A) {
        if (A.length <= 1) {
            return true;
        }
        int i = 0;
        boolean increasing = A[0] < A[A.length - 1];
        while (i < A.length - 1) {
            if ((increasing && A[i] > A[i + 1]) || (!increasing && A[i] < A[i + 1])) {
                return false;
            }
            i++;
        }
        return true;
    }

    public boolean isMonotonicMy(int[] A) {
        int i = 0;
        Boolean increasing = null;
        while (i < A.length - 1) {
            if (increasing == null) {
                if (A[i] != A[i + 1]) {
                    increasing = A[i] < A[i + 1];
                }
            } else {
                if ((increasing && A[i] > A[i + 1]) || (!increasing && A[i] < A[i + 1])) {
                    return false;
                }
            }
            i++;
        }
        return true;
    }
}
