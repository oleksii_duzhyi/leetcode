package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class M0347TopKFrequentElements {
    public int[] topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> counts = new HashMap<>();
        for (int n : nums) {
            counts.put(n, counts.getOrDefault(n, 0) + 1);
        }
        int max = Collections.max(counts.values());

        List<List<Integer>> buckets = new ArrayList<>(max + 1);
        for (int i = 0; i < max + 1; i++) {
            buckets.add(new ArrayList<>());
        }

        for (Map.Entry<Integer, Integer> entry : counts.entrySet()) {
            buckets.get(entry.getValue()).add(entry.getKey());
        }
        int[] res = new int[k];
        for (int i = max, j = 0; j < k; i--) {
            List<Integer> oneB = buckets.get(i);
            for (int n : oneB) {
                res[j++] = n;
            }
        }
        return res;
    }
}
