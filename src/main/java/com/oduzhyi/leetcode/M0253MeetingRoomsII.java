package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

public class M0253MeetingRoomsII {
    public int minMeetingRoomsOld(int[][] intervals) {
        if (intervals.length == 0) {
            return 0;
        }
        int[] starts = new int[intervals.length];
        int[] ends = new int[intervals.length];
        int i = 0;
        for (int[] meet : intervals) {
            starts[i] = meet[0];
            ends[i++] = meet[1];
        }
        Arrays.sort(starts);
        Arrays.sort(ends);

        int maxMeetings = 0;
        int s = 0;
        int e = 0;
        while (s < intervals.length && e < intervals.length) {
            if (starts[s] >= ends[e]) {
                e++;
                maxMeetings--;
            }

            s++;
            maxMeetings = Math.max(maxMeetings, maxMeetings + 1);
        }

        return maxMeetings;
    }


    public int minMeetingRoomsMinHeap(int[][] intervals) {
        if (intervals.length == 0) {
            return 0;
        }
        Arrays.sort(intervals, Comparator.comparingInt(i -> i[0]));

        PriorityQueue<Integer> ongoingMeetings = new PriorityQueue<>();
        int maxMettings = 0;
        for (int i = 0; i < intervals.length; i++) {
            while (!ongoingMeetings.isEmpty() && ongoingMeetings.peek() <= intervals[i][0]) {
                ongoingMeetings.poll();
            }
            ongoingMeetings.offer(intervals[i][1]);
            maxMettings = Math.max(maxMettings, ongoingMeetings.size());
        }

        return maxMettings;
    }

    public int minMeetingRoomsNonOptimized(int[][] intervals) {
        if (intervals.length == 0) {
            return 0;
        }
        Arrays.sort(intervals, Comparator.comparingInt(i -> i[0]));

        List<int[]> ongoingMeetings = new ArrayList<>();
        int maxMettings = 0;
        for (int i = 0; i < intervals.length; i++) {
            Iterator<int[]> iter = ongoingMeetings.iterator();
            while (iter.hasNext()) {
                if (iter.next()[1] <= intervals[i][0]) {
                    iter.remove();
                }
            }
            ongoingMeetings.add(intervals[i]);
            maxMettings = Math.max(maxMettings, ongoingMeetings.size());
        }

        return maxMettings;
    }

    public int minMeetingRoomsN2(int[][] intervals) {
        if (intervals.length == 0) {
            return 0;
        }
        Arrays.sort(intervals, Comparator.comparingInt(i -> i[0]));

        int maxRooms = 1;
        for (int i = intervals.length - 1; i >= 0; i--) {
            int roomsNeeded = 1;
            int j = i - 1;
            while (j >= 0) {
                if (intervals[i][0] < intervals[j--][1]) {
                    roomsNeeded++;
                }
            }
            maxRooms = Math.max(maxRooms, roomsNeeded);
        }

        return maxRooms;
    }

    public int minMeetingRooms(int[][] intervals) {
        if (intervals.length <= 1) {
            return intervals.length;
        }

        int[] starts = new int[intervals.length];
        int[] ends = new int[intervals.length];
        for (int i = 0; i < intervals.length; i++) {
            starts[i] = intervals[i][0];
            ends[i] = intervals[i][1];
        }
        Arrays.sort(starts);
        Arrays.sort(ends);

        int maxRooms = 1;
        int usedRooms = 0;
        int startP = 0;
        int endP = 0;

        while (startP < intervals.length) {
            while (starts[startP] >= ends[endP]) {
                usedRooms--;
                endP++;
            }
            usedRooms++;
            maxRooms = Math.max(maxRooms, usedRooms);
            startP++;
        }
        return maxRooms;
    }
    // collect starts and ends into 2 arrays and sort them
    // create 2 pointers @ the beginning of every array
    // iterate through the starts
    // while end of the first meeting is smaller than next mmeeting start
    // reduce used rooms and increment endP
    // icrease used rooms, startP and update max result

    public int minMeetingRoomsPq(int[][] intervals) {
        if (intervals.length <= 1) {
            return intervals.length;
        }

        Arrays.sort(intervals, Comparator.comparingInt(i -> i[0]));
        PriorityQueue<int[]> active = new PriorityQueue<>(Comparator.comparingInt(i -> i[1]));

        int max = 1;
        for (int[] meeting : intervals) {
            while (!active.isEmpty() && active.peek()[1] <= meeting[0]) {
                active.poll();
            }
            active.add(meeting);
            max = Math.max(active.size(), max);
        }
        return max;
    }

    // sort the meetings by start
    // create a pq sorting by the meetings end
    // for every meeting
    //      if pq not empty and while top element end  smaller or equal to next meeting start
    //          poll from queue
    //      add to queue this meeting
    //      update max with max(max, pq size)
}
