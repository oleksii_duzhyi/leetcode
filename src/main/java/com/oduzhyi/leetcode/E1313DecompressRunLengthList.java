package com.oduzhyi.leetcode;

public class E1313DecompressRunLengthList {
    public int[] decompressRLElist(int[] nums) {
        //[1,2,3,4]
        if (nums.length == 0) {
            return new int[]{};
        }
        int newSize = 0;
        for (int i = 0; i < nums.length; i += 2) {
            newSize += nums[i];
        }

        int[] res = new int[newSize];
        int pointer = 0;
        for (int i = 0; i < nums.length; i += 2) {
            for (int j = 0; j < nums[i]; j++) {
                res[pointer++] = nums[i + 1];
            }
        }
        return res;
    }
}

// Questions
// what is the input size?
// are we bounded by size? Can the ArrayList be used?
// does nums are valid? (negative occurancies, odd size)
// can there be 0 occurencies?
//
// The more memory efficient approach will be the following
// iterate over 2*i and find result array size
// create result array placeholder
// iterate over 2*i + 1 and fill previously created array based on freq in 2 * i
//
// Alternatively we can use an Array list, but the internal implementation of Array list
// does resizing when list capacity has ended - doubling the size;
// and toArray method = will return a new array copying values
// meaning this will be not only less memery efficient but a bit longer as well
// due to resizing, and copying values;