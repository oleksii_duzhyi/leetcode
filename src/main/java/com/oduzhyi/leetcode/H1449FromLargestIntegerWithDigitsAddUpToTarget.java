package com.oduzhyi.leetcode;

import java.util.Arrays;

public class H1449FromLargestIntegerWithDigitsAddUpToTarget {

    public static void main(String[] args) {//
        //[6,10,15,40,40,40,40,40,40]
        //47
        //[4,3,2,5,6,7,2,5,5]
        //9
        int target = 9;
        int[] cost = new int[]{4, 3, 2, 5, 6, 1, 2, 5, 5};
        System.out.println(new H1449FromLargestIntegerWithDigitsAddUpToTarget().largestNumber(cost, target));
        System.out.println(new H1449FromLargestIntegerWithDigitsAddUpToTarget().largestNumberMy(cost, target));
    }

    public String largestNumber(int[] cost, int target) {
        int[] dp = new int[target + 1];
        for (int t = 1; t <= target; ++t) {
            dp[t] = -10000;
            for (int i = 0; i < 9; ++i) {
                if (t >= cost[i]) {
                    dp[t] = Math.max(dp[t], 1 + dp[t - cost[i]]);
                }
            }
        }
        if (dp[target] < 0) {
            return "0";
        }
        StringBuilder res = new StringBuilder();
        for (int i = 8; i >= 0; --i) {
            while (target >= cost[i] && dp[target] == dp[target - cost[i]] + 1) {
                res.append(1 + i);
                target -= cost[i];
            }
        }
        return res.toString();
    }

    public String largestNumberMy(int[] cost, int target) {
        String[] dp = new String[target + 1];
        Arrays.fill(dp, "");
        for (int i = 1; i < dp.length; i++) {
            dp[i] = "";
            for (int j = 8; j >= 0; j--) {
                if (cost[j] <= i) {
                    String ex = dp[i];
                    String newV = (j + 1) + dp[i - cost[j]];
                    if (total(cost, newV) == i
                            && (newV.length() > ex.length() || (newV.length() == ex.length() && newV.compareTo(ex) > 0))) {
                        dp[i] = newV;
                    }
                }
            }
        }

        String max = dp[target];
        if (max.isEmpty()) {
            return "0";
        }
        return max;
    }

    private int total(int[] cost, String s) {
        int tot = 0;
        for (char c : s.toCharArray()) {
            tot += cost[c - '1'];
        }
        return tot;
    }
}

// it looks like 0-1 knapsack problem with a little twist
// instead of finding maximum sum - I need to find maximum width
//