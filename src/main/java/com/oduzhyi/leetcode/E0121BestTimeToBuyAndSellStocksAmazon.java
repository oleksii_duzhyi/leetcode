package com.oduzhyi.leetcode;

public class E0121BestTimeToBuyAndSellStocksAmazon {

    // [2, 3, 4, 1]
    public int maxProfit(int[] prices) {
        if (prices.length == 0) {
            return 0;
        }
        int profit = 0;
        int buy = prices[0]; // 2

        for (int i = 1; i < prices.length; i++) {
            if (buy > prices[i]) {
                buy = prices[i]; // 1
            } else {
                profit = Math.max(profit, prices[i] - buy); // 7
            }
        }

        return profit;
    }

    public int maxProfitDaniil(int[] prices) {
        int minPrice = Integer.MAX_VALUE;
        int maxProfit = 0;
        for (int i = 0; i < prices.length; i++) {
            if (prices[i] < minPrice) {
                minPrice = prices[i];
            }
            int profit = prices[i] - minPrice;
            if (profit > maxProfit) {
                maxProfit = profit;
            }
        }
        return maxProfit;
    }
}

//the most obvious solution is n^2 - find max between i and [i+1, n] , where i  [0, n-1]
// but we can do it in 1 go
// first we are starting with 0 and moving left remembering max seen profit
// [2,4,5 ...]
// in this case our buy is 0 and sell is 2 with profit of 3
// but what if we encounter
// [2,4,5,1..]
// this a potential buy day - if the price will go high enough later on
// [2,4,5,1,9]
// it is guaranteed that the profit will be higher,
// if stock doesn't go to at lease the same number - we still remember maximum profit gained
