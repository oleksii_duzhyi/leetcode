package com.oduzhyi.leetcode;

public class H0042TrappingRainWater {
    public int trap(int[] a) {
        if (a.length < 3) {
            return 0;
        }

        int l = 0;
        int r = a.length - 1;
        int lH = 0;
        int rH = 0;
        int trapped = 0;
        while (l < r) {
            if (a[l] < a[r]) {
                trapped += Math.max(0, lH - a[l]);
                lH = Math.max(lH, a[l]);
                l++;
            } else {
                trapped += Math.max(0, rH - a[r]);
                rH = Math.max(rH, a[r]);
                r--;
            }

        }
        return trapped;
    }
}

// Initialize \text{left}left pointer to 0 and \text{right}right pointer to size-1
// While \text{left}< \text{right}left<right, do:
// If \text{height[left]}height[left] is smaller than \text{height[right]}height[right]
// If \text{height[left]} \geq \text{left\_max}height[left]≥left_max, update \text{left\_max}left_max
// Else add \text{left\_max}-\text{height[left]}left_max−height[left] to \text{ans}ans
// Add 1 to \text{left}left.
// Else
// If \text{height[right]} \geq \text{right\_max}height[right]≥right_max, update \text{right\_max}right_max
// Else add \text{right\_max}-\text{height[right]}right_max−height[right] to \text{ans}ans
// Subtract 1 from \text{right}right.
