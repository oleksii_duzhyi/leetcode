package com.oduzhyi.leetcode;

public class E1304NUniqueSumToZero {
    public int[] sumZero(int n) {
        if (n == 1) {
            return new int[1];
        }
        int[] res = new int[n];
        int max = n / 2;
        for (int i = 0, next = -max; i < n && next <= max; i++, next++) {
            if (next == 0) {
                next++;
            }
            res[i] = next;
        }
        if (n % 2 == 1) {
            res[n - 1] = 0;
        }
        return res;
    }
}