package com.oduzhyi.leetcode;

public class E0160IntersectionOf2LinkedList {
    public ListNode getIntersectionNode(ListNode l1, ListNode l2) {
        int s1 = size(l1);
        int s2 = size(l2);

        while (s1 > 0 && s2 > 0 && s1 != s2) {
            if (s1 > s2) {
                s1--;
                l1 = l1.next;
            } else {
                s2--;
                l2 = l2.next;
            }
        }

        while (l1 != null && l2 != null) {
            if (l1 == l2) {
                return l1;
            }
            l1 = l1.next;
            l2 = l2.next;
        }
        return null;
    }

    private int size(ListNode l1) {
        int size = 0;
        while (l1 != null) {
            size++;
            l1 = l1.next;
        }
        return size;
    }
}