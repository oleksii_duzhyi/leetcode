package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

public class M0946EmulateStackBehavior {
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        Deque<Integer> stack = new ArrayDeque<>();

        int i = 0;
        int j = 0;
        while (i < pushed.length || j < popped.length) {
            if (stack.isEmpty()) {
                stack.push(pushed[i++]);
            } else {
                if (stack.peek() == popped[j]) {
                    stack.pop();
                    j++;
                } else if (i < pushed.length) {
                    stack.push(pushed[i++]);
                } else {
                    return false;
                }
            }
        }
        return true;
    }
}

// emulate the stack behavior
