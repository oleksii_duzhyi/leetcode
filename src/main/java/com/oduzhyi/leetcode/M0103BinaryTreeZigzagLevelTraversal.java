package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class M0103BinaryTreeZigzagLevelTraversal {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        zigzag(root, res, 1);
        return res;
    }

    private void zigzag(TreeNode tn, List<List<Integer>> res, int level) {
        if (tn == null) {
            return;
        }
        if (res.size() < level) {
            res.add(new LinkedList<>());
        }
        LinkedList<Integer> inner = (LinkedList) res.get(level - 1);
        if ((level) % 2 == 0) {
            inner.addFirst(tn.val);
        } else {
            inner.addLast(tn.val);
        }
        zigzag(tn.left, res, level + 1);
        zigzag(tn.right, res, level + 1);
    }
}