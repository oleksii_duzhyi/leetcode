package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.LinkedList;

public class H0297SerDeBinaryTree {
    private int pointer = 0;

    public String serialize(TreeNode root) {
        StringBuilder res = new StringBuilder();
        serialize(root, res);
        res.deleteCharAt(res.length() - 1);
        return res.toString();
    }

    private void serialize(TreeNode root, StringBuilder sb) {
        if (root != null) {
            sb.append(root.val).append(",");
            serialize(root.left, sb);
            serialize(root.right, sb);
        } else {
            sb.append(",");
        }
    }

    public TreeNode deserialize(String data) {
        String[] parts = data.split(",");
        TreeNode root = deserialize(parts);
        return root;
    }

    private TreeNode deserialize(String[] parts) {
        if (pointer >= parts.length || parts[pointer].isEmpty()) {
            return null;
        }
        TreeNode tn = new TreeNode(Integer.parseInt(parts[pointer++]));
        tn.left = deserialize(parts);
        pointer++;
        tn.right = deserialize(parts);
        return tn;
    }

    // Encodes a tree to a single string.
    public String serializeBFS(TreeNode root) {
        if (root == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        int depth = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode tn = queue.poll();
                if (tn != null) {
                    sb.append(tn.val);
                    queue.add(tn.left);
                    queue.add(tn.right);
                }
                sb.append(",");
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserializeBFS(String data) {
        String[] parts = data.split(",");
        if (parts.length == 1 && parts[0].isEmpty()) {
            return null;
        }
        TreeNode root = new TreeNode(Integer.parseInt(parts[0]));

        ArrayDeque<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);

        int i = 1;
        while (i < parts.length) {
            TreeNode curr = queue.poll();
            curr.left = parts[i].isEmpty() ? null : new TreeNode(Integer.parseInt(parts[i]));
            if (i + 1 < parts.length) {
                curr.right = parts[i + 1].isEmpty() ? null : new TreeNode(Integer.parseInt(parts[i + 1]));
            }
            if (curr.left != null) {
                queue.add(curr.left);
            }

            if (curr.right != null) {
                queue.add(curr.right);
            }
            i += 2;
        }
        return root;
    }
}
// We can serialize/deserialize tree with either DFS and/or BFS