package com.oduzhyi.leetcode;

public class E1370IncreasingDecreasingString {
    public String sortString(String s) {
        int[] letters = new int[26];
        for (char c : s.toCharArray()) {
            letters[c - 'a']++;
        }

        StringBuilder sb = new StringBuilder();
        int cnt = s.length();
        while (cnt > 0) {
            for (int i = 0; i < letters.length; i++) {
                if (letters[i] > 0) {
                    sb.append((char) (i + 'a'));
                    cnt--;
                    letters[i]--;
                }
            }
            for (int i = letters.length - 1; i >= 0; i--) {
                if (letters[i] > 0) {
                    sb.append((char) (i + 'a'));
                    cnt--;
                    letters[i]--;
                }
            }
        }
        return sb.toString();
    }
}