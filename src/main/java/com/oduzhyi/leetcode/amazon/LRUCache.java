package com.oduzhyi.leetcode.amazon;

import java.util.HashMap;
import java.util.Map;

public class LRUCache {


    private Node head = new Node(-1, -1);
    private Node tail = new Node(-1, -1);
    private Map<Integer, Node> map = new HashMap<>();
    private int maxSize;
    public LRUCache(int capacity) {
        this.maxSize = capacity;
        head.next = tail;
        tail.prev = head;
    }

    public static void main(String[] args) {
        LRUCache lruCache = new LRUCache(2);
        lruCache.put(1, 1);
        lruCache.put(2, 2);
        lruCache.put(3, 3);
        lruCache.put(4, 4);
        System.out.println(lruCache.get(4));
        System.out.println(lruCache.get(3));
        System.out.println(lruCache.get(2));
        System.out.println(lruCache.get(1));
//        lruCache.put(3, 3);
//        System.out.println(lruCache.get(2));
//        lruCache.put(4, 1);
//        System.out.println(lruCache.get(2));
//        System.out.println(lruCache.get(1));
//        lruCache.put(5, 5);
//        System.out.println(lruCache.get(1));
//        System.out.println(lruCache.get(2));
//        System.out.println(lruCache.get(3));
//        System.out.println(lruCache.get(4));
        System.out.println(lruCache.get(5));


    }

    public int get(int key) {
        Node n = getNode(key);
        return n != null ? n.value : -1;
    }

    private Node getNode(int key) {
        if (map.containsKey(key)) {
            Node cur = map.get(key);

            // taking the node from it's place
            Node prev = cur.prev;
            Node next = cur.next;
            prev.next = next;
            next.prev = prev;

            Node lru = head.next;
            head.next = cur;
            cur.next = lru;
            lru.prev = cur;
            cur.prev = head;
            return cur;
        }
        return null;
    }

    public void put(int key, int value) {
        if (map.containsKey(key)) {
            Node node = getNode(key);
            node.value = value;
        } else {
            if (map.size() + 1 > maxSize) {
                Node last = tail.prev;
                Node prev = last.prev;

                tail.prev = prev;
                prev.next = tail;
                map.remove(last.key);
            }
            Node lru = head.next;
            Node newNode = new Node(key, value);
            head.next = newNode;
            newNode.next = lru;
            lru.prev = newNode;
            newNode.prev = head;
            map.put(key, newNode);
        }
    }

    private static class Node {
        private Node prev;
        private Node next;
        private int key;
        private int value;

        private Node(int key, int value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return "{" +
                    "key=" + key +
                    ", value=" + value +
                    '}';
        }
    }
}
