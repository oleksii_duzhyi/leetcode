package com.oduzhyi.leetcode.amazon.search;

import java.util.*;

/**
 * Created by Нелла on 25.12.2019.
 */
public class HProblem675CutTrees {
    //    [[1,2,3],[0,0,4],[7,6,5]]
    public static void main(String[] args) {
        System.out.println(
                new Solution().cutOffTree(
                        new ArrayList<List<Integer>>() {{
                            add(new ArrayList<Integer>() {{
                                add(8);
                                add(2);
                                add(3);
                            }});
                            add(new ArrayList<Integer>() {{
                                add(0);
                                add(0);
                                add(4);
                            }});
                            add(new ArrayList<Integer>() {{
                                add(7);
                                add(6);
                                add(5);
                            }});

                        }}
                )
        );
    }

    static class Solution {
        public int cutOffTree(List<List<Integer>> forest) {
            PriorityQueue<TL> treesToCut = new PriorityQueue<>();

            for (int i = 0; i < forest.size(); i++) {
                for (int j = 0; j < forest.get(0).size(); j++) {
                    if (forest.get(i).get(j) != 0 && forest.get(i).get(j) != 1) {
                        treesToCut.offer(new TL(i, j, forest.get(i).get(j)));
                    }
                }
            }

            int myR = 0, myC = 0;
            int step = 0;
            while (!treesToCut.isEmpty()) {
                TL next = treesToCut.poll();
                int n = dist(myR, myC, next.row, next.col, forest);
                if (n < 0) {
                    return n;
                } else {
                    step += n;
                }
                myR = next.row;
                myC = next.col;
            }

            return step;
        }

        public int dist(int frR, int frC, int tR, int tC, List<List<Integer>> forest) {
            Queue<TL> next = new LinkedList<>();
            next.offer(new TL(frR, frC, 0));
            boolean[][] seen = new boolean[forest.size()][forest.get(0).size()];
            seen[frR][frC] = true;
            while (!next.isEmpty()) {
                TL cur = next.poll();
                if (cur.row == tR && cur.col == tC) {
                    return cur.tree;
                } else {
                    if (cur.row - 1 >= 0 && !seen[cur.row - 1][cur.col] && forest.get(cur.row - 1).get(cur.col) != 0) {
                        next.offer(new TL(cur.row - 1, cur.col, cur.tree + 1));
                        seen[cur.row - 1][cur.col] = true;
                    }
                    if (cur.row + 1 < forest.size() && !seen[cur.row + 1][cur.col] && forest.get(cur.row + 1).get(cur.col) != 0) {
                        next.offer(new TL(cur.row + 1, cur.col, cur.tree + 1));
                        seen[cur.row + 1][cur.col] = true;
                    }

                    if (cur.col - 1 >= 0 && !seen[cur.row][cur.col - 1] && forest.get(cur.row).get(cur.col - 1) != 0) {
                        next.offer(new TL(cur.row, cur.col - 1, cur.tree + 1));
                        seen[cur.row][cur.col - 1] = true;
                    }
                    if (cur.col + 1 < forest.get(0).size() && !seen[cur.row][cur.col + 1] && forest.get(cur.row).get(cur.col + 1) != 0) {
                        next.offer(new TL(cur.row, cur.col + 1, cur.tree + 1));
                        seen[cur.row][cur.col + 1] = true;
                    }
                }
            }
            return -1;
        }
    }


    static class TL implements Comparable<TL> {
        int row;
        int col;
        int tree;

        TL(int row, int col, int tree) {
            this.row = row;
            this.col = col;
            this.tree = tree;
        }

        public int compareTo(TL o) {
            return this.tree - o.tree;
        }

        public String toString() {
            return "[" + row + "," + col + "], h=" + tree;
        }
    }
}

