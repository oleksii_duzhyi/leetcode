package com.oduzhyi.leetcode;

import java.util.TreeMap;

public class H0975OddEvenJump {
    private int[] oddJumps;
    private int[] evenJumps;

    public int oddEvenJumps(int[] A) {
        int l = A.length;
        boolean[] odd = new boolean[l];
        boolean[] even = new boolean[l];

        TreeMap<Integer, Integer> valToI = new TreeMap<>();
        odd[l - 1] = true;
        even[l - 1] = true;
        valToI.put(A[l - 1], l - 1);

        for (int i = l - 2; i >= 0; i--) {
            if (valToI.containsKey(A[i])) {
                odd[i] = even[valToI.get(A[i])];
                even[i] = odd[valToI.get(A[i])];
            } else {
                Integer lower = valToI.lowerKey(A[i]);
                Integer higher = valToI.higherKey(A[i]);

                if (lower != null) {
                    even[i] = odd[valToI.get(lower)];
                }
                if (higher != null) {
                    odd[i] = even[valToI.get(higher)];
                }
            }
            valToI.put(A[i], i);
        }

        int res = 0;
        for (int i = 0; i < l; i++) {
            if (odd[i]) {
                res++;
            }
        }
        return res;
    }

    public int oddEvenJumpsBF(int[] A) {
        int res = 0;
        oddJumps = new int[A.length];
        evenJumps = new int[A.length];
        for (int i = 0; i < A.length; i++) {
            int jumpCnt = 1;
            int pos = i;
            for (int j = i; j < A.length - 1; ) {
                int nextPos;
                if (jumpCnt % 2 == 1) {
                    nextPos = canMakeOddJump(j, A);
                } else {
                    nextPos = canMakeEvenJump(j, A);
                }
                if (nextPos == -1) {
                    break;
                }
                jumpCnt++;
                j = nextPos;
                pos = j;
            }
            if (pos == A.length - 1) {
                res++;
            }
        }
        return res;
    }

    private int canMakeOddJump(int from, int[] A) {
        if (oddJumps[from] != 0) {
            return oddJumps[from];
        }
        int cur = A[from];
        int min = 1000000;
        int minIdx = 1000000;
        for (int i = from + 1; i < A.length; i++) {
            if (A[i] >= cur && A[i] < min) {
                min = A[i];
                minIdx = i;
            }
        }
        int nextIdx = cur <= min ? minIdx : -1;
        oddJumps[from] = nextIdx;
        return nextIdx;
    }


    private int canMakeEvenJump(int from, int[] A) {
        if (evenJumps[from] != 0) {
            return evenJumps[from];
        }
        int cur = A[from];
        int max = -1;
        int maxIdx = -1;
        for (int i = from + 1; i < A.length; i++) {
            if (A[i] <= cur && A[i] > max) {
                max = A[i];
                maxIdx = i;
            }
        }
        int nextIdx = cur >= max ? maxIdx : -1;
        evenJumps[from] = nextIdx;
        return nextIdx;
    }
}
