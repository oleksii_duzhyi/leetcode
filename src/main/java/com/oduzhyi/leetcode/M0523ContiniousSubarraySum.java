package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class M0523ContiniousSubarraySum {

    public boolean checkSubarraySum(int[] nums, int k) {
        Map<Integer, Integer> mapping = new HashMap<>();
        mapping.put(0, -1);
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];

            Integer prev = mapping.putIfAbsent(k == 0 ? sum : sum % k, i);
            if (prev != null && i - prev >= 2) {
                return true;
            }
        }
        return false;
    }
}

// main idea if a % k == b % k   => b - a = k

//main idea
// sum(i,j) = sum(0,j) - sum(0,i)
// if sum(0,j) % k == sum(0,i) % k  ===> sum(i, j) % k == 0
// create a map to store first index of reminder
// IMPORTANT - for reminder 0 - store -1 to account for first 2 elements being 0
// try put if absent reminder and current position
// if there is prev position - return true if diff is more than 1
// if exausted -  return false