package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class M0560SubbarraySumEqualsK {
    public int subarraySum(int[] nums, int k) {
        Map<Integer, Integer> sums = new HashMap<>();

        int res = 0;
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            if (sum == k) {
                res++;
            }
            if (sums.containsKey(sum - k)) {
                res += sums.get(sum - k);
            }
            sums.put(sum, sums.getOrDefault(sum, 0) + 1);
        }
        return res;
    }

    public int subarraySumN2(int[] nums, int k) {
        int res = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == k) {
                res++;
            }
            int sum = nums[i];
            for (int j = i + 1; j < nums.length; j++) {
                sum += nums[j];
                if (sum == k) {
                    res++;
                }
            }
        }
        return res;
    }
}

// brute force - pretty straitforward n^2
// alternatively can be done in O(n)
// main idea - sum(i, j) = sum(0,j) - sum(0,i)
// if we want to find a subarray sum to k we need to look for sum(0,j) - sum(0,i) = k => sum(0,j) - k = sum(0,i)
// this means if we are at j and know sum(0,j) - we can look for sum(0,j) - k  in map and increment result
// by the frequence of this sum
// it may be sum(0,k) == sum(0,i)