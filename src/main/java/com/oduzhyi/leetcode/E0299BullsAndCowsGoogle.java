package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class E0299BullsAndCowsGoogle {

    public static void main(String[] args) {
        new E0299BullsAndCowsGoogle().getHint("1123", "0111");
    }

    public String getHintMy(String secret, String guess) {
        int bulls = 0, cows = 0;
        int[] numberCount = new int[10];
        Set<Integer> bullIdxs = new HashSet<>();

        for (int i = 0; i < secret.length(); i++) {
            if (secret.charAt(i) == guess.charAt(i)) {
                bulls++;
                bullIdxs.add(i);
            } else {
                numberCount[secret.charAt(i) - 48] = numberCount[secret.charAt(i) - 48] + 1;
            }
        }

        for (int i = 0; i < guess.length(); i++) {
            if (!bullIdxs.contains(i)) {
                int count = numberCount[guess.charAt(i) - 48];
                if (count > 0) {
                    cows++;
                    numberCount[guess.charAt(i) - 48] = numberCount[guess.charAt(i) - 48] - 1;
                }
            }
        }
        return bulls + "A" + cows + "B";
    }

    public String getHint(String secret, String guess) {
        int bulls = 0;// "1123" "0111"
        int cows = 0;
        int[] numbers = new int[10];
        for (int i = 0; i < secret.length(); i++) {
            if (secret.charAt(i) == guess.charAt(i)) {
                bulls++;
            } else {
                if (numbers[secret.charAt(i) - '0']++ < 0) {
                    cows++; // [0, 1, 0, 0] [-1, 1, 1, 0]
                }
                if (numbers[guess.charAt(i) - '0']-- > 0) {
                    cows++;  //[-1, 1, 0, 0] []
                }
            }
        }
        return bulls + "A" + cows + "B";
    }


    public String getHintMaps(String secret, String guess) {
        int bulls = 0, cows = 0;
        Map<Character, Integer> positions = new HashMap<>();
        Set<Integer> bullIdxs = new HashSet<>();

        for (int i = 0; i < secret.length(); i++) {
            if (secret.charAt(i) == guess.charAt(i)) {
                bulls++;
                bullIdxs.add(i);
            } else {
                Integer prev = positions.putIfAbsent(secret.charAt(i), 1);
                if (prev != null) {
                    positions.put(secret.charAt(i), prev + 1);
                }
            }
        }

        for (int i = 0; i < guess.length(); i++) {
            if (!bullIdxs.contains(i)) {
                Integer count = positions.get(guess.charAt(i));
                if (count != null && count > 0) {
                    cows++;
                    positions.put(guess.charAt(i), --count);
                }
            }
        }
        return String.format("%dA%dB", bulls, cows);
    }
}

// we can solve this puzzle in O(n) time
// because we have to iterate over every character at least once to understand if it bull or cow
// pseudo code

// bulls = 0; cows = 0
// for every character in guess
//      if char == secretChar -> bulls++
//      otherwise -> cows++
// construct and return answer

//understnading of task was incorrect, the cow - is character in secret out of place
// lets create a data structure based on our guess
// this data structure should have fast search by letter and keeps track of all positions of this letter
// then we iterate over the guess and find bull and cows using the following algorithm
// we try to get from the map location for character @ i
//      if is not present -> do nothing
//      otherviwe ->
//          if index is in list -> delete index and increment bull
//          othervise -> increment cow (need to make desicion which index to delete)

// easiest solution - is to have 2 cycles, first find all bulls and then count cows
// lets implement it. Also it is better to use set in this case. It is even better to use counters
// but Can we do it better?
// map is pretty expensive data structure, so given input contains only numbers - we can
// use fixed size array!

// tricky case
// secret = 1234
// guess  = 2256
// result = 1A0B