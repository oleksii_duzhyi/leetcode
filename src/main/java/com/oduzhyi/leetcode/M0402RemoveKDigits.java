package com.oduzhyi.leetcode;

import java.util.LinkedList;

public class M0402RemoveKDigits {

    public String removeKdigits(String num, int k) {
        if (num.length() == k) {
            return "0";
        }
        LinkedList<Character> stack = new LinkedList<>();
        char[] chars = num.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            while (!stack.isEmpty() && stack.peek() > chars[i] && k > 0) {
                stack.pop();
                k--;
            }
            stack.push(chars[i]);
        }
        while (k-- > 0) {
            stack.pop();
        }
        StringBuilder res = new StringBuilder();
        while (!stack.isEmpty()) {
            char c = stack.removeLast();
            if (res.length() == 0 && c == '0') {
                continue;
            }
            res.append(c);
        }
        return res.length() == 0 ? "0" : res.toString();
    }

    public String removeKdigitsMy(String num, int k) {
        if (num.length() == k) {
            return "0";
        }
        StringBuilder res = new StringBuilder(num);
        while (k > 0) {
            int next = -1;
            for (int i = 0; i < res.length() - 1; i++) {
                if (res.charAt(i) > res.charAt(i + 1)) {
                    next = i;
                    break;
                }
            }
            next = next == -1 ? res.length() - 1 : next;
            res.deleteCharAt(next);
            k--;
        }

        while (res.length() > 0 && res.charAt(0) == '0') {
            res.deleteCharAt(0);
        }
        return res.length() == 0 ? "0" : res.toString();
    }

    public String removeKdigitsIncorrect(String num, int k) {
        if (num.length() == k) {
            return "0";
        }
        int[] countDig = new int[10];
        char[] chars = num.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            countDig[chars[i] - '0']++;
        }
        int[] targetDig = new int[10];
        for (int i = 9; i >= 0; i--) {
            if (k >= countDig[i]) {
                k -= countDig[i];
                targetDig[i] = 0;
            } else {
                targetDig[i] = countDig[i] - k;
                k = 0;
            }
        }

        StringBuilder result = new StringBuilder();
        for (int i = chars.length - 1; i >= 0; i--) {
            if (targetDig[chars[i] - '0'] > 0) {
                result.append(chars[i]);
                targetDig[chars[i] - '0']--;
            }
        }

        return result.reverse().toString();
    }
}

// basic idea:
// 1Axxxx > 1Bxxxx - iff A>B
// can utilize stack. while c[i - 1] > c[i] and k > 0 - pop from stack
// then push c[i]
// reconstruct result from stack from the end removing preleading 0