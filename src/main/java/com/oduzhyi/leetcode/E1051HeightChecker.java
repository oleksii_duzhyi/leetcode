package com.oduzhyi.leetcode;

import java.util.Arrays;

public class E1051HeightChecker {
    public int heightChecker(int[] heights) {
        int[] buckets = new int[101];
        for (int h : heights) {
            buckets[h]++;
        }
        int res = 0;
        int curBucket = 0;

        for (int i = 0; i < heights.length; i++) {
            while (buckets[curBucket] == 0) {
                curBucket++;
            }
            if (heights[i] != curBucket) {
                res++;
            }
            buckets[curBucket]--;
        }
        return res;
    }

    public int heightCheckerSort(int[] heights) {
        int[] copy = Arrays.copyOf(heights, heights.length);
        Arrays.sort(copy);
        int res = 0;
        for (int i = 0; i < heights.length; i++) {
            if (copy[i] != heights[i]) {
                res++;
            }
        }
        return res;
    }
}