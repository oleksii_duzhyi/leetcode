package com.oduzhyi.leetcode;

public class M1448CountingGoodNodesInBinaryTree {
    private int count = 0;

    public int goodNodes(TreeNode root) {
        goodNodes(root, root.val);
        return count;
    }

    private void goodNodes(TreeNode tn, int max) {
        if (tn == null) {
            return;
        }
        if (tn.val >= max) {
            count++;
        }
        int newMax = Math.max(tn.val, max);
        goodNodes(tn.left, newMax);
        goodNodes(tn.right, newMax);
    }
}
