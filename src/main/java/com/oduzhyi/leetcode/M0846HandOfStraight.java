package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class M0846HandOfStraight {
    public boolean isNStraightHand(int[] hand, int W) {
        Map<Integer, Integer> map = new HashMap<>();
        Arrays.sort(hand);
        for (int num : hand) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        for (int num : hand) {
            if (map.get(num) == 0) {
                continue;
            }
            int count = map.get(num);
            for (int i = 0; i < W; i++) {
                if (map.getOrDefault(num + i, 0) < count) {
                    return false;
                }
                map.put(num + i, map.get(num + i) - count);
            }
        }
        return true;
    }

    public boolean isNStraightHandTreeMap(int[] hand, int W) {
        Map<Integer, Integer> c = new TreeMap<>();
        for (int i : hand) {
            c.put(i, c.getOrDefault(i, 0) + 1);
        }
        for (int it : c.keySet()) {
            if (c.get(it) > 0) {
                for (int i = W - 1; i >= 0; --i) {
                    if (c.getOrDefault(it + i, 0) < c.get(it)) {
                        return false;
                    }
                    c.put(it + i, c.get(it + i) - c.get(it));
                }
            }
        }
        return true;
    }

    public boolean isNStraightHandSorting(int[] hand, int w) {
        if (hand.length == 0 || hand.length % w != 0) {
            return false;
        }
        Arrays.sort(hand);
        int groupsCnt = hand.length / w;
        List<int[]> groups = new ArrayList<>(groupsCnt);
        for (int i = 0; i < groupsCnt; i++) {
            groups.add(new int[w]);
        }
        int[] positions = new int[groupsCnt];

        for (int card : hand) {
            boolean placed = false;
            for (int i = 0; i < groupsCnt && !placed; i++) {
                if (positions[i] < w) {
                    if (positions[i] == 0 || groups.get(i)[positions[i] - 1] - card == -1) {
                        groups.get(i)[positions[i]++] = card;
                        placed = true;
                    }
                }
            }
            if (!placed) {
                return false;
            }
        }
        return true;
    }

}
//sorting or tree map approach
