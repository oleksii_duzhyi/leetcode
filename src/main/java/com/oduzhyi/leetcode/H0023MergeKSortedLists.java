package com.oduzhyi.leetcode;

import java.util.Comparator;
import java.util.PriorityQueue;

public class H0023MergeKSortedLists {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists.length == 0) {
            return null;
        }
        if (lists.length == 1) {
            return lists[0];
        }

        PriorityQueue<ListNode> pq = new PriorityQueue<>(Comparator.comparingInt(l -> l.val));

        for (ListNode ln : lists) {
            if (ln != null) {
                pq.offer(ln);
            }
        }

        ListNode res = new ListNode();
        ListNode cur = res;
        while (!pq.isEmpty()) {
            ListNode next = pq.poll();
            cur.next = new ListNode(next.val);
            if (next.next != null) {
                pq.offer(next.next);
            }
            cur = cur.next;
        }
        return res.next;

    }
}

// Create priority queue with Lists and comparator comparing first element of list
// create result head
// while !queue is empty
// poll from Queue, add to  result
// if next element of polled is not null - push back to queue
