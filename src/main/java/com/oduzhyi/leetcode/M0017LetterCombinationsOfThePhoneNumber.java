package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class M0017LetterCombinationsOfThePhoneNumber {

    public List<String> letterCombinations(String digits) {
        if (digits.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        Map<Character, Character[]> map = new HashMap<>();
        map.put('2', new Character[]{'a', 'b', 'c'});
        map.put('3', new Character[]{'d', 'e', 'f'});
        map.put('4', new Character[]{'g', 'h', 'i'});
        map.put('5', new Character[]{'j', 'k', 'l'});
        map.put('6', new Character[]{'m', 'n', 'o'});
        map.put('7', new Character[]{'p', 'q', 'r', 's'});
        map.put('8', new Character[]{'t', 'u', 'v'});
        map.put('9', new Character[]{'w', 'x', 'y', 'z'});

        List<String> res = new ArrayList<>();

        buildRes(digits, 0, "", res, map);
        return res;
    }

    private void buildRes(String digits, int index, String acc, List<String> res, Map<Character, Character[]> map) {
        for (char c : map.get(digits.charAt(index))) {
            if (index == digits.length() - 1) {
                res.add(acc + c);
            } else {
                buildRes(digits, index + 1, acc + c, res, map);
            }
        }
    }
}