package com.oduzhyi.leetcode;

public class E0026RemoveDuplicatesFromSortedArray {
    public int removeDuplicates(int[] nums) {
        int size = 0;

        for (int i = 0; i < nums.length; ) {
            while (i + 1 < nums.length && nums[i] == nums[i + 1]) {
                i++;
            }
            nums[size++] = nums[i];
            i++;
        }
        return size;
    }
}

// have a pointer into array where to set element
// iterate until i == i+ 1 in array
// set size++ to nums[i]
// increment i


