package com.oduzhyi.leetcode;

public class E0415AddStrings {
    public String addStrings(String num1, String num2) {
        StringBuilder sb = new StringBuilder();
        int p1 = num1.length() - 1;
        int p2 = num2.length() - 1;

        int carry = 0;
        while (p1 >= 0 || p2 >= 0) {
            int c1 = p1 >= 0 ? num1.charAt(p1) - '0' : 0;
            int c2 = p2 >= 0 ? num2.charAt(p2) - '0' : 0;

            int res = c1 + c2 + carry;
            sb.insert(0, res % 10);
            carry = res / 10;
            p1--;
            p2--;
        }
        if (carry > 0) {
            sb.insert(0, '1');
        }
        return sb.toString();
    }

    public String addStringsMy(String num1, String num2) {
        StringBuilder sb = new StringBuilder();
        int p1 = num1.length() - 1;
        int p2 = num2.length() - 1;

        boolean carry = false;
        while (p1 >= 0 || p2 >= 0) {
            char c1 = p1 >= 0 ? num1.charAt(p1) : '0';
            char c2 = p2 >= 0 ? num2.charAt(p2) : '0';

            int res = c1 + c2 - '0';
            if (carry) {
                res = res + 1;
            }
            carry = false;
            if (res >= '9' + 1) {
                carry = true;
                res = '0' + (res - '9' - 1);
            }
            sb.insert(0, (char) res);
            p1--;
            p2--;
        }
        if (carry) {
            sb.insert(0, '1');
        }
        return sb.toString();
    }
}

//simple math - easier to work   with integers than chars
