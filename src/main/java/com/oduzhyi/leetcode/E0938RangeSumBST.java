package com.oduzhyi.leetcode;

public class E0938RangeSumBST {
    public int rangeSumBST(TreeNode root, int L, int R) {
        if (root == null) {
            return 0;
        }
        if (root.val < L) {
            return rangeSumBST(root.right, L, R);
        } else if (root.val > R) {
            return rangeSumBST(root.left, L, R);
        } else {
            return root.val + rangeSumBST(root.left, L, R) + rangeSumBST(root.right, L, R);
        }
    }
}

// Questions:
// do we include L and R?
// How deep a tree can be? I'd like to use Recursive algo, should I worry about stack overflow?
// can L > R?
//
// pretty easy recursive solution - we add sum of
// left and right parts of the tree to the current if the current is in the range
//
// also, one very important details is that this tree is binary search tree
// this means we can skip entire subbtrees
// if value in the root is small then the LEFT border - account only for right subtree
// if value in the root is bigger then the RIGHT border - account only for left subtree
// this can be easily transformed into BFS or DFS non-recursive search
