package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class M0525ContiguousArray {
    public int findMaxLength(int[] nums) {
        Map<Integer, Integer> cntMap = new HashMap<>();
        int cnt = 0;
        int res = 0;
        cntMap.put(0, 0);

        for (int i = 1; i <= nums.length; i++) {
            if (nums[i - 1] == 0) {
                cnt++;
            } else {
                cnt--;
            }

            if (cntMap.containsKey(cnt)) {
                res = Math.max(res, i - cntMap.get(cnt));
            }
            cntMap.putIfAbsent(cnt, i);
        }
        return res;
    }

    public int findMaxLengthBruteForce(int[] nums) {
        int res = 0;

        for (int i = 0; i < nums.length; i++) {
            int zeroCnt = 0;
            int oneCnt = 0;
            int subRes = 0;
            for (int j = i; j < nums.length; j++) {
                if (nums[j] == 1) {
                    oneCnt++;
                } else {
                    zeroCnt++;
                }
                if (oneCnt == zeroCnt) {
                    subRes = j - i + 1;
                }
            }
            res = Math.max(res, subRes);
            if (res == nums.length - i) {
                return res;
            }
        }
        return res;
    }
}