package com.oduzhyi.leetcode;

public class E1232CheckIfPointsAreOnOneLine {
    public boolean checkStraightLine(int[][] coordinates) {
        if (coordinates.length == 2) {
            return true;
        }
        int x1 = coordinates[0][0];
        int y1 = coordinates[0][1];
        int x2 = coordinates[1][0];
        int y2 = coordinates[1][1];
        for (int i = 2; i < coordinates.length; i++) {
            int x3 = coordinates[i][0];
            int y3 = coordinates[i][1];
            if (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2) != 0) {
                return false;
            }
        }

        return true;
    }
}

// questions:
// can there be just one coordinate?
// does [1,1] [2,2] and [0,0] should return true?
//
// idea:
// we need to find delta x and y between 2 points,
// next we can find a delta x and y between first point and every other point
// if the delta is proportinal initial delta - then check next point
// otherwise - return false;
//
// simple idea:
// sort coordinates, check i and i + 1 delta - if it changed -return false
//
// all ideas are wrong - need to check points collineartiry