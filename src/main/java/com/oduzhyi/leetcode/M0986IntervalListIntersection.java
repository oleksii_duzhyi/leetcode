package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class M0986IntervalListIntersection {
    public int[][] intervalIntersection(int[][] A, int[][] B) {
        if (A.length == 0 || B.length == 0) {
            return new int[0][0];
        }
        List<int[]> res = new ArrayList<>();
        int i = 0;
        int j = 0;

        while (i < A.length && j < B.length) {
            int fA = A[i][0];
            int tA = A[i][1];
            int fB = B[j][0];
            int tB = B[j][1];

            if (fA > tB) {
                j++;
            } else if (fB > tA) {
                i++;
            } else {
                int f = Math.max(fA, fB);
                int t = Math.min(tA, tB);
                res.add(new int[]{f, t});
                if (tA > tB) {
                    j++;
                } else {
                    i++;
                }
            }
        }
        return res.toArray(new int[res.size()][]);
    }
}