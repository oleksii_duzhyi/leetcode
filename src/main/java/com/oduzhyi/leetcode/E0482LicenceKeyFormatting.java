package com.oduzhyi.leetcode;

public class E0482LicenceKeyFormatting {
    public String licenseKeyFormatting(String s, int k) {
        StringBuilder res = new StringBuilder();
        int cnt = k;
        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) != '-') {
                res.append(s.charAt(i));
                cnt--;
            }
            if (cnt == 0) {
                res.append('-');
                cnt = k;
            }
        }
        return res.reverse().toString().toUpperCase();
    }

}
