package com.oduzhyi.leetcode;

import java.util.Arrays;
import java.util.List;

public class M0055JumpGame {
    public boolean canJump(int[] nums) {
        int last = nums.length - 1;
        for (int i = nums.length - 1; i >= 0; i--) {
            if (i + nums[i] >= last) {
                last = i;
            }
        }
        List<boolean[]> booleans = Arrays.asList(new boolean[]{true, false});
        return last == 0;
    }

    public boolean canJumpMy(int[] nums) {
        if (nums.length == 1) {
            return true;
        }
        int[] jumps = new int[nums.length];
        int maxP = 0;

        for (int i = 0; i < nums.length && jumps[nums.length - 1] == 0; i++) {
            if (i > 0 && jumps[i] == 0) {
                break;
            }
            int maxJ = nums[i];
            if (i + maxJ > maxP) {
                for (int j = maxP - i; j <= maxJ && j + i < nums.length; j++) {
                    jumps[i + j] = jumps[i] + 1;
                    maxP = i + j;
                }
            }
        }

        return jumps[nums.length - 1] > 0;
    }
}