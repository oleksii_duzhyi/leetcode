package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;

public class H0124MaxPathSum {
    Map<TreeNode, Integer> cache = new HashMap<>();
    private int max = Integer.MIN_VALUE;

    public int maxPathSum(TreeNode root) {
        maxGain(root);
        return max;
    }

    private int maxGain(TreeNode tn) {
        if (tn == null) {
            return 0;
        }

        int maxLeft = Math.max(maxGain(tn.left), 0);
        int maxRight = Math.max(maxGain(tn.right), 0);

        max = Math.max(max, tn.val + maxLeft + maxRight);

        return tn.val + Math.max(maxLeft, maxRight);
    }

    public int maxPathSumMy(TreeNode root) {
        int max = Integer.MIN_VALUE;
        if (root == null) {
            return max;
        }
        ArrayDeque<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            TreeNode tn = queue.poll();
            max = Math.max(_maxPathSum(tn, true), max);
            if (tn.left != null) {
                queue.add(tn.left);
            }
            if (tn.right != null) {
                queue.add(tn.right);
            }
        }
        return max;
    }

    private int _maxPathSum(TreeNode tn, boolean root) {
        if (tn == null) {
            return 0;
        }
        if (!root && cache.containsKey(tn)) {
            return cache.get(tn);
        }
        int maxLeft = _maxPathSum(tn.left, false);
        int maxRight = _maxPathSum(tn.right, false);


        if (root) {
            return Math.max(tn.val, Math.max(tn.val + maxRight, Math.max(maxLeft + maxRight + tn.val, tn.val + maxLeft)));
        } else {
            int max = Math.max(tn.val, Math.max(tn.val + maxRight, tn.val + maxLeft));
            cache.put(tn, max);
            return max;
        }
    }
}

// start with the root, find max path in left and right subtrees
// cut both with 0 - no need to add smth below zero
// update the max result = root + left + right
// return from recursion call root + Math.max(left, right)