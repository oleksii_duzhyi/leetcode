package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class M0207CoursesSchedule {
    private Set<Integer> white = new HashSet<>();
    private Set<Integer> gray = new HashSet<>();
    private Set<Integer> black = new HashSet<>();
    private Map<Integer, List<Integer>> vert = new HashMap<>();

    public boolean canFinish(int numCourses, int[][] prerequisites) {
        for (int[] pre : prerequisites) {
            vert.computeIfAbsent(pre[0], ArrayList::new).add(pre[1]);
        }

        Set<Integer> visited = new HashSet<>();
        for (int[] v : prerequisites) {
            if (!visited.contains(v[0])) {
                boolean cyclic = dfs(v[0], new HashSet<>(), visited);
                if (cyclic) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean dfs(int v, Set<Integer> parents, Set<Integer> visited) {
        if (parents.contains(v)) {
            return true;
        }
        parents.add(v);
        for (int dep : vert.getOrDefault(v, Collections.emptyList())) {
            if (!visited.contains(dep)) {
                boolean cyclic = dfs(dep, parents, visited);
                if (cyclic) {
                    return true;
                }
            }
        }
        parents.remove(v);
        visited.add(v);
        return false;
    }

    public boolean canFinishFirst(int numCourses, int[][] prerequisites) {
        for (int[] pre : prerequisites) {
            vert.computeIfAbsent(pre[0], ArrayList::new).add(pre[1]);
            white.add(pre[0]);
        }

        for (int[] v : prerequisites) {
            if (white.contains(v[0]) && !black.contains(v[0])) {
                white.remove(v[0]);
                boolean cycle = dfsCycle(v[0]);
                if (cycle) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean dfsCycle(int v) {
        if (!vert.containsKey(v) || vert.get(v).isEmpty()) {
            return false;
        }
        gray.add(v);
        for (int con : vert.get(v)) {
            if (gray.contains(con)) {
                return true;
            }
            if (white.contains(con) && !black.contains(con)) {
                white.remove(con);
                boolean cycle = dfsCycle(con);
                if (cycle) {
                    return true;
                }
            }
        }
        gray.remove(v);
        black.add(v);
        return false;
    }
}