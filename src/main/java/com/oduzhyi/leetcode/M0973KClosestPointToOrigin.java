package com.oduzhyi.leetcode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class M0973KClosestPointToOrigin {
    public int[][] kClosest(int[][] points, int K) {
        if (points.length == K) {
            return points;
        }

        Comparator<int[]> comparator = Comparator.comparingDouble(p -> Math.sqrt(p[0] * p[0] + p[1] * p[1]));
        comparator = comparator.reversed();
        PriorityQueue<int[]> pq = new PriorityQueue<>(K, comparator);


        for (int i = 0; i < points.length; i++) {
            if (pq.size() < K) {
                pq.offer(points[i]);
            } else {
                if (comparator.compare(pq.peek(), points[i]) < 0) {
                    pq.poll();
                    pq.offer(points[i]);
                }
            }
        }

        return pq.toArray(new int[K][]);

    }

    public int[][] kClosestSort(int[][] points, int K) {
        if (points.length == K) {
            return points;
        }

        Arrays.sort(points, Comparator.comparingDouble(p -> Math.sqrt(p[0] * p[0] + p[1] * p[1])));

        int[][] res = new int[K][];
        for (int i = 0; i < K; i++) {
            res[i] = points[i];
        }
        return res;

    }
}

// Can be done via quick select