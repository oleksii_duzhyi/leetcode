package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class H1192CriticalConnections {
    private int time = 0;
    private Map<Integer, Integer> visitedTime = new HashMap<>();
    private Map<Integer, Integer> lowTime = new HashMap<>();
    private Set<Integer> visited = new HashSet<>();
    private List<List<Integer>> criticalConnections = new ArrayList<>();
    private int currentTime = 0;

    public static void main(String[] args) {
        List<List<Integer>> connections = new ArrayList<>();
        connections.add(Arrays.asList(1, 2));
        connections.add(Arrays.asList(2, 3));
        connections.add(Arrays.asList(3, 1));
        connections.add(Arrays.asList(4, 3));
        connections.add(Arrays.asList(4, 5));
        connections.add(Arrays.asList(5, 6));
        connections.add(Arrays.asList(7, 5));
        connections.add(Arrays.asList(6, 7));
        connections.add(Arrays.asList(6, 8));


//        System.out.println(new H1192CriticalConnections().criticalConnections(0, connections));
//
//        connections = new ArrayList<>();
//        connections.add(Arrays.asList(1,2));
//        connections.add(Arrays.asList(0,1));
//        connections.add(Arrays.asList(3,1));
//        connections.add(Arrays.asList(2,0));
//
//        System.out.println(new H1192CriticalConnections().criticalConnections(0, connections));

        connections = new ArrayList<>();
        connections.add(Arrays.asList(1, 2));
        connections.add(Arrays.asList(2, 3));
        connections.add(Arrays.asList(3, 1));
        connections.add(Arrays.asList(4, 3));
        connections.add(Arrays.asList(4, 5));
        connections.add(Arrays.asList(5, 6));
        connections.add(Arrays.asList(6, 7));
        connections.add(Arrays.asList(7, 5));
        connections.add(Arrays.asList(6, 8));
        connections.add(Arrays.asList(7, 3));


        System.out.println(new H1192CriticalConnections().criticalConnections(0, connections));
    }

    public List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        Map<Integer, List<Integer>> vertex = new HashMap<>();
        int last = -1;
        for (List<Integer> vert : connections) {
            vertex.computeIfAbsent(vert.get(0), key -> new ArrayList<>()).add(vert.get(1));
            vertex.computeIfAbsent(vert.get(1), key -> new ArrayList<>()).add(vert.get(0));
            last = vert.get(0);
        }

        dfs(n, null, vertex);
        return criticalConnections;
    }

    private void dfs(Integer node, Integer parent, Map<Integer, List<Integer>> vertex) {
        visited.add(node);
        lowTime.put(node, time);
        visitedTime.put(node, time);

        time++;

        for (Integer child : vertex.get(node)) {
            if (Objects.equals(child, parent)) {
                continue;
            }
            if (!visited.contains(child)) {
                dfs(child, node, vertex);

                if (visitedTime.get(node) < lowTime.get(child)) {
                    criticalConnections.add(Arrays.asList(node, child));
                } else {
                    lowTime.put(node, Math.min(lowTime.get(node), lowTime.get(child)));
                }
            } else {
                lowTime.put(node, Math.min(lowTime.get(node), lowTime.get(child)));
            }
        }

    }

    public List<List<Integer>> criticalConnections2(int n, List<List<Integer>> connections) {
        {
            int[] times = new int[n + 1];
            int[] lowTimes = new int[n + 1];
            boolean[] visitedNodes = new boolean[n + 1];
            List<List<Integer>> ans = new ArrayList<>();

            ArrayList<Integer>[] graph = new ArrayList[n + 1];
            for (int i = 0; i < graph.length; i++) {
                graph[i] = new ArrayList<Integer>();
            }
            for (List<Integer> connection : connections) {
                graph[connection.get(0)].add(connection.get(1));
                graph[connection.get(1)].add(connection.get(0));
            }

            for (int i = 0; i < n; i++) {
                visitedNodes[i] = false;
            }

            dfs2(graph, 6, -1, times, lowTimes, visitedNodes, ans);

            return ans;
        }
    }

    private void dfs2(ArrayList<Integer>[] graph, int currentNode, int parentNode, int[] times, int[] lowTimes, boolean[] visitedNodes, List<List<Integer>> ans) {
        visitedNodes[currentNode] = true;
        times[currentNode] = lowTimes[currentNode] = currentTime++;

        for (int neighbourNode : graph[currentNode]) {
            if (neighbourNode == parentNode) {
                continue;
            } else if (!visitedNodes[neighbourNode]) {
                dfs2(graph, neighbourNode, currentNode, times, lowTimes, visitedNodes, ans);
                lowTimes[currentNode] = Math.min(lowTimes[currentNode], lowTimes[neighbourNode]);
                if (times[currentNode] < lowTimes[neighbourNode]) {
                    ans.add(Arrays.asList(currentNode, neighbourNode));
                }
            } else {
                lowTimes[currentNode] = Math.min(lowTimes[currentNode], lowTimes[neighbourNode]);
            }
        }
    }


}