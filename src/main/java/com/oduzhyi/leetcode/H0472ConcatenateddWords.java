package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class H0472ConcatenateddWords {

    public static void main(String[] args) {
        System.out.println(new H0472ConcatenateddWords().findAllConcatenatedWordsInADict(new String[]{"cat", "cats", "catsdogcats", "dog", "dogcatsdog", "hippopotamuses", "rat", "ratcatdogcat"}));
    }

    public List<String> findAllConcatenatedWordsInADict(String[] words) {
        Trie2 root = new Trie2();
        for (String w : words) {
            root.add(w);
        }
        List<String> res = new ArrayList<>();
        for (String w : words) {
            if (isComposite(w, root, 0, 1)) {
                res.add(w);
            }
        }
        return res;
    }

    private boolean isComposite(String word, Trie2 root, int offset, int wordCnt) {
        Trie2 prefix = root.getFirstPrefix(word, offset);
        if (prefix == null) {
            return false;
        }
        if (prefix.word.equals(word)) {
            if (wordCnt == 1) {
                return false;
            } else {
                return true;
            }
        }
        return isComposite(word.substring(prefix.word.length()), root, 0, wordCnt + 1)
                || isComposite(word, root, offset + prefix.word.length(), wordCnt);
    }
}

class Trie2 {
    Trie2[] children = new Trie2[26];
    boolean isWord = false;
    String word = "";

    public void add(String word) {
        Trie2 last = this;
        for (char c : word.toCharArray()) {
            if (last.children[c - 'a'] == null) {
                last.children[c - 'a'] = new Trie2();
            }
            last = last.children[c - 'a'];
        }
        last.isWord = true;
        last.word = word;
    }

    public Trie2 getFirstPrefix(String word, int idx) {
        Trie2 last = this;
        for (int i = 0; i < word.length(); i++) {
            Trie2 next = last.children[word.charAt(i) - 'a'];
            if (next == null) {
                return null;
            }
            if (next.isWord && i >= idx) {
                return next;
            }
            last = next;
        }
        return last.isWord ? last : null;
    }

    public String toString() {
        return word;
    }

}
