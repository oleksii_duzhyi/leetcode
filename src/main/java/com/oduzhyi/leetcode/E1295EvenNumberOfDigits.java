package com.oduzhyi.leetcode;

public class E1295EvenNumberOfDigits {
    public int findNumbers(int[] nums) {
        int res = 0;
        for (int n : nums) {
            int div = 10;
            while (n / div != 0) {
                if (n / (div * 10) == 0) {
                    res++;
                }
                div *= 100;
            }
        }
        return res;
    }
}

// QUESTIONS:
// can nums be empty?
// what is the size of the nums?
//
// first idea that comes to the mind is to transform every number to string and get it length
// but this will require additional space proportinal to O(n * m) - where m is the length
// also we have to account for negative numbers in strings approach  "-123" = 4 length
// so we need to check is charAt(0) == '-'

// alternatively what is common between numbers of 10-99, 1000-9999 - / 10 ^ 2n != 0; / 10 ^ 2n + 1 == 0
// we can use this condition to find numbers with even count of digits