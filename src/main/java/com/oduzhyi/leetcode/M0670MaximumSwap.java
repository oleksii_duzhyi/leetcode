package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class M0670MaximumSwap {

    public int maximumSwap(int num) {
        List<Integer> numbers = new ArrayList<>();
        while (num > 0) {
            numbers.add(num % 10);
            num /= 10;
        }
        boolean swapDone = false;
        for (int i = numbers.size() - 1; i >= 0 && !swapDone; i--) {
            int maxDiff = 0;
            int swapJ = -1;
            for (int j = 0; j < i; j++) {
                if (numbers.get(j) - numbers.get(i) > maxDiff) {
                    maxDiff = numbers.get(j) - numbers.get(i);
                    swapJ = j;
                    swapDone = true;
                }
            }
            if (swapJ != -1) {
                Collections.swap(numbers, i, swapJ);
            }
        }

        int res = 0;
        for (int i = 0; i < numbers.size(); i++) {
            res += numbers.get(i) * (int) Math.pow(10, i);
        }

        return res;
    }
}

// put all the digits to array
// tha last number - the biggest significance
// try to find THE BIGGEST number FROM BEGINNING that is bigger than the last
// if found - swap, and reconstruct the number