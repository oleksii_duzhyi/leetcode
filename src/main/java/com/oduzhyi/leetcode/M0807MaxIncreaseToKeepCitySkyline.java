package com.oduzhyi.leetcode;

public class M0807MaxIncreaseToKeepCitySkyline {
    public int maxIncreaseKeepingSkyline(int[][] grid) {
        int n = grid.length;
        int[] skTB = new int[n];
        int[] skLR = new int[n];

        for (int i = 0; i < n; i++) {
            int maxTB = 0;
            int maxLR = 0;
            for (int j = 0; j < n; j++) {
                maxTB = Math.max(maxTB, grid[j][i]);
                maxLR = Math.max(maxLR, grid[i][j]);
            }
            skTB[i] = maxTB;
            skLR[i] = maxLR;
        }

        int res = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                res += Math.min(skTB[i], skLR[j]) - grid[i][j];
            }
        }
        return res;
    }
}

// need to find maximum for every row and every column
// and then for every element find diff between min(maxR, maxC) - current diff