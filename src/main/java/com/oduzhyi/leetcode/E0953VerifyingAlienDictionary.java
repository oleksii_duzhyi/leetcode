package com.oduzhyi.leetcode;

import java.util.Arrays;

public class E0953VerifyingAlienDictionary {
    public boolean isAlienSortedSorting(String[] words, String order) {
        if (words.length <= 1) {
            return true;
        }
        int[] charMap = new int[order.length()];
        for (int i = 0; i < order.length(); i++) {
            charMap[order.charAt(i) - 'a'] = i;
        }

        String[] copy = new String[words.length];
        System.arraycopy(words, 0, copy, 0, words.length);

        Arrays.sort(copy, (w1, w2) -> {
            int lim = Math.min(w1.length(), w2.length());
            int i = 0;
            while (i < lim) {
                if (w1.charAt(i) != w2.charAt(i)) {
                    return charMap[w1.charAt(i) - 'a'] - charMap[w2.charAt(i) - 'a'];
                }
                i++;
            }
            return w1.length() - w2.length();
        });

        for (int i = 0; i < words.length; i++) {
            if (!words[i].equals(copy[i])) {
                return false;
            }
        }

        return true;
    }

    // lexicograf order
    // a
    // abbbb
    // ac
    // d
    public boolean isAlienSorted(String[] words, String order) {
        if (words.length <= 1) {
            return true;
        }
        int[] charMap = new int[order.length()];
        for (int i = 0; i < order.length(); i++) {
            charMap[order.charAt(i) - 'a'] = i;
        }

        for (int i = 0; i < words.length - 1; i++) {
            if (compare(words[i], words[i + 1], charMap)) {
                return false;
            }
        }
        return true;
    }

    private boolean compare(String w1, String w2, int[] charMap) {
        int w1L = w1.length();
        int w2L = w2.length();
        for (int i = 0; i < Math.min(w1L, w2L); i++) {
            if (w1.charAt(i) != w2.charAt(i)) {
                return charMap[w1.charAt(i) - 'a'] > charMap[w2.charAt(i) - 'a'];
            }
        }
        return w1L > w2L;
    }
}

// build a dictionary of char -> pos in order
// compare adjesent words
// if all chars are equal - 2 words are not equal if w1L > w2L
// if char is different w1 > w2 if w1[i] > w2[i]
