package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class E0811SubDomainVisitCount {
    public List<String> subdomainVisits(String[] cpdomains) {
        Map<String, Integer> counts = new HashMap<>();
        for (String line : cpdomains) {
            String[] parts = line.split(" ");
            String domain = parts[1];
            Integer count = Integer.parseInt(parts[0]);
            while (!domain.isEmpty()) {
                counts.put(domain, counts.getOrDefault(domain, 0) + count);
                int nextDot = domain.indexOf(".");
                domain = nextDot != -1 ? domain.substring(nextDot + 1) : "";
            }
        }

        List<String> result = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : counts.entrySet()) {
            result.add(String.valueOf(entry.getValue()) + ' ' + entry.getKey());
        }
        return result;
    }
}

// questions:
// are all strings valid?
//
