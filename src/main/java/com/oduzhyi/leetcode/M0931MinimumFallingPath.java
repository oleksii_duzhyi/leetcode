package com.oduzhyi.leetcode;

import java.util.Arrays;

public class M0931MinimumFallingPath {

    int[][] minSums;

    public static void main(String[] args) {
        new M0931MinimumFallingPath().minFallingPathSum(new int[][]{
                new int[]{1, 2, 3},
                new int[]{4, 5, 6},
                new int[]{7, 8, 9},
        });
    }

    public int minFallingPathSum(int[][] matrix) {
        // input is not empty
        minSums = new int[matrix.length][];
        for (int i = 0; i < matrix.length; i++) {
            minSums[i] = new int[matrix[0].length];
            Arrays.fill(minSums[i], Integer.MAX_VALUE);
        }
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < matrix[0].length; i++) {
            min = Math.min(min, dfs(0, i, matrix));
        }
        return min;
    }

    // O(n * m) - SC, TC
    private int dfs(int row, int col, int[][] matrix) {
        if (col < 0 || col >= matrix[0].length) {
            return Integer.MAX_VALUE;
        }
        if (row == matrix.length - 1) {
            return matrix[row][col];
        }
        if (minSums[row][col] != Integer.MAX_VALUE) {
            return minSums[row][col];
        }
        int localMin = Integer.MAX_VALUE;
        for (int k = -1; k <= 1; k++) {
            int below = dfs(row + 1, col + k, matrix);
            if (below != Integer.MAX_VALUE) {
                localMin = Math.min(below + matrix[row][col], localMin);
            }
        }
        minSums[row][col] = localMin;
        return minSums[row][col];

    }
}
