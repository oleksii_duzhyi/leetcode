package com.oduzhyi.leetcode;

/**
 * Created by Нелла on 14.11.2018.
 */
public class Problem709WorkingWithChars {
    public static void main(String[] args) {
        System.out.println(new Problem709WorkingWithChars().toLowerCase("PiTAs"));
    }

    public String toLowerCase(String str) {
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            int index = c - 'A';
            if (index >= 0 && index < 26) {
                chars[i] = (char) (c + 32);
            }
        }
        return new String(chars);
    }
}
