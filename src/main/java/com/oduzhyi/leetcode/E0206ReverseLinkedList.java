package com.oduzhyi.leetcode;

import java.util.LinkedList;

public class E0206ReverseLinkedList {

    public static void main(String[] args) {
        new E0206ReverseLinkedList().reverseList(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4)))));
        
    }

    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode p = reverseList(head.next);
        head.next.next = head;
        head.next = null;
        return p;
    }

    public ListNode reverseListIterative(ListNode head) {
        ListNode prev = null;
        while (head != null) {
            ListNode tmpNext = head.next;
            head.next = prev;
            prev = head;
            head = tmpNext;
        }
        return prev;
    }

    public ListNode reverseListIterativeStack(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        LinkedList<ListNode> stack = new LinkedList<>();
        while (head != null) {
            stack.push(head);
            head = head.next;
        }

        ListNode newHead = stack.pop();
        ListNode next = newHead;
        while (!stack.isEmpty()) {
            next.next = stack.pop();
            next = next.next;
        }
        next.next = null;
        return newHead;
    }
}
