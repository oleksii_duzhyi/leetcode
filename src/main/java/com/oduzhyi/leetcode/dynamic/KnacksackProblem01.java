package com.oduzhyi.leetcode.dynamic;

public class KnacksackProblem01 {

    public static int maxWeightInBackpack(int[] weights, int[] values, int maxWeight) {
        int[][] subProblemSolution = new int[weights.length + 1][maxWeight + 1];

        for (int i = 1; i < weights.length + 1; i++) {
            for (int j = 1; j < maxWeight + 1; j++) {
                if (weights[i] <= j) {
                    subProblemSolution[i][j] = Math.max(subProblemSolution[i - 1][j],
                            subProblemSolution[i - 1][j - weights[j]] + values[i - 1]
                    );
                } else {
                    subProblemSolution[i][j] = subProblemSolution[i - 1][j];
                }
            }
        }
        return subProblemSolution[weights.length][maxWeight];
    }
}
