package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class H0642Autocomplete {
    public static void main(String[] args) {
        AutocompleteSystem autocompleteSystem = new AutocompleteSystem(new String[]{"i love you", "island", "iroman", "i love leetcode"}, new int[]{2, 3, 2, 2});

        System.out.println(autocompleteSystem.input('i'));
        System.out.println(autocompleteSystem.input(' '));
        System.out.println(autocompleteSystem.input('l'));

    }
}

class AutocompleteSystem {
    private Trie root;
    private StringBuilder sb = new StringBuilder();

    public AutocompleteSystem(String[] sentences, int[] times) {
        this.root = new Trie();
        for (int i = 0; i < sentences.length; i++) {
            root.add(sentences[i], times[i]);
        }
    }

    public List<String> input(char c) {
        if (c == '#') {
            root.add(sb.toString(), 1);
            sb = new StringBuilder();
            return Collections.emptyList();
        }
        "".substring(1);
        sb.append(c);
        return root.find(sb.toString());
    }
}

class Trie {
    Map<Character, Trie> children = new HashMap<>();
    boolean end = false;
    int frequency = 0;
    String sentence;

    public Trie() {
    }

    public void add(String word) {
        add(word, 1);
    }

    public void add(String word, int freq) {
        Trie prev = this;
        for (int i = 0; i < word.length(); i++) {
            prev = prev.children.computeIfAbsent(word.charAt(i), k -> new Trie());
        }
        prev.frequency += freq;
        prev.sentence = word;
        prev.end = true;
    }

    public List<String> find(String prefix) {
        PriorityQueue<Pair> pq = new PriorityQueue<>();
        Trie prev = this;
        for (int i = 0; i < prefix.length(); i++) {
            if (!prev.children.containsKey(prefix.charAt(i))) {
                return Collections.emptyList();
            }
            prev = prev.children.get(prefix.charAt(i));
        }
        if (prev == null) {
            return Collections.emptyList();
        }
        findAllWord(prev, pq);
        List<String> res = new ArrayList<>();
        while (!pq.isEmpty()) {
            res.add(0, pq.poll().sentence);
        }
        return res;
    }

    private void findAllWord(Trie trie, PriorityQueue<Pair> pq) {
        if (trie.end) {
            addToResult(pq, new Pair(trie.sentence, trie.frequency));
        }
        for (Trie next : trie.children.values()) {
            findAllWord(next, pq);
        }
    }

    private void addToResult(PriorityQueue<Pair> pq, Pair pair) {
        if (pq.size() < 3) {
            pq.add(pair);
        } else if (pair.compareTo(pq.peek()) > 0) {
            pq.poll();
            pq.add(pair);
        }
    }
}

class Pair implements Comparable<Pair> {
    String sentence;
    int times;

    public Pair(String sentence, int times) {
        this.sentence = sentence;
        this.times = times;
    }

    public int compareTo(Pair o) {
        if (Integer.compare(times, o.times) == 0) {
            return o.sentence.compareTo(sentence);
        }
        return Integer.compare(times, o.times);
    }

    @Override
    public String toString() {
        return "Pair{" +
                "sentence='" + sentence + '\'' +
                ", times=" + times +
                '}';
    }
}

/**
 * Your AutocompleteSystem object will be instantiated and called as such:
 * AutocompleteSystem obj = new AutocompleteSystem(sentences, times);
 * List<String> param_1 = obj.input(c);
 */