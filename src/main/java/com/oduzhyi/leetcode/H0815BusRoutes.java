package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class H0815BusRoutes {
    public static void main(String[] args) {
        System.out.println(new H0815BusRoutes().numBusesToDestination(
                new int[][]{
                        new int[]{1, 2, 7},
                        new int[]{3, 6, 7},
                },
                1,
                6
        ));
    }

    public int numBusesToDestination(int[][] routes, int start, int target) {
        List<Bus> buses = Arrays.stream(routes).map(Bus::new)
                .collect(Collectors.toList());

        for (int i = 0; i < routes.length; i++) {
            Arrays.sort(routes[i]);
        }
        int level = 0;
        Deque<Bus> queue = new ArrayDeque<>();
        for (int i = 0; i < buses.size(); i++) {
            Bus first = buses.get(i);
            for (int j = i + 1; j < buses.size(); j++) {
                Bus second = buses.get(j);
                if (connected(routes[i], routes[j])) {
                    first.connections.add(second);
                    second.connections.add(first);
                }
            }
            if (first.hasStop(start)) {
                if (start == target) {
                    return level;
                }
                queue.add(first);
            }
        }

        Set<Bus> visited = new HashSet<>();

        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                Bus bus = queue.poll();
                visited.add(bus);
                if (bus.hasStop(target)) {
                    return level + 1;
                }
                for (Bus connection : bus.connections) {
                    if (!visited.contains(connection)) {
                        queue.add(connection);
                    }
                }
            }
            level++;
        }
        return -1;

    }

    private boolean connected(int[] b1, int[] b2) {
        int i = 0, j = 0;
        while (i < b1.length && j < b2.length) {
            if (b1[i] == b2[j]) {
                return true;
            }
            if (b1[i] > b2[j]) {
                j++;
            } else {
                i++;
            }
        }
        return false;
    }
}

class Bus {
    Set<Integer> stops;
    List<Bus> connections;

    public Bus(int[] stops) {
        this.stops = IntStream.of(stops).boxed().collect(Collectors.toSet());
        this.connections = new ArrayList<>();
    }

    public boolean hasStop(int stop) {
        return stops.contains(stop);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bus bus = (Bus) o;
        return Objects.equals(stops, bus.stops);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stops);
    }
}

// build a graph of buses and do BFS