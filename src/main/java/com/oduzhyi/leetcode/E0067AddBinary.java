package com.oduzhyi.leetcode;

public class E0067AddBinary {
    public String addBinary(String a, String b) {
        StringBuilder res = new StringBuilder();

        int i = a.length() - 1;
        int j = b.length() - 1;
        int carry = 0;
        while (i >= 0 || j >= 0) {
            int n1 = i >= 0 ? a.charAt(i) - '0' : 0;
            int n2 = j >= 0 ? b.charAt(j) - '0' : 0;

            int sum = n1 + n2 + carry;
            res.insert(0, sum % 2);
            carry = sum / 2;
            i--;
            j--;
        }
        if (carry > 0) {
            res.insert(0, carry);
        }
        return res.toString();
    }
}

// the same idea like in add 2 strings but instead of / 10 and % 10
// I need to use 2
// DO NOT FORGET ABOUT CARRY IN THE END
