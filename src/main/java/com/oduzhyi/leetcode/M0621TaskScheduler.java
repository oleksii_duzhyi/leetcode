package com.oduzhyi.leetcode;

import java.util.Arrays;

public class M0621TaskScheduler {
    public int leastInterval(char[] tasks, int n) {
        // frequencies of the tasks
        int[] frequencies = new int[26];
        for (int t : tasks) {
            frequencies[t - 'A']++;
        }

        Arrays.sort(frequencies);

        // max frequency
        int f_max = frequencies[25];
        int idle_time = (f_max - 1) * n;

        for (int i = frequencies.length - 2; i >= 0 && idle_time > 0; --i) {
            idle_time -= Math.min(f_max - 1, frequencies[i]);
        }
        idle_time = Math.max(0, idle_time);

        return idle_time + tasks.length;
    }

    public int leastIntervalV2(char[] tasks, int n) {
        // frequencies of the tasks
        int[] frequencies = new int[26];
        for (int t : tasks) {
            frequencies[t - 'A']++;
        }

        // max frequency
        int f_max = 0;
        for (int f : frequencies) {
            f_max = Math.max(f_max, f);
        }

        // count the most frequent tasks
        int n_max = 0;
        for (int f : frequencies) {
            if (f == f_max) {
                n_max++;
            }
        }

        return Math.max(tasks.length, (f_max - 1) * (n + 1) + n_max);
    }
}

// find the most frequent task
// calculate maximum idle time =>  (most_freq - 1) * n
// for  every other task - subcstract the min of (most_freq - 1) and (task_freq) from idle
// to account for A A B B C C, n = 1
// return array length and max between 0 and left idle time
