package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class E0993CousinsInBinaryTree {
    public boolean isCousins(TreeNode root, int x, int y) {
        List<NodeWithD> parents = new ArrayList();
        findParents(root, x, y, 0, parents);
        if (parents.size() == 2) {
            NodeWithD p1 = parents.get(0);
            NodeWithD p2 = parents.get(1);
            return p1.d == p2.d && p1.n.val != p2.n.val;
        }
        return false;
    }

    public void findParents(TreeNode root, int x, int y, int depth, List<NodeWithD> parents) {
        if (root == null || parents.size() == 2) {
            return;
        }
        if (root.right != null && x == root.right.val
                || root.left != null && x == root.left.val) {
            parents.add(new NodeWithD(root, depth));
        }
        if (root.right != null && y == root.right.val
                || root.left != null && y == root.left.val) {
            parents.add(new NodeWithD(root, depth));
        }
        findParents(root.right, x, y, depth + 1, parents);
        findParents(root.left, x, y, depth + 1, parents);
    }

    class NodeWithD {
        TreeNode n;
        int d;

        NodeWithD(TreeNode n, int d) {
            this.d = d;
            this.n = n;
        }
    }
}

// questions:
// what is the max depth?
// are values unique?
// does provided node always exists?
//
// i think it is good to start with simple step first - find parent of the specific nodes
// and the depth of those nodes
