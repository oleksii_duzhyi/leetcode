package com.oduzhyi.leetcode;

public class E0833ProjectionAreaOf3DShapes {
    public int projectionArea(int[][] grid) {
        int res = 0;

        for (int x = 0; x < grid.length; x++) {
            int bestRow = 0;
            int bestCol = 0;
            for (int y = 0; y < grid.length; y++) {
                if (grid[x][y] > 0) {
                    res++;
                }
                bestRow = Math.max(bestRow, grid[x][y]);
                bestCol = Math.max(bestCol, grid[y][x]);
            }
            res += bestRow;
            res += bestCol;
        }
        return res;
    }

    public int projectionAreaMy(int[][] grid) {
        if (grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        int res = grid.length * grid[0].length;

        for (int x = 0; x < grid.length; x++) {
            for (int y = 0; y < grid[0].length; y++) {
                if (grid[x][y] == 0) {
                    res--;
                }
            }
        }
        for (int y = 0; y < grid[0].length; y++) {
            int max = grid[0][y];
            for (int x = 1; x < grid.length; x++) {
                max = Math.max(max, grid[x][y]);
            }
            res += max;
        }

        int[] maxXZ = new int[grid.length];
        for (int x = 0; x < grid.length; x++) {
            int max = grid[x][0];
            for (int y = 1; y < grid[0].length; y++) {
                max = Math.max(max, grid[x][y]);
            }
            res += max;
        }
        return res;
    }
}
//so we have to calculte 3 planes projections area
//the simplest area is on xy plane - equal to number of element in the array
//to count projection  on yz plane - we have to find max for each X
//to count projection  on xz plane - we have to find max for each y
//lets assume that input is always positive number and rectangular (NxN)

//we can do it in O(n) where n is number of inputs in the array
//can we bo better? probably no, because we have to pass whole array at least once

