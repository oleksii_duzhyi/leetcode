package com.oduzhyi.leetcode;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class E1279TrafficLightControlledIntersection {

    private final ReentrantLock lock = new ReentrantLock(true);
    private AtomicBoolean roadAGreen;

    public E1279TrafficLightControlledIntersection() {
        roadAGreen = new AtomicBoolean(true);
    }

    public void carArrived(
            int carId,           // ID of the car
            int roadId,          // ID of the road the car travels on. Can be 1 (road A) or 2 (road B)
            int direction,       // Direction of the car
            Runnable turnGreen,  // Use turnGreen.run() to turn light to green on current road
            Runnable crossCar    // Use crossCar.run() to make car cross the intersection
    ) {
        lock.lock();
        try {
            if (roadId == 1 && !roadAGreen.get()) {
                turnGreen.run();
                roadAGreen.set(true);
            } else if (roadId == 2 && roadAGreen.get()) {
                turnGreen.run();
                roadAGreen.set(false);
            }
            crossCar.run();

        } finally {
            lock.unlock();
        }
    }
}