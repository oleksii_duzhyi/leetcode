package com.oduzhyi.leetcode;

public class E0680ValidPalindrome2 {

    public boolean validPalindrome(String s) { // 7 (1) -> 6
        return validPalindrome(s, false);
    }

    private boolean validPalindrome(String s, boolean skipped) {
        int i = 0;
        int j = s.length() - 1;

        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                if (!skipped) {
                    return validPalindrome(s.substring(i + 1, j + 1), true) || validPalindrome(s.substring(i, j), true);
                }
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
}

// create an inner method to check if the current string is valid palindrome and if the char has skipped
// return the result of this method with false argument
// i = 0, j = l - 1
// if chars are not equal
// if the skipped is false -> return result of this method for 2 substrings i + 1, j +1 || i, j, skipped = false
// otherwise return false
