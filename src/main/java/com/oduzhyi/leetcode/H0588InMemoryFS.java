package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class H0588InMemoryFS {

    private Directory root = new Directory("");

    public List<String> ls(String path) {
        String[] parts = path.split("/");
        Directory target = root;
        for (int i = 1; i < parts.length - 1; i++) {
            target = target.subDirs.get(parts[i]);
            if (target == null) {
                return Collections.emptyList();
            }
        }
        if (parts.length - 1 >= 0) {
            if (target.subDirs.containsKey(parts[parts.length - 1])) {
                target = target.subDirs.get(parts[parts.length - 1]);
            } else {
                if (target.files.containsKey(parts[parts.length - 1])) {
                    return Collections.singletonList(parts[parts.length - 1]);
                } else {
                    return Collections.emptyList();
                }
            }
        }
        List<String> inside = new ArrayList<>();
        inside.addAll(target.subDirs.keySet());
        inside.addAll(target.files.keySet());
        Collections.sort(inside);
        return inside;
    }

    public void mkdir(String path) {
        String[] subdir = path.split("/");
        Directory dir = root;
        for (int i = 1; i < subdir.length; i++) {
            dir = dir.subDirs.computeIfAbsent(subdir[i], Directory::new);
        }
    }

    public void addContentToFile(String filePath, String content) {
        Directory target = getTargetDir(filePath);
        String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
        target.files.put(fileName, target.files.getOrDefault(fileName, "") + content);
    }

    public String readContentFromFile(String filePath) {
        Directory target = getTargetDir(filePath);
        String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
        return target.files.get(fileName);
    }

    private Directory getTargetDir(String path) {
        String[] parts = path.split("/");
        Directory target = root;
        for (int i = 1; i < parts.length - 1; i++) {
            target = target.subDirs.get(parts[i]);
        }
        return target;
    }
}

class Directory {
    String name;
    Map<String, Directory> subDirs = new HashMap<>();
    Map<String, String> files = new HashMap<>();

    public Directory(String name) {
        this.name = name;
    }
}
