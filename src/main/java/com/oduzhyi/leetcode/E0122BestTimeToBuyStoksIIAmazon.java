package com.oduzhyi.leetcode;

public class E0122BestTimeToBuyStoksIIAmazon {
    //[7,1,5,3,6,4]
    public int maxProfit(int[] prices) {
        if (prices.length == 0) {
            return 0;
        }
        int totalProfit = 0;
        int maxProfit = 0;
        int buy = prices[0]; //7

        for (int i = 1; i < prices.length; i++) {
            int newProfit = prices[i] - buy; // 2
            if (newProfit >= maxProfit) {
                maxProfit = newProfit;
            } else {
                totalProfit += maxProfit;
                buy = prices[i];
                maxProfit = 0;
            }
        }
        return totalProfit + maxProfit;
    }
}

// the idea will remain pretty similar to previous one, with just one simple correction -
// when we see a new price that appears to be smaller than the previous one
// it is potential for us to make additional profit