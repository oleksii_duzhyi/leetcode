package com.oduzhyi.leetcode;

public class M0079WordSearch {
    public boolean exist(char[][] board, String word) {
        if (board.length * board[0].length < word.length()) {
            return false;
        }
        char[] chars = word.toCharArray();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] == chars[0]) {
                    boolean found = dfs(board, i, j, 0, chars);
                    if (found) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean dfs(char[][] board, int i, int j, int pos, char[] word) {
        if (pos + 1 == word.length) {
            return true;
        }
        char c = board[i][j];
        board[i][j] = (char) 0;

        boolean found = false;
        if (i > 0 && word[pos + 1] == board[i - 1][j]) {
            found = dfs(board, i - 1, j, pos + 1, word);
        }
        if (!found && i + 1 < board.length && word[pos + 1] == board[i + 1][j]) {
            found = dfs(board, i + 1, j, pos + 1, word);
        }
        if (!found && j > 0 && word[pos + 1] == board[i][j - 1]) {
            found = dfs(board, i, j - 1, pos + 1, word);
        }
        if (!found && j + 1 < board[0].length && word[pos + 1] == board[i][j + 1]) {
            found = dfs(board, i, j + 1, pos + 1, word);
        }
        board[i][j] = c;
        return found;
    }
}