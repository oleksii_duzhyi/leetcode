package com.oduzhyi.leetcode;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class M0133CloneGraph {
    Map<Integer, Node> visited = new HashMap<>();

    public Node cloneGraph(Node node) {
        if (node == null) {
            return null;
        }
        if (visited.containsKey(node.val)) {
            return visited.get(node.val);
        }
        PriorityQueue<int[]> pq = new PriorityQueue<>(Comparator.comparingDouble(array -> Math.sqrt(array[0] * array[0] + array[1] * array[1])));
        Node root = new Node(node.val);
        visited.put(node.val, root);
        List<Node> children = root.children;
        for (Node c : node.children) {
            Node newC = cloneGraph(c);
            children.add(newC);
        }
        return root;
    }
}

// do a DFS - start from the given node
// put cloned node to HM under it's value
// for every children -> make a recursive call, add it's result to neighboors
