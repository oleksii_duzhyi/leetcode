package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class M0003LongestUniqueSubstring {

    public static void main(String[] args) {
        new M0003LongestUniqueSubstring().lengthOfLongestSubstring2("dvxyzvabc");
    }

    public int lengthOfLongestSubstring(String s) {
        int maxL = 0;
        Map<Character, Integer> seen = new HashMap<>();
        char[] chars = s.toCharArray();

        for (int i = 0, j = 0; i < s.length() && j < s.length(); ) {
            if (seen.containsKey(chars[j])) {
                i = Math.max(seen.get(chars[j]), i);
            }
            seen.put(chars[j], ++j);
            maxL = Math.max(maxL, j - i);
        }
        return maxL;
    }

    public int lengthOfLongestSubstringSet(String s) {
        int maxL = 0;
        Set<Character> seen = new HashSet<>();
        char[] chars = s.toCharArray();

        for (int i = 0, j = 0; i < s.length() && j < s.length(); ) {
            while (seen.contains(chars[j])) {
                seen.remove(chars[i++]);
            }
            seen.add(chars[j++]);
            maxL = Math.max(maxL, j - i);
        }
        return maxL;
    }

    public int lengthOfLongestSubstringv0(String s) {
        Map<Character, Integer> seen = new HashMap<>();
        int res = 0;
        int maxRes = Integer.MIN_VALUE;
        for (int i = 0; i < s.length(); ) {
            char c = s.charAt(i);
            if (seen.containsKey(c)) {
                maxRes = Math.max(maxRes, res);
                i = seen.get(c) + 1;
                res = 0;
                seen.clear();
            } else {
                res++;
                seen.put(c, i);
                i++;
            }
        }
        return Math.max(maxRes, res);
    }


    public int lengthOfLongestSubstringOld(String s) {
        short[] indexes = new short[128];
        int start = 0, max = 0;
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (indexes[ch] >= start) {
                start = indexes[ch] + 1;
            } else {
                max = Math.max(max, i - start + 1);
            }
            indexes[ch] = (short) i;
        }
        return max;
    }

    public int lengthOfLongestSubstring2(String s) {
        int n = s.length();
        Set<Character> set = new HashSet<>();
        int ans = 0, i = 0, j = 0;
        while (i < n && j < n) {
            // try to extend the range [i, j]
            if (!set.contains(s.charAt(j))) {
                set.add(s.charAt(j++));
                ans = Math.max(ans, j - i);
            } else {
                set.remove(s.charAt(i++));
            }
        }
        return ans;
    }
}

// basic idea for set - use 2 pointer - i,j
// both start @ 0.
// check the character @ j is in set
// if yes - remove character @ i++ until char @ j is not at the set
// put char @ j to set
// update max length and increment j

// basic idea for map - use 2 pointer - i,j
// both start @ 0.
// check the character @ j is in map
// if yes - update i position with max between i and map.get(char @ j)
// put the next position after the current character
// update max length (j - i)