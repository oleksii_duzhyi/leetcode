package com.oduzhyi.leetcode;

public class M1265PrintImmutableListInReverse {

    public void printLinkedListInReverse(ImmutableListNode head) {
        if (head == null) {
            return;
        }
        int size = getSize(head);

        while (size > 0) {
            ImmutableListNode curr = head;
            for (int i = 0; i < size - 1; i++) {
                curr = curr.getNext();
            }
            curr.printValue();
            size--;
        }
    }

    public void printLinkedListInReverseLinearSpace(ImmutableListNode head) {
        if (head == null) {
            return;
        }
        int size = getSize(head);

        while (size > 0) {
            ImmutableListNode curr = head;
            for (int i = 0; i < size - 1; i++) {
                curr = curr.getNext();
            }
            curr.printValue();
            size--;
        }
    }

    private int getSize(ImmutableListNode head) {
        int size = 0;
        while (head != null) {
            head = head.getNext();
            size++;
        }
        return size;
    }

    public void printLinkedListInReverseRec(ImmutableListNode head) {
        if (head == null) {
            return;
        }
        printLinkedListInReverse(head.getNext());
        head.printValue();
    }
}


