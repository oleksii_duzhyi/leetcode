package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class M0046Permutations {
    private List<List<Integer>> res = new ArrayList<>();

    public List<List<Integer>> permute(int[] nums) {
        permute(nums, 0);
        return res;
    }

    private void permute(int[] nums, int first) {
        if (first == nums.length) {
            List<Integer> r = new ArrayList<>();
            for (int elem : nums) {
                r.add(elem);
            }
            res.add(r);
        } else {
            for (int i = first; i < nums.length; i++) {
                int tmp = nums[i];
                nums[i] = nums[first];
                nums[first] = tmp;

                permute(nums, first + 1);

                tmp = nums[i];
                nums[i] = nums[first];
                nums[first] = tmp;
            }
        }
    }
}