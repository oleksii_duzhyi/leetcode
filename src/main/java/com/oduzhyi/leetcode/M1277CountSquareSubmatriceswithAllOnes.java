package com.oduzhyi.leetcode;

public class M1277CountSquareSubmatriceswithAllOnes {

    public int countSquares(int[][] matrix) {
    /*
      Time Complexity: O(MN)
      Space Complexity: O(1) input array is modified
    */

        int result = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {

                // for top most row(i == 0) and left most column j == 0, directly
                // value in the matrix will be added as result.
                if (matrix[i][j] > 0 && i > 0 && j > 0) {
                    int min = Math.min(matrix[i - 1][j], Math.min(matrix[i][j - 1], matrix[i - 1][j - 1]));
                    matrix[i][j] = min + 1;
                }

                result += matrix[i][j];
            }
        }

        return result;
    }

    public int countSquaresIntuitive(int[][] matrix) {
        int res = 0;
        int r = matrix.length;
        int c = matrix[0].length;
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                if (matrix[i][j] == 1) {
                    res++;
                    int step = 1;
                    while (i + step < r && j + step < c && isSquare(i, j, step, matrix)) {
                        res++;
                        step++;
                    }
                }
            }
        }
        return res;
    }

    private boolean isSquare(int i, int j, int step, int[][] matrix) {
        boolean square = true;
        for (int k = j; k <= j + step && square; k++) {
            square = matrix[i + step][k] == 1;
        }
        for (int k = i; k <= i + step && square; k++) {
            square = matrix[k][j + step] == 1;
        }
        return square;
    }
}