package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class H0057InsertInterval {

    public int[][] insert(int[][] intervals, int[] newInterval) {
        int start = newInterval[0];
        int end = newInterval[1];
        List<int[]> list = new ArrayList<>();
        for (int[] interval : intervals) {
            int curStart = interval[0];
            int curEnd = interval[1];
            if (curEnd < start) {
                list.add(new int[]{curStart, curEnd});
            } else if (curStart > end) {
                list.add(new int[]{start, end});
                start = curStart;
                end = curEnd;
            } else {
                start = Math.min(start, curStart);
                end = Math.max(end, curEnd);
            }
        }
        list.add(new int[]{start, end});
        return list.toArray(new int[list.size()][]);
    }

    public int[][] insertInitial(int[][] intervals, int[] newInterval) {
        if (intervals.length == 0) {
            return new int[][]{newInterval};
        }
        List<int[]> joint = new ArrayList<>();
        boolean addedNew = false;
        for (int i = 0; i < intervals.length; i++) {
            if (!addedNew && intervals[i][0] >= newInterval[0]) {
                addedNew = true;
                joint.add(newInterval);
            }
            joint.add(intervals[i]);
        }
        if (!addedNew) {
            joint.add(newInterval);
        }

        List<int[]> res = new ArrayList<>();
        for (int i = 0; i < joint.size(); i++) {
            int start = joint.get(i)[0];
            int end = joint.get(i)[1];
            while (i + 1 < joint.size() && joint.get(i + 1)[0] <= end) {
                end = Math.max(end, joint.get(i + 1)[1]);
                i++;
            }
            res.add(new int[]{start, end});
        }
        return res.toArray(new int[res.size()][]);
    }
}

// my solution: find a place where to insert the new interval
// on the next go - merge all overlapping intervals
// return result
// better solution:
// first step: keep adding intervals to result while end < newStart
// if the nextStart > newEnd - add newS, newE, and update them to nextSt, nextE
// if intervals intersect - keep min start and max end
// in the end add last interval
