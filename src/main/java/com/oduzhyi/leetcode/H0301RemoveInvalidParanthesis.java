package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class H0301RemoveInvalidParanthesis {
    Set<String> res = new HashSet<>();
    Set<String> seen = new HashSet<>();

    public List<String> removeInvalidParentheses(String s) {
        StringBuilder sb = new StringBuilder(s);
        int invalidParanthesis = countInvalid(sb);
        if (invalidParanthesis == 0) {
            return Collections.singletonList(s);
        }
        if (invalidParanthesis == s.length()) {
            return Collections.singletonList("");
        }
        removeInvalidParentheses(new StringBuilder(sb), invalidParanthesis);
        return new ArrayList<>(res);
    }

    public void removeInvalidParentheses(StringBuilder s, int invalid) {
        String str = s.toString();
        if (seen.contains(str)) {
            return;
        }
        if (!isInvalid(s)) {
            res.add(s.toString());
        }
        seen.add(str);
        if (invalid == 0) {
            return;
        }
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(' || s.charAt(i) == ')') {
                char c = s.charAt(i);
                s.deleteCharAt(i);
                removeInvalidParentheses(s, invalid - 1);
                s.insert(i, c);
            }
        }

    }

    private boolean isInvalid(StringBuilder s) {
        return countInvalid(s) > 0;
    }

    private int countInvalid(StringBuilder s) {
        int cnt = 0;
        int invalid = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == ')') {
                if (cnt == 0) {
                    invalid++;
                } else {
                    cnt--;
                }
            } else if (c == '(') {
                cnt++;
            }
        }
        return invalid + cnt;
    }
}

// count the brackets out of place
// if all in place - return array with input
// if all chars are out of place - return array with empty string
// initiate a recursive call with initial string and number of brackets trying to remove
// if string is valid - add to result
// if number of invalid string removed has  been  reached - exit current method
// otherwise for every bracket - make recursive call without it
// also cache seen strings to no repeat ourselved
