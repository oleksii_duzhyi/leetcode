package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


public class E0760FindAnagramMappings {
    public int[] anagramMappings(int[] A, int[] B) {
        Map<Integer, Integer> bLoc = new HashMap<>();

        for (int i = 0; i < B.length; i++) {
            bLoc.put(B[i], i);
        }
        // (2 -> [0], 1 -> [1])

        int[] res = new int[A.length];
        for (int i = 0; i < A.length; i++) {
            res[i] = bLoc.get(A[i]);
        }

        return res;
    }


    public int[] anagramMappingsUniques(int[] A, int[] B) {
        Map<Integer, LinkedList<Integer>> bLoc = new HashMap<>();

        for (int i = 0; i < B.length; i++) {
            int b = B[i];
            LinkedList<Integer> loc = bLoc.computeIfAbsent(b, l -> new LinkedList<>());
            loc.add(i);
        }
        // (2 -> [0], 1 -> [1])

        int[] res = new int[A.length];
        for (int i = 0; i < A.length; i++) {
            LinkedList<Integer> loc = bLoc.get(A[i]);
            int j = loc.removeLast();
            res[i] = j;
        }

        return res;
    }
}
// Questions:
// what are the sizes of arrays rnage
// are they equal
// can they be null?
// are there dupicates?
// MISSED QUESTION - can we use same resul index twice if there are duplicates?


// simple case
// A = [1, 2]   B = [2, 1]
// RES = [1, 0]   A[0] == B[1]

// straight forward solution is to create a 2 for loops
// get element at A[i]
// if A[i] == B[j] -> add j to at postion i to result array

// alternatively we can create a Map<Integer, Set<Intger>> numLocations from B array
// and eliminate inner loop
// to the cost of additional O(n)
// NEED TO THINK MORE - why set? will we remove elements from it? how we'll do it?
