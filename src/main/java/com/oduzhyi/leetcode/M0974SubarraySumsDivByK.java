package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class M0974SubarraySumsDivByK {

    public int subarraysDivByK(int[] A, int K) {
        int[] map = new int[K];
        map[0] = 1;
        int count = 0, sum = 0;
        for (int a : A) {
            sum = (sum + a) % K;
            if (sum < 0) {
                sum += K;  // Because -1 % 5 = -1, but we need the positive mod 4
            }
            count += map[sum];
            map[sum]++;
        }
        return count;
    }

    // sum (i, j) % k == 0
    //    8              3
    // (sum(0, j) - sum(0, i)) % k = 0
    // sum(0,j) % k == sum(0,i) => sum (i, j) % k == 0
    //

    // k = 5
    //        V
    // arr: 4,5,0,1
    // map: {1: 1, }
    public int subarraysDivByKMap(int[] a, int k) {
        Map<Integer, Integer> sums = new HashMap<>();
        sums.put(0, 1);
        int res = 0;
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
            int reminder = sum % k < 0 ? sum % k + k : sum % k;
            res += sums.getOrDefault(reminder, 0);
            sums.put(reminder, sums.getOrDefault(reminder, 0) + 1);

        }
        return res;
    }

    public int subarraysDivByKN2(int[] a, int k) {
        int res = 0;

        for (int i = 0; i < a.length; i++) {
            if (a[i] % k == 0) {
                res++;
            }
            int sum = a[i];
            for (int j = i + 1; j < a.length; j++) {
                sum += a[j];
                if (sum % k == 0) {
                    res++;
                }
            }
        }

        return res;
    }
}

//main idea
// sum(i,j) = sum(0,j) - sum(0,i)
// if sum(0,j) % k == sum(0,i) % k  ===> sum(i, j) % k == 0
// create a map to count freq of reminders
// freq of 0 reminder - is 1
// if reminder < 0 => add k to it
// if there is already such reminder in map - add count of thi reminders to result
// update frequency map