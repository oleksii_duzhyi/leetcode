package com.oduzhyi.leetcode;

public class E1460MakeTwoArraysEqualByReversingSubArrays {
    public boolean canBeEqual(int[] target, int[] arr) {
        int[] hash = new int[1001];
        for (int i : target) {
            hash[i]++;
        }
        for (int i : arr) {
            if (hash[i] - 1 < 0) {
                return false;
            }
            hash[i]--;
        }

        return true;
    }
}