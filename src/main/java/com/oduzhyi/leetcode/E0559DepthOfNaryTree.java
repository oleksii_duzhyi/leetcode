package com.oduzhyi.leetcode;

public class E0559DepthOfNaryTree {
    public int maxDepth(Node root) {
        return maxDepth(root, 0);
    }

    public int maxDepth(Node root, int depth) {
        if (root == null) {
            return depth;
        }
        int cDepth = depth + 1;
        for (Node child : root.children) {
            cDepth = Math.max(cDepth, maxDepth(child, depth + 1));
        }
        return cDepth;
    }
}

// questions:
// what is the max depth possible?
//
// easy recursive call
