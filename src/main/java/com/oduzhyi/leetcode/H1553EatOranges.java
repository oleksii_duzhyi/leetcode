package com.oduzhyi.leetcode;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class H1553EatOranges {

    Map<Integer, Integer> dp = new HashMap<>();

    public static void main(String[] args) {
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            if (i % 3 == 0) {
                if (i - 2 * i / 3 != i - 2 * (i / 3)) {
                    System.out.println(i);
                    return;
                }
            }
        }
    }

    public int minDays(int n) {
        if (n <= 1) {
            return n;
        }
        if (!dp.containsKey(n)) {
            dp.put(n, 1 + Math.min(n % 2 + minDays(n / 2), n % 3 + minDays(n / 3)));
        }
        return dp.get(n);
    }

    public int minDaysBFS(int n) {
        Set<Integer> currLevel = Collections.singleton(n);

        int days = 0; //3

        while (!currLevel.isEmpty()) {
            Set<Integer> nextLevel = new HashSet<>();// {}

            for (Integer curr : currLevel) {        // 5
                if (curr - 1 == 0) {
                    return days + 1;
                }
                if (curr % 2 == 0) {
                    nextLevel.add(curr - curr / 2);// 1
                }
                if (curr % 3 == 0) {
                    nextLevel.add(curr - 2 * (curr / 3));  // 1
                }
                nextLevel.add(curr - 1);
            }
            currLevel = nextLevel;
            days++;
        }
        return days;

    }
}
// BFS to find the minumum path
// or use a DP - 1 + min( (n%2) + f(n/2), (n%3) + f(n/3) )
// with memoization of min(n) in map