package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

public class M0230KthElementInBST {

    public int kthSmallest(TreeNode root, int k) {
        Deque<TreeNode> stack = new ArrayDeque<>();
        int i = 0;
        TreeNode cur = root;
        while (cur != null || !stack.isEmpty()) {
            while (cur != null) {
                stack.push(cur);
                cur = cur.left;
            }
            TreeNode leftmost = stack.pop();
            if (++i == k) {
                return leftmost.val;
            }
            cur = leftmost.right;
        }
        throw new IllegalArgumentException();
    }
}