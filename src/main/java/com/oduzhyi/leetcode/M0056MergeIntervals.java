package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class M0056MergeIntervals {
    public int[][] merge(int[][] intervals) {
        if (intervals.length == 0) {
            return intervals;
        }
        Arrays.sort(intervals, Comparator.comparingInt(i -> i[0]));

        List<int[]> res = new ArrayList<>();
        int[] interval = intervals[0];
        for (int i = 1; i < intervals.length; i++) {
            int[] next = intervals[i];
            if (next[0] <= interval[1]) {
                interval[1] = Math.max(next[1], interval[1]);
            } else {
                res.add(interval);
                interval = next;
            }
        }
        res.add(interval);
        return res.toArray(new int[res.size()][]);
    }
}

// sort arrays by the start of the interval
// create a list of arrays for result
// take first interval
// try to merge and simultaneously update an end
// if can't merge - add to result prev and procees with next