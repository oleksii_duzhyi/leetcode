package com.oduzhyi.leetcode;

import java.util.LinkedList;
import java.util.List;

public class E0989AddToArrayFormOfInteger {
    public List<Integer> addToArrayForm(int[] a, int k) {
        LinkedList<Integer> res = new LinkedList<>();
        int aP = a.length - 1;
        int carry = 0;
        while (aP >= 0 || k > 0 || carry > 0) {
            int n1 = aP >= 0 ? a[aP] : 0;
            int n2 = k % 10;

            int next = n1 + n2 + carry;
            res.addFirst(next % 10);
            carry = next / 10;
            aP--;
            k /= 10;
        }
        return res;
    }
}

// 3 ways - either transfrom a[] to int and sum and convert back
// or transform k to int sum and return
// or build result as you go
