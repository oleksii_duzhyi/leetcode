package com.oduzhyi.leetcode;

import java.util.ArrayList;
import java.util.List;

public class E0728IsSelfDividing {
    public List<Integer> selfDividingNumbers(int left, int right) {
        List<Integer> result = new ArrayList<>();

        for (int i = left; i <= right; i++) {
            if (isSelfDividing(i)) {
                result.add(i);
            }
        }
        return result;
    }

    private boolean isSelfDividing(int i) {
        boolean res = true;
        int I = i;
        while (I > 0 && res) {
            res = (I % 10) != 0 && (i % (I % 10) == 0);
            I /= 10;
        }
        return res;
    }

    public List<Integer> selfDividingNumbersOld(int left, int right) {
        List<Integer> ans = new ArrayList<>();
        for (int n = left; n <= right; ++n) {
            if (selfDividing(n)) {
                ans.add(n);
            }
        }
        return ans;
    }

    public boolean selfDividing(int n) {
        for (char c : String.valueOf(n).toCharArray()) {
            if (c == '0' || (n % (c - '0') > 0)) {
                return false;
            }
        }
        return true;
    }
}