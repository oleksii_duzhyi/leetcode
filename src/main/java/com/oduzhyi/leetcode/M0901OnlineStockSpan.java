package com.oduzhyi.leetcode;

import java.util.LinkedList;

public class M0901OnlineStockSpan {

    class StockSpanner {
        private int[] prices = new int[10_000];
        private int[] spans = new int[10_000];
        private int pointer = 0;

        private LinkedList<Integer> ps;
        private LinkedList<Integer> ws;

        public StockSpanner() {
            this.ps = new LinkedList<>();
            this.ws = new LinkedList<>();
        }

        public int next(int price) {
            int w = 1;
            while (!ps.isEmpty() && ps.peek() <= price) {
                ps.pop();
                w += ws.pop();
            }
            ps.push(price);
            ws.push(w);
            return w;
        }

        public int nextArray(int price) {
            prices[pointer] = price;
            if (pointer == 0) {
                spans[pointer++] = 1;
                return 1;
            } else {
                int res = 1;
                int i = pointer - 1;
                while (i >= 0 && price >= prices[i]) {
                    res += spans[i];
                    i -= spans[i];
                }
                spans[pointer++] = res;
                return res;
            }
        }
    }

}

