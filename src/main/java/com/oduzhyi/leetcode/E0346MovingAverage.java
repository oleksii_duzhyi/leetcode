package com.oduzhyi.leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class E0346MovingAverage {
    class MovingAverage {
        private final int size;
        private final Queue<Integer> queue;
        private long currentSum = 0;

        /**
         * Initialize your data structure here.
         */
        public MovingAverage(int size) {
            this.size = size;
            this.queue = new LinkedList<>();
        }

        public double next(int val) {
            queue.add(val);
            currentSum += val;
            if (queue.size() > size) {
                currentSum -= queue.remove();
            }
            return currentSum / (double) queue.size();
        }
    }
}

/**
 * Your MovingAverage object will be instantiated and called as such:
 * MovingAverage obj = new MovingAverage(size);
 * double param_1 = obj.next(val);
 */

// Questions:
// is size > 0?
// can there be negative integers?
// can sum result in integer overflow?
//
// we need to track integers that were added and their order
// also it makes sense to track of a sum to not calculate it over every time next is called