package com.oduzhyi.leetcode;

/**
 * Created by Нелла on 25.12.2019.
 */
public class M0008StringToIntegerAtoi {
    public int myAtoi(String str) {
        long num = 0;
        boolean positive = true;
        int i = 0;


        while (i < str.length() && str.charAt(i) == ' ') {
            i++;
        }
        if (i < str.length() && (str.charAt(i) == '-' || str.charAt(i) == '+')) {
            positive = str.charAt(i) == '+';
            i++;
        }

        for (; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c >= '0' && c <= '9') {
                num = num * 10L + (c - '0');
                if (num > Integer.MAX_VALUE + 1L) {
                    break;
                }
            } else {
                break;
            }
        }
        if (positive) {
            return num >= Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) num;
        } else {
            return num >= Integer.MAX_VALUE + 1L ? Integer.MIN_VALUE : (int) -num;
        }

    }

    public int myAtoiWorking(String str) {
        long num = 0;
        boolean positive = true;
        boolean numberStarted = false;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (!numberStarted) {
                if (c == ' ') {
                    continue;
                }
                if (c == '-' || c == '+') {
                    numberStarted = true;
                    positive = c == '+';
                } else if (c >= '0' && c <= '9') {
                    numberStarted = true;
                    num = num * 10 + (c - '0');
                } else {
                    return 0;
                }
            } else {
                if (c >= '0' && c <= '9') {
                    num = num * 10L + (c - '0');
                    if (num > Integer.MAX_VALUE + 1L) {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        if (positive) {
            return num >= Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) num;
        } else {
            return num >= Integer.MAX_VALUE + 1L ? Integer.MIN_VALUE : (int) -num;
        }

    }

    public int myAtoiOld(String str) {
        int s = -1, f = -1;
        Boolean positive = null;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (s < 0) {
                if (c != ' ') {
                    if (c == '+' && positive == null) {
                        positive = true;
                    } else if (c == '-' && positive == null) {
                        positive = false;
                    } else if (c >= 48 && c <= 57) {
                        s = i;
                        f = i;
                    } else {
                        return 0;
                    }
                }
            } else {
                if (c < 48 && c > 57) {
                    f = i - 1;
                    break;
                } else {
                    f = i;
                }
            }
        }

        if (s == -1) {
            return 0;
        }
        long sum = 0;
        for (int i = f, count = 0; i >= s; i--, count++) {
            sum += (str.charAt(i) - 48) * (long) Math.pow(10, count);
            if (sum > Integer.MAX_VALUE) {
                return positive == null || positive ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            }
        }
        return positive == null || positive ? (int) sum : (int) sum * -1;
    }
}

// first skip all preleading spaces
// then check if the first char is sign if yes - remember it
// iterarte over string
// if char is not Digit - break
// otherwise increment sum
//      if sum exceeds Integer MAX_VALUE + 1 break
// if number is positive and >= than MAX_VALUE - return MAX_VALUE otherwise return sum
// if number is negative and >= than MAX_VALUE + 1 - return MIN_VALUE otherwise return -sum