package com.oduzhyi.leetcode;

public class E0700SearchInABST {
    public TreeNode searchBST(TreeNode root, int val) {
        if (root == null) {
            return null;
        }
        if (root.val == val) {
            return root;
        }
        if (val > root.val) {
            return searchBST(root.right, val);
        } else {
            return searchBST(root.left, val);
        }
    }
}

// Questions:
// what to return if number is not found?
// what is the maximum depth of binary tree?
//
// pretty easy recursive call