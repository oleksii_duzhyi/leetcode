package com.oduzhyi.leetcode;

import java.util.ArrayDeque;

public class H0273IntegerToEnglishWords {
    private final String[] LESS_THAN_20 = {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
    private final String[] TENS = {"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
    private final String[] THOUSANDS = {"", "Thousand", "Million", "Billion"};

    public String numberToWords(int num) {
        if (num == 0) {
            return "Zero";
        }
        ArrayDeque<String> stack = new ArrayDeque<>();

        int level = 0;
        while (num > 0) {
            if (num % 1000 > 0) {
                stack.push(THOUSANDS[level]);
                if (num % 100 < 20) {
                    stack.push(LESS_THAN_20[num % 100]);
                } else {
                    stack.push(LESS_THAN_20[num % 10]);
                    stack.push(TENS[num % 100 / 10]);
                }
                if (num % 1000 >= 100) {
                    stack.push("Hundred");
                    stack.push(LESS_THAN_20[num % 1000 / 100]);
                }
            }
            level++;
            num /= 1000;
        }
        StringBuilder res = new StringBuilder();
        while (!stack.isEmpty()) {
            String next = stack.pop();
            if (!next.isEmpty()) {
                res.append(next).append(" ");
            }
        }
        res.deleteCharAt(res.length() - 1);
        return res.toString();
    }
}

// create aux arrays for < 20, tens, and thousands
// process a thousand at a time
// if num%1000 == 0 - this means there is nothing we can add, increment the level and continue
// else
//    push current level
//    construct < 100 correctly
// add hundred only if reminder from % 1000 is >= 1000
