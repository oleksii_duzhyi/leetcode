package com.oduzhyi.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class M0322CoinChange {
    Map<Integer, Integer> dp = new HashMap<>();

    public int coinChange(int[] coins, int amount) {
        int max = amount + 1;
        int[] dp = new int[amount + 1];
        Arrays.fill(dp, max);
        dp[0] = 0;
        for (int i = 1; i <= amount; i++) {
            for (int j = 0; j < coins.length; j++) {
                if (coins[j] <= i) {
                    dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
                }
            }
        }
        return dp[amount] > amount ? -1 : dp[amount];
    }

    public int coinChangeTopDown(int[] coins, int amount) {
        if (amount < 0) {
            return -1;
        }
        if (amount == 0) {
            return 0;
        }
        if (dp.containsKey(amount)) {
            return dp.get(amount);
        }
        int res = Integer.MAX_VALUE;

        for (int c : coins) {
            int tries = coinChange(coins, amount - c);
            if (tries >= 0 && tries < res) {
                res = 1 + tries;
            }
        }
        dp.put(amount, res == Integer.MAX_VALUE ? -1 : res);
        return dp.get(amount);
    }
}
// possible solutions: BFS, resursive with Memoization, DP bottom  up
