package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

public class M0285InorderSuccessorBST {

    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        TreeNode succ = null, curr = root;
        while (curr != null) {
            if (p.val < curr.val) {
                succ = curr;
                curr = curr.left;
            } else {
                curr = curr.right;
            }
        }
        return succ;
    }

    public TreeNode inorderSuccessorFullSearch(TreeNode root, TreeNode p) {
        if (root == null || p == null) {
            return null;
        }

        if (p.right != null) {
            p = p.right;
            while (p.left != null) {
                p = p.left;
            }
            return p;
        }

        Deque<TreeNode> stack = new ArrayDeque<>();
        TreeNode cur = root;
        int prev = Integer.MIN_VALUE;
        while (cur != null || !stack.isEmpty()) {
            while (cur != null) {
                stack.push(cur);
                cur = cur.left;
            }
            TreeNode leftmost = stack.pop();
            if (prev == p.val) {
                return leftmost;
            }
            prev = leftmost.val;
            cur = leftmost.right;
        }

        return null;
    }
}