package com.oduzhyi.leetcode;

public class E0203RemoveFromLinkedList {
    public ListNode removeElements(ListNode head, int val) {
        ListNode fakeHead = new ListNode();
        ListNode res = fakeHead;

        while (head != null) {
            if (head.val != val) {
                fakeHead.next = head;
                fakeHead = fakeHead.next;
            }
            head = head.next;
        }
        fakeHead.next = null;
        return res.next;
    }

    public ListNode removeElementsRec(ListNode head, int val) {
        if (head == null) {
            return null;
        }
        if (head.val == val) {
            return removeElementsRec(head.next, val);
        }
        head.next = removeElementsRec(head.next, val);
        return head;
    }
}
