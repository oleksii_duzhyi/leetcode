package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

public class M0098ValidateBST {
    public boolean isValidBSTRec(TreeNode root) {
        return helper(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    private boolean helper(TreeNode tn, long lower, long upper) {
        if (tn == null) {
            return true;
        }
        if (tn.val <= lower || tn.val >= upper) {
            return false;
        }
        return helper(tn.left, lower, tn.val) && helper(tn.right, tn.val, upper);
    }

    public boolean isValidBST(TreeNode root) {
        if (root == null) {
            return true;
        }
        ArrayDeque<TreeNode> stack = new ArrayDeque<>();
        long min = Long.MIN_VALUE;

        TreeNode curr = root;
        while (curr != null || !stack.isEmpty()) {
            while (curr != null) {
                stack.push(curr);
                curr = curr.left;
            }
            curr = stack.pop();
            if (min >= curr.val) {
                return false;
            }
            min = curr.val;
            curr = curr.right;
        }

        return true;
    }

    public boolean isValidBSTInOrder(TreeNode root) {
        if (root == null) {
            return true;
        }
        ArrayDeque<TreeNode> stack = new ArrayDeque<>();
        stack.push(root);
        List<Integer> vals = new ArrayList<>();

        while (!stack.isEmpty()) {
            while (stack.peek().left != null) {
                TreeNode tn = stack.peek();
                stack.push(tn.left);
                tn.left = null;
            }
            TreeNode next = stack.pop();
            vals.add(next.val);
            if (next.right != null) {
                stack.push(next.right);
            }
        }
        for (int i = 0; i < vals.size() - 1; i++) {
            if (vals.get(i) >= vals.get(i + 1)) {
                return false;
            }
        }
        return true;
    }
}
