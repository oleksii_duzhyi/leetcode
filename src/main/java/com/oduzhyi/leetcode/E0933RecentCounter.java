package com.oduzhyi.leetcode;

import java.util.LinkedList;

/**
 * Created by Нелла on 20.11.2018.
 */
public class E0933RecentCounter {

    public static void main(String[] args) {
        int[] times = new int[]{1, 100, 3001, 3002, 3300};
        RecentCounter rc = new RecentCounter();
        for (int t : times) {
            System.out.println(rc.ping(t));
        }
    }

    static class RecentCounter {

        private LinkedList<Integer> list = new LinkedList<>();

        public RecentCounter() {
        }

        public int ping(int t) {
            while (!list.isEmpty() && t - list.getFirst() > 3000) {
                list.removeFirst();
            }
            list.add(t);
            return list.size();
        }
    }
}
