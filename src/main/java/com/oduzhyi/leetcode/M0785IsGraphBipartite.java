package com.oduzhyi.leetcode;

import java.util.HashMap;
import java.util.Map;

public class M0785IsGraphBipartite {
    public boolean isBipartite(int[][] graph) {

        Map<Integer, Integer> colored = new HashMap<>();
        for (int v = 0; v < graph.length; v++) {
            if (!colored.containsKey(v) && !dfs(v, 0, graph, colored)) {
                return false;
            }
        }
        return true;
    }

    private boolean dfs(Integer v, int color, int[][] graph, Map<Integer, Integer> colored) {
        if (colored.containsKey(v)) {
            return color == colored.get(v);
        }
        colored.put(v, color);

        for (int connected : graph[v]) {
            if (!dfs(connected, color == 0 ? 1 : 0, graph, colored)) {
                return false;
            }
        }
        return true;
    }
}
