package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

public class M0200NumberOfIslands {
    public int numIslands(char[][] grid) {
        if (grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        int r = grid.length;
        int c = grid[0].length;
        int res = 0;

        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                if (grid[i][j] == '1') {
                    exploreIsland(i, j, grid);
                    res++;
                }
            }
        }
        return res;
    }

    private void exploreIslandRec(int x, int y, char[][] grid) {
        if (x < 0 || y < 0 || x > grid.length || y > grid[0].length || grid[x][y] == '0') {
            return;
        }

        grid[x][y] = '0';
        exploreIslandRec(x - 1, y, grid);
        exploreIslandRec(x + 1, y, grid);
        exploreIslandRec(x, y - 1, grid);
        exploreIslandRec(x, y + 1, grid);
    }


    private void exploreIsland(int x, int y, char[][] grid) {
        int r = grid.length;
        int c = grid[0].length;
        int max = Math.max(r, c);
        Deque<Integer> queue = new ArrayDeque<>();
        queue.add(x * max + y);
        grid[x][y] = '0';

        while (!queue.isEmpty()) {
            int next = queue.poll();
            int i = next / max;
            int j = next % max;

            if (i > 0 && grid[i - 1][j] == '1') {
                queue.add((i - 1) * max + j);
                grid[i - 1][j] = '0';
            }
            if (i < r - 1 && grid[i + 1][j] == '1') {
                queue.add((i + 1) * max + j);
                grid[i + 1][j] = '0';
            }
            if (j > 0 && grid[i][j - 1] == '1') {
                queue.add(i * max + j - 1);
                grid[i][j - 1] = '0';
            }
            if (j < c - 1 && grid[i][j + 1] == '1') {
                queue.add(i * max + j + 1);
                grid[i][j + 1] = '0';
            }
        }
    }
}

// iterate over the input array
// if island is met BFS/DFS it and increment counter
// during BFS/DFS - update met 1 to 0
// continue iteration