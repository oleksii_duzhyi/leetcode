package com.oduzhyi.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class H0317ShortestDistance {
    public static void main(String[] args) {
        System.out.println(new H0317ShortestDistance().shortestDistance(new int[][]{
                new int[]{1, 0, 2, 0, 1},
                new int[]{0, 0, 0, 0, 0},
                new int[]{0, 0, 1, 0, 0},
        }));
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        System.out.println(Arrays.binarySearch(new int[]{1, 2, 3}, 100));
        System.out.println(Collections.binarySearch(list, 100));
    }

    public int shortestDistance(int[][] grid) {
        int cntHomes = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 1) {
                    cntHomes++;
                }
            }
        }

        int minDistance = Integer.MAX_VALUE;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 0) {
                    int dist = bfs(grid, i, j, cntHomes);
                    if (dist == -1) {
                        return -1;
                    }
                    minDistance = Math.min(dist, minDistance);
                }
            }
        }

        return minDistance;
    }

    private int bfs(int[][] grid, int i, int j, int cntHomes) {
        int rows = grid.length;
        int cols = grid[0].length;

        int dist = 0;

        Deque<Integer> queue = new ArrayDeque<>();
        queue.add(i * cols + j);
        Set<Integer> visited = new HashSet<>();
        visited.add(i * cols + j);
        int level = 0;

        while (!queue.isEmpty() && cntHomes > 0) {
            int size = queue.size();

            for (int k = 0; k < size; k++) {
                int next = queue.poll();
                int r = next / cols;
                int c = next % cols;

                if (grid[r][c] == 1) {
                    cntHomes--;
                    dist += level;
                } else if (grid[r][c] == 0) {
                    if (r + 1 < rows && !visited.contains((r + 1) * cols + c)) {
                        queue.add((r + 1) * cols + c);
                        visited.add((r + 1) * cols + c);
                    }

                    if (r - 1 >= 0 && !visited.contains((r - 1) * cols + c)) {
                        queue.add((r - 1) * cols + c);
                        visited.add((r - 1) * cols + c);
                    }

                    if (c - 1 >= 0 && !visited.contains((r) * cols + c - 1)) {
                        queue.add((r) * cols + c - 1);
                        visited.add((r) * cols + c - 1);
                    }

                    if (c + 1 < cols && !visited.contains((r) * cols + c + 1)) {
                        queue.add((r) * cols + c + 1);
                        visited.add((r) * cols + c + 1);
                    }
                }
            }
            level++;
        }
        return cntHomes == 0 ? dist : -1;
    }
}
